// File size: 108432 Bytes
// Author:    mtanaka
// Source:    rbn_a14.src
// Date:      00/00 00:00
// Binary:    section_000.bin

option authorName = "mtanaka/ffgriever";
option fileName = "rbn_a14-montblanc.src";
option spawnOrder = {-1, -1, -1, -1, -1, -1, -1, -1};
option positionFlags = 65535;
option unknownFlags1 = 65535;
option unknownScale = {100, 0, 0};
option unknownPosition2d = {0, 0};

//======================================================================
//                Global and scratchpad variable imports                
//======================================================================
import global   short   シナリオフラグ = 0x0;
import global   u_char  g_btl_アースドラゴン = 0xa03;
import global   u_char  g_btl_ミミッククイーン = 0xa04;
import global   u_char  g_btl_プリン = 0xa05;
import global   u_char  g_btl_ブッシュファイア = 0xa06;
import global   u_char  g_btl_ハイドラ = 0xa07;
import global   u_char  g_btl_ティアマット = 0xa08;
import global   u_char  g_btl_ダイダロス = 0xa09;
import global   u_char  g_btl_タイラント = 0xa0a;
import global   u_char  g_btl_メタルドラゴン = 0xa0b;
import global   u_char  g_btl_アーリマン = 0xa0c;
import global   u_char  g_btl_ラフレシア = 0xa0d;
import global   u_char  g_btl_マンドラゴラーズ = 0xa0e;
import global   u_char  g_btl_フンババ = 0xa0f;
import global   u_char  g_btl_フューリー = 0xa10;
import global   u_char  g_btl_エルダードラゴン = 0xa11;
import global   u_char  g_btl_ギルガメッシュA = 0xa12;
import global   u_char  g_btl_ギルガメッシュB = 0xa13;
import global   u_char  g_btl_デモンズウォールA = 0xa14;
import global   u_char  g_btl_デモンズウォールB = 0xa15;
import global   u_char  g_btl_ボムキング = 0xa16;
import global   u_char  g_btl_神竜 = 0xa17;
import global   u_char  g_btl_魔神竜 = 0xa18;
import global   u_char  g_btl_アントリオン = 0xa2f;
import global   u_char  g_btl_おんみょうし = 0xa38;
import global   u_char  g_btl_ノートリ_ダークファイア = 0xa3d;
import global   u_char  g_btl_ノートリ_デスゲイズ = 0xa40;
import global   u_char  g_btl_nm_パック = 0xa43;
import global   u_char  g_btl_nm_テクスタ = 0xa44;
import global   u_char  g_btl_nm_花サボテン = 0xa45;
import global   u_char  g_btl_nm_レイス = 0xa46;
import global   u_char  g_btl_nm_ニーズヘッグ = 0xa47;
import global   u_char  g_btl_nm_ホワイトムース = 0xa48;
import global   u_char  g_btl_nm_リングドラゴン = 0xa49;
import global   u_char  g_btl_nm_ワイバーンロード = 0xa4a;
import global   u_char  g_btl_nm_マリリス = 0xa4b;
import global   u_char  g_btl_nm_エンケドラス = 0xa4c;
import global   u_char  g_btl_nm_ケロゲロス = 0xa4d;
import global   u_char  g_btl_nm_イシュタム = 0xa4e;
import global   u_char  g_btl_nm_チョッパー = 0xa4f;
import global   u_char  g_btl_nm_ボーパルバニー = 0xa50;
import global   u_char  g_btl_nm_マインドフレア = 0xa51;
import global   u_char  g_btl_nm_ブラッディ = 0xa52;
import global   u_char  g_btl_nm_アトモス = 0xa53;
import global   u_char  g_btl_nm_ロビー = 0xa54;
import global   u_char  g_btl_nm_ブライ = 0xa55;
import global   u_char  g_btl_nm_ダークスティール = 0xa56;
import global   u_char  g_btl_nm_ヴィラール = 0xa57;
import global   u_char  g_btl_nm_リンドヴルム = 0xa58;
import global   u_char  g_btl_nm_オーバーロード = 0xa59;
import global   u_char  g_btl_nm_ゴリアテ = 0xa5a;
import global   u_char  g_btl_nm_デスサイズ = 0xa5b;
import global   u_char  g_btl_nm_ディアボロス = 0xa5c;
import global   u_char  g_btl_nm_ピスコディーモン = 0xa5d;
import global   u_char  g_btl_nm_ワイルドモルボル = 0xa5e;
import global   u_char  g_btl_nm_カトブレパス = 0xa5f;
import global   u_char  g_btl_nm_ファーヴニル = 0xa60;
import global   u_char  g_btl_nm_パイルラスタ = 0xa61;
import global   u_char  g_btl_nm_ヒナドリス = 0xa62;
import global   u_char  g_btl_nm_ニワトリス = 0xa63;
import global   u_char  g_btl_nm_ロックタイタス = 0xa64;
import global   u_char  g_btl_nm_オルトロス = 0xa65;
import global   u_char  g_btl_nm_ギルガメ = 0xa66;
import global   u_char  g_btl_nm_トリックスター = 0xa67;
import global   u_char  g_btl_nm_キャロット = 0xa68;
import global   u_char  g_btl_nm_キングベヒーモス = 0xa69;
import global   short   battle_global_flag[64] = 0xa72;
import global   u_char  quest_global_flag[94] = 0xb14;
import global   float   g_com_navi_footcalc[3] = 0xc7c;
import global   int     g_com_set_get_label = 0xc88;
import global   int     g_com_set_get_label_2 = 0xc8c;
import global   u_char  g_btl_オメガ = 0xcaf;
import global   int     g_com_set_get_label_3 = 0xcf0;
import global   float   mp_map_set_angle = 0x900;
import global   float   mp_map_set_x = 0x904;
import global   float   mp_map_set_y = 0x908;
import global   float   mp_map_set_z = 0x90c;
import global   int     mp_map_flg = 0x910;
import global   char    mp_last_weather = 0x940;
import global   char    mp_4map = 0x941;
import global   char    mp_map_set_hitse = 0x943;
import global   u_char  g_iw_カトリーヌ結果 = 0x408;
import global   u_char  g_iw_カトリーヌその後[1] = 0x409;
import global   u_char  g_iw_さっきあげた報酬のクランランク = 0x418;
import global   u_char  g_iw_報酬あげたボスフラグ[3] = 0x419;
import global   u_char  g_iw_報酬あげた召喚獣フラグ = 0x41c;
import global   u_char  g_iw_クラン情報フラグ[3] = 0x41d;
import global   u_char  iw_moni_on_off_memory = 0x42f;
import global   u_char  mp_rbn_flg = 0x91e;
import scratch1 short   scratch1_var_60 = 0x1e;



script 台詞(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut01()
	{
		return;
	}


	function cut02()
	{
		return;
	}


	function cut0202()
	{
		モンブラン２.setkutipakustatus(1);
		モンブラン２.setunazukistatus(1);
		モンブラン２.amese(0, 0x10000f2);
		モンブラン２.messync(0, 1);
		モンブラン２.setkutipakustatus(0);
		モンブラン２.setunazukistatus(0);
		return;
	}


	function cut03()
	{
		ノノ.setkutipakustatus(1);
		ノノ.setunazukistatus(1);
		ノノ.amese(0, 0x10000f3);
		ノノ.messync(0, 1);
		ノノ.setkutipakustatus(0);
		ノノ.setunazukistatus(0);
		return;
	}


	function cut0302()
	{
		ソルベ.setkutipakustatus(1);
		ソルベ.setunazukistatus(1);
		ソルベ.amese(0, 0x10000f4);
		ソルベ.messync(0, 1);
		ソルベ.setkutipakustatus(0);
		ソルベ.setunazukistatus(0);
		return;
	}


	function cut04()
	{
		ハーディ.setkutipakustatus(1);
		ハーディ.setunazukistatus(1);
		ハーディ.amese(0, 0x10000f5);
		ハーディ.messync(0, 1);
		ハーディ.setkutipakustatus(0);
		ハーディ.setunazukistatus(0);
		return;
	}


	function cut0402()
	{
		ホルン.setkutipakustatus(1);
		ホルン.setunazukistatus(1);
		ホルン.amese(0, 0x10000f6);
		ホルン.messync(0, 1);
		ホルン.setkutipakustatus(0);
		ホルン.setunazukistatus(0);
		return;
	}


	function cut05()
	{
		ガーディ.setkutipakustatus(1);
		ガーディ.setunazukistatus(1);
		ガーディ.amese(0, 0x10000f7);
		ガーディ.messync(0, 1);
		ガーディ.setkutipakustatus(0);
		ガーディ.setunazukistatus(0);
		return;
	}


	function cut0502()
	{
		モンブラン２.setkutipakustatus(1);
		モンブラン２.setunazukistatus(1);
		モンブラン２.amese(0, 0x10000f8);
		モンブラン２.messync(0, 1);
		モンブラン２.setkutipakustatus(0);
		モンブラン２.setunazukistatus(0);
		return;
	}


	function cut06()
	{
		モンブラン２.setkutipakustatus(1);
		モンブラン２.setunazukistatus(1);
		モンブラン２.amese(0, 0x10000f9);
		モンブラン２.messync(0, 1);
		モンブラン２.setkutipakustatus(0);
		モンブラン２.setunazukistatus(0);
		return;
	}


	function event_end()
	{
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_5b;             // pos: 0x18;
u_char  file_var_5c;             // pos: 0x1c;
u_char  file_var_5d;             // pos: 0x1d;
u_char  file_var_5e;             // pos: 0x1e;


script 常駐監督(0)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_62;            // pos: 0x1;
	u_char  local_var_63;            // pos: 0x2;

	function init()
	{
		if (g_iw_カトリーヌ結果 == 1)
		{
			file_var_5c = 1;
		}
		if (!((g_iw_クラン情報フラグ[0] & 16)))
		{
			fadeout(1);
			fadecancel_15a(2);
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			setcharseplayall(0);
			sethpmenufast(0);
		}
		local_var_62 = 0;
		for (regI0 = 160; regI0 <= 171; regI0 = (regI0 + 1))
		{
			if (isquestclear(regI0))
			{
				local_var_62 = (local_var_62 + 1);
			}
		}
		local_var_63 = 0;
		if (((シナリオフラグ >= 0x1004 && local_var_62 >= 3) && isquestclear(164)))
		{
			if ((!(isquestorder(172)) && !(isquestclear(172))))
			{
				local_var_63 = 1;
			}
		}
		if (((g_btl_nm_トリックスター == 2 && (g_iw_クラン情報フラグ[0] & 128)) && local_var_63 == 0))
		{
			if ((シナリオフラグ >= 0xbb8 && !(isquestclear(172))))
			{
				if ((iw_moni_on_off_memory & 3) == 3)
				{
					file_var_5d = 0;
					iw_moni_on_off_memory = (iw_moni_on_off_memory << 1);
					iw_moni_on_off_memory = (iw_moni_on_off_memory | 0);
				}
				else if ((rand_29(100) % 3) <= 1)
				{
					file_var_5d = 1;
					iw_moni_on_off_memory = (iw_moni_on_off_memory << 1);
					iw_moni_on_off_memory = (iw_moni_on_off_memory | 1);
				}
				else
				{
					iw_moni_on_off_memory = (iw_moni_on_off_memory << 1);
					iw_moni_on_off_memory = (iw_moni_on_off_memory | 0);
				}
			}
		}
		if (シナリオフラグ >= 0xbb8)
		{
			file_var_5e = (file_var_5e | 1);
		}
		if (isquestclear(172))
		{
			file_var_5e = (file_var_5e | 4);
		}
		sysReqiall(2, reqArr4);
		sysReqwaitall(reqArr4);
		if (file_var_5c == 1)
		{
			modelread(0x3000009);
			modelreadsync(0x3000009);
		}
		return;
	}


	function main(1)
	{
		setmapjumpgroupflag(0x139);
		releasemapjumpgroupflag(0x138);
		regY = シナリオフラグ;
		goto localjmp_2;
		while (true)
		{
			sysReqi(1, 配置監督::ベース配置);
			sysReqwait(配置監督::ベース配置);
			break;
		localjmp_2:
		}
		sysReq(1, 配置監督::戦闘訓練);
		if (!((g_iw_クラン情報フラグ[0] & 16)))
		{
			musictrans(-1, 30, 32);
			wait(15);
			hideparty();
			NPC17.setlod(1);
			NPC18.setlod(1);
			sysReqall(3, reqArr5);
			unkCall_5ca(0);
			fadein(fadesystemtime());
			wait(45);
			sysReqall(3, reqArr6);
			crossfade(4, 1, 15);
			wait(45);
			sysReqall(3, reqArr7);
			crossfade(4, 1, 15);
			wait(45);
			sysReqall(3, reqArr8);
			crossfade(4, 1, 15);
			wait(90);
			モンブラン.stdmotionplay(0x1000011);
			wait(90);
			unkCall_5ca(1);
			crossfade(0, 1, 15);
			NPC17.setlod(-1);
			NPC18.setlod(-1);
			showparty();
			sysReqall(4, reqArr9);
			musictrans(-1, 60, 127);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			sethpmenufast(1);
			setcharseplayall(1);
			g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 16);
		}
		return;
	}


	function 賞金首クリア()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(file_var_5b);
		setquestorder(file_var_5b, 0);
		setquestclear(file_var_5b, 1);
		setquestok(file_var_5b, 0);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_61;            // pos: 0x0;

	function カトリーヌ台詞()
	{
		if (!((g_iw_カトリーヌその後[0] & 1)))
		{
			amese(0, 0x10000fa);
			messync(0, 1);
			sebsoundplay(0, 39);
			additemmes(0x9058, 1);
			g_iw_カトリーヌその後[0] = (g_iw_カトリーヌその後[0] | 1);
		}
		else
		{
			NPC08.setkutipakustatus(1);
			NPC08.setunazukistatus(1);
			switch (local_var_61)
			{
				case 0:
					amese(0, 0x10000fb);
					messync(0, 1);
					local_var_61 = (local_var_61 + 1);
					break;
				default:
					amese(0, 0x10000fc);
					messync(0, 1);
					break;
			}
			NPC08.setkutipakustatus(0);
			NPC08.setunazukistatus(0);
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function 神竜撃破でおめでとう()
	{
		if (((getsummonmodebit() || getchocobomodebit()) || unkCall_3085()))
		{
			if (!(istownmap()))
			{
				goto localjmp_1;
				goto localjmp_3;
			localjmp_1:
				fadeout_d0(1, 1);
				fadesync_d3(1);
				if (unkCall_3085())
				{
					scratch1_var_60 = 1;
				}
				unkCall_3083();
				wait(1);
			}
		localjmp_3:
			if (scratch1_var_60 != 1)
			{
				sethpmenufast(0);
				seteventpartymode(1);
				partyallread();
				goto localjmp_8;
				if (!((getnowmusicno() == -1 || getnowmapmusicno() == -1)))
				{
					musicfadeout_231(getnowmusicno(), 90, 1);
					musicfadeoutsync();
					wait(1);
					musicstop(-1);
					musicclear(-1);
					musicread(getnowmapmusicno_47a(1));
					musicreadsync(getnowmapmusicno_47a(1));
					musicplay(getnowmapmusicno_47a(1));
					musictrans(getnowmapmusicno_47a(1), 0, 0);
					wait(1);
					musictrans(getnowmapmusicno_47a(1), 90, 127);
				}
				seteventpartymode(1);
				partyallread();
			}
		localjmp_8:
			if (!(istownmap()))
			{
				fadein_d2(1, 1);
				fadesync_d3(1);
				scratch1_var_60 = 0;
			}
		}
		hideparty();
		motionread(2);
		motionreadsync(2);
		sysReqall(1, reqArr10);
		sysReqwaitall(reqArr10);
		sysReqall(1, reqArr11);
		startenvsoundall();
		setcharseplayall(1);
		setcharseplayall(1);
		sethpmenufast(0);
		settrapshowstatus(0);
		fadein(15);
		wait(95);
		sysReqall(1, reqArr12);
		wait(95);
		sysReqall(2, reqArr13);
		sysReqwaitall(reqArr13);
		sysReqall(2, reqArr14);
		sysReqwaitall(reqArr14);
		sysReqall(1, reqArr15);
		sysReqwaitall(reqArr15);
		sysReqall(2, reqArr16);
		sysReqwaitall(reqArr16);
		sysReqall(1, reqArr17);
		sysReqwaitall(reqArr17);
		sysReqall(1, reqArr18);
		sysReqwaitall(reqArr18);
		sysReqall(1, reqArr19);
		sysReqwaitall(reqArr19);
		sysReqall(1, reqArr20);
		sysReqwaitall(reqArr20);
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(171);
		setquestorder(171, 0);
		setquestclear(171, 1);
		setquestok(171, 0);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		sysReqall(1, reqArr21);
		sysReqwaitall(reqArr21);
		fadeout(15);
		fadesync();
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		wait(1);
		setfocusstatus(0);
		blurstop();
		setalllightstatus(0, 1);
		resetmapambient();
		seteventfogstatus(0);
		setlightallcutstatus(0);
		setcharseplayall(0);
		sysReqall(2, reqArr22);
		sysReqwaitall(reqArr22);
		resetbehindcamera(-3.08575702, 0.200000003);
		cameraclear();
		showparty();
		wait(15);
		if ((getsummonmodebit() || getchocobomodebit()))
		{
			if (!(istownmap()))
			{
				fadeout_d0(1, 1);
				fadesync_d3(1);
			}
			sethpmenufast(0);
			seteventpartymode(0);
			partyallread();
			partystdmotionplay_540(0x1000000, 0);
			if (getsummonmode() == 1)
			{
				if (isbossmusic(getnowmapmusicno_47a(1)) == 0)
				{
					musiccrossplay_22f(getnowmapmusicno(), 90, 1);
				}
			}
			else if (getchocobomode() == 1)
			{
				if (isbossmusic(getnowmapmusicno_47a(1)) == 0)
				{
					musiccrossplay_22f(getnowmapmusicno(), 90, 1);
				}
			}
			if (getsummonmodebit())
			{
				set_summon_effect_filter_status(0, 1);
			}
			if (!(istownmap()))
			{
				fadein_d2(1, 1);
				fadesync_d3(1);
			}
		}
		clearnavimapfootmark();
		return;
	}
}


script カメラ(0)
{

	function init()
	{
		return;
	}


	function camclear()
	{
		cameraclear();
		camerashakeclear();
		setfocusstatus(0);
		return;
	}


	function op_cut01()
	{
		camerastart_7e(0, 0x2000008);
		return;
	}


	function op_cut03()
	{
		camerastart_7d(0, 0x2000009, getcamerasubframe());
		return;
	}


	function cut01()
	{
		camerastart_7e(0, 0x200000a);
		return;
	}


	function cut0102()
	{
		camerastart_7e(0, 0x200000b);
		return;
	}


	function cut02()
	{
		camerastart_7e(0, 0x200000c);
		return;
	}


	function cut0202()
	{
		camerastart_7e(0, 0x200000d);
		return;
	}


	function event_end()
	{
		cameraclear();
		return;
	}
}


script カメラ２(0)
{

	function init()
	{
		return;
	}


	function op_cut02()
	{
		camerastart_7d(1, 0x200000e, getcamerasubframe());
		return;
	}


	function op_cut04()
	{
		camerastart_7d(1, 0x200000f, getcamerasubframe());
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_59;             // pos: 0xc;


script funcActor(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function getOfferedCount()
	{
		file_var_59 = 0;
		if (g_btl_nm_パック == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_テクスタ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_花サボテン == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_レイス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ニーズヘッグ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ホワイトムース == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_リングドラゴン == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ワイバーンロード == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_マリリス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_エンケドラス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ケロゲロス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_イシュタム == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_チョッパー == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ボーパルバニー == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_マインドフレア == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ブラッディ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_アトモス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ロビー == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ブライ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ダークスティール == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ヴィラール == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_リンドヴルム == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_オーバーロード == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ゴリアテ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_デスサイズ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_ノートリ_デスゲイズ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ディアボロス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ピスコディーモン == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ワイルドモルボル == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_カトブレパス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ファーヴニル == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_パイルラスタ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ニワトリス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ロックタイタス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_オルトロス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_ギルガメ == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_トリックスター == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_キャロット == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_アントリオン == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_ギルガメッシュA == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_ギルガメッシュB == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_nm_キングベヒーモス == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_ノートリ_ダークファイア == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_おんみょうし == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		if (g_btl_神竜 == 1)
		{
			file_var_59 = (file_var_59 + 1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_64;            // pos: 0x0;
	u_char  local_var_65;            // pos: 0x1;
	u_char  local_var_66;            // pos: 0x2;

	function getOfferedRnd()
	{
		file_var_59 = 0;
		local_var_64 = 0;
		local_var_65 = 0;
		local_var_66 = (rand_29(0x1b8) % 44);
		sysDVAR(13, local_var_66);
	localjmp_0:
		switch (local_var_66)
		{
			case 0:
			localjmp_1:
				if (g_btl_nm_パック == 1)
				{
					file_var_59 = 128;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 1:
				if (g_btl_nm_テクスタ == 1)
				{
					file_var_59 = 129;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 2:
				if (g_btl_nm_花サボテン == 1)
				{
					file_var_59 = 130;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 3:
				if (g_btl_nm_レイス == 1)
				{
					file_var_59 = 131;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 4:
				if (g_btl_nm_ニーズヘッグ == 1)
				{
					file_var_59 = 132;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 5:
				if (g_btl_nm_ホワイトムース == 1)
				{
					file_var_59 = 133;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 6:
				if (g_btl_nm_リングドラゴン == 1)
				{
					file_var_59 = 134;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 7:
				if (g_btl_nm_ワイバーンロード == 1)
				{
					file_var_59 = 135;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 8:
				if (g_btl_nm_マリリス == 1)
				{
					file_var_59 = 136;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 9:
				if (g_btl_nm_エンケドラス == 1)
				{
					file_var_59 = 137;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 10:
				if (g_btl_nm_ケロゲロス == 1)
				{
					file_var_59 = 138;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 11:
				if (g_btl_nm_イシュタム == 1)
				{
					file_var_59 = 139;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 12:
				if (g_btl_nm_チョッパー == 1)
				{
					file_var_59 = 140;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 13:
				if (g_btl_nm_ボーパルバニー == 1)
				{
					file_var_59 = 141;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 14:
				if (g_btl_nm_マインドフレア == 1)
				{
					file_var_59 = 142;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 15:
				if (g_btl_nm_ブラッディ == 1)
				{
					file_var_59 = 143;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 16:
				if (g_btl_nm_アトモス == 1)
				{
					file_var_59 = 144;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 17:
				if (g_btl_nm_ロビー == 1)
				{
					file_var_59 = 145;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 18:
				if (g_btl_nm_ブライ == 1)
				{
					file_var_59 = 146;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 19:
				if (g_btl_nm_ダークスティール == 1)
				{
					file_var_59 = 147;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 20:
				if (g_btl_nm_ヴィラール == 1)
				{
					file_var_59 = 148;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 21:
				if (g_btl_nm_リンドヴルム == 1)
				{
					file_var_59 = 149;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 22:
				if (g_btl_nm_オーバーロード == 1)
				{
					file_var_59 = 150;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 23:
				if (g_btl_nm_ゴリアテ == 1)
				{
					file_var_59 = 151;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 24:
				if (g_btl_nm_デスサイズ == 1)
				{
					file_var_59 = 152;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 25:
				if (g_btl_ノートリ_デスゲイズ == 1)
				{
					file_var_59 = 153;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 26:
				if (g_btl_nm_ディアボロス == 1)
				{
					file_var_59 = 154;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 27:
				if (g_btl_nm_ピスコディーモン == 1)
				{
					file_var_59 = 155;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 28:
				if (g_btl_nm_ワイルドモルボル == 1)
				{
					file_var_59 = 156;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 29:
				if (g_btl_nm_カトブレパス == 1)
				{
					file_var_59 = 157;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 30:
				if (g_btl_nm_ファーヴニル == 1)
				{
					file_var_59 = 158;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 31:
				if (g_btl_nm_パイルラスタ == 1)
				{
					file_var_59 = 159;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 32:
				if (g_btl_nm_ニワトリス == 1)
				{
					file_var_59 = 160;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 33:
				if (g_btl_nm_ロックタイタス == 1)
				{
					file_var_59 = 161;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 34:
				if (g_btl_nm_オルトロス == 1)
				{
					file_var_59 = 162;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 35:
				if (g_btl_nm_ギルガメ == 1)
				{
					file_var_59 = 163;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 36:
				if (g_btl_nm_トリックスター == 1)
				{
					file_var_59 = 164;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 37:
				if (g_btl_アントリオン == 1)
				{
					file_var_59 = 165;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 38:
				if (g_btl_nm_キャロット == 1)
				{
					file_var_59 = 166;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 39:
				if (g_btl_ギルガメッシュA == 1)
				{
					file_var_59 = 167;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
				if (g_btl_ギルガメッシュB == 1)
				{
					file_var_59 = 167;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 40:
				if (g_btl_nm_キングベヒーモス == 1)
				{
					file_var_59 = 168;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 41:
				if (g_btl_ノートリ_ダークファイア == 1)
				{
					file_var_59 = 169;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			case 42:
				if (g_btl_おんみょうし == 1)
				{
					file_var_59 = 170;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
			default:
				if (g_btl_神竜 == 1)
				{
					file_var_59 = 171;
					return;
				}
				if (local_var_64 >= 20)
				{
					local_var_65 = (local_var_65 + 1);
					if (local_var_65 >= 50)
					{
						file_var_59 = 255;
						return;
					}
				}
				else
				{
					local_var_64 = (local_var_64 + 1);
					local_var_66 = (rand_29(0x1b8) % 44);
					goto localjmp_0;
				}
				goto localjmp_1;
		}
		return;
	}


	function getClearBoss()
	{
		file_var_59 = -1;
		if ((g_btl_プリン == 2 && !((g_iw_報酬あげたボスフラグ[0] & 1))))
		{
			file_var_59 = 0;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 1);
			return;
		}
		if ((g_btl_ブッシュファイア == 2 && !((g_iw_報酬あげたボスフラグ[0] & 2))))
		{
			file_var_59 = 1;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 2);
			return;
		}
		if ((g_btl_アースドラゴン == 2 && !((g_iw_報酬あげたボスフラグ[0] & 4))))
		{
			file_var_59 = 2;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 4);
			return;
		}
		if ((g_btl_ミミッククイーン == 2 && !((g_iw_報酬あげたボスフラグ[0] & 8))))
		{
			file_var_59 = 3;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 8);
			return;
		}
		if ((g_btl_デモンズウォールA == 2 && !((g_iw_報酬あげたボスフラグ[0] & 16))))
		{
			file_var_59 = 4;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 16);
			return;
		}
		if ((g_btl_デモンズウォールB == 2 && !((g_iw_報酬あげたボスフラグ[0] & 32))))
		{
			file_var_59 = 5;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 32);
			return;
		}
		if ((g_btl_エルダードラゴン == 2 && !((g_iw_報酬あげたボスフラグ[0] & 64))))
		{
			file_var_59 = 6;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 64);
			return;
		}
		if ((g_btl_ティアマット == 2 && !((g_iw_報酬あげたボスフラグ[0] & 128))))
		{
			file_var_59 = 7;
			g_iw_報酬あげたボスフラグ[0] = (g_iw_報酬あげたボスフラグ[0] | 128);
			return;
		}
		if ((g_btl_メタルドラゴン == 2 && !((g_iw_報酬あげたボスフラグ[1] & 1))))
		{
			file_var_59 = 8;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 1);
			return;
		}
		if ((g_btl_ボムキング == 2 && !((g_iw_報酬あげたボスフラグ[1] & 2))))
		{
			file_var_59 = 9;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 2);
			return;
		}
		if ((g_btl_アーリマン == 2 && !((g_iw_報酬あげたボスフラグ[1] & 4))))
		{
			file_var_59 = 10;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 4);
			return;
		}
		if ((g_btl_マンドラゴラーズ == 2 && !((g_iw_報酬あげたボスフラグ[1] & 8))))
		{
			file_var_59 = 11;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 8);
			return;
		}
		if ((g_btl_魔神竜 == 2 && !((g_iw_報酬あげたボスフラグ[1] & 16))))
		{
			file_var_59 = 12;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 16);
			return;
		}
		if ((g_btl_ラフレシア == 2 && !((g_iw_報酬あげたボスフラグ[1] & 32))))
		{
			file_var_59 = 13;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 32);
			return;
		}
		if ((g_btl_ダイダロス == 2 && !((g_iw_報酬あげたボスフラグ[1] & 64))))
		{
			file_var_59 = 14;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 64);
			return;
		}
		if ((g_btl_タイラント == 2 && !((g_iw_報酬あげたボスフラグ[1] & 128))))
		{
			file_var_59 = 15;
			g_iw_報酬あげたボスフラグ[1] = (g_iw_報酬あげたボスフラグ[1] | 128);
			return;
		}
		if ((g_btl_ハイドラ == 2 && !((g_iw_報酬あげたボスフラグ[2] & 1))))
		{
			file_var_59 = 16;
			g_iw_報酬あげたボスフラグ[2] = (g_iw_報酬あげたボスフラグ[2] | 1);
			return;
		}
		if ((g_btl_フンババ == 2 && !((g_iw_報酬あげたボスフラグ[2] & 2))))
		{
			file_var_59 = 17;
			g_iw_報酬あげたボスフラグ[2] = (g_iw_報酬あげたボスフラグ[2] | 2);
			return;
		}
		if ((g_btl_フューリー == 2 && !((g_iw_報酬あげたボスフラグ[2] & 4))))
		{
			file_var_59 = 18;
			g_iw_報酬あげたボスフラグ[2] = (g_iw_報酬あげたボスフラグ[2] | 4);
			return;
		}
		return;
	}
}


script 配置監督(0)
{

	function init()
	{
		if (!(((g_btl_nm_トリックスター == 1 || battle_global_flag[3] == 1) || file_var_5d == 1)))
		{
			sysReq(1, hitactor01_monion::setHitObj);
		}
		return;
	}


	function main(1)
	{
		return;
	}


	function ベース配置()
	{
		sysReq(1, hitactor01::setHitObj);
		sysReq(1, hitactor02::setHitObj);
		sysReq(1, hitactor03::setHitObj);
		sysReq(1, hitactor04::setHitObj);
		motionread(0);
		motionreadsync(0);
		motionread(1);
		motionreadsync(1);
		sysReqiall(2, reqArr1);
		sysReqwaitall(reqArr1);
		return;
	}


	function 戦闘訓練()
	{
		while (true)
		{
			if (file_var_5c != 1)
			{
				NPC07.stdmotionplay(0x100001c);
				wait(8);
				NPC08.stdmotionplay_2c2(0x100001a, 5);
				NPC07.motionsync_282(1);
				NPC08.motionsync_282(1);
				NPC08.stdmotionplay(0x100001c);
				wait(8);
				NPC07.stdmotionplay_2c2(0x100001a, 5);
				NPC07.motionsync_282(1);
				NPC08.motionsync_282(1);
				NPC07.setwalkspeed((NPC07.getdefaultrunspeed() * 1.5));
				NPC08.setwalkspeed((NPC08.getdefaultrunspeed() * 1.5));
				wait(3);
				NPC07.sysLookata(NPC08);
				NPC08.sysLookata(NPC07);
				switch ((rand_29(0x100) % 2))
				{
					case 0:
						NPC07.rcmove(36.3116035, 0, 51.8869476);
						NPC08.cmove(37.9652023, 0, 50.6833038);
						NPC07.movesync();
						NPC07.raturn_25e(2.58919311);
						NPC08.setaturnlookatlockstatus(1);
						NPC08.aturn_261(-0.939167976);
						break;
					default:
						NPC08.rcmove(36.3116035, 0, 51.8869476);
						NPC07.cmove(37.9652023, 0, 50.6833038);
						NPC08.movesync();
						NPC08.raturn_25e(2.58919311);
						NPC07.setaturnlookatlockstatus(1);
						NPC07.aturn_261(-0.939167976);
						break;
				}
				wait(3);
				NPC07.sysLookata(NPC08);
				NPC08.sysLookata(NPC07);
				NPC07.stdmotionplay(0x100001c);
				wait(8);
				NPC08.stdmotionplay_2c2(0x100001a, 5);
				NPC07.motionsync_282(1);
				NPC08.motionsync_282(1);
				NPC08.stdmotionplay(0x100001c);
				wait(8);
				NPC07.stdmotionplay_2c2(0x100001a, 5);
				NPC07.motionsync_282(1);
				NPC08.motionsync_282(1);
				NPC07.setwalkspeed((NPC07.getdefaultrunspeed() * 1.5));
				NPC08.setwalkspeed((NPC08.getdefaultrunspeed() * 1.5));
				switch ((rand_29(0x100) % 2))
				{
					case 0:
						NPC07.rcmove(37.5871201, 0, 51.9400215);
						NPC08.cmove(36.3618317, 0, 50.211628);
						NPC07.movesync();
						NPC07.raturn_25e(-2.37957191);
						NPC08.setaturnlookatlockstatus(1);
						NPC08.aturn_261(0.597588003);
						break;
					default:
						NPC08.rcmove(37.5871201, 0, 51.9400215);
						NPC07.cmove(36.3618317, 0, 50.211628);
						NPC08.movesync();
						NPC08.raturn_25e(-2.37957191);
						NPC07.setaturnlookatlockstatus(1);
						NPC07.aturn_261(0.597588003);
						break;
				}
			}
			else
			{
				NPC07.stdmotionplay(0x100001c);
				wait(8);
				NPC08.stdmotionplay_2c2(0x100001a, 5);
				NPC07.motionsync_282(1);
				NPC08.motionsync_282(1);
				NPC08.stdmotionplay(0x100001c);
				wait(8);
				NPC07.stdmotionplay_2c2(0x100001a, 5);
				NPC07.motionsync_282(1);
				NPC08.motionsync_282(1);
			}
		}
	}


	function NPC_神竜後バインドオフ()
	{
		return 0;
	}


	function 停止()
	{
		return 0;
		return;
	}
}


script イベント特殊効果画面(0)
{

	function init()
	{
		return;
	}


	function だいじなものゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x10000fd, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x10000fd, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_MJFADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout_d0(2, 15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆メッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000fd, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆ゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_2, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_GET表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000fd, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_3, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_SYSMES表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000fd, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_3);
		ames_1fe(0, 0x10000fd, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}
}


script 振動処理(0)
{

	function init()
	{
		return;
	}


	function 大きな門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(8);
		vibsync();
		return;
	}


	function ＤＴ扉振動開始()
	{
		vibplay(3);
		vibsync();
		vibplay(0);
		vibsync();
		return;
	}


	function ＤＴ門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		wait(20);
		vibstop();
		vibplay(7);
		wait(25);
		vibstop();
		return;
	}
}


script Map_Director(0)
{

	function init()
	{
		mp_rbn_flg = (mp_rbn_flg | 8);
		resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
		return;
	}


	function main(1)
	{
		resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
		return;
	}
}


script door_out(1)
{

	function init()
	{
		fieldsign(0);
		reqenable(2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 3.14159274;
		pausesestop();
		sysReqew(1, map_ダミーＰＣ::扉オープン１);
		mp_map_flg = 0;
		mp_map_flg = sebsoundplay(1, 1);
		animeplay(0);
		wait(15);
		regI3 = sysucoff();
		if (regI3)
		{
			clearmapjumpstatus();
			sysucon();
		}
		else
		{
			mp_last_weather = getweatherslot();
			steppoint(3);
			mp_4map = 30;
			fadelayer(6);
			fadeprior(255);
			fadeout_d0(2, mp_4map);
			steppoint(4);
			wait(15);
			steppoint(3);
			spotsoundtrans(15, 0);
			steppoint(2);
			wait(17);
			stopspotsound();
			pausesestop();
			hideparty();
			eventsoundplaysync(mp_map_flg);
			steppoint(1);
			setbattlethinkstatus_freetarget_group(-1, 0);
			voicestopall();
			mapjump(0x121, 7, 1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_67;            // pos: 0x2;

	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_67 = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_67 = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}

symlink NPC02(NPC01) : 0x1;

symlink NPC03(NPC01) : 0x2;

symlink NPC04(NPC01) : 0x3;

symlink NPC05(NPC01) : 0x4;

symlink NPC06(NPC01) : 0x5;

symlink NPC07(NPC01) : 0x6;

symlink NPC08(NPC01) : 0x7;

symlink NPC09(NPC01) : 0x8;

symlink NPC10(NPC01) : 0x9;

symlink NPC11(NPC01) : 0xa;

symlink NPC12(NPC01) : 0xb;

symlink NPC13(NPC01) : 0xc;

symlink NPC14(NPC01) : 0xd;

symlink NPC15(NPC01) : 0xe;

symlink NPC16(NPC01) : 0xf;

symlink NPC17(NPC01) : 0x10;

symlink NPC18(NPC01) : 0x11;

symlink NPC19(NPC01) : 0x12;

symlink NPC20(NPC01) : 0x13;

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_5a;             // pos: 0x14;
u_char  file_var_5f;             // pos: 0x1f;


script モンブラン(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_73;            // pos: 0x92;
	int     local_var_74;            // pos: 0x94;
	short   local_var_76;            // pos: 0x9c;
	short   local_var_77;            // pos: 0x9e;
	u_char  local_var_78;            // pos: 0xa0;
	u_char  local_var_79;            // pos: 0xa1;
	u_char  local_var_7a;            // pos: 0xa2;
	u_char  local_var_7b;            // pos: 0xa4;
	u_char  local_var_7c;            // pos: 0xa5;
	u_char  local_var_7d;            // pos: 0xa6;
	u_char  local_var_7e[6];         // pos: 0xa7;
	u_char  local_var_7f;            // pos: 0xad;
	int     local_var_80[15];        // pos: 0xb0;
	int     local_var_81[15];        // pos: 0xec;
	int     local_var_82[15];        // pos: 0x128;
	u_char  local_var_83[15];        // pos: 0x164;
	u_char  local_var_84;            // pos: 0x173;
	char    local_var_85;            // pos: 0x174;
	u_char  local_var_86;            // pos: 0x175;
	u_char  local_var_87;            // pos: 0x176;
	u_char  local_var_88;            // pos: 0x177;
	u_char  local_var_89;            // pos: 0x178;
	u_char  local_var_8a;            // pos: 0x179;
	u_char  local_var_8b;            // pos: 0x17a;
	u_char  local_var_8c;            // pos: 0x17b;
	u_char  local_var_8d;            // pos: 0x17c;
	u_char  local_var_8e;            // pos: 0x17d;
	u_char  local_var_8f[15];        // pos: 0x17e;
	u_char  local_var_90;            // pos: 0x18d;
	u_char  local_var_91;            // pos: 0x194;
	u_char  local_var_92;            // pos: 0x195;
	u_char  local_var_93;            // pos: 0x198;
	u_char  local_var_94;            // pos: 0x199;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		if (!((g_iw_クラン情報フラグ[0] & 1)))
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x1000000);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			g_com_set_get_label = 142;
			sysReqew(1, イベント特殊効果画面::システムメッセージ表示);
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x1000001);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 1);
		}
		else
		{
			local_var_74 = getclanrank();
			if ((((0xac0000 | getquestscenarioflag(172)) == 0xac0064 && battle_global_flag[3] == 2) && isquestclear(172)))
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000002);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setquestscenarioflag(172, 120);
			}
			if (((isquestorder(167) && g_btl_ギルガメッシュB == 2) && !(isquestclear(167))))
			{
				file_var_5b = 167;
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000004);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setquestscenarioflag(167, 130);
				sysReq(1, 常駐監督::賞金首クリア);
				sysReqwait(常駐監督::賞金首クリア);
			}
			if (((isquestorder(170) && g_btl_おんみょうし == 2) && !(isquestclear(170))))
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000005);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setquestscenarioflag(170, 100);
				file_var_5b = 170;
				sysReq(1, 常駐監督::賞金首クリア);
				sysReqwait(常駐監督::賞金首クリア);
			}
			if (((isquestorder(171) && g_btl_神竜 == 2) && !(isquestclear(171))))
			{
				ucoff();
				fadeout(15);
				fadesync();
				sethpmenufast(0);
				settrapshowstatus(0);
				if (!(istownmap()))
				{
					setstatuserrordispdenystatus(1);
				}
				setcharseplayall(0);
				setquestscenarioflag(171, 100);
				hide();
				sysReqall(5, reqArr0);
				sysReqwaitall(reqArr0);
				motiondispose(1);
				sysReq(1, 常駐監督::神竜撃破でおめでとう);
				sysReqwait(常駐監督::神竜撃破でおめでとう);
				local_var_78 = 1;
				motionread(1);
				motionreadsync(1);
				motiondispose(2);
				sysReqiall(1, reqArr1);
				sysReqwaitall(reqArr1);
				sysReqall(1, reqArr2);
				wait(3);
				charcachedispose();
				sysReq(1, 配置監督::戦闘訓練);
				local_var_78 = 0;
				if (g_btl_nm_ギルガメ == 1)
				{
					sysReqi(2, NPC19::バインドオフ);
					sysReqwait(NPC19::バインドオフ);
				}
				if (g_btl_nm_トリックスター == 1)
				{
					sysReqi(2, NPC01::バインドオフ);
					sysReqwait(NPC01::バインドオフ);
				}
				if (g_btl_nm_キャロット == 1)
				{
					sysReqi(2, カロリーヌ::バインドオフ);
					sysReqwait(カロリーヌ::バインドオフ);
				}
				show();
				setstatuserrordispdenystatus(0);
				clearnavimapfootmark();
				sethpmenufast(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				wait(1);
				setcharseplayall(1);
				fadein(15);
				sethpmenufast(0);
				settrapshowstatus(0);
			}
		localjmp_14:
			if (g_iw_さっきあげた報酬のクランランク < local_var_74)
			{
				g_iw_さっきあげた報酬のクランランク = (g_iw_さっきあげた報酬のクランランク + 1);
				switch (g_iw_さっきあげた報酬のクランランク)
				{
					case 1:
						file_var_5a = 0x902f;
						break;
					case 2:
						file_var_5a = 0x9030;
						break;
					case 3:
						file_var_5a = 0x9031;
						break;
					case 4:
						file_var_5a = 0x9032;
						break;
					case 5:
						file_var_5a = 0x9033;
						break;
					case 6:
						file_var_5a = 0x9034;
						break;
					case 7:
						file_var_5a = 0x9035;
						break;
					case 8:
						file_var_5a = 0x9036;
						break;
					case 9:
						file_var_5a = 0x9037;
						break;
					case 10:
						file_var_5a = 0x9038;
						break;
					case 11:
						file_var_5a = 0x9039;
						break;
					case 12:
						file_var_5a = 0x903a;
						break;
				}
				setmesmacro(0, 0, 5, g_iw_さっきあげた報酬のクランランク);
				if (g_iw_さっきあげた報酬のクランランク >= 12)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000006);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					sebsoundplay(0, 39);
					additemmes(file_var_5a, 1);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000007);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else if (g_iw_さっきあげた報酬のクランランク == 1)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000008);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					sebsoundplay(0, 39);
					additemmes(file_var_5a, 1);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000009);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100000a);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					sebsoundplay(0, 39);
					additemmes(file_var_5a, 1);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000007);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				wait(1);
			}
			else
			{
				goto localjmp_35;
			}
			goto localjmp_14;
		localjmp_35:
			sysReq(1, funcActor::getClearBoss);
			sysReqwait(funcActor::getClearBoss);
			if (file_var_59 != -1)
			{
				switch (file_var_59)
				{
					case 0:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000b);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000c);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x903b;
						break;
					case 1:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000d);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000e);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x903c;
						break;
					case 2:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000f);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000010);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x903d;
						break;
					case 3:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000011);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000012);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x903e;
						break;
					case 4:
						if ((g_iw_報酬あげたボスフラグ[0] & 32))
						{
							local_var_79 = 0;
						}
						else
						{
							local_var_79 = 1;
						}
						if (local_var_79)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000013);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000014);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000015);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x903f;
						break;
					case 5:
						if ((g_iw_報酬あげたボスフラグ[0] & 16))
						{
							local_var_79 = 0;
						}
						else
						{
							local_var_79 = 1;
						}
						if (local_var_79)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000013);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000014);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000016);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9040;
						break;
					case 6:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000017);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000018);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9041;
						break;
					case 7:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000019);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100001a);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9042;
						break;
					case 8:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100001b);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100001c);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9043;
						break;
					case 9:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100001d);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100001e);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9044;
						break;
					case 10:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100001f);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000020);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9045;
						break;
					case 11:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000021);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000022);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9046;
						break;
					case 12:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000023);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000024);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9047;
						break;
					case 13:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000025);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000026);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9048;
						break;
					case 14:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000027);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000028);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x9049;
						break;
					case 15:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000029);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002a);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x904a;
						break;
					case 16:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002b);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002c);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x904b;
						break;
					case 17:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002d);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002e);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x904c;
						break;
					case 18:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002f);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000030);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						file_var_5a = 0x904d;
						break;
				}
				sebsoundplay(0, 39);
				additemmes(file_var_5a, 1);
				wait(1);
				goto localjmp_35;
			}
			if ((battle_global_flag[0] >= 1 && g_iw_報酬あげた召喚獣フラグ < 1))
			{
				setmesmacro(0, 0, 0, 1);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000031);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000032);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				g_iw_報酬あげた召喚獣フラグ = 1;
				sebsoundplay(0, 39);
				additemmes(0x904e, 1);
			}
			if ((battle_global_flag[0] >= 4 && g_iw_報酬あげた召喚獣フラグ < 2))
			{
				setmesmacro(0, 0, 0, 4);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000033);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000034);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				g_iw_報酬あげた召喚獣フラグ = 2;
				sebsoundplay(0, 39);
				additemmes(0x904f, 1);
			}
			if ((battle_global_flag[0] >= 8 && g_iw_報酬あげた召喚獣フラグ < 3))
			{
				setmesmacro(0, 0, 0, 8);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000033);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000035);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				g_iw_報酬あげた召喚獣フラグ = 3;
				sebsoundplay(0, 39);
				additemmes(0x9050, 1);
			}
			if ((battle_global_flag[0] >= 13 && g_iw_報酬あげた召喚獣フラグ < 4))
			{
				setmesmacro(0, 0, 0, 13);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000033);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000036);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				g_iw_報酬あげた召喚獣フラグ = 4;
				sebsoundplay(0, 39);
				additemmes(0x9051, 1);
			}
			sysReq(1, funcActor::getOfferedCount);
			sysReqwait(funcActor::getOfferedCount);
			setkutipakustatus(1);
			setunazukistatus(1);
			askpos(0, 0, 127);
			file_var_5f = aaske(0, 0x1000037);
			mesclose(0);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			switch (file_var_5f)
			{
				case 0:
					break;
				case 1:
					local_var_7e[0] = 0;
					local_var_7e[1] = 3;
					local_var_7e[2] = 2;
					local_var_7e[3] = 4;
					local_var_7e[4] = 1;
					local_var_7e[5] = 5;
					if ((((((hasJobAssigned(0) == 0 && hasJobAssigned(3) == 0) && hasJobAssigned(2) == 0) && hasJobAssigned(4) == 0) && hasJobAssigned(1) == 0) && hasJobAssigned(5) == 0))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000038);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else
					{
						local_var_7b = 0;
						if (ispartymember(0) == 1)
						{
							local_var_7b = (local_var_7b + 1);
						}
						else
						{
							setaskselectignore(0, 0);
						}
						if (ispartymember(3) == 1)
						{
							local_var_7b = (local_var_7b + 1);
						}
						else
						{
							setaskselectignore(0, 1);
						}
						if (ispartymember(2) == 1)
						{
							local_var_7b = (local_var_7b + 1);
						}
						else
						{
							setaskselectignore(0, 2);
						}
						if (ispartymember(4) == 1)
						{
							local_var_7b = (local_var_7b + 1);
						}
						else
						{
							setaskselectignore(0, 3);
						}
						if (ispartymember(1) == 1)
						{
							local_var_7b = (local_var_7b + 1);
						}
						else
						{
							setaskselectignore(0, 4);
						}
						if (ispartymember(5) == 1)
						{
							local_var_7b = (local_var_7b + 1);
						}
						else
						{
							setaskselectignore(0, 5);
						}
						local_var_7c = local_var_7b;
						if (local_var_7b >= 4)
						{
							local_var_7b = 3;
						}
						setmeswinline(0, (local_var_7b + 1));
						askpos(0, 0, local_var_7c);
						local_var_7a = aask(0, 0x1000039, 48, 0x3fe, 1);
						if (local_var_7a == 6)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100003a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else if (hasJobAssigned(local_var_7e[local_var_7a]) == 0)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100003b);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							askpos(0, 1, 1);
							local_var_7d = aaske(0, 0x100003c);
							mesclose(0);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							switch (local_var_7d)
							{
								case 0:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100003d);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									sebsoundplay(0, 93);
									stdmotionplay(0x1000010);
									resetJobs(local_var_7e[local_var_7a]);
									wait(50);
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100003e);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100003f);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
						}
					}
					goto localjmp_357;
				case 2:
					//ffg's edit
					sysReqew(0, help_talk::message_loop);
					goto localjmp_357;
				default:
					goto localjmp_357;
			}
			local_var_84 = 0;
			local_var_86 = 0;
			local_var_85 = -1;
			local_var_87 = 0;
			local_var_80[0] = ((g_btl_nm_ヒナドリス + g_btl_nm_ニワトリス) / 2);
			local_var_80[1] = g_btl_nm_ロックタイタス;
			local_var_80[2] = g_btl_nm_オルトロス;
			local_var_80[3] = g_btl_nm_ギルガメ;
			local_var_80[4] = g_btl_nm_トリックスター;
			local_var_80[5] = g_btl_アントリオン;
			local_var_80[6] = g_btl_nm_キャロット;
			local_var_80[7] = ((g_btl_ギルガメッシュA + g_btl_ギルガメッシュB) / 2);
			local_var_80[8] = g_btl_nm_キングベヒーモス;
			local_var_80[9] = g_btl_ノートリ_ダークファイア;
			local_var_80[10] = g_btl_おんみょうし;
			local_var_80[11] = g_btl_神竜;
			local_var_80[12] = battle_global_flag[3];
			local_var_81[0] = 2;
			local_var_81[1] = 2;
			local_var_81[2] = 5;
			local_var_81[3] = 4;
			local_var_81[4] = 5;
			local_var_81[5] = 5;
			local_var_81[6] = 7;
			local_var_81[7] = 7;
			local_var_81[8] = 8;
			local_var_81[9] = 8;
			local_var_81[10] = 10;
			local_var_81[11] = 11;
			local_var_81[12] = 1;
			local_var_82[0] = 223;
			local_var_82[1] = 0x18c;
			local_var_82[2] = 0x246;
			local_var_82[3] = 231;
			local_var_82[4] = 52;
			local_var_82[5] = 0x171;
			local_var_82[6] = 0x2b5;
			local_var_82[7] = 0x2cd;
			local_var_82[8] = 0x273;
			local_var_82[9] = 0x342;
			local_var_82[10] = 0x2cd;
			local_var_82[11] = 0x2cd;
			local_var_82[12] = 0x2cd;
			local_var_7f = 13;
			local_var_88 = 0;
			local_var_8a = 0;
			for (local_var_89 = 160; local_var_89 <= 171; local_var_89 = (local_var_89 + 1))
			{
				if (isquestclear(local_var_89))
				{
					local_var_88 = (local_var_88 + 1);
				}
			}
			if (((シナリオフラグ >= 0x1004 && local_var_88 >= 3) && isquestclear(164)))
			{
				if (!(isquestclear(172)))
				{
					local_var_8a = 1;
					local_var_7f = 14;
				}
			}
			local_var_74 = getclanrank();
			goto localjmp_121;
		localjmp_105:
			goto localjmp_120;
		localjmp_106:
			シナリオフラグ = 0x122;
			goto localjmp_120;
		localjmp_107:
			シナリオフラグ = 0x15e;
			goto localjmp_120;
		localjmp_108:
			シナリオフラグ = 0x4ba;
			goto localjmp_120;
		localjmp_109:
			シナリオフラグ = 0x604;
			goto localjmp_120;
		localjmp_110:
			シナリオフラグ = 0x7be;
			goto localjmp_120;
		localjmp_111:
			シナリオフラグ = 0xbb8;
			goto localjmp_120;
		localjmp_112:
			シナリオフラグ = 0x1004;
			goto localjmp_120;
		localjmp_113:
			シナリオフラグ = 0x141e;
			goto localjmp_120;
		localjmp_114:
			シナリオフラグ = 0x17d4;
			goto localjmp_120;
		localjmp_115:
			シナリオフラグ = 0x2af8;
			goto localjmp_120;
		localjmp_116:
			シナリオフラグ = 0x141e;
			goto localjmp_120;
		localjmp_117:
			シナリオフラグ = 0x17d4;
			goto localjmp_120;
		localjmp_118:
			シナリオフラグ = 0x2af8;
			goto localjmp_120;
			if (regY == 0) goto localjmp_105;
			if (regY <= 1) goto localjmp_106;
			if (regY <= 2) goto localjmp_107;
			if (regY <= 3) goto localjmp_108;
			if (regY <= 4) goto localjmp_109;
			if (regY <= 5) goto localjmp_110;
			if (regY <= 6) goto localjmp_111;
			if (regY <= 7) goto localjmp_112;
			if (regY <= 8) goto localjmp_113;
			if (regY <= 9) goto localjmp_114;
			if (regY <= 10) goto localjmp_115;
			if (regY <= 11) goto localjmp_116;
			if (regY <= 12) goto localjmp_117;
			goto localjmp_118;
		localjmp_120:
			goto localjmp_122;
		localjmp_121:
			local_var_74 = getclanrank();
			local_var_76 = handbook_getkillcount(64);
			local_var_77 = handbook_getordernum();
		localjmp_122:
			sysDVAR(1, local_var_77);
			sysDVAR(2, local_var_76);
			switch (シナリオフラグ)
			{
				case lt(105):
					local_var_73 = 0;
					break;
				case lt(0x122):
					local_var_73 = 0;
					break;
				case lt(0x15e):
					local_var_73 = 0;
					break;
				case lt(0x4ba):
					local_var_73 = 2;
					break;
				case lt(0x604):
					local_var_73 = 2;
					break;
				case lt(0x7be):
					local_var_73 = 4;
					break;
				case lt(0xbb8):
					local_var_73 = 6;
					break;
				case lt(0x1004):
					local_var_73 = 7;
					break;
				case lt(0x141e):
					local_var_73 = 8;
					break;
				case lt(0x17d4):
					local_var_73 = 9;
					break;
				case lt(0x2af8):
					local_var_73 = 12;
					break;
				case lt(0x141e):
					local_var_73 = 12;
					break;
				case lt(0x17d4):
					local_var_73 = 12;
					break;
				default:
					local_var_73 = 12;
					break;
			}
			local_var_8b = 0;
			sysDVAR(10, local_var_8b);
			sysDVAR(2, local_var_90);
			local_var_8d = 0;
			while (true)
			{
				local_var_90 = 0;
				switch (シナリオフラグ)
				{
					case lt(105):
						local_var_73 = 0;
						break;
					case lt(0x122):
						local_var_73 = 0;
						break;
					case lt(0x15e):
						local_var_73 = 0;
						break;
					case lt(0x4ba):
						local_var_73 = 2;
						break;
					case lt(0x604):
						local_var_73 = 2;
						break;
					case lt(0x7be):
						local_var_73 = 4;
						break;
					case lt(0xbb8):
						local_var_73 = 6;
						break;
					case lt(0x1004):
						local_var_73 = 7;
						break;
					case lt(0x141e):
						local_var_73 = 8;
						break;
					case lt(0x17d4):
						local_var_73 = 9;
						break;
					case lt(0x2af8):
						local_var_73 = 12;
						break;
					case lt(0x141e):
						local_var_73 = 12;
						break;
					case lt(0x17d4):
						local_var_73 = 12;
						break;
					default:
						local_var_73 = 12;
						break;
				}
				for (local_var_8e = 0; local_var_8e <= (local_var_73 - 1); local_var_8e = (local_var_8e + 1))
				{
					local_var_8f[local_var_8e] = 0;
				}
				sysDVAR(7, local_var_8e);
				for (local_var_8e = 0; local_var_8e <= (local_var_73 - 1); local_var_8e = (local_var_8e + 1))
				{
					if (isquestclear((local_var_8e + 160)))
					{
						local_var_8f[local_var_8e] = 1;
					}
					else if (!(isquestorder((local_var_8e + 160))))
					{
						if (local_var_74 >= local_var_81[local_var_8e])
						{
							switch ((local_var_8e + 160))
							{
								case 168:
									if (g_btl_nm_ファーヴニル == 2)
									{
										local_var_92 = 1;
									}
									else
									{
										local_var_92 = 0;
									}
									break;
								case 167:
									if (g_btl_アントリオン == 2)
									{
										local_var_92 = 1;
									}
									else
									{
										local_var_92 = 0;
									}
									break;
								case 171:
									if ((quest_global_flag[7] >= 7 && g_btl_魔神竜 == 2))
									{
										local_var_92 = 1;
									}
									else
									{
										local_var_92 = 0;
									}
									break;
								default:
									local_var_92 = 1;
									break;
							}
							if (local_var_92 == 1)
							{
								local_var_83[local_var_8e] = local_var_90;
								local_var_90 = (local_var_90 + 1);
								local_var_8f[local_var_8e] = 2;
								if (local_var_86 == 0)
								{
									local_var_85 = local_var_83[local_var_8e];
									local_var_87 = (local_var_8e + 160);
									local_var_86 = 1;
								}
							}
							else
							{
								local_var_8f[local_var_8e] = 0;
							}
						}
						else
						{
							local_var_8f[local_var_8e] = 0;
						}
					}
					else if (local_var_80[local_var_8e] == 2)
					{
						local_var_83[local_var_8e] = local_var_90;
						local_var_90 = (local_var_90 + 1);
						local_var_8f[local_var_8e] = 4;
					}
					else
					{
						local_var_83[local_var_8e] = local_var_90;
						local_var_90 = (local_var_90 + 1);
						local_var_8f[local_var_8e] = 3;
					}
				}
				local_var_8e = local_var_88;
				if (local_var_8a == 1)
				{
					local_var_90 = (local_var_90 + 1);
				}
				else if (isquestclear(172))
				{
					local_var_8e = (local_var_88 + 1);
				}
				if (local_var_90 == 0)
				{
					if (local_var_8d == 0)
					{
						if (local_var_8e >= 13)
						{
							setmeswincaptionid(0, 3);
							amese(0, 0x1000042);
							messync(0, 1);
						}
						else
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000043);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						local_var_8b = 99;
						break;
					}
					break;
				}
				else
				{
					if (local_var_8a == 1)
					{
						for (local_var_89 = (local_var_7f - 2); local_var_89 >= 8; local_var_89 = (local_var_89 - 1))
						{
							local_var_83[(local_var_89 + 1)] = (local_var_83[local_var_89] + 1);
							local_var_8f[(local_var_89 + 1)] = local_var_8f[local_var_89];
						}
						if (!(isquestorder(172)))
						{
							local_var_8f[8] = 2;
							if (local_var_87 >= 168)
							{
								local_var_85 = 0;
								for (local_var_89 = 0; local_var_89 < 8; local_var_89 = (local_var_89 + 1))
								{
									if ((local_var_8f[local_var_89] >= 2 && local_var_8f[local_var_89] <= 4))
									{
										local_var_85 = (local_var_85 + 1);
									}
								}
								local_var_86 = 1;
							}
						}
						else
						{
							if (battle_global_flag[3] == 2)
							{
								local_var_8f[8] = 4;
							}
							else
							{
								local_var_8f[8] = 3;
							}
							if (local_var_87 >= 168)
							{
								local_var_85 = (local_var_85 + 1);
								local_var_86 = 1;
							}
						}
						local_var_83[8] = 0;
						for (local_var_89 = 0; local_var_89 < 8; local_var_89 = (local_var_89 + 1))
						{
							if ((local_var_8f[local_var_89] >= 2 && local_var_8f[local_var_89] <= 4))
							{
								local_var_83[8] = (local_var_83[8] + 1);
							}
						}
						local_var_73 = (local_var_73 + 1);
					}
					if ((local_var_90 + 1) >= 7)
					{
						local_var_90 = 6;
					}
					setmeswinline(0, (local_var_90 + 1));
					for (local_var_8e = 0; local_var_8e <= (local_var_7f - 2); local_var_8e = (local_var_8e + 1))
					{
						if (local_var_8e <= (local_var_73 - 1))
						{
							if (local_var_8f[local_var_8e] >= 2)
							{
								switch (local_var_8f[local_var_8e])
								{
									case 2:
										setmesmacro(0, local_var_8e, 1, 0x806d);
										break;
									case 3:
										setmesmacro(0, local_var_8e, 1, 0x806e);
										break;
									case 4:
										setmesmacro(0, local_var_8e, 1, 0x806f);
										break;
								}
							}
							else
							{
								setaskselectignore(0, local_var_8e);
							}
						}
						else
						{
							setaskselectignore(0, local_var_8e);
						}
					}
					if ((local_var_84 == 0 && local_var_85 > 0))
					{
						askpos(0, local_var_85, local_var_8e);
						local_var_84 = 1;
					}
					else
					{
						askpos(0, local_var_83[local_var_8b], local_var_8e);
					}
					if (local_var_8a == 1)
					{
						local_var_8b = aask(0, 0x1000044, 52, 0x3fe, 1);
					}
					else
					{
						local_var_8b = aask(0, 0x1000045, 48, 0x3fe, 1);
					}
					mesclose(0);
					messync(0, 1);
					if (local_var_8b >= (local_var_7f - 1))
					{
						break;
					}
					if (local_var_8a == 1)
					{
						if ((local_var_8b + 160) == 168)
						{
							local_var_93 = 12;
							questeffectread(((local_var_93 + 160) + 0));
						}
						else if ((local_var_8b + 160) > 168)
						{
							local_var_93 = (local_var_8b - 1);
							questeffectread(((local_var_93 + 160) + 0));
						}
						else
						{
							local_var_93 = local_var_8b;
							questeffectread(((local_var_93 + 160) + 0));
						}
						effectreadsync();
						effectplay(0);
						sebsoundplay(0, 23);
						setmesmacro(0, 0, 0, (local_var_8b + 1));
						switch (local_var_8f[local_var_8b])
						{
							case 2:
								setmesmacro(0, 1, 1, 0x806d);
								break;
							case 3:
								setmesmacro(0, 1, 1, 0x806e);
								break;
							case 4:
								setmesmacro(0, 1, 1, 0x806f);
								break;
						}
					}
					else
					{
						questeffectread(((local_var_8b + 160) + 0));
						effectreadsync();
						effectplay(0);
						sebsoundplay(0, 23);
						if ((local_var_8b + 160) >= 168)
						{
							setmesmacro(0, 0, 0, (local_var_8b + 2));
						}
						else
						{
							setmesmacro(0, 0, 0, (local_var_8b + 1));
						}
						switch (local_var_8f[local_var_8b])
						{
							case 2:
								setmesmacro(0, 1, 1, 0x806d);
								break;
							case 3:
								setmesmacro(0, 1, 1, 0x806e);
								break;
							case 4:
								setmesmacro(0, 1, 1, 0x806f);
								break;
						}
						local_var_93 = local_var_8b;
					}
					if (local_var_8f[local_var_8b] == 2)
					{
						local_var_94 = local_var_8b;
						local_var_8b = local_var_93;
						switch (local_var_8b)
						{
							case 0:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000046);
								mesclose(0);
								messync(0, 1);
								break;
							case 1:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000047);
								mesclose(0);
								messync(0, 1);
								break;
							case 2:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000048);
								mesclose(0);
								messync(0, 1);
								break;
							case 3:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000049);
								mesclose(0);
								messync(0, 1);
								break;
							case 4:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100004a);
								mesclose(0);
								messync(0, 1);
								break;
							case 5:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100004b);
								mesclose(0);
								messync(0, 1);
								break;
							case 6:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100004c);
								mesclose(0);
								messync(0, 1);
								break;
							case 7:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100004d);
								mesclose(0);
								messync(0, 1);
								break;
							case 8:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100004e);
								mesclose(0);
								messync(0, 1);
								break;
							case 9:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100004f);
								mesclose(0);
								messync(0, 1);
								break;
							case 10:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000050);
								mesclose(0);
								messync(0, 1);
								break;
							case 11:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000051);
								mesclose(0);
								messync(0, 1);
								break;
							case 12:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000052);
								mesclose(0);
								messync(0, 1);
								break;
						}
					}
					else
					{
						local_var_94 = local_var_8b;
						local_var_8b = local_var_93;
						switch (local_var_8b)
						{
							case 0:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000053);
								mesclose(0);
								messync(0, 1);
								break;
							case 1:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000054);
								mesclose(0);
								messync(0, 1);
								break;
							case 2:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000055);
								mesclose(0);
								messync(0, 1);
								break;
							case 3:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000056);
								mesclose(0);
								messync(0, 1);
								break;
							case 4:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000057);
								mesclose(0);
								messync(0, 1);
								break;
							case 5:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000058);
								mesclose(0);
								messync(0, 1);
								break;
							case 6:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x1000059);
								mesclose(0);
								messync(0, 1);
								break;
							case 7:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100005a);
								mesclose(0);
								messync(0, 1);
								break;
							case 8:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100005b);
								mesclose(0);
								messync(0, 1);
								break;
							case 9:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100005c);
								mesclose(0);
								messync(0, 1);
								break;
							case 10:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100005d);
								mesclose(0);
								messync(0, 1);
								break;
							case 11:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100005e);
								mesclose(0);
								messync(0, 1);
								break;
							case 12:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_8c = aaske(0, 0x100005f);
								mesclose(0);
								messync(0, 1);
								break;
						}
					}
					local_var_8b = local_var_94;
					effectplay_e2(0, 1);
					if (local_var_8c == 0)
					{
						if (local_var_8f[local_var_8b] == 2)
						{
							wait(15);
							settalknpcname(local_var_82[local_var_93]);
							switch ((local_var_93 + 160))
							{
								case 167:
								case 170:
								case 171:
								case 172:
									setkutipakustatus(1);
									setunazukistatus(1);
									askpos(0, 0, 127);
									local_var_91 = aaske(0, 0x1000060);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									if (local_var_91 != 0)
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										amese(0, 0x1000061);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
										effectsync_e3(0);
									}
									else
									{
										switch ((local_var_93 + 160))
										{
											case 167:
												setkutipakustatus(1);
												setunazukistatus(1);
												askpos(0, 0, 127);
												local_var_91 = aaske(0, 0x1000062);
												mesclose(0);
												messync(0, 1);
												setkutipakustatus(0);
												setunazukistatus(0);
												break;
											case 170:
												setkutipakustatus(1);
												setunazukistatus(1);
												askpos(0, 0, 127);
												local_var_91 = aaske(0, 0x1000063);
												mesclose(0);
												messync(0, 1);
												setkutipakustatus(0);
												setunazukistatus(0);
												break;
											case 171:
												setkutipakustatus(1);
												setunazukistatus(1);
												askpos(0, 0, 127);
												local_var_91 = aaske(0, 0x1000064);
												mesclose(0);
												messync(0, 1);
												setkutipakustatus(0);
												setunazukistatus(0);
												break;
											case 172:
												setkutipakustatus(1);
												setunazukistatus(1);
												askpos(0, 0, 127);
												local_var_91 = aaske(0, 0x1000065);
												mesclose(0);
												messync(0, 1);
												setkutipakustatus(0);
												setunazukistatus(0);
												break;
										}
										if (local_var_91 != 0)
										{
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000061);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											effectsync_e3(0);
										}
										else
										{
											switch ((local_var_93 + 160))
											{
												case 167:
													setkutipakustatus(1);
													setunazukistatus(1);
													amese(0, 0x1000066);
													messync(0, 1);
													setkutipakustatus(0);
													setunazukistatus(0);
													break;
												case 170:
													setkutipakustatus(1);
													setunazukistatus(1);
													amese(0, 0x1000067);
													messync(0, 1);
													setkutipakustatus(0);
													setunazukistatus(0);
													break;
												case 171:
													setkutipakustatus(1);
													setunazukistatus(1);
													amese(0, 0x1000068);
													messync(0, 1);
													setkutipakustatus(0);
													setunazukistatus(0);
													break;
												case 172:
													setkutipakustatus(1);
													setunazukistatus(1);
													amese(0, 0x1000069);
													messync(0, 1);
													setkutipakustatus(0);
													setunazukistatus(0);
													break;
											}
											setquestorder((local_var_93 + 160), 1);
											setquestscenarioflag((local_var_93 + 160), 10);
											effectsync_e3(0);
											switch ((local_var_93 + 160))
											{
												case 167:
													questeffectread(((local_var_93 + 160) + 64));
													effectreadsync();
													effectplay(0);
													effectsync();
													g_btl_ギルガメッシュA = 1;
													setquestscenarioflag(167, 30);
													break;
												case 170:
													questeffectread(((local_var_93 + 160) + 64));
													effectreadsync();
													effectplay(0);
													effectsync();
													g_btl_おんみょうし = 1;
													setquestscenarioflag(170, 30);
													break;
												case 171:
													questeffectread(((local_var_93 + 160) + 64));
													effectreadsync();
													effectplay(0);
													effectsync();
													g_btl_神竜 = 1;
													g_btl_オメガ = 1;
													setquestscenarioflag(171, 30);
													break;
												case 172:
													questeffectread(((local_var_93 + 160) + 64));
													effectreadsync();
													effectplay(0);
													effectsync();
													battle_global_flag[3] = 1;
													battle_global_flag[3] = 1;
													setquestscenarioflag(172, 30);
													break;
											}
											goto localjmp_331;
										}
									}
									goto localjmp_332;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100006a);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							setquestorder((local_var_93 + 160), 1);
							setquestscenarioflag((local_var_93 + 160), 10);
						}
						else
						{
							switch ((local_var_93 + 160))
							{
								case 167:
									if (g_btl_ギルガメッシュA != 2)
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										askpos(0, 0, 127);
										local_var_91 = aaske(0, 0x100006b);
										mesclose(0);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									else
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										askpos(0, 0, 127);
										local_var_91 = aaske(0, 0x100006c);
										mesclose(0);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									if (local_var_91 == 0)
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										amese(0, 0x100006d);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									goto localjmp_332;
								case 170:
									setkutipakustatus(1);
									setunazukistatus(1);
									askpos(0, 0, 127);
									local_var_91 = aaske(0, 0x100006e);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									if (local_var_91 == 0)
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										amese(0, 0x100006f);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									goto localjmp_332;
								case 171:
									setkutipakustatus(1);
									setunazukistatus(1);
									askpos(0, 0, 127);
									local_var_91 = aaske(0, 0x1000070);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									if (local_var_91 == 0)
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										amese(0, 0x1000068);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									goto localjmp_332;
								case 172:
									setkutipakustatus(1);
									setunazukistatus(1);
									askpos(0, 0, 127);
									local_var_91 = aaske(0, 0x1000071);
									mesclose(0);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									if (local_var_91 == 0)
									{
										setkutipakustatus(1);
										setunazukistatus(1);
										amese(0, 0x1000069);
										messync(0, 1);
										setkutipakustatus(0);
										setunazukistatus(0);
									}
									goto localjmp_332;
							}
							effectsync_e3(0);
						}
						effectsync_e3(0);
						openfullscreenmenu_48d(4, (local_var_93 + 160));
						local_var_8d = 1;
						if (local_var_8f[local_var_8b] == 2)
						{
						localjmp_331:
							break;
						}
					localjmp_332:
						wait(30);
					}
					else
					{
						local_var_8d = 1;
						wait(20);
					}
					effectcancel();
				}
			}
			effectcancel();
			if ((local_var_8b >= (local_var_7f - 1) && local_var_8b != 99))
			{
				if ((シナリオフラグ >= 0x1004 && !(isquestclear(172))))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000072);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000061);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
			}
			else
			{
				if (local_var_8b != 99)
				{
					if ((シナリオフラグ >= 0x1004 && !(isquestclear(172))))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000073);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else
					{
						switch (rand_29(2))
						{
							case 0:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000074);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000075);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
					}
					if ((isquestorder(164) && !((g_iw_クラン情報フラグ[1] & 4))))
					{
						fadeout(15);
						fadesync();
						g_com_navi_footcalc[0] = getx_12b(-1);
						g_com_navi_footcalc[1] = gety_12c(-1);
						g_com_navi_footcalc[2] = getz_12d(-1);
						setnavimapfootmarkstatus(0);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(0);
						if (!(istownmap()))
						{
							setstatuserrordispdenystatus(1);
						}
						unkCall_5ac(1, 2.35800004, 100);
						wait(5);
						partyusemapid(1);
						setposparty(37.5148277, 6.00000095, 55.606041, -2.79766989);
						camerastart_7e(0, 0x2000000);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(1);
						fadein(15);
						ヴァン.capturepc(-1);
						sysAturna(NPC01);
						ヴァン.sysAturna(NPC01);
						NPC01.sysLookata(-1);
						wait(10);
						NPC01.setkutipakustatus(1);
						NPC01.setunazukistatus(1);
						NPC01.amese(0, 0x1000076);
						NPC01.messync(0, 1);
						NPC01.setkutipakustatus(0);
						NPC01.setunazukistatus(0);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						ヴァン.lookatoff();
						ヴァン.releasepc();
						sysReq(1, NPC01::NPC挙動);
						cameraclear();
						g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 4);
						g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 32);
						showparty();
						if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
						{
							clearnavimapfootmark();
						}
						setnavimapfootmarkstatus(1);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						setstatuserrordispdenystatus(0);
					}
					if ((isquestorder(163) && !((g_iw_クラン情報フラグ[1] & 8))))
					{
						fadeout(15);
						fadesync();
						g_com_navi_footcalc[0] = getx_12b(-1);
						g_com_navi_footcalc[1] = gety_12c(-1);
						g_com_navi_footcalc[2] = getz_12d(-1);
						setnavimapfootmarkstatus(0);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(0);
						if (!(istownmap()))
						{
							setstatuserrordispdenystatus(1);
						}
						unkCall_5ac(1, 2.35800004, 100);
						wait(5);
						partyusemapid(1);
						setposparty(37.5148277, 6.00000095, 55.606041, -2.79766989);
						camerastart_7e(0, 0x2000001);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(1);
						fadein(15);
						ヴァン.capturepc(-1);
						sysAturna(NPC19);
						ヴァン.sysAturna(NPC19);
						NPC19.sysLookata(-1);
						wait(10);
						NPC19.setkutipakustatus(1);
						NPC19.setunazukistatus(1);
						NPC19.amese(0, 0x1000077);
						NPC19.messync(0, 1);
						NPC19.setkutipakustatus(0);
						NPC19.setunazukistatus(0);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						ヴァン.lookatoff();
						ヴァン.releasepc();
						sysReq(1, NPC19::NPC挙動);
						cameraclear();
						g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 8);
						g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 64);
						showparty();
						if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
						{
							clearnavimapfootmark();
						}
						setnavimapfootmarkstatus(1);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						setstatuserrordispdenystatus(0);
					}
					if ((isquestorder(166) && !((g_iw_クラン情報フラグ[1] & 16))))
					{
						fadeout(15);
						fadesync();
						g_com_navi_footcalc[0] = getx_12b(-1);
						g_com_navi_footcalc[1] = gety_12c(-1);
						g_com_navi_footcalc[2] = getz_12d(-1);
						setnavimapfootmarkstatus(0);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(0);
						if (!(istownmap()))
						{
							setstatuserrordispdenystatus(1);
						}
						unkCall_5ac(1, 2.35800004, 100);
						wait(5);
						partyusemapid(1);
						setposparty(37.5148277, 6.00000095, 55.606041, -2.79766989);
						camerastart_7e(0, 0x2000002);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(1);
						fadein(15);
						ヴァン.capturepc(-1);
						sysAturna(カロリーヌ);
						ヴァン.sysAturna(カロリーヌ);
						カロリーヌ.sysLookata(-1);
						wait(10);
						カロリーヌ.setkutipakustatus(1);
						カロリーヌ.setunazukistatus(1);
						カロリーヌ.amese(0, 0x1000078);
						カロリーヌ.messync(0, 1);
						カロリーヌ.setkutipakustatus(0);
						カロリーヌ.setunazukistatus(0);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						ヴァン.lookatoff();
						ヴァン.releasepc();
						sysReq(1, カロリーヌ::NPC挙動);
						cameraclear();
						g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 16);
						g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 4);
						showparty();
						if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
						{
							clearnavimapfootmark();
						}
						setnavimapfootmarkstatus(1);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						setstatuserrordispdenystatus(0);
					}
					if ((isquestorder(172) && !((g_iw_クラン情報フラグ[1] & 64))))
					{
						fadeout(15);
						fadesync();
						g_com_navi_footcalc[0] = getx_12b(-1);
						g_com_navi_footcalc[1] = gety_12c(-1);
						g_com_navi_footcalc[2] = getz_12d(-1);
						setnavimapfootmarkstatus(0);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(0);
						if (!(istownmap()))
						{
							setstatuserrordispdenystatus(1);
						}
						unkCall_5ac(1, 2.35800004, 100);
						wait(5);
						partyusemapid(1);
						setposparty(37.5148277, 6.00000095, 55.606041, -2.79766989);
						sysReq(1, hitactor01_monion::clearHitObj);
						sysReq(3, 配置監督::停止);
						sysReqwait(配置監督::停止);
						sysReq(3, NPC02::停止);
						sysReq(3, NPC07::停止);
						sysReq(3, NPC08::停止);
						sysReq(3, NPC13::停止);
						sysReq(3, NPC14::停止);
						sysReqwait(NPC02::停止);
						sysReqwait(NPC07::停止);
						sysReqwait(NPC08::停止);
						sysReqwait(NPC13::停止);
						sysReqwait(NPC14::停止);
						sysReq(2, NPC02::NPC配置);
						ヴァン.capturepc(-1);
						ヴァン.hidew();
						ヴァン.hides();
						sysReq(3, NPC07::怪しいモーニ準備);
						sysReq(3, NPC08::怪しいモーニ準備);
						sysReq(3, NPC13::怪しいモーニ準備);
						sysReq(3, NPC14::怪しいモーニ準備);
						sysReq(4, NPC01::怪しいモーニ準備);
						sysReq(4, パシリ::怪しいモーニ準備);
						sysReqwaitall(reqArr3);
						camerastart_7e(0, 0x2000003);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(1);
						fadein(15);
						sysAturna(NPC01);
						ヴァン.sysAturna(NPC01);
						wait(10);
						sysReq(1, NPC01::怪しいモーニcut01);
						NPC01.setkutipakustatus(1);
						NPC01.setunazukistatus(1);
						NPC01.amese(0, 0x1000079);
						NPC01.messync(0, 1);
						NPC01.setkutipakustatus(0);
						NPC01.setunazukistatus(0);
						wait(10);
						sysReq(1, NPC01::怪しいモーニcut02);
						wait(15);
						camerastart_7e(0, 0x2000004);
						sebsoundplay(1, 1);
						sysReq(1, パシリ::怪しいモーニcut02);
						wait(60);
						sysReqwait(パシリ::怪しいモーニcut02);
						sysRlookata(パシリ);
						wait(30);
						sysReq(1, パシリ::怪しいモーニcut02_02);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100007a);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						sysReqwait(パシリ::怪しいモーニcut02_02);
						wait(15);
						sysReq(1, パシリ::怪しいモーニcut03);
						wait(15);
						camerastart_7e(0, 0x2000005);
						パシリ.setkutipakustatus(1);
						パシリ.setunazukistatus(1);
						パシリ.amese(0, 0x100007b);
						パシリ.messync(0, 1);
						パシリ.setkutipakustatus(0);
						パシリ.setunazukistatus(0);
						sysReq(3, NPC07::怪しいモーニcut03_02);
						sysReq(3, NPC08::怪しいモーニcut03_02);
						stdmotionplay(0x1000016);
						モンブラン.setkutipakustatus(1);
						モンブラン.setunazukistatus(1);
						モンブラン.amese(0, 0x100007c);
						モンブラン.messync(0, 1);
						モンブラン.setkutipakustatus(0);
						モンブラン.setunazukistatus(0);
						camerastart_7e(0, 0x2000006);
						sysReq(1, パシリ::怪しいモーニcut04);
						パシリ.setkutipakustatus(1);
						パシリ.setunazukistatus(1);
						パシリ.amese(0, 0x100007d);
						パシリ.messync(0, 1);
						パシリ.setkutipakustatus(0);
						パシリ.setunazukistatus(0);
						sysReq(1, パシリ::怪しいモーニcut05);
						wait(15);
						camerastart_7e(0, 0x2000007);
						ヴァン.sysRlookata(モンブラン);
						sysAturna(-1);
						stdmotionplay(0x1000016);
						モンブラン.setkutipakustatus(1);
						モンブラン.setunazukistatus(1);
						モンブラン.amese(0, 0x100007e);
						モンブラン.messync(0, 1);
						モンブラン.setkutipakustatus(0);
						モンブラン.setunazukistatus(0);
						wait(10);
						fadeout(15);
						fadesync();
						setcharseplayall(0);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						setstatuserrordispdenystatus(0);
						unkCall_5ac(0, 0, 0);
						sysReqwait(パシリ::怪しいモーニcut05);
						ヴァン.showw();
						ヴァン.shows();
						ヴァン.lookatoff();
						ヴァン.releasepc();
						cameraclear();
						sysReq(3, NPC07::停止);
						sysReq(3, NPC08::停止);
						sysReqwait(NPC07::停止);
						sysReqwait(NPC08::停止);
						sysReq(2, NPC07::NPC配置);
						sysReq(2, NPC08::NPC配置);
						sysReq(2, NPC13::NPC配置);
						sysReq(2, NPC14::NPC配置);
						sysReq(4, パシリ::怪しいモーニ終了);
						sysReqwait(NPC07::NPC配置);
						sysReqwait(NPC08::NPC配置);
						sysReq(1, 配置監督::戦闘訓練);
						file_var_5e = (file_var_5e | 2);
						g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 64);
						showparty();
						if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
						{
							clearnavimapfootmark();
						}
						setnavimapfootmarkstatus(1);
						setcharseplayall(1);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						wait(1);
						fadein(15);
					}
				}
			localjmp_357:
			}
		localjmp_357:
		}
	localjmp_357:
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_68;            // pos: 0x4;
	float   local_var_69[12];        // pos: 0x8;
	float   local_var_6a[4];         // pos: 0x38;
	float   local_var_6b;            // pos: 0x4c;
	float   local_var_6c[4];         // pos: 0x50;
	float   local_var_6d;            // pos: 0x68;
	int     local_var_6e;            // pos: 0x84;
	u_char  local_var_6f;            // pos: 0x8c;
	u_char  local_var_70;            // pos: 0x8e;
	u_char  local_var_71;            // pos: 0x8f;
	u_char  local_var_72;            // pos: 0x90;

	function NPC配置_mon()
	{
		hidecomplete();
		usemapid(0);
		local_var_68 = 9;
		local_var_71 = 1;
		local_var_72 = 0;
		local_var_6f = 0;
		local_var_70 = 0;
		local_var_6e = 0;
		local_var_6d = 0;
		local_var_69[0] = 37.0395699;
		local_var_69[1] = 6.92498112;
		local_var_69[2] = 55.0979996;
		local_var_69[3] = 0;
		local_var_69[4] = 0;
		local_var_69[5] = 0;
		local_var_69[6] = 0;
		local_var_69[7] = 0;
		local_var_69[8] = 0;
		local_var_69[9] = 0;
		local_var_69[10] = 0;
		local_var_69[11] = 0;
		local_var_6a[0] = 3.10536695;
		local_var_6a[1] = 0;
		local_var_6a[2] = 0;
		local_var_6a[3] = 0;
		local_var_6c[0] = 37.0395699;
		local_var_6c[1] = 6.92498112;
		local_var_6c[2] = 55.0979996;
		local_var_6c[3] = 3.10536695;
		setpos(local_var_69[0], local_var_69[1], local_var_69[2]);
		dir(local_var_6a[0]);
		bindp2_d4(0x3000006, local_var_71, local_var_72);
		set_ignore_hitgroup(1);
		fetchambient_4e1(36.4163284, 6.00000095, 55.5995636);
		reqenable(2);
		setreachr(0.300000012);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		local_var_6b = getdefaultwalkspeed();
		setwalkspeed(local_var_6b);
		setweight(-1);
		setnpcname(0x2cd);
		settalkradiusoffset(0, 0, 0.5);
		talkradius(0.300000012);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function 挙動()
	{
		return;
	}


	function op_cut01()
	{
		hidecomplete();
		return;
	}


	function op_cut03()
	{
		clearhidecomplete();
		return;
	}


	function camclear()
	{
		clearhidecomplete();
		return;
	}
}


script マッケンロウ(6)
{

	function init()
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_a1[12];        // pos: 0x90;
	int     local_var_a2[12];        // pos: 0x9c;

	function main(1)
	{
		local_var_a1[0] = 0;
		local_var_a2[0] = 0;
		local_var_a1[1] = 2;
		local_var_a2[1] = 0x2bc;
		local_var_a1[2] = 4;
		local_var_a2[2] = 0x1f40;
		local_var_a1[3] = 8;
		local_var_a2[3] = 0x4e20;
		local_var_a1[4] = 10;
		local_var_a2[4] = 0x7530;
		local_var_a1[5] = 12;
		local_var_a2[5] = 0x9c40;
		local_var_a1[6] = 14;
		local_var_a2[6] = 0x186a0;
		local_var_a1[7] = 16;
		local_var_a2[7] = 0x30d40;
		local_var_a1[8] = 24;
		local_var_a2[8] = 0x3d090;
		local_var_a1[9] = 28;
		local_var_a2[9] = 0x493e0;
		local_var_a1[10] = 32;
		local_var_a2[10] = 0x7a120;
		local_var_a1[11] = 44;
		local_var_a2[11] = 0x989680;
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	short   local_var_a0;            // pos: 0x8e;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		setkubifuristatus(0);
		if (!((g_iw_クラン情報フラグ[0] & 2)))
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x100007f);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 2);
		}
		else
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x1000080);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			sysReq(1, funcActor::getOfferedCount);
			sysReqwait(funcActor::getOfferedCount);
			switch (file_var_59)
			{
				case 0:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000081);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				default:
					sysReq(1, funcActor::getOfferedRnd);
					sysReqwait(funcActor::getOfferedRnd);
					switch (file_var_59)
					{
						case 128:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000082);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 129:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000083);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 130:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000084);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 131:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000085);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 132:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000086);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 133:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000087);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 134:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000088);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 135:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000089);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 136:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100008a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 137:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100008b);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 138:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100008c);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 139:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100008d);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 140:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100008e);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 141:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100008f);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 142:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000090);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 143:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000091);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 144:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000092);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 145:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000093);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 146:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000094);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 147:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000095);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 148:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000096);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 149:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000097);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 150:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000098);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 151:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000099);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 152:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100009a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 153:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100009b);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 154:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100009c);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 155:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100009d);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 156:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100009e);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 157:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100009f);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 158:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a0);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 159:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a1);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 160:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a2);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 161:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a3);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 162:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a4);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 163:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a5);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 164:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a6);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 165:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a7);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 166:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a8);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 167:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000a9);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 168:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000aa);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 169:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000ab);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 170:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000ac);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 171:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000ad);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
					break;
			}
			local_var_a0 = getclanrank();
			if (local_var_a0 == 0)
			{
				local_var_a0 = 1;
			}
			switch (local_var_a0)
			{
				case lt(11):
					setmesmacro(0, 0, 0, local_var_a1[local_var_a0]);
					setmesmacro(0, 1, 0, local_var_a2[local_var_a0]);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000ae);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 11:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000af);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case gte(12):
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b0);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
			}
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		setkubifuristatus(1);
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_95;            // pos: 0x0;
	float   local_var_96[12];        // pos: 0x4;
	float   local_var_97[4];         // pos: 0x34;
	float   local_var_98;            // pos: 0x48;
	float   local_var_99[4];         // pos: 0x4c;
	float   local_var_9a;            // pos: 0x64;
	int     local_var_9b;            // pos: 0x80;
	u_char  local_var_9c;            // pos: 0x88;
	u_char  local_var_9d;            // pos: 0x8a;
	u_char  local_var_9e;            // pos: 0x8b;
	u_char  local_var_9f;            // pos: 0x8c;

	function NPC配置()
	{
		hidecomplete();
		usemapid(0);
		local_var_95 = 5;
		local_var_9e = 2;
		local_var_9f = 2;
		local_var_9c = 0;
		local_var_9d = 0;
		local_var_9b = 0;
		local_var_9a = 0;
		local_var_96[0] = 42.366478;
		local_var_96[1] = 7.49997091;
		local_var_96[2] = 63.6059227;
		local_var_96[3] = 0;
		local_var_96[4] = 0;
		local_var_96[5] = 0;
		local_var_96[6] = 0;
		local_var_96[7] = 0;
		local_var_96[8] = 0;
		local_var_96[9] = 0;
		local_var_96[10] = 0;
		local_var_96[11] = 0;
		local_var_97[0] = -2.601614;
		local_var_97[1] = 0;
		local_var_97[2] = 0;
		local_var_97[3] = 0;
		local_var_99[0] = 42.366478;
		local_var_99[1] = 7.49997091;
		local_var_99[2] = 63.6059227;
		local_var_99[3] = -2.601614;
		setpos(local_var_96[0], local_var_96[1], local_var_96[2]);
		dir(local_var_97[0]);
		bindp2_d4(0x3000007, local_var_9e, local_var_9f);
		set_ignore_hitgroup(1);
		reqenable(2);
		setreachr(0.300000012);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		local_var_98 = getdefaultwalkspeed();
		setwalkspeed(local_var_98);
		setweight(-1);
		setradius_221(0.349999994, 0.349999994);
		setnpcname(0x2cb);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		setkubifuristatus(1);
		return;
	}


	function 挙動()
	{
		return;
	}


	function op_cut01()
	{
		hidecomplete();
		return;
	}


	function op_cut03()
	{
		clearhidecomplete();
		return;
	}


	function camclear()
	{
		clearhidecomplete();
		return;
	}


	function NPC_神竜後バインドオフ()
	{
		bindoff();
		return 0;
		return;
	}
}


script カロリーヌ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		if (!((g_iw_クラン情報フラグ[0] & 4)))
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x10000b1);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 4);
		}
		else if ((g_iw_カトリーヌ結果 != 0 && !((g_iw_カトリーヌその後[0] & 2))))
		{
			switch (g_iw_カトリーヌ結果)
			{
				case 1:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b2);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 2:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b3);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 3:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b4);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 4:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b5);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				default:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b6);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
			}
			g_iw_カトリーヌその後[0] = (g_iw_カトリーヌその後[0] | 2);
		}
		else if ((isquestorder(166) && g_btl_nm_キャロット == 0))
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x1000078);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
		}
		else if ((g_btl_nm_キャロット == 2 && !((g_iw_クラン情報フラグ[1] & 2))))
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x10000b7);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 2);
		}
		else
		{
			sysReq(1, funcActor::getOfferedCount);
			sysReqwait(funcActor::getOfferedCount);
			switch (file_var_59)
			{
				case 0:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b8);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				default:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000b9);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
			}
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_a3;            // pos: 0x0;
	float   local_var_a4[12];        // pos: 0x4;
	float   local_var_a5[4];         // pos: 0x34;
	float   local_var_a6;            // pos: 0x48;
	float   local_var_a7[4];         // pos: 0x4c;
	float   local_var_a8;            // pos: 0x64;
	int     local_var_a9;            // pos: 0x80;
	u_char  local_var_aa;            // pos: 0x88;
	u_char  local_var_ab;            // pos: 0x8a;
	u_char  local_var_ac;            // pos: 0x8b;
	u_char  local_var_ad;            // pos: 0x8c;

	function NPC配置()
	{
		if (g_btl_nm_キャロット != 1)
		{
			hidecomplete();
			usemapid(0);
			local_var_a3 = 10;
			local_var_ac = 2;
			local_var_ad = 3;
			local_var_aa = 0;
			local_var_ab = 0;
			local_var_a9 = 0;
			local_var_a8 = 0;
			local_var_a4[0] = 39.9459991;
			local_var_a4[1] = 6.00000095;
			local_var_a4[2] = 62.729557;
			local_var_a4[3] = 0;
			local_var_a4[4] = 0;
			local_var_a4[5] = 0;
			local_var_a4[6] = 0;
			local_var_a4[7] = 0;
			local_var_a4[8] = 0;
			local_var_a4[9] = 0;
			local_var_a4[10] = 0;
			local_var_a4[11] = 0;
			local_var_a5[0] = 3.010324;
			local_var_a5[1] = 0;
			local_var_a5[2] = 0;
			local_var_a5[3] = 0;
			local_var_a7[0] = 39.9459991;
			local_var_a7[1] = 6.00000095;
			local_var_a7[2] = 62.729557;
			local_var_a7[3] = 3.010324;
			setpos(local_var_a4[0], local_var_a4[1], local_var_a4[2]);
			dir(local_var_a5[0]);
			bindp2_d4(0x3000008, local_var_ac, local_var_ad);
			set_ignore_hitgroup(1);
			reqenable(2);
			setreachr(0.300000012);
			stdmotionread(16);
			stdmotionreadsync();
			stdmotionplay(0x1000000);
			local_var_a6 = getdefaultwalkspeed();
			setwalkspeed(local_var_a6);
			setweight(-1);
			fieldsignmes(0x10000ba);
			rgbatrans(1, 1, 1, 0, 0);
			istouchucsync();
			clearhidecomplete();
			rgbatrans(1, 1, 1, 1, 10);
		}
		return;
	}


	function NPC挙動()
	{
		if (g_btl_nm_キャロット != 1)
		{
			lookatoff();
		}
		return;
	}


	function op_cut01()
	{
		hidecomplete();
		return;
	}


	function op_cut03()
	{
		clearhidecomplete();
		return;
	}


	function camclear()
	{
		clearhidecomplete();
		return;
	}


	function バインドオフ()
	{
		bindoff();
		return 0;
	}


	function NPC_神竜後バインドオフ()
	{
		bindoff();
		return 0;
		return;
	}
}


script NPC01(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_ae;            // pos: 0x0;

	function init()
	{
		local_var_ae = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_b1;            // pos: 0x7;
	u_char  local_var_b2;            // pos: 0x8;

	function talk(2)
	{
		switch (local_var_ae)
		{
			case 0:
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				sysLookata(-1);
				if (!((g_iw_クラン情報フラグ[0] & 32)))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000bb);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 32);
				}
				else if ((isquestorder(164) && g_btl_nm_トリックスター == 0))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000076);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else if ((g_btl_nm_トリックスター == 2 && !((g_iw_クラン情報フラグ[0] & 128))))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000bc);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 128);
				}
				else if ((((file_var_5e & 4) && !((g_iw_クラン情報フラグ[2] & 8))) || local_var_b1 == 1))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000bd);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_b1 = 1;
					g_iw_クラン情報フラグ[2] = (g_iw_クラン情報フラグ[2] | 8);
				}
				else if (g_btl_神竜 == 2)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000be);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					switch (getclanrank())
					{
						case lte(2):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000bf);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case lte(4):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000c0);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case lte(6):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000c1);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case lte(9):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000c2);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000c3);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
				}
				break;
			case 18:
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				setkubifuristatus(0);
				sysLookata(-1);
				if (!((g_iw_クラン情報フラグ[0] & 64)))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000c4);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					g_iw_クラン情報フラグ[0] = (g_iw_クラン情報フラグ[0] | 64);
				}
				else if ((((file_var_5e & 4) && !((g_iw_クラン情報フラグ[1] & 128))) || local_var_b1 == 1))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000c5);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_b1 = 1;
					g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 128);
					local_var_b2 = 1;
				}
				else if (local_var_b2 == 0)
				{
					if ((!((file_var_5e & 4)) && (file_var_5e & 2)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000c6);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					else if ((!((file_var_5e & 4)) && (file_var_5e & 1)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000c7);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					if (local_var_b2 != 0)
					{
					}
				}
				else
				{
					if ((isquestorder(163) && g_btl_nm_ギルガメ == 0))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000077);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else if ((g_btl_nm_ギルガメ == 2 && !((g_iw_クラン情報フラグ[1] & 1))))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000c8);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						g_iw_クラン情報フラグ[1] = (g_iw_クラン情報フラグ[1] | 1);
					}
					else if (g_btl_神竜 == 2)
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000c9);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else
					{
						switch (getclanrank())
						{
							case lte(2):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000ca);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(4):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000cb);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(6):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000cc);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(9):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000cd);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000ce);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
					}
					local_var_b2 = 0;
				}
				setkubifuristatus(1);
				break;
			case 10:
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				sysLookata(-1);
				if ((((file_var_5e & 4) && !((g_iw_クラン情報フラグ[2] & 1))) || local_var_b1 == 1))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000cf);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_b1 = 1;
					g_iw_クラン情報フラグ[2] = (g_iw_クラン情報フラグ[2] | 1);
					local_var_b2 = 1;
				}
				else if (local_var_b2 == 0)
				{
					if ((!((file_var_5e & 4)) && (file_var_5e & 2)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000d0);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					else if ((!((file_var_5e & 4)) && (file_var_5e & 1)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000d1);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					if (local_var_b2 != 0)
					{
					}
				}
				else
				{
					if (g_btl_神竜 == 2)
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000d2);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else
					{
						switch (getclanrank())
						{
							case lte(2):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000d3);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(4):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000d4);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(6):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000d5);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(9):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000d6);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000d7);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
					}
					local_var_b2 = 0;
				}
				break;
			case 3:
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				sysLookata(-1);
				if (g_btl_神竜 == 2)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000d8);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					switch (getclanrank())
					{
						case lte(2):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000d9);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case lte(4):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000da);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case lte(6):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000db);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case lte(9):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000dc);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x10000dd);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
				}
				break;
			case 8:
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				sysLookata(-1);
				if ((((file_var_5e & 4) && !((g_iw_クラン情報フラグ[2] & 2))) || local_var_b1 == 1))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000de);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_b1 = 1;
					g_iw_クラン情報フラグ[2] = (g_iw_クラン情報フラグ[2] | 2);
					local_var_b2 = 1;
				}
				else if (local_var_b2 == 0)
				{
					if ((!((file_var_5e & 4)) && (file_var_5e & 2)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000df);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					else if ((!((file_var_5e & 4)) && (file_var_5e & 1)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000e0);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					if (local_var_b2 != 0)
					{
					}
				}
				else
				{
					if (g_btl_神竜 == 2)
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000e1);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else
					{
						switch (getclanrank())
						{
							case lte(2):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000e2);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(4):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000e3);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(6):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000e4);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(9):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000e5);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000e6);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
					}
					local_var_b2 = 0;
				}
				break;
			case 17:
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				setaturnlookatlockstatus(0);
				sysRaturna(-1);
				regI2 = 0;
				if (isturn())
				{
					turnsync();
					stdmotionplay_2c2(0x1000002, 20);
				}
				else
				{
					regI2 = 1;
				}
				sysLookata(-1);
				if ((((file_var_5e & 4) && !((g_iw_クラン情報フラグ[2] & 4))) || local_var_b1 == 1))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x10000e7);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_b1 = 1;
					g_iw_クラン情報フラグ[2] = (g_iw_クラン情報フラグ[2] | 4);
					local_var_b2 = 1;
				}
				else if (local_var_b2 == 0)
				{
					if ((!((file_var_5e & 4)) && (file_var_5e & 2)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000e8);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					else if ((!((file_var_5e & 4)) && (file_var_5e & 1)))
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000e9);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						local_var_b2 = 1;
					}
					if (local_var_b2 != 0)
					{
					}
				}
				else
				{
					if (g_btl_神竜 == 2)
					{
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x10000ea);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
					}
					else
					{
						switch (getclanrank())
						{
							case lte(2):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000eb);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(4):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000ec);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(6):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000ed);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							case lte(9):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000ee);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x10000ef);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
					}
					local_var_b2 = 0;
				}
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				aturn_261(getdestrotangzx_227(0));
				break;
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_b0;            // pos: 0x6;
	float   local_var_b3[12];        // pos: 0xc;
	float   local_var_b4[4];         // pos: 0x3c;
	float   local_var_b5;            // pos: 0x50;
	float   local_var_b6[4];         // pos: 0x54;
	float   local_var_b7;            // pos: 0x6c;
	int     local_var_b8;            // pos: 0x88;
	u_char  local_var_b9;            // pos: 0x90;
	u_char  local_var_ba;            // pos: 0x92;
	u_char  local_var_bb;            // pos: 0x93;
	u_char  local_var_bc;            // pos: 0x94;

	function NPC配置()
	{
		hidecomplete();
		reqdisable(2);
		switch (local_var_ae)
		{
			case 0:
				if (((g_btl_nm_トリックスター == 1 || battle_global_flag[3] == 1) || file_var_5d == 1))
				{
					if ((battle_global_flag[3] == 1 || file_var_5d == 1))
					{
						file_var_5e = (file_var_5e | 2);
					}
					return;
				}
				local_var_b0 = 7;
				local_var_bb = 0;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 32.1083336;
				local_var_b3[1] = 0;
				local_var_b3[2] = 42.0158691;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = 0.822385013;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 32.1083336;
				local_var_b6[1] = 0;
				local_var_b6[2] = 42.0158691;
				local_var_b6[3] = 0.822385013;
				fieldsignmes(0x10000f0);
				reqenable(2);
				break;
			case 1:
				if (((g_btl_nm_トリックスター == 1 || battle_global_flag[3] == 1) || file_var_5d == 1))
				{
					local_var_b0 = 3;
					local_var_bb = 2;
					local_var_bc = 0;
					local_var_b9 = 0;
					local_var_ba = 0;
					local_var_b8 = 0;
					local_var_b7 = 0;
					local_var_b3[0] = 31.1852932;
					local_var_b3[1] = 0;
					local_var_b3[2] = 43.4893494;
					local_var_b3[3] = 0;
					local_var_b3[4] = 0;
					local_var_b3[5] = 0;
					local_var_b3[6] = 0;
					local_var_b3[7] = 0;
					local_var_b3[8] = 0;
					local_var_b3[9] = 0;
					local_var_b3[10] = 0;
					local_var_b3[11] = 0;
					local_var_b4[0] = 1.49595904;
					local_var_b4[1] = 0;
					local_var_b4[2] = 0;
					local_var_b4[3] = 0;
					local_var_b6[0] = 31.1852932;
					local_var_b6[1] = 0;
					local_var_b6[2] = 43.4893494;
					local_var_b6[3] = 1.49595904;
				}
				else
				{
					sysReq(1, hitactor01_monion::setHitObj);
					local_var_b0 = 3;
					local_var_bb = 2;
					local_var_bc = 0;
					local_var_b9 = 0;
					local_var_ba = 0;
					local_var_b8 = 0;
					local_var_b7 = 0;
					local_var_b3[0] = 32.9477844;
					local_var_b3[1] = 0;
					local_var_b3[2] = 42.8363304;
					local_var_b3[3] = 0;
					local_var_b3[4] = 0;
					local_var_b3[5] = 0;
					local_var_b3[6] = 0;
					local_var_b3[7] = 0;
					local_var_b3[8] = 0;
					local_var_b3[9] = 0;
					local_var_b3[10] = 0;
					local_var_b3[11] = 0;
					local_var_b4[0] = -2.63698792;
					local_var_b4[1] = 0;
					local_var_b4[2] = 0;
					local_var_b4[3] = 0;
					local_var_b6[0] = 32.9477844;
					local_var_b6[1] = 0;
					local_var_b6[2] = 42.8363304;
					local_var_b6[3] = -2.63698792;
				}
				break;
			case 2:
				local_var_b0 = 9;
				local_var_bb = 3;
				local_var_bc = 4;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 30.6809711;
				local_var_b3[1] = 6.26280212;
				local_var_b3[2] = 56.9152489;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = 0.172702;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 30.6809711;
				local_var_b6[1] = 6.26280212;
				local_var_b6[2] = 56.9152489;
				local_var_b6[3] = 0.172702;
				break;
			case 3:
				local_var_b0 = 7;
				local_var_bb = 2;
				local_var_bc = 3;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 33.6306267;
				local_var_b3[1] = 6.00000095;
				local_var_b3[2] = 61.7006035;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -1.42730296;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 33.6306267;
				local_var_b6[1] = 6.00000095;
				local_var_b6[2] = 61.7006035;
				local_var_b6[3] = -1.42730296;
				setnpcname(133);
				reqenable(2);
				break;
			case 4:
				local_var_b0 = 7;
				local_var_bb = 3;
				local_var_bc = 4;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 32.018734;
				local_var_b3[1] = 6.00000095;
				local_var_b3[2] = 62.3622665;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = 2.45535898;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 32.018734;
				local_var_b6[1] = 6.00000095;
				local_var_b6[2] = 62.3622665;
				local_var_b6[3] = 2.45535898;
				break;
			case 5:
				return;
			case 6:
				local_var_b0 = 7;
				local_var_bb = 3;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 37.5871201;
				local_var_b3[1] = 0;
				local_var_b3[2] = 51.9400215;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -2.37957191;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 37.5871201;
				local_var_b6[1] = 0;
				local_var_b6[2] = 51.9400215;
				local_var_b6[3] = -2.37957191;
				setradius_221(0.5, 0.5);
				break;
			case 7:
				if (file_var_5c == 1)
				{
					local_var_b0 = 10;
					local_var_bb = 3;
					local_var_bc = 1;
					local_var_b9 = 0;
					local_var_ba = 0;
					local_var_b8 = 0;
					local_var_b7 = 0;
					local_var_b3[0] = 36.3618317;
					local_var_b3[1] = 0;
					local_var_b3[2] = 50.211628;
					local_var_b3[3] = 0;
					local_var_b3[4] = 0;
					local_var_b3[5] = 0;
					local_var_b3[6] = 0;
					local_var_b3[7] = 0;
					local_var_b3[8] = 0;
					local_var_b3[9] = 0;
					local_var_b3[10] = 0;
					local_var_b3[11] = 0;
					local_var_b4[0] = 0.597588003;
					local_var_b4[1] = 0;
					local_var_b4[2] = 0;
					local_var_b4[3] = 0;
					local_var_b6[0] = 36.3618317;
					local_var_b6[1] = 0;
					local_var_b6[2] = 50.211628;
					local_var_b6[3] = 0.597588003;
					setnpcname(159);
					reqenable(2);
					sysReqchg(2, NPC08::catrine_talk);
				}
				else
				{
					local_var_b0 = 8;
					local_var_bb = 2;
					local_var_bc = 2;
					local_var_b9 = 0;
					local_var_ba = 0;
					local_var_b8 = 0;
					local_var_b7 = 0;
					local_var_b3[0] = 36.3618317;
					local_var_b3[1] = 0;
					local_var_b3[2] = 50.211628;
					local_var_b3[3] = 0;
					local_var_b3[4] = 0;
					local_var_b3[5] = 0;
					local_var_b3[6] = 0;
					local_var_b3[7] = 0;
					local_var_b3[8] = 0;
					local_var_b3[9] = 0;
					local_var_b3[10] = 0;
					local_var_b3[11] = 0;
					local_var_b4[0] = 0.597588003;
					local_var_b4[1] = 0;
					local_var_b4[2] = 0;
					local_var_b4[3] = 0;
					local_var_b6[0] = 36.3618317;
					local_var_b6[1] = 0;
					local_var_b6[2] = 50.211628;
					local_var_b6[3] = 0.597588003;
				}
				setradius_221(0.5, 0.5);
				break;
			case 8:
				local_var_b0 = 9;
				local_var_bb = 2;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 33.5740623;
				local_var_b3[1] = 0;
				local_var_b3[2] = 52.8796997;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = 2.27815199;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 33.5740623;
				local_var_b6[1] = 0;
				local_var_b6[2] = 52.8796997;
				local_var_b6[3] = 2.27815199;
				setnpcname(133);
				reqenable(2);
				break;
			case 9:
				local_var_b0 = 9;
				local_var_bb = 3;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 37.0492172;
				local_var_b3[1] = 0.724999011;
				local_var_b3[2] = 55.4542236;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -3.0246079;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 37.0492172;
				local_var_b6[1] = 0.724999011;
				local_var_b6[2] = 55.4542236;
				local_var_b6[3] = -3.0246079;
				break;
			case 10:
				local_var_b0 = 1;
				local_var_bb = 2;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 44.4094009;
				local_var_b3[1] = 1.16494906;
				local_var_b3[2] = 49.89394;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -2.92799497;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 44.4094009;
				local_var_b6[1] = 1.16494906;
				local_var_b6[2] = 49.89394;
				local_var_b6[3] = -2.92799497;
				setnpcname(132);
				reqenable(2);
				setradius_221(0.300000012, 0.300000012);
				break;
			case 11:
				local_var_b0 = 2;
				local_var_bb = 3;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 44.4426689;
				local_var_b3[1] = 1.73496199;
				local_var_b3[2] = 49.2538261;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -0.98912102;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 44.4426689;
				local_var_b6[1] = 1.73496199;
				local_var_b6[2] = 49.2538261;
				local_var_b6[3] = -0.98912102;
				setradius_221(0.300000012, 0.300000012);
				break;
			case 12:
				local_var_b0 = 8;
				local_var_bb = 4;
				local_var_bc = 3;
				local_var_b9 = 0;
				local_var_ba = 4;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 35.8496246;
				local_var_b3[1] = 0;
				local_var_b3[2] = 45.3237419;
				local_var_b3[3] = 31.0544949;
				local_var_b3[4] = 4.72617102;
				local_var_b3[5] = 54.7996407;
				local_var_b3[6] = 37.2038536;
				local_var_b3[7] = 6.00000095;
				local_var_b3[8] = 58.9958649;
				local_var_b3[9] = 43.0377121;
				local_var_b3[10] = 2.13301992;
				local_var_b3[11] = 49.9997292;
				local_var_b4[0] = -0.877844989;
				local_var_b4[1] = 0.400088012;
				local_var_b4[2] = 1.51879704;
				local_var_b4[3] = -1.51385498;
				local_var_b6[0] = 35.8496246;
				local_var_b6[1] = 0;
				local_var_b6[2] = 45.3237419;
				local_var_b6[3] = -0.877844989;
				break;
			case 13:
				local_var_b0 = 9;
				local_var_bb = 4;
				local_var_bc = 2;
				local_var_b9 = 0;
				local_var_ba = 4;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 37.6665421;
				local_var_b3[1] = 0;
				local_var_b3[2] = 44.8305588;
				local_var_b3[3] = 30.1542053;
				local_var_b3[4] = 3.76821709;
				local_var_b3[5] = 53.1154938;
				local_var_b3[6] = 37.0273285;
				local_var_b3[7] = 6.00000095;
				local_var_b3[8] = 59.0455742;
				local_var_b3[9] = 43.0091553;
				local_var_b3[10] = 2.85460997;
				local_var_b3[11] = 51.2326965;
				local_var_b4[0] = -1.29644704;
				local_var_b4[1] = 0.185881004;
				local_var_b4[2] = 1.42474794;
				local_var_b4[3] = 0.369349003;
				local_var_b6[0] = 37.6665421;
				local_var_b6[1] = 0;
				local_var_b6[2] = 44.8305588;
				local_var_b6[3] = -1.29644704;
				break;
			case 14:
				local_var_b0 = 1;
				local_var_bb = 4;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 42.4558105;
				local_var_b3[1] = 0;
				local_var_b3[2] = 43.1461372;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -1.49046898;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 42.4558105;
				local_var_b6[1] = 0;
				local_var_b6[2] = 43.1461372;
				local_var_b6[3] = -1.49046898;
				break;
			case 15:
				return;
			case 16:
				local_var_b0 = 1;
				local_var_bb = 4;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 34.4231758;
				local_var_b3[1] = 0;
				local_var_b3[2] = 53.4531975;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = 2.93331003;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 34.4231758;
				local_var_b6[1] = 0;
				local_var_b6[2] = 53.4531975;
				local_var_b6[3] = 2.93331003;
				break;
			case 17:
				local_var_b0 = 2;
				local_var_bb = 2;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 38.8588753;
				local_var_b3[1] = 0;
				local_var_b3[2] = 54.7419434;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -2.87301111;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 38.8588753;
				local_var_b6[1] = 0;
				local_var_b6[2] = 54.7419434;
				local_var_b6[3] = -2.87301111;
				setnpcname(135);
				reqenable(2);
				break;
			case 18:
				if (g_btl_nm_ギルガメ == 1)
				{
					return;
				}
				sysReq(1, hitactor01_bansatoon::setHitObj);
				local_var_b0 = 8;
				local_var_bb = 0;
				local_var_bc = 1;
				local_var_b9 = 0;
				local_var_ba = 0;
				local_var_b8 = 0;
				local_var_b7 = 0;
				local_var_b3[0] = 42.3180351;
				local_var_b3[1] = 0;
				local_var_b3[2] = 44.4662285;
				local_var_b3[3] = 0;
				local_var_b3[4] = 0;
				local_var_b3[5] = 0;
				local_var_b3[6] = 0;
				local_var_b3[7] = 0;
				local_var_b3[8] = 0;
				local_var_b3[9] = 0;
				local_var_b3[10] = 0;
				local_var_b3[11] = 0;
				local_var_b4[0] = -2.12327504;
				local_var_b4[1] = 0;
				local_var_b4[2] = 0;
				local_var_b4[3] = 0;
				local_var_b6[0] = 42.3180351;
				local_var_b6[1] = 0;
				local_var_b6[2] = 44.4662285;
				local_var_b6[3] = -2.12327504;
				fieldsignmes(0x10000f1);
				reqenable(2);
				break;
			case 19:
				if (file_var_5c == 1)
				{
					local_var_b0 = 8;
					local_var_bb = 2;
					local_var_bc = 2;
					local_var_b9 = 0;
					local_var_ba = 0;
					local_var_b8 = 0;
					local_var_b7 = 0;
					local_var_b3[0] = 40.2045708;
					local_var_b3[1] = 0;
					local_var_b3[2] = 52.1358948;
					local_var_b3[3] = 0;
					local_var_b3[4] = 0;
					local_var_b3[5] = 0;
					local_var_b3[6] = 0;
					local_var_b3[7] = 0;
					local_var_b3[8] = 0;
					local_var_b3[9] = 0;
					local_var_b3[10] = 0;
					local_var_b3[11] = 0;
					local_var_b4[0] = -1.940943;
					local_var_b4[1] = 0;
					local_var_b4[2] = 0;
					local_var_b4[3] = 0;
					local_var_b6[0] = 40.2045708;
					local_var_b6[1] = 0;
					local_var_b6[2] = 52.1358948;
					local_var_b6[3] = -1.940943;
				}
				else
				{
					return;
				}
				break;
		}
		usemapid(0);
		setpos(local_var_b3[0], local_var_b3[1], local_var_b3[2]);
		dir(local_var_b4[0]);
		switch (local_var_b0)
		{
			case 1:
				bindp2_d4(0x3000000, local_var_bb, local_var_bc);
				break;
			case 2:
				bindp2_d4(0x3000001, local_var_bb, local_var_bc);
				break;
			case 3:
				bindp2_d4(0x3000004, local_var_bb, local_var_bc);
				break;
			case 7:
				bindp2_d4(0x3000002, local_var_bb, local_var_bc);
				break;
			case 8:
				bindp2_d4(0x3000003, local_var_bb, local_var_bc);
				break;
			case 9:
				bindp2_d4(0x3000005, local_var_bb, local_var_bc);
				break;
			case 10:
				bindp2_d4(0x3000009, local_var_bb, local_var_bc);
				break;
		}
		set_ignore_hitgroup(1);
		switch (local_var_ae)
		{
			case 0:
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				setradius_221(0.600000024, 0.600000024);
				break;
			case 7:
				setradius_221(0.600000024, 0.600000024);
				break;
			case 8:
				break;
			case 9:
				break;
			case 10:
				setradius_221(0.400000006, 0.400000006);
				break;
			case 11:
				setradius_221(0.400000006, 0.400000006);
				break;
			case 12:
				setradius_221(0.5, 0.5);
				break;
			case 13:
				setradius_221(0.400000006, 0.400000006);
				break;
			case 14:
				break;
			case 15:
				setradius_221(0.600000024, 0.600000024);
				break;
			case 16:
				break;
			case 17:
				break;
		}
		if ((local_var_ae == 6 || local_var_ae == 7))
		{
			stdmotionread(1);
			stdmotionreadsync();
		}
		else if (local_var_ae == 10)
		{
			stdmotionread(18);
			stdmotionreadsync();
		}
		else
		{
			stdmotionread(16);
			stdmotionreadsync();
		}
		stdmotionplay(0x1000000);
		local_var_b5 = getdefaultwalkspeed();
		setreachr(0);
		usecharhit(1);
		setweight(-1);
		//Couldn't get labels for REQ because either script or function is not immediate
		sysReq(1, getmyid(), 6);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_af;            // pos: 0x4;

	function NPC挙動()
	{
		switch (local_var_ae)
		{
			case 0:
				if (g_btl_nm_トリックスター == 1)
				{
					return;
				}
				stdmotionread(18);
				stdmotionreadsync();
				stdmotionvariation(1);
				stdmotionplay_2c2(0x1000000, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				sysLookata(NPC02);
				setkutipakustatus(1);
				setunazukistatus(1);
				break;
			case 1:
				if (((g_btl_nm_トリックスター == 1 || battle_global_flag[3] == 1) || file_var_5d == 1))
				{
					stdmotionread(18);
					stdmotionreadsync();
					stdmotionplay_2c2(0x1000000, 0);
					rgbatrans(1, 1, 1, 0, 0);
					istouchucsync();
					clearhidecomplete();
					rgbatrans(1, 1, 1, 1, 10);
					sysLookata(-1);
				}
				else
				{
					rgbatrans(1, 1, 1, 0, 0);
					istouchucsync();
					clearhidecomplete();
					rgbatrans(1, 1, 1, 1, 10);
					sysLookata(NPC01);
					setkutipakustatus(1);
					setunazukistatus(1);
					while (true)
					{
						stdmotionplay(0x1000013);
						wait(1);
						motionsync_282(1);
						stdmotionplay(0x1000011);
						wait(1);
						motionsync_282(1);
						wait(1);
					}
				}
				break;
			case 2:
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x11000008, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				return;
			case 3:
				setkutipakustatus(1);
				setunazukistatus(1);
				stdmotionread(18);
				stdmotionreadsync();
				stdmotionvariation(1);
				stdmotionplay_2c2(0x1000000, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				while (true)
				{
					sysLookata(NPC05);
					wait((60 + rand_29(30)));
					sysLookata(NPC05);
					wait((60 + rand_29(30)));
				}
			case 4:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					switch (rand_29(2))
					{
						case 0:
							stdmotionplay(0x1000013);
							break;
						default:
							stdmotionplay(0x1000016);
							break;
					}
					switch ((rand_29(4) % 2))
					{
						case 0:
							sysLookata(NPC04);
							break;
						default:
							sysLookata(NPC04);
							break;
					}
					wait(1);
					motionsync_282(1);
				}
			case 5:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					switch (rand_29(2))
					{
						case 0:
							stdmotionplay(0x1000013);
							break;
						default:
							motionplay_bb(0x11000002, 20);
							break;
					}
					switch ((rand_29(8) % 2))
					{
						case 0:
							sysLookata(NPC04);
							break;
						default:
							sysLookata(NPC05);
							break;
					}
					wait(1);
					motionsync_282(1);
				}
			case 6:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				sysLookata(NPC08);
				return;
			case 7:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				sysLookata(NPC07);
				return;
			case 8:
				setkutipakustatus(1);
				setunazukistatus(1);
				stdmotionread(18);
				stdmotionreadsync();
				stdmotionplay_2c2(0x1000000, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				while (true)
				{
					sysLookata(NPC07);
					wait((60 + rand_29(30)));
					sysLookata(NPC08);
					wait((60 + rand_29(30)));
				}
			case 9:
				setkutipakustatus(1);
				setunazukistatus(1);
				setautorelax(0);
				motionstartframe(0);
				motionloop(1);
				motionloopframe(0, -1);
				motionplay_bb(0x11000004, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				while (true)
				{
					sysLookata(NPC07);
					wait((60 + rand_29(30)));
					sysLookata(NPC08);
					wait((60 + rand_29(30)));
				}
			case 10:
				setkutipakustatus(1);
				setunazukistatus(1);
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x11000005, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				lookat_24f(43.8900871, 2.93495607, 48.9596939);
				return;
			case 11:
				setkutipakustatus(1);
				setunazukistatus(1);
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x11000006, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				lookat_24f(44.2299538, 2.18994904, 50.0750542);
				return;
			case 12:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				usemapid(1);
				sysLookata(NPC14);
				setwalkspeed(getdefaultrunspeed());
				while (true)
				{
					move(32.846611, 0.371549994, 47.5669746);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(32.009079, 1.31886697, 48.7782631);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(31.7664356, 3.07232809, 51.567688);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(33.5802956, 5.71943903, 55.0768967);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(35.6351967, 6.00000095, 58.1161499);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(39.304348, 6.00000095, 58.2717857);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(42.7433319, 4.78263712, 54.8317108);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(41.8812904, 3.16722989, 51.6773987);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(40.784729, 0.130546004, 47.2882996);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(39.0447807, 0, 46.0393295);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
					move(35.8496246, 0, 45.3237419);
					while (getlength2(getx_12b(22), getz_12d(22)) >= 2)
					{
						wait(1);
					}
				}
			case 13:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				sysLookata(NPC13);
				usemapid(1);
				setwalkspeed(getdefaultrunspeed());
				setreachr(1);
				while (true)
				{
					sysRmovea(NPC13);
					wait(10);
				}
			case 14:
				setautorelax(0);
				motionstartframe(150);
				motionloopframe(150, 210);
				motionloop(1);
				motionplay_bb(0x11000009, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkubifuristatus(1);
				return;
			case 15:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkutipakustatus(1);
				setunazukistatus(1);
				sysLookata(NPC15);
				while (true)
				{
					stdmotionplay(0x1000010);
					wait(1);
					motionsync_282(1);
					stdmotionplay(0x1000012);
					wait(1);
					motionsync_282(1);
					wait(1);
				}
			case 16:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					switch (local_var_af)
					{
						case 0:
							stdmotionplay(0x1000012);
							local_var_af = (local_var_af + 1);
							break;
						default:
							stdmotionplay(0x1000011);
							local_var_af = 0;
							break;
					}
					switch ((rand_29(8) % 2))
					{
						case 0:
							sysLookata(NPC07);
							break;
						default:
							sysLookata(NPC08);
							break;
					}
					wait(1);
					wait(1);
					motionsync_282(1);
				}
			case 17:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					switch (local_var_af)
					{
						case 0:
							motionplay_bb(0x11000007, 20);
							local_var_af = (local_var_af + 1);
							break;
						default:
							stdmotionplay(0x1000011);
							local_var_af = 0;
							break;
					}
					switch ((rand_29(16) % 2))
					{
						case 0:
							sysLookata(NPC07);
							break;
						default:
							sysLookata(NPC08);
							break;
					}
					wait(1);
					wait(1);
					motionsync_282(1);
				}
			case 18:
				if (g_btl_nm_ギルガメ == 1)
				{
					return;
				}
				lookatoff();
				stdmotionread(18);
				stdmotionreadsync();
				stdmotionplay_2c2(0x1000000, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				setkubifuristatus(1);
				break;
			case 19:
				if (file_var_5c == 1)
				{
					stdmotionread(18);
					stdmotionreadsync();
					stdmotionplay_2c2(0x1000000, 0);
					rgbatrans(1, 1, 1, 0, 0);
					istouchucsync();
					clearhidecomplete();
					rgbatrans(1, 1, 1, 1, 10);
				}
				break;
		}
		return;
	}


	function op_cut01()
	{
		switch (local_var_ae)
		{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 14:
			case 15:
			case 18:
			case 12:
			case 13:
				hidecomplete();
				break;
		}
		return;
	}


	function op_cut02()
	{
		switch (local_var_ae)
		{
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 14:
			case 15:
			case 18:
				hidecomplete();
				break;
			case 8:
			case 9:
				wait(15);
				hidecomplete();
				break;
		}
		return;
	}


	function op_cut03()
	{
		switch (local_var_ae)
		{
			case 6:
			case 7:
			case 8:
			case 9:
				hidecomplete();
				break;
			case 3:
			case 4:
			case 5:
				clearhidecomplete();
				break;
		}
		return;
	}


	function op_cut04()
	{
		switch (local_var_ae)
		{
			case 0:
			case 1:
			case 2:
			case 6:
			case 7:
			case 8:
			case 9:
			case 14:
			case 15:
			case 17:
			case 18:
				hidecomplete();
				break;
		}
		return;
	}


	function camclear()
	{
		clearhidecomplete();
		return;
	}


	function catrine_talk()
	{
		reqdisable(17);
		reqdisable(16);
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		settalknpcname(159);
		sysReq(1, 常駐監督::カトリーヌ台詞);
		return;
	}


	function バインドオフ()
	{
		bindoff();
		return 0;
	}


	function NPC_神竜後バインドオフ()
	{
		bindoff();
		return 0;
	}


	function 怪しいモーニ準備()
	{
		switch (local_var_ae)
		{
			case 0:
				stdmotionread(16);
				stdmotionreadsync();
				stdmotionplay_2c2(0x1000000, 0);
				setpos(36.3733978, 0, 40.4523811);
				dir(-0.0123629998);
				sysLookata(-1);
				break;
			case 6:
				setpos(37.8193588, 0, 49.4942551);
				dir_4e(37.4587631, 0, 38.7909088);
				break;
			case 7:
				setpos(35.1949692, 0, 49.7904701);
				dir_4e(37.4587631, 0, 38.7909088);
				break;
			case 12:
				setpos(41.1000824, 0.586628973, 47.973732);
				dir(3.12552691);
				lookat_24f(36.4840317, 1.5, 40.3450661);
				break;
			case 13:
				setpos(42.6849976, 0.840516984, 47.6242485);
				dir(3.12552691);
				lookat_24f(36.4840317, 1.5, 40.3450661);
				break;
		}
		return 0;
	}


	function 怪しいモーニcut01()
	{
		stdmotionplay(0x1000016);
		return;
	}


	function 怪しいモーニcut02()
	{
		usemapid(0);
		aturn(36.5541649, 0, 31.2633362);
		setwalkspeed(getdefaultwalkspeed());
		rmove(36.5541649, 0, 31.2633362);
		wait(90);
		sebsoundplay(1, 2);
		bindoff();
		return 0;
	}


	function 怪しいモーニcut03_02()
	{
		switch (local_var_ae)
		{
			case 6:
				sysLookata(NPC08);
				wait(60);
				lookatoff();
				break;
			case 7:
				sysLookata(NPC07);
				wait(60);
				lookatoff();
				break;
		}
		return;
	}


	function 怪しいモーニ終了()
	{
		bindoff();
		return 0;
	}


	function 停止()
	{
		stdmotionplay_2c2(0x1000000, 0);
		return 0;
		return;
	}
}


script モンブラン２(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		usemapid(0);
		setpos(37.0395699, 6.92498112, 55.0979996);
		dir(1.53457057);
		bindp2_d4(0x3000006, 1, 0);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut01()
	{
		return;
	}


	function cut02()
	{
		setpos(37.4627724, 6.00000095, 56.5482216);
		dir(0.844810009);
		fetchambient_4e1(38.5545883, 6.00000095, 57.6578484);
		setwalkspeed(getdefaultwalkspeed());
		move(38.5545883, 6.00000095, 57.6578484);
		sysAturna(ヴァン);
		return;
	}


	function cut03()
	{
		return;
	}


	function cut04()
	{
		return;
	}


	function cut05()
	{
		return;
	}


	function event_end()
	{
		resetambient();
		bindoff();
		return;
	}


	function cut0202()
	{
		stdmotionplay(0x1000015);
		return;
	}


	function cut0502()
	{
		stdmotionplay(0x1000010);
		return;
	}


	function cut06()
	{
		stdmotionplay(0x1000010);
		return;
	}
}


script ノノ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		setpos(43.5874252, 2.95026708, 51.4372025);
		dir(-0.615283012);
		bindp2_d4(0x300000a, 1, 0);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut01()
	{
		setwalkspeed(getdefaultwalkspeed());
		move(39.9507904, 6.00000095, 56.8928299);
		return;
	}


	function cut02()
	{
		movecancel();
		stdmotionplay_2c2(0x1000000, 0);
		setpos(39.4302559, 6.00000095, 57.7693443);
		dir(-0.753306985);
		return 0;
	}


	function cut03()
	{
		stdmotionplay_2c2(0x1000016, 5);
		return;
	}


	function cut04()
	{
		return;
	}


	function cut05()
	{
		return;
	}


	function cut06()
	{
		stdmotionplay_2c2(0x1000010, 5);
		wait(1);
		motionsync_282(1);
		return;
	}


	function event_end()
	{
		bindoff();
		modeldispose(0x300000a);
		stdmotiondispose();
		return 0;
		return;
	}
}


script ソルベ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		setpos(43.661747, 2.52677011, 50.6589813);
		dir(-0.615283012);
		bindp2_d4(0x300000b, 1, 0);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut01()
	{
		setwalkspeed(getdefaultwalkspeed());
		move(40.5086441, 5.96431494, 56.051693);
		return;
	}


	function cut02()
	{
		movecancel();
		stdmotionplay_2c2(0x1000000, 0);
		setpos(40.4824295, 6.00000095, 58.2140465);
		dir(-1.14345002);
		return 0;
	}


	function cut03()
	{
		return;
	}


	function cut0302()
	{
		motionplay(0x12000000);
		return;
	}


	function cut04()
	{
		return;
	}


	function cut05()
	{
		return;
	}


	function cut06()
	{
		motionplay(0x12000001);
		return;
	}


	function event_end()
	{
		bindoff();
		modeldispose(0x300000b);
		stdmotiondispose();
		return;
	}
}


script ホルン(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		setpos(43.8133736, 2.13777804, 49.8626022);
		dir(-0.615283012);
		bindp2_d4(0x300000c, 1, 0);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut01()
	{
		setwalkspeed(getdefaultwalkspeed());
		move(41.0500603, 5.50742483, 55.209919);
		return;
	}


	function cut02()
	{
		movecancel();
		stdmotionplay_2c2(0x1000000, 0);
		setpos(40.3529625, 6.00000095, 57.1590805);
		dir(-0.945932984);
		return 0;
	}


	function cut03()
	{
		return;
	}


	function cut04()
	{
		return;
	}


	function cut0402()
	{
		motionplay(0x12000001);
		return;
	}


	function cut05()
	{
		return;
	}


	function cut06()
	{
		motionplay(0x12000000);
		return;
	}


	function event_end()
	{
		bindoff();
		modeldispose(0x300000c);
		stdmotiondispose();
		return;
	}
}


script ハーディ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		setpos(43.7594452, 1.80639899, 49.1637688);
		dir(-0.615283012);
		bindp2_d4(0x300000d, 3, 2);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut01()
	{
		setwalkspeed(getdefaultwalkspeed());
		wait(10);
		move(41.5730247, 4.89809799, 54.3439331);
		return;
	}


	function cut02()
	{
		movecancel();
		stdmotionplay_2c2(0x1000000, 0);
		setpos(39.3715134, 6.00000095, 56.0783272);
		dir(-0.695441008);
		move(38.9502487, 6.00000095, 56.6294823);
		setaturnlookatlockstatus(1);
		aturn_261(-0.406969994);
		return 0;
	}


	function cut03()
	{
		return;
	}


	function cut04()
	{
		motionplay(0x12000000);
		return;
	}


	function cut05()
	{
		return;
	}


	function cut06()
	{
		motionplay(0x12000000);
		return;
	}


	function event_end()
	{
		bindoff();
		modeldispose(0x300000d);
		stdmotiondispose();
		return;
	}
}


script ガーディ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		setpos(43.6890488, 1.53744102, 48.5924721);
		dir(-0.615283012);
		bindp2_d4(0x300000e, 1, 0);
		stdmotionread(17);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut01()
	{
		setwalkspeed(getdefaultwalkspeed());
		wait(15);
		move(42.0381355, 4.23141193, 53.4383507);
		return;
	}


	function cut02()
	{
		movecancel();
		usemapid(0);
		stdmotionplay_2c2(0x1000000, 0);
		setpos(40.447998, 5.54999924, 55.1151123);
		dir(-0.604225993);
		fetchambient_4e1(39.7021866, 5.54999924, 56.4565849);
		move(39.7021866, 5.54999924, 56.4565849);
		sysLookata(ヴァン);
		return 0;
	}


	function cut03()
	{
		return;
	}


	function cut04()
	{
		return;
	}


	function cut05()
	{
		motionplay_bb(0x12000002, 8);
		return;
	}


	function cut06()
	{
		motionplay_bb(0x12000002, 8);
		return;
	}


	function event_end()
	{
		resetambient();
		bindoff();
		modeldispose(0x300000e);
		stdmotiondispose();
		return;
	}
}


script ヴァン(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function cut00()
	{
		setpos(37.8630791, 6.00000095, 59.0439034);
		dir(2.165025);
		bindp2(0x300000f);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		usecharhit(0);
		setautorelax(1);
		return;
	}


	function cut0202()
	{
		wait(5);
		sysLookata(モンブラン２);
		return;
	}


	function cut03()
	{
		wait(5);
		sysLookata(ノノ);
		return;
	}


	function cut0302()
	{
		wait(5);
		sysLookata(ソルベ);
		return;
	}


	function cut04()
	{
		wait(5);
		sysLookata(ハーディ);
		return;
	}


	function cut0402()
	{
		wait(5);
		sysLookata(ホルン);
		return;
	}


	function cut05()
	{
		wait(5);
		sysLookata(ガーディ);
		return;
	}


	function cut0502()
	{
		wait(5);
		sysLookata(モンブラン２);
		return;
	}


	function cut06()
	{
		return;
	}


	function event_end()
	{
		bindoff();
		return 0;
		return;
	}
}


script パシリ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function 怪しいモーニ準備()
	{
		usemapid(0);
		setpos(38.1045456, 0, 37.0867157);
		dir(0.0773599967);
		bindp2_d4(0x3000002, 4, 1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		setautorelax(1);
		hide();
		return;
	}


	function 怪しいモーニ終了()
	{
		bindoff();
		return 0;
	}


	function 怪しいモーニcut02()
	{
		wait(45);
		show();
		sysRlookata(NPC01);
		setwalkspeed(getdefaultwalkspeed());
		move(37.9450836, 0, 39.9615364);
		sysAturna(NPC01);
		return;
	}


	function 怪しいモーニcut02_02()
	{
		wait(10);
		sysAturna(モンブラン);
		return;
	}


	function 怪しいモーニcut03()
	{
		sysRlookata(モンブラン);
		stdmotionplay(0x1000016);
		return;
	}


	function 怪しいモーニcut04()
	{
		stdmotionplay(0x1000012);
		return;
	}


	function 怪しいモーニcut05()
	{
		aturn(38.0788498, 0, 31.2914925);
		setwalkspeed(getdefaultwalkspeed());
		rmove(38.0788498, 0, 31.2914906);
		wait(60);
		sebsoundplay(1, 1);
		wait(45);
		sebsoundplay(1, 2);
		wait(45);
		return;
	}
}


script hitactor01(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(32.2985268, 6.00000095, 62.7923508);
		dir(3.02263498);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.89999998, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(32.2985268, 6.00000095, 62.7923508);
		dir(3.02263498);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.89999998, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor02(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(45.4102325, 1.73496199, 49.9102402);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(45.4102325, 1.73496199, 49.9102402);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor03(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(43.2822533, 0, 43.1364059);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(43.2822533, 0, 43.1364059);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor04(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(33.6067009, 0, 53.6376152);
		dir(2.18472505);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(33.6067009, 0, 53.6376152);
		dir(2.18472505);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor01_monion(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(31.7454109, 0, 41.6921082);
		dir(2.18472505);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(31.7454109, 0, 41.6921082);
		dir(2.18472505);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor01_bansatoon(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(42.9107857, 0, 44.148304);
		dir(2.18472505);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(42.9107857, 0, 44.148304);
		dir(2.18472505);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script map_ダミーＰＣ(6)
{

	function init()
	{
		return;
	}


	function 扉オープン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function アイテム１()
	{
		capturepc(-1);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(0);
		motionsync_282(1);
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック1()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ターン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ギミックアタックxyz()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１xyz()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２xyz()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタックxyz1()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタックxyz2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function アイテム１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ターンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function map_behind_return()
	{
		capturepc(-1);
		setnoupdatebehindcamera(0);
		releasepc();
		return;
	}


	function ＳＥＴＰＯＳxyz()
	{
		capturepc(-1);
		setpos(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		releasepc();
		return;
	}


	function ＳＥＴＴＵＲＮxyz()
	{
		capturepc(-1);
		turnt(mp_map_set_x, mp_map_set_y, mp_map_set_z, 0);
		releasepc();
		return;
	}
}

#include "generous.c"

script PC00(7) : 0x80
{

	function init()
	{
		setupbattle(0);
		return;
	}
}


script PC01(7) : 0x81
{

	function init()
	{
		setupbattle(1);
		return;
	}
}


script PC02(7) : 0x82
{

	function init()
	{
		setupbattle(2);
		return;
	}
}


script PC03(7) : 0x83
{

	function init()
	{
		setupbattle(3);
		return;
	}
}

//======================================================================
//                           Map exit arrays                            
//======================================================================

mapExitArray mapExitGroup0[0] = {

};

mapExitArray mapExitGroup1[0] = {

};

mapExitArray mapExitGroup2[0] = {

};

mapExitArray mapExitGroup3[0] = {

};

mapExitArray mapExitGroup4[0] = {

};


//======================================================================
//                      Map jump position vectors                       
//======================================================================
mjPosArr1 mapJumpPositions1[1] = {

	mjPos mapJumpPos0 = {
		37, 2.54700012e-07, 40, 0,
		0, 0, 0, 0
	};

};
mjPosArr2 mapJumpPositions2[1] = {

	mjPos mapJumpPos0 = {
		37, 2.54700012e-07, 40, 0,
		0, 0, 0, 0
	};

};


//======================================================================
//                       Unknown position arrays                        
//======================================================================
unkPos1 unknownPosition0[7] = {37, 2.54700012e-07, 35, 3.04166675, 3.96616364, 0, 0};
unkPos1 unknownPosition1[7] = {37, 0, 35, 1, 0.649999976, 0, 0};

unkPos2 unknownPosition0[7] = {37, 1.20000005, 35, 1, 1, 0, 0};


//======================================================================
//                          Unknown u16 Arrays                          
//======================================================================
unk16Arr1 unknown16Arrays1[1] = {
	unk16ArrEntry unknown16Array0 = {0, 1, 0, 0, 1, 0, 0, 0};
};
unk16Arr2 unknown16Arrays2[21] = {
	unk16ArrEntry unknown16Array0 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array1 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array2 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array3 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array4 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array5 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array6 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array7 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array8 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array9 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array10 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array11 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array12 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array13 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array14 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array15 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array16 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array17 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array18 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array19 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array20 = {0, 1, 0, 0, 1, 65535, 0, 0};
};
unk16Arr3 unknown16Arrays3[6] = {
	unk16ArrEntry unknown16Array0 = {0, 768, 1, 1};
	unk16ArrEntry unknown16Array1 = {1, 768, 1, 1};
	unk16ArrEntry unknown16Array2 = {2, 768, 1, 1};
	unk16ArrEntry unknown16Array3 = {3, 768, 1, 1};
	unk16ArrEntry unknown16Array4 = {4, 768, 1, 1};
	unk16ArrEntry unknown16Array5 = {5, 768, 1, 1};
};

//======================================================================
//                       REQALL/REQWAITALL arrays                       
//======================================================================
reqa   reqArr0[] = {
	配置監督::NPC_神竜後バインドオフ,
	NPC02::NPC_神竜後バインドオフ,
	NPC03::NPC_神竜後バインドオフ,
	NPC04::NPC_神竜後バインドオフ,
	NPC05::NPC_神竜後バインドオフ,
	NPC06::NPC_神竜後バインドオフ,
	NPC07::NPC_神竜後バインドオフ,
	NPC08::NPC_神竜後バインドオフ,
	NPC09::NPC_神竜後バインドオフ,
	NPC10::NPC_神竜後バインドオフ,
	NPC11::NPC_神竜後バインドオフ,
	NPC12::NPC_神竜後バインドオフ,
	NPC13::NPC_神竜後バインドオフ,
	NPC14::NPC_神竜後バインドオフ,
	NPC15::NPC_神竜後バインドオフ,
	NPC16::NPC_神竜後バインドオフ,
	NPC17::NPC_神竜後バインドオフ,
	NPC18::NPC_神竜後バインドオフ,
	NPC19::NPC_神竜後バインドオフ,
	NPC20::NPC_神竜後バインドオフ,
	マッケンロウ::NPC_神竜後バインドオフ,
	カロリーヌ::NPC_神竜後バインドオフ,
	NPC01::NPC_神竜後バインドオフ
};

reqa   reqArr1[] = {
	NPC02::NPC配置,
	NPC03::NPC配置,
	NPC04::NPC配置,
	NPC05::NPC配置,
	NPC06::NPC配置,
	NPC07::NPC配置,
	NPC08::NPC配置,
	NPC09::NPC配置,
	NPC10::NPC配置,
	NPC11::NPC配置,
	NPC12::NPC配置,
	NPC13::NPC配置,
	NPC14::NPC配置,
	NPC15::NPC配置,
	NPC16::NPC配置,
	NPC17::NPC配置,
	NPC18::NPC配置,
	NPC19::NPC配置,
	NPC20::NPC配置,
	マッケンロウ::NPC配置,
	カロリーヌ::NPC配置,
	NPC01::NPC配置
};

reqa   reqArr2[] = {
	NPC02::NPC挙動,
	NPC03::NPC挙動,
	NPC04::NPC挙動,
	NPC05::NPC挙動,
	NPC06::NPC挙動,
	NPC07::NPC挙動,
	NPC08::NPC挙動,
	NPC09::NPC挙動,
	NPC10::NPC挙動,
	NPC11::NPC挙動,
	NPC12::NPC挙動,
	NPC13::NPC挙動,
	NPC14::NPC挙動,
	NPC15::NPC挙動,
	NPC16::NPC挙動,
	NPC17::NPC挙動,
	NPC18::NPC挙動,
	NPC19::NPC挙動,
	NPC20::NPC挙動,
	カロリーヌ::NPC挙動,
	NPC01::NPC挙動
};

reqa   reqArr3[] = {
	NPC02::怪しいモーニ準備,
	NPC03::怪しいモーニ準備,
	NPC04::怪しいモーニ準備,
	NPC05::怪しいモーニ準備,
	NPC06::怪しいモーニ準備,
	NPC07::怪しいモーニ準備,
	NPC08::怪しいモーニ準備,
	NPC09::怪しいモーニ準備,
	NPC10::怪しいモーニ準備,
	NPC11::怪しいモーニ準備,
	NPC12::怪しいモーニ準備,
	NPC13::怪しいモーニ準備,
	NPC14::怪しいモーニ準備,
	NPC15::怪しいモーニ準備,
	NPC16::怪しいモーニ準備,
	NPC17::怪しいモーニ準備,
	NPC18::怪しいモーニ準備,
	NPC19::怪しいモーニ準備,
	NPC20::怪しいモーニ準備,
	NPC01::怪しいモーニ準備,
	パシリ::怪しいモーニ準備
};

reqa   reqArr4[] = {
	モンブラン::NPC配置_mon
};

reqa   reqArr5[] = {
	カメラ::op_cut01,
	NPC02::op_cut01,
	NPC03::op_cut01,
	NPC04::op_cut01,
	NPC05::op_cut01,
	NPC06::op_cut01,
	NPC07::op_cut01,
	NPC08::op_cut01,
	NPC09::op_cut01,
	NPC10::op_cut01,
	NPC11::op_cut01,
	NPC12::op_cut01,
	NPC13::op_cut01,
	NPC14::op_cut01,
	NPC15::op_cut01,
	NPC16::op_cut01,
	NPC17::op_cut01,
	NPC18::op_cut01,
	NPC19::op_cut01,
	NPC20::op_cut01,
	モンブラン::op_cut01,
	マッケンロウ::op_cut01,
	カロリーヌ::op_cut01,
	NPC01::op_cut01
};

reqa   reqArr6[] = {
	カメラ２::op_cut02,
	NPC02::op_cut02,
	NPC03::op_cut02,
	NPC04::op_cut02,
	NPC05::op_cut02,
	NPC06::op_cut02,
	NPC07::op_cut02,
	NPC08::op_cut02,
	NPC09::op_cut02,
	NPC10::op_cut02,
	NPC11::op_cut02,
	NPC12::op_cut02,
	NPC13::op_cut02,
	NPC14::op_cut02,
	NPC15::op_cut02,
	NPC16::op_cut02,
	NPC17::op_cut02,
	NPC18::op_cut02,
	NPC19::op_cut02,
	NPC20::op_cut02,
	NPC01::op_cut02
};

reqa   reqArr7[] = {
	カメラ::op_cut03,
	NPC02::op_cut03,
	NPC03::op_cut03,
	NPC04::op_cut03,
	NPC05::op_cut03,
	NPC06::op_cut03,
	NPC07::op_cut03,
	NPC08::op_cut03,
	NPC09::op_cut03,
	NPC10::op_cut03,
	NPC11::op_cut03,
	NPC12::op_cut03,
	NPC13::op_cut03,
	NPC14::op_cut03,
	NPC15::op_cut03,
	NPC16::op_cut03,
	NPC17::op_cut03,
	NPC18::op_cut03,
	NPC19::op_cut03,
	NPC20::op_cut03,
	モンブラン::op_cut03,
	マッケンロウ::op_cut03,
	カロリーヌ::op_cut03,
	NPC01::op_cut03
};

reqa   reqArr8[] = {
	カメラ２::op_cut04,
	NPC02::op_cut04,
	NPC03::op_cut04,
	NPC04::op_cut04,
	NPC05::op_cut04,
	NPC06::op_cut04,
	NPC07::op_cut04,
	NPC08::op_cut04,
	NPC09::op_cut04,
	NPC10::op_cut04,
	NPC11::op_cut04,
	NPC12::op_cut04,
	NPC13::op_cut04,
	NPC14::op_cut04,
	NPC15::op_cut04,
	NPC16::op_cut04,
	NPC17::op_cut04,
	NPC18::op_cut04,
	NPC19::op_cut04,
	NPC20::op_cut04,
	NPC01::op_cut04
};

reqa   reqArr9[] = {
	カメラ::camclear,
	NPC02::camclear,
	NPC03::camclear,
	NPC04::camclear,
	NPC05::camclear,
	NPC06::camclear,
	NPC07::camclear,
	NPC08::camclear,
	NPC09::camclear,
	NPC10::camclear,
	NPC11::camclear,
	NPC12::camclear,
	NPC13::camclear,
	NPC14::camclear,
	NPC15::camclear,
	NPC16::camclear,
	NPC17::camclear,
	NPC18::camclear,
	NPC19::camclear,
	NPC20::camclear,
	モンブラン::camclear,
	マッケンロウ::camclear,
	カロリーヌ::camclear,
	NPC01::camclear
};

reqa  reqArr10[] = {
	モンブラン２::cut00,
	ノノ::cut00,
	ソルベ::cut00,
	ホルン::cut00,
	ハーディ::cut00,
	ガーディ::cut00,
	ヴァン::cut00
};

reqa  reqArr11[] = {
	台詞::cut01,
	カメラ::cut01,
	モンブラン２::cut01,
	ノノ::cut01,
	ソルベ::cut01,
	ホルン::cut01,
	ハーディ::cut01,
	ガーディ::cut01
};

reqa  reqArr12[] = {
	カメラ::cut0102
};

reqa  reqArr13[] = {
	台詞::cut02,
	カメラ::cut02,
	モンブラン２::cut02,
	ノノ::cut02,
	ソルベ::cut02,
	ホルン::cut02,
	ハーディ::cut02,
	ガーディ::cut02
};

reqa  reqArr14[] = {
	台詞::cut0202,
	カメラ::cut0202,
	モンブラン２::cut0202,
	ヴァン::cut0202
};

reqa  reqArr15[] = {
	台詞::cut03,
	モンブラン２::cut03,
	ノノ::cut03,
	ソルベ::cut03,
	ホルン::cut03,
	ハーディ::cut03,
	ガーディ::cut03,
	ヴァン::cut03
};

reqa  reqArr16[] = {
	台詞::cut0302,
	ソルベ::cut0302,
	ヴァン::cut0302
};

reqa  reqArr17[] = {
	台詞::cut04,
	モンブラン２::cut04,
	ノノ::cut04,
	ソルベ::cut04,
	ホルン::cut04,
	ハーディ::cut04,
	ガーディ::cut04,
	ヴァン::cut04
};

reqa  reqArr18[] = {
	台詞::cut0402,
	ホルン::cut0402,
	ヴァン::cut0402
};

reqa  reqArr19[] = {
	台詞::cut05,
	モンブラン２::cut05,
	ノノ::cut05,
	ソルベ::cut05,
	ホルン::cut05,
	ハーディ::cut05,
	ガーディ::cut05,
	ヴァン::cut05
};

reqa  reqArr20[] = {
	台詞::cut0502,
	モンブラン２::cut0502,
	ヴァン::cut0502
};

reqa  reqArr21[] = {
	台詞::cut06,
	モンブラン２::cut06,
	ノノ::cut06,
	ソルベ::cut06,
	ホルン::cut06,
	ハーディ::cut06,
	ガーディ::cut06,
	ヴァン::cut06
};

reqa  reqArr22[] = {
	台詞::event_end,
	カメラ::event_end,
	モンブラン２::event_end,
	ノノ::event_end,
	ソルベ::event_end,
	ホルン::event_end,
	ハーディ::event_end,
	ガーディ::event_end,
	ヴァン::event_end
};


