//モンブラン
//ffg's edit

//some needed globals
import global	u_char	globU8[8192]	= 0x0;
import global	u_short	globU16[4096]	= 0x0;
import global	int		globU32[2048]	= 0x0;

script help_talk(6)
{

	function init()
	{
		return;
	}

	//some needed local variables, though I could just go with the available registers
	u_char  retval;
	u_char	doLoop;

	function message_loop()
	{
		モンブラン.setkutipakustatus(1);
		モンブラン.setunazukistatus(1);
		
		doLoop = true;
		while (doLoop)
		{
			//ask whether he needs something
			sysReqew(1, help_talk::ask_help);

			switch (ucMBChoiceHelp)
			{
				case 0:
					sysReqew(1, help_talk::handle_money);
					break;
				case 1:
					sysReqew(1, help_talk::handle_items);
					break;
				case 2:
					sysReqew(1, help_talk::handle_ngplus);
					break;
				default:
					doLoop = false;
					continue;
			}
			if (retval) sysReqew(1, help_talk::confirm_message);
		}

		モンブラン.setkutipakustatus(0);
		モンブラン.setunazukistatus(0);
		wait(10);
		return;
	}

	u_char  ucMBChoiceHelp;
	
	function ask_help()
	{
#ifndef MOD_LITE
		askpos(0, 0, 3);
#else
		setaskselectignore(0, 0);
		setaskselectignore(0, 1);
		askpos(0, 0, 1);
#endif
		ucMBChoiceHelp = aaske(0, 0x1000000 | 254);
		mesclose(0);
		messync(0, 1);
		return;
	}

	u_char  ucMBChoiceGil;
	function handle_money()
	{
		askpos(0, 0, 3);
		ucMBChoiceGil = aaske(0, 0x1000000 | 255);
		mesclose(0);
		messync(0, 1);
		
		switch (ucMBChoiceGil)
		{
			case 0:
				addgill(1000);
				break;
			case 1:
				addgill(10000);
				break;
			case 2:
				addgill(1000000);
				break;
			default:
				retval = false;
				return;
		}
		retval = true;
		return;
	}

	u_char  ucMBChoiceIt;
	
	function handle_items()
	{
		askpos(0, 0, 7);
		ucMBChoiceIt = aaske(0, 0x1000000 | 256);
		mesclose(0);
		messync(0, 1);
		
		switch (ucMBChoiceIt)
		{
			case 0:
				btlAtelGetAllItems();
				break;
			case 1:
				btlAtelGetAllEquips();
				break;
			case 2:
				btlAtelGetAllMagics();
				break;
			case 3:
				btlAtelGetAllTechs();
				break;
			case 4:
				btlAtelGetAllTgtChips(); //gambits
				break;
			case 5:
				btlAtelGetAllMistCarts(); //espers
				break;
			case 6:
				btlAtelGetAllPrecious(); //key items
				break;
			default:
				retval = false;
				return;
		}
	
		retval = true;
		return;
	}

	u_char  ucMBChoiceNgPlus;
	u_char  ucMBResetHunt;
	u_char  ucMBRemoveParty;

	function handle_ngplus()
	{
		//ask if he is sure
		askpos(0, 0, 1);
		ucMBChoiceNgPlus = aaske(0, 0x1000000 | 258);
		mesclose(0);
		messync(0, 1);
		
		if (ucMBChoiceNgPlus != 0)
		{//cancel
			retval = false;
			return;
		}

		//ask about hunts
		askpos(0, 0, 1);
		ucMBResetHunt = aaske(0, 0x1000000 | 259);
		mesclose(0);
		messync(0, 1);

		//ask about party
		askpos(0, 0, 1);
		ucMBRemoveParty = aaske(0, 0x1000000 | 260);
		mesclose(0);
		messync(0, 1);
	
		//perform_new_game_start
		モンブラン.setkutipakustatus(0);
		モンブラン.setunazukistatus(0);
		wait(10);

		ucon();
		fadeout(15);
		stopspotsound();
		pausesestop();
		voicestopall();
		fadesync();
		hideparty();

		//here resetting stuff
		sysReqew(0, ngplus_resets::reset_basic);
		
		if (ucMBResetHunt == 0)
			sysReqew(0, ngplus_resets::reset_hunts);
		
		sysReqew(0, ngplus_resets::reset_fix);

		if (ucMBRemoveParty == 0)
			sysReqew(0, ngplus_resets::reset_party);
		else
			sysReqew(0, ngplus_resets::reset_battle);
		
		sysReqew(0, ngplus_resets::remove_items);
		
		//end resetting
		シナリオフラグ = 0x19;
		mapjump(0x479, 1, 0);
		
		retval = false;
		return;
	}

	function confirm_message()
	{
		amese(0, 0x1000000 | 257);
		messync(0, 1);
		mesclose(0);
		return;
	}
}

script ngplus_resets(6)
{
	function init()
	{
		return;
	}
	
	function reset_basic()
	{
		globU8[0x302] = 0;
		globU8[0x305] = 0;
		globU8[0x309] = 0;
		globU8[0x310] = 0;
		globU8[0x31a] = 0;
		globU8[0x685] = 0;
		
		//index = 9a0
		globU32[0x268] = 0;
		
		//index = 9a4
		globU32[0x269] = 0;
		
		//index - ash dragon
		globU8[0xa03] = 0;
		
		//index = a04 - mimc queen, flan, firemane, hydra
		globU32[0x281] = 0;
		
		//index = a08 - tiamat, daedalus, tyrant, metadragon
		globU32[0x282] = 0;
		
		//index = a0c - ahriman, rafflesia, mandragoras, humbaba
		globU32[0x283] = 0;
		
		//index = a10 - fury, elder dragon, gilgamesh1, gilgamesh2
		globU32[0x284] = 0;
		
		//index = a14 - daemon wall 1, daemon wall 2, bomb king, heavenly dragon
		globU32[0x285] = 0;
		
		//index = a18 - magic dragon, belias, chaos, zodiark
		globU32[0x286] = 0;
		
		//index = a1c - cuchulainn, zalera, ffamfrit, hashmal
		globU32[0x287] = 0;
		
		//index = a20 - shemhazai, matheus, zaromus, exodus
		globU32[0x288] = 0;
		
		//index = a24 - altema, adramelech, bagamnon, judge
		globU32[0x289] = 0;
		
		//index = a28 - ghis, cid draklor, cid tower, gabranth tower
		globU32[0x28a] = 0;
		
		//index = a2c - vayne, hyper vayne, last vayne, antlion
		globU32[0x28b] = 0;
		
		//index = a30 - gabranth bahamut, mimic crystal green, mimic crystal blue, mimic crystal red - setting crystals to done, since we'll have access to teleport
		globU32[0x28c] = 0;
		
		//index = a34 - gembu, old dragon, blight, suzaku
		globU32[0x28d] = 0;
		
		//index = a38 - jing yang, seeq prisoner, garamsythe imperial, vossler
		globU32[0x28e] = 0;
		
		//index = a3c - judge bergan, darkfire, soul of darkness, soul of saint
		globU32[0x28f] = 0;
		
		//index = a40 - deathgaze, remora, mice, rogue tomato
		globU32[0x290] = 0;
		
		//index -  reset rogue tomato hunt quest
		globU8[0x1030] = 0xfe;
		
		//index = a6a
		globU16[0x535] = 1;
		
		//index = a6c
		globU16[0x536] = 1;
		
		//index
		globU8[0xa6e] = 1;
		
		//index - FIXME: test this one completion
		globU8[0xcc7] = 0;
		
		return;
	}
	
	function reset_hunts()
	{
		//index - reset hunt quests completion
		globU8[0x1030] = 0;
		
		//index - reset hunt quests completion
		globU8[0x1031] = 0;
		
		//index - reset hunt quests completion
		globU8[0x1032] = 0;
		
		//index - reset hunt quests completion
		globU8[0x1033] = 0;
		
		//index - reset hunt quests completion
		globU8[0x1034] = 0;
		
		//index - reset hunt quests completion
		globU8[0x1035] = 0;
		
	//reset more hunt quest progress -
		//index - reset hunt quest taken
		globU8[0x1010] = 0;
		
		//index - reset hunt quest taken
		globU8[0x1011] = 0;
		
		//index - reset hunt quest taken
		globU8[0x1012] = 0;
		
		//index - reset hunt quest taken
		globU8[0x1013] = 0;
		
		//index - reset hunt quest taken
		globU8[0x1014] = 0;
		
		//index - reset hunt quest taken
		globU8[0x1015] = 0;
		
		//index = 10e4 - hunt progress
		globU32[0x439] = 0;
		
		//index = 10e8 - hunt progress
		globU32[0x43a] = 0;
		
		//index = 10ec - hunt progress
		globU32[0x43b] = 0;
		
		//index = 10f0 - hunt progress
		globU32[0x43c] = 0;
		
		//index = 10f4 - hunt progress
		globU32[0x43d] = 0;
		
		//index = 10f8 - hunt progress
		globU32[0x43e] = 0;
		
		//index = 10fc - hunt progress
		globU32[0x43f] = 0;
		
		//index = 1100 - hunt progress
		globU32[0x440] = 0;
		
		//index = 1104 - hunt progress
		globU32[0x441] = 0;
		
		//index = 1108 - hunt progress
		globU32[0x442] = 0;
		
		//index = 110c - hunt progress
		globU32[0x443] = 0;
		
		//index = 1110 - hunt progress
		globU32[0x444] = 0;
	
	//reset hunt enemy spawns: 0 - not yet (have to pick quest), 1 - spawned, 2 - killed
		//index = a44
		globU32[0x291] = 0;
		
		//index = a48
		globU32[0x292] = 0;
		
		//index = a4c
		globU32[0x293] = 0;
		
		//index = a50
		globU32[0x294] = 0;
		
		//index = a54
		globU32[0x295] = 0;
		
		//index = a58
		globU32[0x296] = 0;
		
		//index = a5c
		globU32[0x297] = 0;
		
		//index = a60
		globU32[0x298] = 0;
		
		//index = a64
		globU32[0x299] = 0;
		
		//index = a68
		globU16[0x534] = 0;
		
		return;
	}
	
	function reset_fix()
	{
		//index
		globU8[0x1067] = 0;
		
		//index - tomato progress
		globU8[0x10e4] = 0;
		
		//index
		globU8[0x10ea] = 0;
		
		//index
		globU8[0x1101] = 0;
		
	//barheim mimic fix
		//index = 9c0
		globU32[0x270] = 0;
		
		//index = 9c4
		globU32[0x271] = 0;
		
		//index = 9c8
		globU32[0x272] = 0;
		
		//index = 9cc
		globU32[0x273] = 0;
		
		//index = 9d0
		globU32[0x274] = 0;
		
		//index = 9d4
		globU32[0x275] = 0;
		
		//index = 9d8
		globU32[0x276] = 0;
		
		//index = 9dc
		globU8[0x9dc] = 0;
		
	//leviathan fix
		//index
		globU8[0xa28] = 0;

	//jahara fix
		globU8[0x1104] = 100;
	
	//balzac fix
		setquestorder(134, 1);
		setquestclear(134, 0);
		setquestok(134, 1);
		setquestscenarioflag(134, 0);
		
		return;
	}
	
	function reset_party()
	{
		for (regI0 = 1; regI0 < 7; regI0++)
		{
			if (ispartymember(regI0) == 1)
				removepartymember(regI0);
		}
		
		for (regI0 = 7; regI0 < 16; regI0++)
			removeguestbattlemember(regI0);
		
		
		setbattlemember(-1, 0, -1); //set van
		showparty();
		return;
	}
	
	function reset_battle()
	{
		setbattlemember(-1, 0, -1); //set van
		
		for (regI0 = 1; regI0 < 7; regI0++)		
			removebattlemember(regI0);

		showparty();
		return;
	}

	u_short removeItemsArr[] = {
		0x8079,	//shadestone
		0x807a	//sunstone
	};
	function remove_items()
	{
		for (regI0 = 0; regI0 < countof(removeItemsArr); regI0++)
		{
			regI1 = haveitem(removeItemsArr[regI0]);
			if (regI1 > 0)
				subitem(removeItemsArr[regI0], regI1);
		}
		return;
	}
}
