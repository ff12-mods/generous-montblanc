// File size: 167744 Bytes
// Author:    mtanaka
// Source:    slm_a01.src
// Date:      00/00 00:00
// Binary:    C:\Users\ffgri\Desktop\test-script\0_mods\generous_montblanc\fix\slm_a01.ebp_unc\section_000.bin

option authorName = "mtanaka/ffgriever";
option fileName = "slm_a01-azelasfix.src";
option dataFile = "slm_a01.src.data";
option spawnOrder = {-1, 2, 1, -1, -1, -1, -1, -1};
option positionFlags = 0;
option unknownFlags1 = 0;
option unknownScale = {100, 0, 0};
option unknownPosition2d = {144, 768};

//======================================================================
//                Global and scratchpad variable imports                
//======================================================================
import global   short   シナリオフラグ = 0x0;
import global   u_char  g_com_会話段階変数 = 0xa00;
import global   u_char  g_btl_キュクレイン = 0xa1c;
import global   u_char  g_btl_ネズミ = 0xa42;
import global   u_char  g_btl_nm_レイス = 0xa46;
import global   u_char  g_btl_nm_リングドラゴン = 0xa49;
import global   u_char  g_btl_nm_オルトロス = 0xa65;
import global   float   g_com_navi_footcalc[3] = 0xc7c;
import global   int     g_com_set_get_label = 0xc88;
import global   int     g_com_set_get_label_2 = 0xc8c;
import global   u_char  g_com_counter_for_horidasi[10] = 0xcd0;
import global   u_char  g_com_check_for_horidasi[2] = 0xcda;
import global   int     g_com_set_get_label_3 = 0xcf0;
import global   float   mp_map_set_angle = 0x900;
import global   float   mp_map_set_x = 0x904;
import global   float   mp_map_set_y = 0x908;
import global   float   mp_map_set_z = 0x90c;
import global   int     mp_map_flg = 0x910;
import global   char    mp_last_weather = 0x940;
import global   char    mp_4map = 0x941;
import global   char    mp_map_set_hitse = 0x943;
import global   u_char  ミュートと集積場フラグ[1] = 0x404;
import global   u_char  g_iw_便利フラグ[1] = 0x405;
import global   u_char  メダルかけら２進行フラグ = 0x416;
import global   u_char  メダルかけら２進行フラグ２ = 0x417;
import scratch2 u_char  scratch2_var_2d[2] = 0xc0;
import scratch2 u_char  scratch2_var_2e = 0xc4;
import scratch2 u_char  scratch2_var_2f[2] = 0xc5;



script setup(0)
{

	function init()
	{
		if (シナリオフラグ < 0x182 && isquestclear(134))
		{
			setquestorder(134, 1);
			setquestclear(134, 0);
			setquestok(134, 1);
			setquestscenarioflag(134, 0);
		}
		return;
	}


	function battle()
	{
		btlAtelSetupActorStart();
		return;
	}


	function event()
	{
		switch (シナリオフラグ)
		{
			case 0x11d:
				stopenvsoundall();
				setcharseplayall(0);
				break;
			case 0x182:
				stopenvsoundall();
				setcharseplayall(0);
				break;
		}
		return;
	}
}


script __MJ_CTRL000(0)
{

	function init()
	{
		reqenable(12);
		setmapjumpgroup(1);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_30;            // pos: 0x0;

	function mapjump(12)
	{
		local_var_30 = 1;
		if (local_var_30)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(40, 0);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
				for (regI0 = 1; regI0 <= 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, 100, -0.275570542, 249.033493);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, 100, -0.275570542, 249.033493);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, 100, -0.275570542, 249.033493);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, 100, -0.275570542, 249.033493);
				}
				wait(12);
				stopspotsound();
				pausesestop();
				fadesync();
				wait(2);
				mapjump(0x2be, 1, 0);
			}
		}
		return;
	}
}


script __MJ_CTRL001(0)
{

	function init()
	{
		reqenable(12);
		setmapjumpgroup(2);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_31;            // pos: 0x0;

	function mapjump(12)
	{
		local_var_31 = 1;
		if (local_var_31)
		{
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				spotsoundtrans(40, 0);
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, 12);
				for (regI0 = 1; regI0 <= 15; regI0 = (regI0 + 1))
				{
					setmapidmj(regI0, 1, 1);
				}
				if ((check_move_battlestatus(0) == 1 || istownmap() == 1))
				{
					ucmove_52b(0, 140, -0.275570542, 249.033493);
				}
				if ((check_move_battlestatus(1) == 1 || istownmap() == 1))
				{
					ucmove_52b(1, 140, -0.275570542, 249.033493);
				}
				if ((check_move_battlestatus(2) == 1 || istownmap() == 1))
				{
					ucmove_52b(2, 140, -0.275570542, 249.033493);
				}
				if ((check_move_battlestatus(3) == 1 || istownmap() == 1))
				{
					ucmove_52b(3, 140, -0.275570542, 249.033493);
				}
				wait(12);
				stopspotsound();
				pausesestop();
				fadesync();
				wait(2);
				mapjump(0x2be, 2, 0);
			}
		}
		return;
	}
}


script btl_trap_ctrl(0)
{

	function init()
	{
		modelread(0x3000001);
		modelreadsync(0x3000001);
		settrapresource(0x3000001);
		return;
	}
}


script 振動処理(0)
{

	function init()
	{
		return;
	}


	function 大きな門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(8);
		vibsync();
		return;
	}


	function ＤＴ扉振動開始()
	{
		vibplay(3);
		vibsync();
		vibplay(0);
		vibsync();
		return;
	}


	function ＤＴ門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		wait(20);
		vibstop();
		vibplay(7);
		wait(25);
		vibstop();
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
int     file_var_21;             // pos: 0x34;
u_char  file_var_22;             // pos: 0x38;
u_char  file_var_26;             // pos: 0x51;
u_char  file_var_27;             // pos: 0x52;
u_char  file_var_28;             // pos: 0x53;
u_char  file_var_29;             // pos: 0x54;
u_char  file_var_2a;             // pos: 0x55;
u_char  file_var_2b;             // pos: 0x56;


script 常駐助監督(0)
{

	function init()
	{
		return;
	}


	function オルトロス判定()
	{
		file_var_27 = 0;
		if ((getclanrank() >= 5 && シナリオフラグ >= 0x604))
		{
			if (isquestclear(162))
			{
				file_var_26 = 5;
			}
			else if (g_btl_nm_オルトロス == 2)
			{
				file_var_26 = 4;
			}
			else if (g_btl_nm_オルトロス == 1)
			{
				file_var_26 = 3;
			}
			else if (isquestorder(162))
			{
				file_var_26 = 2;
			}
			else
			{
				file_var_26 = 1;
			}
		}
		else
		{
			file_var_26 = 0;
		}
		if ((file_var_26 >= 1 && file_var_26 < 5))
		{
			file_var_27 = 1;
		}
		return;
	}


	function レイス判定()
	{
		file_var_29 = 0;
		if ((getclanrank() >= 2 && シナリオフラグ >= 0x15e))
		{
			if (isquestclear(131))
			{
				file_var_28 = 5;
			}
			else if (g_btl_nm_レイス == 2)
			{
				file_var_28 = 4;
			}
			else if (g_btl_nm_レイス == 1)
			{
				file_var_28 = 3;
			}
			else if (isquestorder(131))
			{
				file_var_28 = 2;
			}
			else
			{
				file_var_28 = 1;
			}
		}
		else
		{
			file_var_28 = 0;
		}
		if ((file_var_28 >= 1 && file_var_28 < 5))
		{
			file_var_29 = 1;
		}
		return;
	}


	function リングドラゴン判定()
	{
		file_var_2b = 0;
		if ((getclanrank() >= 4 && シナリオフラグ >= 0x4ba))
		{
			if (isquestclear(134))
			{
				file_var_2a = 5;
			}
			else if (g_btl_nm_リングドラゴン == 2)
			{
				file_var_2a = 4;
			}
			else if (g_btl_nm_リングドラゴン == 1)
			{
				file_var_2a = 3;
			}
			else if (isquestorder(134))
			{
				file_var_2a = 2;
			}
			else
			{
				file_var_2a = 1;
			}
		}
		else
		{
			file_var_2a = 0;
		}
		if ((file_var_2a >= 1 && file_var_2a < 5))
		{
			file_var_2b = 1;
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_32;            // pos: 0x0;

	function NPC台詞代理()
	{
		switch (file_var_21)
		{
			case 1:
				NPC01.setkutipakustatus(1);
				NPC01.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC01.amese(0, 0x100004f);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC01.amese(0, 0x1000050);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC01.amese(0, 0x1000051);
						messync(0, 1);
						break;
					default:
						NPC01.amese(0, 0x1000052);
						messync(0, 1);
						break;
				}
				NPC01.setkutipakustatus(0);
				NPC01.setunazukistatus(0);
				break;
			case 3:
				NPC01.setkutipakustatus(1);
				NPC01.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC01.amese(0, 0x1000053);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC01.amese(0, 0x1000054);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC01.amese(0, 0x1000055);
						messync(0, 1);
						break;
					default:
						NPC01.amese(0, 0x1000056);
						messync(0, 1);
						break;
				}
				NPC01.setkutipakustatus(0);
				NPC01.setunazukistatus(0);
				break;
			case 0x10002:
				NPC02.setkutipakustatus(1);
				NPC02.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC02.amese(0, 0x1000057);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC02.amese(0, 0x1000058);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC02.amese(0, 0x1000059);
						messync(0, 1);
						break;
					default:
						NPC02.amese(0, 0x100005a);
						messync(0, 1);
						break;
				}
				NPC02.setkutipakustatus(0);
				NPC02.setunazukistatus(0);
				break;
			case 0x20001:
				switch (file_var_26)
				{
					case 0:
					case 5:
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						switch (シナリオフラグ)
						{
							case lte(0x157):
								NPC03.amese(0, 0x100005b);
								messync(0, 1);
								break;
							case lte(0x5f0):
								NPC03.amese(0, 0x100005c);
								messync(0, 1);
								break;
							case lte(0x1004):
								NPC03.amese(0, 0x100005d);
								messync(0, 1);
								break;
							default:
								NPC03.amese(0, 0x100005e);
								messync(0, 1);
								break;
						}
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
					case lte(2):
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x100005f);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
					case lt(4):
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x1000060);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
					default:
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x1000061);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
				}
				break;
			case 0x20002:
				switch (file_var_28)
				{
					case 0:
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x1000062);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
					case 1:
					case 2:
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x1000063);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						settalknpcname(0x26d);
						break;
					case 3:
					case 4:
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x1000064);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
					case gte(5):
						NPC03.setkutipakustatus(1);
						NPC03.setunazukistatus(1);
						NPC03.amese(0, 0x1000065);
						NPC03.messync(0, 1);
						NPC03.setkutipakustatus(0);
						NPC03.setunazukistatus(0);
						break;
				}
				break;
			case 0x20004:
				NPC03.setkutipakustatus(1);
				NPC03.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC03.amese(0, 0x1000066);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC03.amese(0, 0x1000067);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC03.amese(0, 0x1000068);
						messync(0, 1);
						break;
					default:
						NPC03.amese(0, 0x1000069);
						messync(0, 1);
						break;
				}
				NPC03.setkutipakustatus(0);
				NPC03.setunazukistatus(0);
				break;
			case 0x30003:
				if ((シナリオフラグ >= 0x186 && シナリオフラグ < 0x1a4))
				{
					NPC04.setkutipakustatus(1);
					NPC04.setunazukistatus(1);
					NPC04.amese(0, 0x100006a);
					NPC04.messync(0, 1);
					NPC04.setkutipakustatus(0);
					NPC04.setunazukistatus(0);
				}
				else
				{
					NPC04.setkutipakustatus(1);
					NPC04.setunazukistatus(1);
					askpos(0, 0, 127);
					local_var_32 = aaske(0, 0x100006b);
					mesclose(0);
					messync(0, 1);
					NPC04.setkutipakustatus(0);
					NPC04.setunazukistatus(0);
					switch (local_var_32)
					{
						case 0:
							NPC04.setkutipakustatus(1);
							NPC04.setunazukistatus(1);
							askpos(0, 0, 127);
							local_var_32 = aaske(0, 0x100006c);
							mesclose(0);
							messync(0, 1);
							NPC04.setkutipakustatus(0);
							NPC04.setunazukistatus(0);
							switch (local_var_32)
							{
								case 0:
									NPC04.setkutipakustatus(1);
									NPC04.setunazukistatus(1);
									NPC04.amese(0, 0x100006d);
									NPC04.messync(0, 1);
									NPC04.setkutipakustatus(0);
									NPC04.setunazukistatus(0);
									break;
								default:
									break;
							}
							break;
						default:
							NPC04.setkutipakustatus(1);
							NPC04.setunazukistatus(1);
							NPC04.amese(0, 0x100006e);
							NPC04.messync(0, 1);
							NPC04.setkutipakustatus(0);
							NPC04.setunazukistatus(0);
							break;
					}
				}
				break;
			case 0x40004:
				NPC05.setkutipakustatus(1);
				NPC05.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC05.amese(0, 0x100006f);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC05.amese(0, 0x1000070);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC05.amese(0, 0x1000071);
						messync(0, 1);
						break;
					default:
						NPC05.amese(0, 0x1000072);
						messync(0, 1);
						break;
				}
				NPC05.setkutipakustatus(0);
				NPC05.setunazukistatus(0);
				break;
			case 0x50001:
				switch (file_var_26)
				{
					case 0:
					case 5:
						NPC06.setkutipakustatus(1);
						NPC06.setunazukistatus(1);
						switch (シナリオフラグ)
						{
							case lte(0x157):
								NPC06.amese(0, 0x1000073);
								messync(0, 1);
								break;
							case lte(0x5f0):
								NPC06.amese(0, 0x1000074);
								messync(0, 1);
								break;
							case lte(0x1004):
								NPC06.amese(0, 0x1000075);
								messync(0, 1);
								break;
							default:
								NPC06.amese(0, 0x1000076);
								messync(0, 1);
								break;
						}
						NPC06.setkutipakustatus(0);
						NPC06.setunazukistatus(0);
						break;
					case lte(2):
						NPC06.setkutipakustatus(1);
						NPC06.setunazukistatus(1);
						NPC06.amese(0, 0x1000077);
						NPC06.messync(0, 1);
						NPC06.setkutipakustatus(0);
						NPC06.setunazukistatus(0);
						break;
					case lt(4):
						NPC06.setkutipakustatus(1);
						NPC06.setunazukistatus(1);
						NPC06.amese(0, 0x1000078);
						NPC06.messync(0, 1);
						NPC06.setkutipakustatus(0);
						NPC06.setunazukistatus(0);
						break;
					default:
						NPC06.setkutipakustatus(1);
						NPC06.setunazukistatus(1);
						NPC06.amese(0, 0x1000079);
						NPC06.messync(0, 1);
						NPC06.setkutipakustatus(0);
						NPC06.setunazukistatus(0);
						break;
				}
				break;
			case 0x50002:
				NPC06.setkutipakustatus(1);
				NPC06.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC06.amese(0, 0x100007a);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC06.amese(0, 0x100007b);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC06.amese(0, 0x100007c);
						messync(0, 1);
						break;
					default:
						NPC06.amese(0, 0x100007d);
						messync(0, 1);
						break;
				}
				NPC06.setkutipakustatus(0);
				NPC06.setunazukistatus(0);
				break;
			case 0x60003:
				if (g_btl_キュクレイン >= 2)
				{
					NPC07.setkutipakustatus(1);
					NPC07.setunazukistatus(1);
					NPC07.amese(0, 0x100007e);
					NPC07.messync(0, 1);
					NPC07.setkutipakustatus(0);
					NPC07.setunazukistatus(0);
				}
				else
				{
					NPC07.setkutipakustatus(1);
					NPC07.setunazukistatus(1);
					NPC07.amese(0, 0x100007f);
					NPC07.messync(0, 1);
					NPC07.setkutipakustatus(0);
					NPC07.setunazukistatus(0);
				}
				break;
			case 0x70001:
				NPC08.setkutipakustatus(1);
				NPC08.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC08.amese(0, 0x1000080);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC08.amese(0, 0x1000081);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC08.amese(0, 0x1000082);
						messync(0, 1);
						break;
					default:
						NPC08.amese(0, 0x1000083);
						messync(0, 1);
						break;
				}
				NPC08.setkutipakustatus(0);
				NPC08.setunazukistatus(0);
				break;
			case 0x80001:
				if (((isquestorder(53) && !(isquestclear(53))) && (0x350000 | getquestscenarioflag(53)) >= 0x35001e))
				{
					switch ((0x350000 | getquestscenarioflag(53)))
					{
						case lte(0x35001e):
							setmesmacro(0, 0, 1, 0x80b2);
							NPC09.setkutipakustatus(1);
							NPC09.setunazukistatus(1);
							NPC09.amese(0, 0x1000084);
							NPC09.messync(0, 1);
							NPC09.setkutipakustatus(0);
							NPC09.setunazukistatus(0);
							setquestscenarioflag(53, 40);
							break;
						case lt(0x350032):
							setmesmacro(0, 0, 1, 0x80b2);
							switch (file_var_22)
							{
								case lte(0):
									NPC09.setkutipakustatus(1);
									NPC09.setunazukistatus(1);
									NPC09.amese(0, 0x1000085);
									NPC09.messync(0, 1);
									NPC09.setkutipakustatus(0);
									NPC09.setunazukistatus(0);
									file_var_22 = (file_var_22 + 1);
									break;
								default:
									NPC09.setkutipakustatus(1);
									NPC09.setunazukistatus(1);
									NPC09.amese(0, 0x1000086);
									NPC09.messync(0, 1);
									NPC09.setkutipakustatus(0);
									NPC09.setunazukistatus(0);
									break;
							}
							break;
						default:
							switch (file_var_22)
							{
								case lte(0):
									NPC09.setkutipakustatus(1);
									NPC09.setunazukistatus(1);
									NPC09.amese(0, 0x1000087);
									NPC09.messync(0, 1);
									NPC09.setkutipakustatus(0);
									NPC09.setunazukistatus(0);
									file_var_22 = (file_var_22 + 1);
									break;
								default:
									NPC09.setkutipakustatus(1);
									NPC09.setunazukistatus(1);
									NPC09.amese(0, 0x1000088);
									NPC09.messync(0, 1);
									NPC09.setkutipakustatus(0);
									NPC09.setunazukistatus(0);
									break;
							}
							break;
					}
				}
				else
				{
					NPC09.setkutipakustatus(1);
					NPC09.setunazukistatus(1);
					switch (シナリオフラグ)
					{
						case lte(0x157):
							NPC09.amese(0, 0x1000089);
							messync(0, 1);
							break;
						case lte(0x5f0):
							NPC09.amese(0, 0x100008a);
							messync(0, 1);
							break;
						case lte(0x1004):
							NPC09.amese(0, 0x100008b);
							messync(0, 1);
							break;
						default:
							NPC09.amese(0, 0x100008c);
							messync(0, 1);
							break;
					}
					NPC09.setkutipakustatus(0);
					NPC09.setunazukistatus(0);
				}
				break;
			case 0x80003:
				NPC09.setkutipakustatus(1);
				NPC09.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC09.amese(0, 0x100008d);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC09.amese(0, 0x100008e);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC09.amese(0, 0x100008f);
						messync(0, 1);
						break;
					default:
						NPC09.amese(0, 0x1000090);
						messync(0, 1);
						break;
				}
				NPC09.setkutipakustatus(0);
				NPC09.setunazukistatus(0);
				break;
			case 0x90004:
				NPC10.setkutipakustatus(1);
				NPC10.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC10.amese(0, 0x1000091);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC10.amese(0, 0x1000092);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC10.amese(0, 0x1000093);
						messync(0, 1);
						break;
					default:
						NPC10.amese(0, 0x1000094);
						messync(0, 1);
						break;
				}
				NPC10.setkutipakustatus(0);
				NPC10.setunazukistatus(0);
				break;
			case 0xa0001:
				stdmotionplay_2c2(0x1000011, 5);
				NPC11.setkutipakustatus(1);
				NPC11.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC11.amese(0, 0x1000095);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC11.amese(0, 0x1000096);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC11.amese(0, 0x1000097);
						messync(0, 1);
						break;
					default:
						NPC11.amese(0, 0x1000098);
						messync(0, 1);
						break;
				}
				NPC11.setkutipakustatus(0);
				NPC11.setunazukistatus(0);
				break;
			case 0xb0002:
				if ((シナリオフラグ >= 0x186 && シナリオフラグ < 0x1a4))
				{
					NPC12.setkutipakustatus(1);
					NPC12.setunazukistatus(1);
					NPC12.amese(0, 0x1000099);
					NPC12.messync(0, 1);
					NPC12.setkutipakustatus(0);
					NPC12.setunazukistatus(0);
				}
				else
				{
					NPC12.setkutipakustatus(1);
					NPC12.setunazukistatus(1);
					switch (シナリオフラグ)
					{
						case lte(0x157):
							NPC12.amese(0, 0x100009a);
							messync(0, 1);
							break;
						case lte(0x5f0):
							NPC12.amese(0, 0x100009b);
							messync(0, 1);
							break;
						case lte(0x1004):
							NPC12.amese(0, 0x100009c);
							messync(0, 1);
							break;
						default:
							NPC12.amese(0, 0x100009d);
							messync(0, 1);
							break;
					}
					NPC12.setkutipakustatus(0);
					NPC12.setunazukistatus(0);
				}
				break;
			case 0xb0003:
				NPC12.setkutipakustatus(1);
				NPC12.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC12.amese(0, 0x100009e);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC12.amese(0, 0x100009f);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC12.amese(0, 0x10000a0);
						messync(0, 1);
						break;
					default:
						NPC12.amese(0, 0x10000a1);
						messync(0, 1);
						break;
				}
				NPC12.setkutipakustatus(0);
				NPC12.setunazukistatus(0);
				break;
			case 0xc0002:
				NPC13.setkutipakustatus(1);
				NPC13.setunazukistatus(1);
				NPC13.amese(0, 0x10000a2);
				NPC13.messync(0, 1);
				NPC13.setkutipakustatus(0);
				NPC13.setunazukistatus(0);
				break;
			case 0xc0004:
				NPC13.setkutipakustatus(1);
				NPC13.setunazukistatus(1);
				NPC13.amese(0, 0x10000a3);
				NPC13.messync(0, 1);
				NPC13.setkutipakustatus(0);
				NPC13.setunazukistatus(0);
				break;
			case 0xc0005:
				NPC13.setkutipakustatus(1);
				NPC13.setunazukistatus(1);
				NPC13.amese(0, 0x10000a4);
				NPC13.messync(0, 1);
				NPC13.setkutipakustatus(0);
				NPC13.setunazukistatus(0);
				break;
			case 0xe0003:
				NPC15.setkutipakustatus(1);
				NPC15.setunazukistatus(1);
				switch (シナリオフラグ)
				{
					case lte(0x157):
						NPC15.amese(0, 0x10000a5);
						messync(0, 1);
						break;
					case lte(0x5f0):
						NPC15.amese(0, 0x10000a6);
						messync(0, 1);
						break;
					case lte(0x1004):
						NPC15.amese(0, 0x10000a7);
						messync(0, 1);
						break;
					default:
						NPC15.amese(0, 0x10000a8);
						messync(0, 1);
						break;
				}
				NPC15.setkutipakustatus(0);
				NPC15.setunazukistatus(0);
				break;
			case 0x140001:
				if ((シナリオフラグ >= 180 && シナリオフラグ <= 0x122))
				{
					if (!((ミュートと集積場フラグ[0] & 2)))
					{
						NPC21.setkutipakustatus(1);
						NPC21.setunazukistatus(1);
						NPC21.amese(0, 0x10000a9);
						NPC21.messync(0, 1);
						NPC21.setkutipakustatus(0);
						NPC21.setunazukistatus(0);
					}
					else
					{
						NPC21.setkutipakustatus(1);
						NPC21.setunazukistatus(1);
						NPC21.amese(0, 0x10000aa);
						NPC21.messync(0, 1);
						NPC21.setkutipakustatus(0);
						NPC21.setunazukistatus(0);
					}
				}
				else
				{
					switch (file_var_26)
					{
						case 0:
						case 5:
							NPC21.setkutipakustatus(1);
							NPC21.setunazukistatus(1);
							switch (シナリオフラグ)
							{
								case lte(0x157):
									NPC21.amese(0, 0x10000ab);
									messync(0, 1);
									break;
								case lte(0x5f0):
									NPC21.amese(0, 0x10000ac);
									messync(0, 1);
									break;
								case lte(0x1004):
									NPC21.amese(0, 0x10000ad);
									messync(0, 1);
									break;
								default:
									NPC21.amese(0, 0x10000ae);
									messync(0, 1);
									break;
							}
							NPC21.setkutipakustatus(0);
							NPC21.setunazukistatus(0);
							break;
						case lte(2):
							NPC21.setkutipakustatus(1);
							NPC21.setunazukistatus(1);
							NPC21.amese(0, 0x10000af);
							NPC21.messync(0, 1);
							NPC21.setkutipakustatus(0);
							NPC21.setunazukistatus(0);
							break;
						case lt(4):
							NPC21.setkutipakustatus(1);
							NPC21.setunazukistatus(1);
							NPC21.amese(0, 0x10000b0);
							NPC21.messync(0, 1);
							NPC21.setkutipakustatus(0);
							NPC21.setunazukistatus(0);
							break;
						default:
							NPC21.setkutipakustatus(1);
							NPC21.setunazukistatus(1);
							NPC21.amese(0, 0x10000b1);
							NPC21.messync(0, 1);
							NPC21.setkutipakustatus(0);
							NPC21.setunazukistatus(0);
							break;
					}
				}
				break;
			default:
				break;
		}
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_23;             // pos: 0x39;
u_char  file_var_2c;             // pos: 0x57;


script 常駐監督(0)
{

	function init()
	{
		setcharclipdistance_327(19, 3);
		sysReqi(1, 常駐助監督::オルトロス判定);
		sysReqwait(常駐助監督::オルトロス判定);
		sysReqi(1, 常駐助監督::レイス判定);
		sysReqwait(常駐助監督::レイス判定);
		sysReqi(1, 常駐助監督::リングドラゴン判定);
		sysReqwait(常駐助監督::リングドラゴン判定);
		switch (シナリオフラグ)
		{
			case 0x11d:
				fadeout(1);
				fadecancel_15a(2);
				fadeout(1);
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				return;
			case 0x182:
				releasemapjumpgroupflag(88);
				fadeout(1);
				fadecancel_15a(2);
				fadeout(1);
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				modelread(0x3000002);
				modelread(0x3000003);
				modelread(0x3000004);
				modelread(0x3000006);
				modelread(0x3000007);
				modelread(0x3000008);
				modelreadsync(0x3000002);
				modelreadsync(0x3000003);
				modelreadsync(0x3000004);
				modelreadsync(0x3000006);
				modelreadsync(0x3000007);
				modelreadsync(0x3000008);
				break;
			default:
				break;
		}
		sysReqi(1, shop_npc01::shopnpc_bindon);
		switch (シナリオフラグ)
		{
			case lt(0x113):
				if ((シナリオフラグ >= 180 && !((ミュートと集積場フラグ[0] & 2))))
				{
					sysReqi(1, ミュート::左の扉配置);
					sysReqwait(ミュート::左の扉配置);
					file_var_2c = 1;
					sysReq(1, map集積所扉南側::フィールドサインＯＮ);
				}
				else
				{
					sysReqi(1, ミュート::解錠配置);
					sysReqwait(ミュート::解錠配置);
					sysReqi(1, 制御レクト::セット);
					sysReqwait(制御レクト::セット);
					sysReq(1, map集積所扉南側::フィールドサインＯＦＦ);
				}
				break;
			case lt(0x157):
				break;
			case lt(0x182):
				break;
			default:
				sysReq(1, map集積所扉南側::フィールドサインＯＮ);
				break;
		}
		if (シナリオフラグ <= 0x122)
		{
			sysReqi(1, map閉じこめ民家::フィールドサインＮＯＴ);
		}
		else if (isquestclear(131))
		{
			sysReqi(1, map閉じこめ民家::フィールドサインＯＫ);
		}
		else
		{
			sysReqi(1, map閉じこめ民家::フィールドサインＮＯＴ);
			file_var_23 = 1;
		}
		return;
	}


	function main(1)
	{
		switch (シナリオフラグ)
		{
			case 0x11d:
				break;
			case 0x182:
				break;
			default:
				sysReqi(1, 配置監督::ベース配置);
				sysReqwait(配置監督::ベース配置);
				break;
		}
		switch (シナリオフラグ)
		{
			case 0x11d:
				healall(2);
				wait(1);
				removeguestbattlemember(7);
				takeaway_item_and_equip();
				subitem(0x80ac, 1);
				removepartymember(3);
				removepartymember(2);
				wait(1);
				setbattlemember(0, -1, -1);
				wait(1);
				charcachedispose();
				partystdmotiondispose();
				wait(1);
				eventread(110);
				eventreadsync();
				eventplay();
				eventsync();
				シナリオフラグ = 0x122;
				setquestscenarioflag(5, 100);
				sethpmenufast(0);
				wait(15);
				fadelayer(5);
				fadeout(1);
				wait(1);
				fadeout_d0(2, 1);
				fadesync_d3(2);
				fadelayer(5);
				fadein(1);
				fadein(1);
				partystdmotionrecover();
				wait(5);
				mapjump(50, 0, 1);
				return;
			case 0x182:
				partyusemapid(1);
				setposparty(63.9947472, -0.263617009, 108.689758, 1.96706498);
				setcharseplayall(0);
				eventread(113);
				eventreadsync();
				eventplay();
				eventsync();
				sethpmenufast(0);
				sysReqi(1, 配置監督::ベース配置);
				sysReqwait(配置監督::ベース配置);
				シナリオフラグ = 0x186;
				addpartymember(4);
				partyusemapid(1);
				setposparty(63.9947472, -0.263617009, 108.689758, 1.96706498);
				resetbehindcamera(1.96706498, 0.200000003);
				wait(1);
				startenvsoundall();
				setstatuserrordispdenystatus(0);
				clearnavimapfootmark();
				sethpmenufast(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				wait(1);
				setcharseplayall(1);
				fadein(15);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				break;
			default:
				break;
		}
		if (nowjumpindex() == 9)
		{
			setmapjumpgroupflag(30);
		}
		return;
	}


	function レイス撃破()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(131);
		setquestorder(131, 0);
		setquestclear(131, 1);
		setquestok(131, 0);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		setquestscenarioflag(131, 100);
		return;
	}


	function 賞金首リングドラゴンクリア()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(134);
		setquestorder(134, 0);
		setquestclear(134, 1);
		setquestok(134, 0);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		setquestscenarioflag(134, 100);
		return;
	}


	function オルトロスクリア()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(162);
		setquestorder(162, 0);
		setquestclear(162, 1);
		setquestok(162, 0);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		setquestscenarioflag(162, 100);
		return;
	}


	function 集積場扉開いたら呼んでねタグ()
	{
		if (file_var_2c != 0)
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			wait(5);
			fadeout(15);
			fadesync();
			g_com_navi_footcalc[0] = getx_12b(-1);
			g_com_navi_footcalc[1] = gety_12c(-1);
			g_com_navi_footcalc[2] = getz_12d(-1);
			setnavimapfootmarkstatus(0);
			sethpmenufast(0);
			settrapshowstatus(0);
			setcharseplayall(0);
			if (!(istownmap()))
			{
				setstatuserrordispdenystatus(1);
			}
			unkCall_5ac(1, 2.35800004, 100);
			wait(5);
			partyusemapid(1);
			setposparty(91.8773575, -0.271838009, 53.2539825, 3.02951598);
			hideparty();
			sysReq(1, ヴァン::ミュート扉開け配置);
			sysReqwait(ヴァン::ミュート扉開け配置);
			camerastart_7e(0, 0x2000004);
			sethpmenufast(0);
			settrapshowstatus(0);
			setcharseplayall(1);
			fadein(15);
			ヴァン.sysLookata(ミュート);
			ミュート.stdmotionplay_2c2(0x1000016, 5);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			ミュート.amese(0, 0x1000039);
			ミュート.messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			ヴァン.stdmotionplay_2c2(0x1000012, 5);
			ミュート.stdmotionplay_2c2(0x1000010, 20);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			ミュート.amese(0, 0x100003a);
			ミュート.messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			ヴァン.stdmotionplay_2c2(0x1000010, 20);
			sysReq(1, ミュート::水路オープン位置移動３);
			wait(30);
			fadelayer(5);
			fadeout(15);
			fadesync();
			setcharseplayall(0);
			wait(10);
			sebsoundplay(0, 31);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			ミュート.amese(0, 0x100003b);
			ミュート.messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			wait(15);
			sebsoundplay(0, 31);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			ミュート.amese(0, 0x100003c);
			ミュート.messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			wait(20);
			sebsoundplay(0, 30);
			camerastart_7e(0, 0x2000005);
			wait(20);
			sysReqwait(ミュート::水路オープン位置移動３);
			sysReq(1, ミュート::水路オープン位置移動４);
			wait(2);
			setcharseplayall(1);
			fadein(15);
			sysReqwait(ミュート::水路オープン位置移動４);
			ミュート.stdmotionplay(0x1000010);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			ミュート.amese(0, 0x100003d);
			ミュート.messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			ミュート.ramove_4cf(91.688736, -0.27018401, 52.2347527, ミュート.getdefaultwalkspeed());
			ヴァン.stdmotionplay_2c2(0x1000014, 20);
			sebsoundplay(0, 39);
			additemmes(0x9054, 1);
			wait(10);
			fadeout(15);
			fadesync();
			setcharseplayall(0);
			sethpmenufast(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			setstatuserrordispdenystatus(0);
			unkCall_5ac(0, 0, 0);
			sysReq(2, ミュート::初期位置戻り);
			sysReq(1, ヴァン::バインドオフ);
			ミュートと集積場フラグ[0] = (ミュートと集積場フラグ[0] | 2);
			resetbehindcamera(-1.179667, 0.600000024);
			cameraclear();
			showparty();
			wait(5);
			showparty();
			if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
			{
				clearnavimapfootmark();
			}
			setnavimapfootmarkstatus(1);
			setcharseplayall(1);
			sethpmenufast(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			wait(1);
			fadein(15);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}
}


script 常駐カメラ(0)
{

	function init()
	{
		return;
	}


	function in_to_syuseki_cam01()
	{
		camerastart_7e(0, 0x2000007);
		return;
	}


	function in_to_syuseki_cam02()
	{
		camerastart_7e(0, 0x2000008);
		return;
	}


	function in_to_syuseki_cam03()
	{
		camerastart_7e(0, 0x2000009);
		return;
	}


	function in_to_syuseki_cam04()
	{
		camerastart_7e(0, 0x200000a);
		return;
	}


	function in_to_syuseki_cam05()
	{
		camerastart_7e(0, 0x200000b);
		return;
	}


	function reis_cut01()
	{
		setff12screenmode(3);
		camerastart_7e(0, 0x200000c);
		return;
	}


	function reis_cut0102()
	{
		camerastart_7e(0, 0x200000d);
		return;
	}


	function reis_cut03()
	{
		camerastart_7e(0, 0x200000e);
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_20;             // pos: 0x23;


script 配置監督(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function ベース配置()
	{
		wait(1);
		regY = g_com_会話段階変数;
		goto localjmp_2;
		while (true)
		{
			sysReqchg(2, NPC01::talk_step01);
			sysReqchg(2, NPC02::talk_step01);
			sysReqchg(2, NPC03::talk_step01);
			sysReqchg(2, NPC04::talk_step01);
			sysReqchg(2, NPC05::talk_step01);
			sysReqchg(2, NPC06::talk_step01);
			sysReqchg(2, NPC07::talk_step01);
			sysReqchg(2, NPC08::talk_step01);
			sysReqchg(2, NPC09::talk_step01);
			sysReqchg(2, NPC10::talk_step01);
			sysReqchg(2, NPC11::talk_step01);
			sysReqchg(2, NPC12::talk_step01);
			sysReqchg(2, NPC13::talk_step01);
			sysReqchg(2, NPC15::talk_step01);
			sysReqchg(2, NPC21::talk_step01);
			break;
		localjmp_2:
		}
		if (file_var_27 == 0)
		{
			sysReq(1, hitactor01::setHitObj);
		}
		sysReq(1, hitactor02::setHitObj);
		sysReq(1, hitactor03::setHitObj);
		sysReq(1, hitactor04::setHitObj);
		sysReq(1, hitactor05::setHitObj);
		motionread(1);
		motionreadsync(1);
		file_var_20 = 1;
		sysReqi(1, 配置監視監督::配置監視);
		sysReqwait(配置監視監督::配置監視);
		wait(1);
		file_var_20 = 0;
		sysReq(1, 配置監視監督::配置監視);
		sysReqwait(shop_npc01::shopnpc_bindon);
		if (file_var_27 == 1)
		{
			motionread(3);
			motionreadsync(3);
			sysReqi(1, 空き巣シーク::オルトロス配置);
			sysReqwait(空き巣シーク::オルトロス配置);
		}
		if (シナリオフラグ <= 0x122)
		{
			sysReqi(1, レイス依頼人::レイス依頼人通常時セット);
		}
		else if (isquestclear(131))
		{
			sysReqi(1, レイス依頼人::レイス依頼人通常時セット);
		}
		else
		{
			sysReqi(1, レイス依頼人::レイス依頼人依頼時セット);
			file_var_23 = 1;
		}
		if (シナリオフラグ < 110)
		{
			motionread(2);
			motionreadsync(2);
			switch (scratch2_var_2e)
			{
				case 0:
					sysReq(1, hitactor01_firo::setHitObj);
					break;
				default:
					break;
			}
			sysReqiall(2, reqArr10);
		}
		else if ((シナリオフラグ >= 0x177 && シナリオフラグ < 0x182))
		{
			sysReqiall(1, reqArr11);
			sysReqchg(2, LITTSSET01::talk_beforeajito);
			sysReqchg(2, LITTSSET02::talk_beforeajito);
			sysReqwaitall(reqArr11);
			sysReqall(1, reqArr12);
		}
		if (!(isquestclear(134)))
		{
			sysReqi(1, 解放軍::セット);
			if (file_var_2a >= 1)
			{
				sysReqchg(2, 解放軍::talk_wanted);
			}
		}
		switch (シナリオフラグ)
		{
			case lt(0x157):
				break;
			case lt(0x182):
				break;
			default:
				if (シナリオフラグ > 0x1a4)
				{
					if (!(((シナリオフラグ >= 0x1004 && isquestclear(58)) && isquestclear(131))))
					{
						sysReqi(1, ミュート::終盤位置);
						sysReqwait(ミュート::終盤位置);
						sysReq(1, ミュート::終盤挙動);
						switch (メダルかけら２進行フラグ)
						{
							case lt(1):
								break;
							case lt(7):
								sysReqchg(2, ミュート::talk_medal2);
								sysReqchg(16, ミュート::talkhold_medal2);
								sysReqchg(17, ミュート::talkterm_medal2);
								break;
							default:
								sysReqchg(2, ミュート::talk_after_medal);
								sysReqchg(16, ミュート::talkhold_medal2);
								sysReqchg(17, ミュート::talkterm_medal2);
								break;
						}
					}
				}
				break;
		}
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_1d;             // pos: 0xc;
u_char  file_var_1e[21];         // pos: 0xd;
u_char  file_var_1f;             // pos: 0x22;
u_char  file_var_24[21];         // pos: 0x3a;


script 配置監視監督(0)
{

	function init()
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_33[2];         // pos: 0x0;
	u_char  local_var_34;            // pos: 0x8;
	u_char  local_var_35;            // pos: 0x9;

	function 配置監視()
	{
		wait(2);
		while (true)
		{
			local_var_33[0] = getx_12b(-1);
			local_var_33[1] = getz_12d(-1);
			switch (local_var_33[1])
			{
				case lte(63):
					switch (local_var_33[0])
					{
						case lte(65):
							file_var_1d = 0;
							break;
						case lte(90):
							file_var_1d = 1;
							break;
						case lte(120):
							file_var_1d = 2;
							break;
						case lte(145):
							file_var_1d = 3;
							break;
						default:
							file_var_1d = 4;
							break;
					}
					break;
				case lte(88):
					switch (local_var_33[0])
					{
						case lte(90):
							file_var_1d = 5;
							break;
						case lte(120):
							file_var_1d = 6;
							break;
						case lte(145):
							file_var_1d = 7;
							break;
						default:
							file_var_1d = 8;
							break;
					}
					break;
				case lte(111):
					switch (local_var_33[0])
					{
						case lte(65):
							file_var_1d = 9;
							break;
						case lte(90):
							file_var_1d = 10;
							break;
						case lte(120):
							file_var_1d = 11;
							break;
						case lte(145):
							file_var_1d = 12;
							break;
						case lte(170):
							file_var_1d = 13;
							break;
						default:
							file_var_1d = 14;
							break;
					}
					break;
				default:
					switch (local_var_33[0])
					{
						case lte(65):
							file_var_1d = 15;
							break;
						case lte(90):
							file_var_1d = 16;
							break;
						case lte(120):
							file_var_1d = 17;
							break;
						case lte(145):
							file_var_1d = 18;
							break;
						case lte(170):
							file_var_1d = 19;
							break;
						default:
							file_var_1d = 20;
							break;
					}
					break;
			}
			switch (file_var_1d)
			{
				case 0:
				case 1:
					if (file_var_1e[0] != 1)
					{
						sysReqi(5, NPC01::NPC配置01);
					}
					if (file_var_1e[1] != 1)
					{
						sysReqi(5, NPC02::NPC配置01);
					}
					if (file_var_1e[12] != 1)
					{
						sysReqi(5, NPC13::NPC配置01);
					}
					if (file_var_1e[2] != 1)
					{
						sysReqi(5, NPC03::NPC配置01);
					}
					if (file_var_1e[3] != 1)
					{
						sysReqi(5, NPC04::NPC配置01);
					}
					if (file_var_1e[13] != 1)
					{
						sysReqi(5, NPC14::NPC配置01);
					}
					if (file_var_1e[4] != 1)
					{
						sysReqi(5, NPC05::NPC配置01);
					}
					if (file_var_1e[5] != 1)
					{
						sysReqi(5, NPC06::NPC配置01);
					}
					if (file_var_1e[14] != 1)
					{
						sysReqi(5, NPC15::NPC配置01);
					}
					if (file_var_1e[18] != 1)
					{
						sysReqi(5, NPC19::NPC配置01);
					}
					if (file_var_1e[19] != 1)
					{
						sysReqi(5, NPC20::NPC配置01);
					}
					if (file_var_1e[20] != 1)
					{
						sysReqi(5, NPC21::NPC配置01);
					}
					if (file_var_1e[6] != 1)
					{
						sysReqi(5, NPC07::NPC配置01);
					}
					if (file_var_1e[7] != 1)
					{
						sysReqi(5, NPC08::NPC配置01);
					}
					if (file_var_1e[15] != 1)
					{
						sysReqi(5, NPC16::NPC配置01);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 2)
					{
						sysReqi(5, NPC11::NPC配置02);
					}
					if (file_var_1e[11] != 2)
					{
						sysReqi(5, NPC12::NPC配置02);
					}
					if (file_var_1e[17] != 2)
					{
						sysReqi(5, NPC18::NPC配置02);
					}
					break;
				case 2:
					if (file_var_1e[0] != 2)
					{
						sysReqi(5, NPC01::NPC配置02);
					}
					if (file_var_1e[1] != 2)
					{
						sysReqi(5, NPC02::NPC配置02);
					}
					if (file_var_1e[12] != 2)
					{
						sysReqi(5, NPC13::NPC配置02);
					}
					if (file_var_1e[2] != 1)
					{
						sysReqi(5, NPC03::NPC配置01);
					}
					if (file_var_1e[3] != 1)
					{
						sysReqi(5, NPC04::NPC配置01);
					}
					if (file_var_1e[13] != 1)
					{
						sysReqi(5, NPC14::NPC配置01);
					}
					if (file_var_1e[4] != 1)
					{
						sysReqi(5, NPC05::NPC配置01);
					}
					if (file_var_1e[5] != 1)
					{
						sysReqi(5, NPC06::NPC配置01);
					}
					if (file_var_1e[14] != 1)
					{
						sysReqi(5, NPC15::NPC配置01);
					}
					if (file_var_1e[18] != 1)
					{
						sysReqi(5, NPC19::NPC配置01);
					}
					if (file_var_1e[19] != 1)
					{
						sysReqi(5, NPC20::NPC配置01);
					}
					if (file_var_1e[20] != 1)
					{
						sysReqi(5, NPC21::NPC配置01);
					}
					if (file_var_1e[6] != 1)
					{
						sysReqi(5, NPC07::NPC配置01);
					}
					if (file_var_1e[7] != 1)
					{
						sysReqi(5, NPC08::NPC配置01);
					}
					if (file_var_1e[15] != 1)
					{
						sysReqi(5, NPC16::NPC配置01);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 3:
					if (file_var_1e[0] != 2)
					{
						sysReqi(5, NPC01::NPC配置02);
					}
					if (file_var_1e[1] != 2)
					{
						sysReqi(5, NPC02::NPC配置02);
					}
					if (file_var_1e[12] != 2)
					{
						sysReqi(5, NPC13::NPC配置02);
					}
					if (file_var_1e[2] != 2)
					{
						sysReqi(5, NPC03::NPC配置02);
					}
					if (file_var_1e[3] != 2)
					{
						sysReqi(5, NPC04::NPC配置02);
					}
					if (file_var_1e[13] != 2)
					{
						sysReqi(5, NPC14::NPC配置02);
					}
					if (file_var_1e[4] != 2)
					{
						sysReqi(5, NPC05::NPC配置02);
					}
					if (file_var_1e[5] != 2)
					{
						sysReqi(5, NPC06::NPC配置02);
					}
					if (file_var_1e[14] != 2)
					{
						sysReqi(5, NPC15::NPC配置02);
					}
					if (file_var_1e[18] != 2)
					{
						sysReqi(5, NPC19::NPC配置02);
					}
					if (file_var_1e[19] != 2)
					{
						sysReqi(5, NPC20::NPC配置02);
					}
					if (file_var_1e[20] != 2)
					{
						sysReqi(5, NPC21::NPC配置02);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 1)
					{
						sysReqi(5, NPC11::NPC配置01);
					}
					if (file_var_1e[11] != 1)
					{
						sysReqi(5, NPC12::NPC配置01);
					}
					if (file_var_1e[17] != 1)
					{
						sysReqi(5, NPC18::NPC配置01);
					}
					break;
				case 4:
					if (file_var_1e[0] != 2)
					{
						sysReqi(5, NPC01::NPC配置02);
					}
					if (file_var_1e[1] != 2)
					{
						sysReqi(5, NPC02::NPC配置02);
					}
					if (file_var_1e[12] != 2)
					{
						sysReqi(5, NPC13::NPC配置02);
					}
					if (file_var_1e[2] != 2)
					{
						sysReqi(5, NPC03::NPC配置02);
					}
					if (file_var_1e[3] != 2)
					{
						sysReqi(5, NPC04::NPC配置02);
					}
					if (file_var_1e[13] != 2)
					{
						sysReqi(5, NPC14::NPC配置02);
					}
					if (file_var_1e[4] != 2)
					{
						sysReqi(5, NPC05::NPC配置02);
					}
					if (file_var_1e[5] != 2)
					{
						sysReqi(5, NPC06::NPC配置02);
					}
					if (file_var_1e[14] != 2)
					{
						sysReqi(5, NPC15::NPC配置02);
					}
					if (file_var_1e[18] != 2)
					{
						sysReqi(5, NPC19::NPC配置02);
					}
					if (file_var_1e[19] != 2)
					{
						sysReqi(5, NPC20::NPC配置02);
					}
					if (file_var_1e[20] != 2)
					{
						sysReqi(5, NPC21::NPC配置02);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 1)
					{
						sysReqi(5, NPC11::NPC配置01);
					}
					if (file_var_1e[11] != 1)
					{
						sysReqi(5, NPC12::NPC配置01);
					}
					if (file_var_1e[17] != 1)
					{
						sysReqi(5, NPC18::NPC配置01);
					}
					break;
				case 5:
					if (file_var_1e[0] != 1)
					{
						sysReqi(5, NPC01::NPC配置01);
					}
					if (file_var_1e[1] != 1)
					{
						sysReqi(5, NPC02::NPC配置01);
					}
					if (file_var_1e[12] != 1)
					{
						sysReqi(5, NPC13::NPC配置01);
					}
					if (file_var_1e[2] != 1)
					{
						sysReqi(5, NPC03::NPC配置01);
					}
					if (file_var_1e[3] != 1)
					{
						sysReqi(5, NPC04::NPC配置01);
					}
					if (file_var_1e[13] != 1)
					{
						sysReqi(5, NPC14::NPC配置01);
					}
					if (file_var_1e[4] != 1)
					{
						sysReqi(5, NPC05::NPC配置01);
					}
					if (file_var_1e[5] != 1)
					{
						sysReqi(5, NPC06::NPC配置01);
					}
					if (file_var_1e[14] != 1)
					{
						sysReqi(5, NPC15::NPC配置01);
					}
					if (file_var_1e[18] != 1)
					{
						sysReqi(5, NPC19::NPC配置01);
					}
					if (file_var_1e[19] != 1)
					{
						sysReqi(5, NPC20::NPC配置01);
					}
					if (file_var_1e[20] != 1)
					{
						sysReqi(5, NPC21::NPC配置01);
					}
					if (file_var_1e[6] != 1)
					{
						sysReqi(5, NPC07::NPC配置01);
					}
					if (file_var_1e[7] != 1)
					{
						sysReqi(5, NPC08::NPC配置01);
					}
					if (file_var_1e[15] != 1)
					{
						sysReqi(5, NPC16::NPC配置01);
					}
					if (file_var_1e[8] != 2)
					{
						sysReqi(5, NPC09::NPC配置02);
					}
					if (file_var_1e[9] != 2)
					{
						sysReqi(5, NPC10::NPC配置02);
					}
					if (file_var_1e[16] != 2)
					{
						sysReqi(5, NPC17::NPC配置02);
					}
					if (file_var_1e[10] != 2)
					{
						sysReqi(5, NPC11::NPC配置02);
					}
					if (file_var_1e[11] != 2)
					{
						sysReqi(5, NPC12::NPC配置02);
					}
					if (file_var_1e[17] != 2)
					{
						sysReqi(5, NPC18::NPC配置02);
					}
					break;
				case 6:
					if (file_var_1e[0] != 2)
					{
						sysReqi(5, NPC01::NPC配置02);
					}
					if (file_var_1e[1] != 2)
					{
						sysReqi(5, NPC02::NPC配置02);
					}
					if (file_var_1e[12] != 2)
					{
						sysReqi(5, NPC13::NPC配置02);
					}
					if (file_var_1e[2] != 1)
					{
						sysReqi(5, NPC03::NPC配置01);
					}
					if (file_var_1e[3] != 1)
					{
						sysReqi(5, NPC04::NPC配置01);
					}
					if (file_var_1e[13] != 1)
					{
						sysReqi(5, NPC14::NPC配置01);
					}
					if (file_var_1e[4] != 1)
					{
						sysReqi(5, NPC05::NPC配置01);
					}
					if (file_var_1e[5] != 1)
					{
						sysReqi(5, NPC06::NPC配置01);
					}
					if (file_var_1e[14] != 1)
					{
						sysReqi(5, NPC15::NPC配置01);
					}
					if (file_var_1e[18] != 1)
					{
						sysReqi(5, NPC19::NPC配置01);
					}
					if (file_var_1e[19] != 1)
					{
						sysReqi(5, NPC20::NPC配置01);
					}
					if (file_var_1e[20] != 1)
					{
						sysReqi(5, NPC21::NPC配置01);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 7:
					if (file_var_1e[0] != 2)
					{
						sysReqi(5, NPC01::NPC配置02);
					}
					if (file_var_1e[1] != 2)
					{
						sysReqi(5, NPC02::NPC配置02);
					}
					if (file_var_1e[12] != 2)
					{
						sysReqi(5, NPC13::NPC配置02);
					}
					if (file_var_1e[2] != 2)
					{
						sysReqi(5, NPC03::NPC配置02);
					}
					if (file_var_1e[3] != 2)
					{
						sysReqi(5, NPC04::NPC配置02);
					}
					if (file_var_1e[13] != 2)
					{
						sysReqi(5, NPC14::NPC配置02);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 8:
					if (file_var_1e[0] != 3)
					{
						sysReqi(5, NPC01::NPC配置03);
					}
					if (file_var_1e[1] != 3)
					{
						sysReqi(5, NPC02::NPC配置03);
					}
					if (file_var_1e[12] != 3)
					{
						sysReqi(5, NPC13::NPC配置03);
					}
					if (file_var_1e[2] != 2)
					{
						sysReqi(5, NPC03::NPC配置02);
					}
					if (file_var_1e[3] != 2)
					{
						sysReqi(5, NPC04::NPC配置02);
					}
					if (file_var_1e[13] != 2)
					{
						sysReqi(5, NPC14::NPC配置02);
					}
					if (file_var_1e[4] != 2)
					{
						sysReqi(5, NPC05::NPC配置02);
					}
					if (file_var_1e[5] != 2)
					{
						sysReqi(5, NPC06::NPC配置02);
					}
					if (file_var_1e[14] != 2)
					{
						sysReqi(5, NPC15::NPC配置02);
					}
					if (file_var_1e[18] != 2)
					{
						sysReqi(5, NPC19::NPC配置02);
					}
					if (file_var_1e[19] != 2)
					{
						sysReqi(5, NPC20::NPC配置02);
					}
					if (file_var_1e[20] != 2)
					{
						sysReqi(5, NPC21::NPC配置02);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 1)
					{
						sysReqi(5, NPC11::NPC配置01);
					}
					if (file_var_1e[11] != 1)
					{
						sysReqi(5, NPC12::NPC配置01);
					}
					if (file_var_1e[17] != 1)
					{
						sysReqi(5, NPC18::NPC配置01);
					}
					break;
				case 9:
					if (file_var_1e[0] != 4)
					{
						sysReqi(5, NPC01::NPC配置04);
					}
					if (file_var_1e[1] != 4)
					{
						sysReqi(5, NPC02::NPC配置04);
					}
					if (file_var_1e[12] != 4)
					{
						sysReqi(5, NPC13::NPC配置04);
					}
					if (file_var_1e[2] != 3)
					{
						sysReqi(5, NPC03::NPC配置03);
					}
					if (file_var_1e[3] != 3)
					{
						sysReqi(5, NPC04::NPC配置03);
					}
					if (file_var_1e[13] != 3)
					{
						sysReqi(5, NPC14::NPC配置03);
					}
					if (file_var_1e[4] != 1)
					{
						sysReqi(5, NPC05::NPC配置01);
					}
					if (file_var_1e[5] != 1)
					{
						sysReqi(5, NPC06::NPC配置01);
					}
					if (file_var_1e[14] != 1)
					{
						sysReqi(5, NPC15::NPC配置01);
					}
					if (file_var_1e[18] != 1)
					{
						sysReqi(5, NPC19::NPC配置01);
					}
					if (file_var_1e[19] != 1)
					{
						sysReqi(5, NPC20::NPC配置01);
					}
					if (file_var_1e[20] != 1)
					{
						sysReqi(5, NPC21::NPC配置01);
					}
					if (file_var_1e[6] != 1)
					{
						sysReqi(5, NPC07::NPC配置01);
					}
					if (file_var_1e[7] != 1)
					{
						sysReqi(5, NPC08::NPC配置01);
					}
					if (file_var_1e[15] != 1)
					{
						sysReqi(5, NPC16::NPC配置01);
					}
					if (file_var_1e[8] != 2)
					{
						sysReqi(5, NPC09::NPC配置02);
					}
					if (file_var_1e[9] != 2)
					{
						sysReqi(5, NPC10::NPC配置02);
					}
					if (file_var_1e[16] != 2)
					{
						sysReqi(5, NPC17::NPC配置02);
					}
					if (file_var_1e[10] != 2)
					{
						sysReqi(5, NPC11::NPC配置02);
					}
					if (file_var_1e[11] != 2)
					{
						sysReqi(5, NPC12::NPC配置02);
					}
					if (file_var_1e[17] != 2)
					{
						sysReqi(5, NPC18::NPC配置02);
					}
					break;
				case 10:
					if (file_var_1e[0] != 4)
					{
						sysReqi(5, NPC01::NPC配置04);
					}
					if (file_var_1e[1] != 4)
					{
						sysReqi(5, NPC02::NPC配置04);
					}
					if (file_var_1e[12] != 4)
					{
						sysReqi(5, NPC13::NPC配置04);
					}
					if (file_var_1e[2] != 3)
					{
						sysReqi(5, NPC03::NPC配置03);
					}
					if (file_var_1e[3] != 3)
					{
						sysReqi(5, NPC04::NPC配置03);
					}
					if (file_var_1e[13] != 3)
					{
						sysReqi(5, NPC14::NPC配置03);
					}
					if (file_var_1e[4] != 1)
					{
						sysReqi(5, NPC05::NPC配置01);
					}
					if (file_var_1e[5] != 1)
					{
						sysReqi(5, NPC06::NPC配置01);
					}
					if (file_var_1e[14] != 1)
					{
						sysReqi(5, NPC15::NPC配置01);
					}
					if (file_var_1e[18] != 1)
					{
						sysReqi(5, NPC19::NPC配置01);
					}
					if (file_var_1e[19] != 1)
					{
						sysReqi(5, NPC20::NPC配置01);
					}
					if (file_var_1e[20] != 1)
					{
						sysReqi(5, NPC21::NPC配置01);
					}
					if (file_var_1e[6] != 1)
					{
						sysReqi(5, NPC07::NPC配置01);
					}
					if (file_var_1e[7] != 1)
					{
						sysReqi(5, NPC08::NPC配置01);
					}
					if (file_var_1e[15] != 1)
					{
						sysReqi(5, NPC16::NPC配置01);
					}
					if (file_var_1e[8] != 2)
					{
						sysReqi(5, NPC09::NPC配置02);
					}
					if (file_var_1e[9] != 2)
					{
						sysReqi(5, NPC10::NPC配置02);
					}
					if (file_var_1e[16] != 2)
					{
						sysReqi(5, NPC17::NPC配置02);
					}
					if (file_var_1e[10] != 2)
					{
						sysReqi(5, NPC11::NPC配置02);
					}
					if (file_var_1e[11] != 2)
					{
						sysReqi(5, NPC12::NPC配置02);
					}
					if (file_var_1e[17] != 2)
					{
						sysReqi(5, NPC18::NPC配置02);
					}
					break;
				case 11:
					if (file_var_1e[0] != 4)
					{
						sysReqi(5, NPC01::NPC配置04);
					}
					if (file_var_1e[1] != 4)
					{
						sysReqi(5, NPC02::NPC配置04);
					}
					if (file_var_1e[12] != 4)
					{
						sysReqi(5, NPC13::NPC配置04);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 1)
					{
						sysReqi(5, NPC09::NPC配置01);
					}
					if (file_var_1e[9] != 1)
					{
						sysReqi(5, NPC10::NPC配置01);
					}
					if (file_var_1e[16] != 1)
					{
						sysReqi(5, NPC17::NPC配置01);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 12:
					if (file_var_1e[0] != 2)
					{
						sysReqi(5, NPC01::NPC配置02);
					}
					if (file_var_1e[1] != 2)
					{
						sysReqi(5, NPC02::NPC配置02);
					}
					if (file_var_1e[12] != 2)
					{
						sysReqi(5, NPC13::NPC配置02);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 13:
					if (file_var_1e[0] != 3)
					{
						sysReqi(5, NPC01::NPC配置03);
					}
					if (file_var_1e[1] != 3)
					{
						sysReqi(5, NPC02::NPC配置03);
					}
					if (file_var_1e[12] != 3)
					{
						sysReqi(5, NPC13::NPC配置03);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 4)
					{
						sysReqi(5, NPC05::NPC配置04);
					}
					if (file_var_1e[5] != 4)
					{
						sysReqi(5, NPC06::NPC配置04);
					}
					if (file_var_1e[14] != 4)
					{
						sysReqi(5, NPC15::NPC配置04);
					}
					if (file_var_1e[18] != 4)
					{
						sysReqi(5, NPC19::NPC配置04);
					}
					if (file_var_1e[19] != 4)
					{
						sysReqi(5, NPC20::NPC配置04);
					}
					if (file_var_1e[20] != 4)
					{
						sysReqi(5, NPC21::NPC配置04);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 1)
					{
						sysReqi(5, NPC11::NPC配置01);
					}
					if (file_var_1e[11] != 1)
					{
						sysReqi(5, NPC12::NPC配置01);
					}
					if (file_var_1e[17] != 1)
					{
						sysReqi(5, NPC18::NPC配置01);
					}
					break;
				case 14:
					if (file_var_1e[0] != 3)
					{
						sysReqi(5, NPC01::NPC配置03);
					}
					if (file_var_1e[1] != 3)
					{
						sysReqi(5, NPC02::NPC配置03);
					}
					if (file_var_1e[12] != 3)
					{
						sysReqi(5, NPC13::NPC配置03);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 4)
					{
						sysReqi(5, NPC05::NPC配置04);
					}
					if (file_var_1e[5] != 4)
					{
						sysReqi(5, NPC06::NPC配置04);
					}
					if (file_var_1e[14] != 4)
					{
						sysReqi(5, NPC15::NPC配置04);
					}
					if (file_var_1e[18] != 4)
					{
						sysReqi(5, NPC19::NPC配置04);
					}
					if (file_var_1e[19] != 4)
					{
						sysReqi(5, NPC20::NPC配置04);
					}
					if (file_var_1e[20] != 4)
					{
						sysReqi(5, NPC21::NPC配置04);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 1)
					{
						sysReqi(5, NPC11::NPC配置01);
					}
					if (file_var_1e[11] != 1)
					{
						sysReqi(5, NPC12::NPC配置01);
					}
					if (file_var_1e[17] != 1)
					{
						sysReqi(5, NPC18::NPC配置01);
					}
					break;
				case 15:
					if (file_var_1e[0] != 4)
					{
						sysReqi(5, NPC01::NPC配置04);
					}
					if (file_var_1e[1] != 4)
					{
						sysReqi(5, NPC02::NPC配置04);
					}
					if (file_var_1e[12] != 4)
					{
						sysReqi(5, NPC13::NPC配置04);
					}
					if (file_var_1e[2] != 3)
					{
						sysReqi(5, NPC03::NPC配置03);
					}
					if (file_var_1e[3] != 3)
					{
						sysReqi(5, NPC04::NPC配置03);
					}
					if (file_var_1e[13] != 3)
					{
						sysReqi(5, NPC14::NPC配置03);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 1)
					{
						sysReqi(5, NPC07::NPC配置01);
					}
					if (file_var_1e[7] != 1)
					{
						sysReqi(5, NPC08::NPC配置01);
					}
					if (file_var_1e[15] != 1)
					{
						sysReqi(5, NPC16::NPC配置01);
					}
					if (file_var_1e[8] != 2)
					{
						sysReqi(5, NPC09::NPC配置02);
					}
					if (file_var_1e[9] != 2)
					{
						sysReqi(5, NPC10::NPC配置02);
					}
					if (file_var_1e[16] != 2)
					{
						sysReqi(5, NPC17::NPC配置02);
					}
					if (file_var_1e[10] != 2)
					{
						sysReqi(5, NPC11::NPC配置02);
					}
					if (file_var_1e[11] != 2)
					{
						sysReqi(5, NPC12::NPC配置02);
					}
					if (file_var_1e[17] != 2)
					{
						sysReqi(5, NPC18::NPC配置02);
					}
					break;
				case 16:
					if (file_var_1e[0] != 4)
					{
						sysReqi(5, NPC01::NPC配置04);
					}
					if (file_var_1e[1] != 4)
					{
						sysReqi(5, NPC02::NPC配置04);
					}
					if (file_var_1e[12] != 4)
					{
						sysReqi(5, NPC13::NPC配置04);
					}
					if (file_var_1e[2] != 3)
					{
						sysReqi(5, NPC03::NPC配置03);
					}
					if (file_var_1e[3] != 3)
					{
						sysReqi(5, NPC04::NPC配置03);
					}
					if (file_var_1e[13] != 3)
					{
						sysReqi(5, NPC14::NPC配置03);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 2)
					{
						sysReqi(5, NPC09::NPC配置02);
					}
					if (file_var_1e[9] != 2)
					{
						sysReqi(5, NPC10::NPC配置02);
					}
					if (file_var_1e[16] != 2)
					{
						sysReqi(5, NPC17::NPC配置02);
					}
					if (file_var_1e[10] != 2)
					{
						sysReqi(5, NPC11::NPC配置02);
					}
					if (file_var_1e[11] != 2)
					{
						sysReqi(5, NPC12::NPC配置02);
					}
					if (file_var_1e[17] != 2)
					{
						sysReqi(5, NPC18::NPC配置02);
					}
					break;
				case 17:
					if (file_var_1e[0] != 4)
					{
						sysReqi(5, NPC01::NPC配置04);
					}
					if (file_var_1e[1] != 4)
					{
						sysReqi(5, NPC02::NPC配置04);
					}
					if (file_var_1e[12] != 4)
					{
						sysReqi(5, NPC13::NPC配置04);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 4)
					{
						sysReqi(5, NPC09::NPC配置04);
					}
					if (file_var_1e[9] != 4)
					{
						sysReqi(5, NPC10::NPC配置04);
					}
					if (file_var_1e[16] != 4)
					{
						sysReqi(5, NPC17::NPC配置04);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 18:
					if (file_var_1e[0] != 5)
					{
						sysReqi(5, NPC01::NPC配置05);
					}
					if (file_var_1e[1] != 5)
					{
						sysReqi(5, NPC02::NPC配置05);
					}
					if (file_var_1e[12] != 5)
					{
						sysReqi(5, NPC13::NPC配置05);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 3)
					{
						sysReqi(5, NPC05::NPC配置03);
					}
					if (file_var_1e[5] != 3)
					{
						sysReqi(5, NPC06::NPC配置03);
					}
					if (file_var_1e[14] != 3)
					{
						sysReqi(5, NPC15::NPC配置03);
					}
					if (file_var_1e[18] != 3)
					{
						sysReqi(5, NPC19::NPC配置03);
					}
					if (file_var_1e[19] != 3)
					{
						sysReqi(5, NPC20::NPC配置03);
					}
					if (file_var_1e[20] != 3)
					{
						sysReqi(5, NPC21::NPC配置03);
					}
					if (file_var_1e[6] != 3)
					{
						sysReqi(5, NPC07::NPC配置03);
					}
					if (file_var_1e[7] != 3)
					{
						sysReqi(5, NPC08::NPC配置03);
					}
					if (file_var_1e[15] != 3)
					{
						sysReqi(5, NPC16::NPC配置03);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 19:
					if (file_var_1e[0] != 3)
					{
						sysReqi(5, NPC01::NPC配置03);
					}
					if (file_var_1e[1] != 3)
					{
						sysReqi(5, NPC02::NPC配置03);
					}
					if (file_var_1e[12] != 3)
					{
						sysReqi(5, NPC13::NPC配置03);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 4)
					{
						sysReqi(5, NPC05::NPC配置04);
					}
					if (file_var_1e[5] != 4)
					{
						sysReqi(5, NPC06::NPC配置04);
					}
					if (file_var_1e[14] != 4)
					{
						sysReqi(5, NPC15::NPC配置04);
					}
					if (file_var_1e[18] != 4)
					{
						sysReqi(5, NPC19::NPC配置04);
					}
					if (file_var_1e[19] != 4)
					{
						sysReqi(5, NPC20::NPC配置04);
					}
					if (file_var_1e[20] != 4)
					{
						sysReqi(5, NPC21::NPC配置04);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 3)
					{
						sysReqi(5, NPC11::NPC配置03);
					}
					if (file_var_1e[11] != 3)
					{
						sysReqi(5, NPC12::NPC配置03);
					}
					if (file_var_1e[17] != 3)
					{
						sysReqi(5, NPC18::NPC配置03);
					}
					break;
				case 20:
					if (file_var_1e[0] != 3)
					{
						sysReqi(5, NPC01::NPC配置03);
					}
					if (file_var_1e[1] != 3)
					{
						sysReqi(5, NPC02::NPC配置03);
					}
					if (file_var_1e[12] != 3)
					{
						sysReqi(5, NPC13::NPC配置03);
					}
					if (file_var_1e[2] != 4)
					{
						sysReqi(5, NPC03::NPC配置04);
					}
					if (file_var_1e[3] != 4)
					{
						sysReqi(5, NPC04::NPC配置04);
					}
					if (file_var_1e[13] != 4)
					{
						sysReqi(5, NPC14::NPC配置04);
					}
					if (file_var_1e[4] != 4)
					{
						sysReqi(5, NPC05::NPC配置04);
					}
					if (file_var_1e[5] != 4)
					{
						sysReqi(5, NPC06::NPC配置04);
					}
					if (file_var_1e[14] != 4)
					{
						sysReqi(5, NPC15::NPC配置04);
					}
					if (file_var_1e[18] != 4)
					{
						sysReqi(5, NPC19::NPC配置04);
					}
					if (file_var_1e[19] != 4)
					{
						sysReqi(5, NPC20::NPC配置04);
					}
					if (file_var_1e[20] != 4)
					{
						sysReqi(5, NPC21::NPC配置04);
					}
					if (file_var_1e[6] != 2)
					{
						sysReqi(5, NPC07::NPC配置02);
					}
					if (file_var_1e[7] != 2)
					{
						sysReqi(5, NPC08::NPC配置02);
					}
					if (file_var_1e[15] != 2)
					{
						sysReqi(5, NPC16::NPC配置02);
					}
					if (file_var_1e[8] != 3)
					{
						sysReqi(5, NPC09::NPC配置03);
					}
					if (file_var_1e[9] != 3)
					{
						sysReqi(5, NPC10::NPC配置03);
					}
					if (file_var_1e[16] != 3)
					{
						sysReqi(5, NPC17::NPC配置03);
					}
					if (file_var_1e[10] != 1)
					{
						sysReqi(5, NPC11::NPC配置01);
					}
					if (file_var_1e[11] != 1)
					{
						sysReqi(5, NPC12::NPC配置01);
					}
					if (file_var_1e[17] != 1)
					{
						sysReqi(5, NPC18::NPC配置01);
					}
					break;
			}
			switch (local_var_33[0])
			{
				case lte(90):
					if (local_var_34 != 1)
					{
						sysReqi(1, パペット監督::パペットＧ０１);
						local_var_34 = 1;
					}
					break;
				case lte(120):
					if (local_var_34 != 2)
					{
						sysReqi(1, パペット監督::パペットＧ０２);
						local_var_34 = 2;
					}
					break;
				case lte(145):
					if (local_var_34 != 3)
					{
						sysReqi(1, パペット監督::パペットＧ０３);
						local_var_34 = 3;
					}
					break;
				default:
					if (local_var_34 != 4)
					{
						sysReqi(1, パペット監督::パペットＧ０４);
						local_var_34 = 4;
					}
					break;
			}
			if (file_var_20 == 1)
			{
				return;
			}
			switch (file_var_1d)
			{
				case 17:
					if (file_var_1f != 1)
					{
						sysReqi(3, EV_BGNPC01::EV_BGNPC配置_BASE01);
					}
					break;
				case 18:
					if (file_var_1f != 2)
					{
						sysReqi(3, EV_BGNPC01::EV_BGNPC配置_BASE02);
					}
					break;
				default:
					if (file_var_1f != 99)
					{
						sysReq(2, EV_BGNPC01::EVBGNPC消す);
					}
					break;
			}
			sysReqwaitall(reqArr4);
			sysReqwaitall(reqArr5);
			sysReqwaitall(reqArr6);
			sysReqwaitall(reqArr7);
			sysReqwaitall(reqArr8);
			if (distance(-1, 54) > 22.5)
			{
				if (file_var_24[0] != 1)
				{
					sysReq(3, NPC01::distance_hidecomp_npc_tag);
				}
			}
			else if (file_var_24[0] != 2)
			{
				sysReq(4, NPC01::distance_clearhidecomp_npc_tag);
			}
			for (local_var_35 = 0; local_var_35 <= 19; local_var_35 = (local_var_35 + 1))
			{
				if (distance(-1, (33 + local_var_35)) > 22.5)
				{
					if (file_var_24[(1 + local_var_35)] != 1)
					{
						//Couldn't get labels for REQ because either script or function is not immediate
						sysReq(3, (33 + local_var_35), 2);
					}
				}
				else if (file_var_24[(1 + local_var_35)] != 2)
				{
					//Couldn't get labels for REQ because either script or function is not immediate
					sysReq(4, (33 + local_var_35), 3);
				}
			}
			wait(1);
		}
	}


	function NPC監視停止()
	{
		return 0;
		return;
	}
}


script EV_BGNPC01(0)
{

	function init()
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_36;            // pos: 0x0;

	function main(1)
	{
		local_var_36 = 0;
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_37;            // pos: 0x1;

	function EV_BGNPC配置_BASE01()
	{
		if (local_var_36 == 0)
		{
			puppetbind(10, 0x3000008, 1, 0);
		}
		puppethide(10);
		puppetsetpos(10, 102.184242, -0.155754998, 156.383698);
		puppetdir(10, 3.12197495);
		puppetfetchambient(10, 101.000275, -0.275570989, 141.015305);
		puppetmotionloop(10, 1);
		puppetmotionstartframe(10, 0);
		puppetmotionloopframe(10, 0, -1);
		puppetmotionplay(10, 0x11000017);
		puppetrgbatrans(10, 1, 1, 1, 0, 0);
		puppetshow(10);
		puppetrgbatrans(10, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_36 == 0)
		{
			puppetbind(11, 0x3000002, 1, 2);
		}
		puppethide(11);
		puppetsetpos(11, 99.57798, -0.155754998, 163.827713);
		puppetdir(11, -0.687186003);
		puppetfetchambient(11, 101.000275, -0.275570989, 141.015305);
		puppetmotionloop(11, 1);
		puppetmotionstartframe(11, 0);
		puppetmotionloopframe(11, 0, -1);
		puppetmotionplay(11, 0x11000002);
		puppetrgbatrans(11, 1, 1, 1, 0, 0);
		puppetshow(11);
		puppetrgbatrans(11, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		local_var_37 = 1;
		goto EV_BGNPC配置_BASE02::localjmp_2;
	}


	function EV_BGNPC配置_BASE02()
	{
		if (local_var_36 == 0)
		{
			puppetbind(10, 0x3000008, 1, 0);
		}
		puppethide(10);
		puppetsetpos(10, 142.766464, -0.255755007, 160.151382);
		puppetdir(10, -2.45260692);
		puppetfetchambient(10, 101.000275, -0.275570989, 141.015305);
		puppetmotionloop(10, 1);
		puppetmotionstartframe(10, 0);
		puppetmotionloopframe(10, 0, -1);
		puppetmotionplay(10, 0x11000017);
		puppetrgbatrans(10, 1, 1, 1, 0, 0);
		puppetshow(10);
		puppetrgbatrans(10, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_36 == 0)
		{
			puppetbind(11, 0x3000002, 1, 2);
		}
		puppethide(11);
		puppetsetpos(11, 139.668106, -0.255755007, 163.605011);
		puppetdir(11, -0.405321985);
		puppetfetchambient(11, 101.000275, -0.275570989, 141.015305);
		puppetmotionloop(11, 1);
		puppetmotionstartframe(11, 0);
		puppetmotionloopframe(11, 0, -1);
		puppetmotionplay(11, 0x11000002);
		puppetrgbatrans(11, 1, 1, 1, 0, 0);
		puppetshow(11);
		puppetrgbatrans(11, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		local_var_37 = 2;
	localjmp_2:
		file_var_1f = local_var_37;
		return;
	}


	function EVBGNPC消す()
	{
		local_var_36 = 0;
		file_var_1f = 99;
		puppetbindoff(10);
		puppetbindoff(11);
		return;
	}
}


script パペット監督(0)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_38;            // pos: 0x0;

	function init()
	{
		local_var_38 = 0;
		return;
	}


	function パペットＧ０１()
	{
		if (local_var_38 == 0)
		{
			puppetbind(0, 0x3000002, 1, 0);
		}
		puppethide(0);
		puppetsetpos(0, 59.334156, 0.470423996, 88.7182846);
		puppetdir(0, 0.829936981);
		puppetfetchambient(0, 60.7516937, -0.124371998, 89.4124069);
		puppetmotionloop(0, 1);
		puppetmotionstartframe(0, 0);
		puppetmotionloopframe(0, 0, -1);
		puppetmotionplay(0, 0x11000003);
		puppetrgbatrans(0, 1, 1, 1, 0, 0);
		puppetshow(0);
		puppetrgbatrans(0, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(1, 0x3000008, 2, 2);
		}
		puppethide(1);
		puppetsetpos(1, 55.7973137, -0.0140110003, 100.05101);
		puppetdir(1, 1.41822195);
		puppetfetchambient(1, 57.357914, -0.111071996, 100.581375);
		puppetmotionloop(1, 1);
		puppetmotionstartframe(1, 0);
		puppetmotionloopframe(1, 0, -1);
		puppetmotionplay(1, 0x11000015);
		puppetrgbatrans(1, 1, 1, 1, 0, 0);
		puppetshow(1);
		puppetrgbatrans(1, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(2, 0x3000005, 3, 4);
		}
		puppethide(2);
		puppetsetpos(2, 75.9726944, 0.00382400001, 103.851151);
		puppetdir(2, 2.99348402);
		puppetfetchambient(2, 86.556076, -0.173061997, 115.799973);
		puppetmotionloop(2, 1);
		puppetmotionstartframe(2, 0);
		puppetmotionloopframe(2, 0, -1);
		puppetmotionplay(2, 0x1100001a);
		puppetrgbatrans(2, 1, 1, 1, 0, 0);
		puppetshow(2);
		puppetrgbatrans(2, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(3, 0x3000003, 2, 2);
		}
		puppethide(3);
		puppetsetpos(3, 43.6707077, 0.181268007, 97.3150787);
		puppetdir(3, 1.69845903);
		puppetfetchambient(3, 108.455238, 0.0596170016, 109.592987);
		puppetmotionloop(3, 1);
		puppetmotionstartframe(3, 0);
		puppetmotionloopframe(3, 0, -1);
		puppetmotionplay(3, 0x1100000a);
		puppetrgbatrans(3, 1, 1, 1, 0, 0);
		puppetshow(3);
		puppetrgbatrans(3, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(4, 0x3000006, 1, 4);
		}
		puppethide(4);
		puppetsetpos(4, 68.5425568, -0.0517419986, 121.740837);
		puppetdir(4, -2.57383299);
		puppetfetchambient(4, 69.2659531, -0.197936997, 117.954926);
		puppetmotionloop(4, 1);
		puppetmotionstartframe(4, 0);
		puppetmotionloopframe(4, 0, -1);
		puppetmotionplay(4, 0x11000013);
		puppetrgbatrans(4, 1, 1, 1, 0, 0);
		puppetshow(4);
		puppetrgbatrans(4, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			local_var_38 = 1;
		}
		return;
	}


	function パペットＧ０２()
	{
		if (local_var_38 == 0)
		{
			puppetbind(0, 0x3000002, 1, 3);
		}
		puppethide(0);
		puppetsetpos(0, 114.063858, 0.163047001, 140.512146);
		puppetdir(0, -1.52384901);
		puppetfetchambient(0, 110.700005, -0.136952996, 140.17952);
		puppetmotionloop(0, 1);
		puppetmotionstartframe(0, 0);
		puppetmotionloopframe(0, 0, -1);
		puppetmotionplay(0, 0x11000005);
		puppetrgbatrans(0, 1, 1, 1, 0, 0);
		puppetshow(0);
		puppetrgbatrans(0, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(1, 0x3000008, 2, 2);
		}
		puppethide(1);
		puppetsetpos(1, 94.6070175, 0.68967098, 147.546066);
		puppetdir(1, -0.0691580027);
		puppetfetchambient(1, 97.3744965, -0.109385997, 148.454269);
		puppetmotionloop(1, 1);
		puppetmotionstartframe(1, 150);
		puppetmotionloopframe(1, 150, 200);
		puppetmotionplay(1, 0x11000016);
		puppetrgbatrans(1, 1, 1, 1, 0, 0);
		puppetshow(1);
		puppetrgbatrans(1, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(2, 0x3000005, 3, 4);
		}
		puppethide(2);
		puppetsetpos(2, 95.1412201, -0.118812002, 139.217896);
		puppetdir(2, 1.00603604);
		puppetfetchambient(2, 96.9500122, -0.0674889982, 139.533493);
		puppetmotionloop(2, 1);
		puppetmotionstartframe(2, 0);
		puppetmotionloopframe(2, 0, -1);
		puppetmotionplay(2, 0x11000019);
		puppetrgbatrans(2, 1, 1, 1, 0, 0);
		puppetshow(2);
		puppetrgbatrans(2, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(3, 0x3000003, 4, 2);
		}
		puppethide(3);
		puppetsetpos(3, 112.573929, 0.0124420002, 92.5358582);
		puppetdir(3, -1.35271895);
		puppetfetchambient(3, 110.700005, -0.137463003, 92.2663498);
		puppetmotionloop(3, 1);
		puppetmotionstartframe(3, 0);
		puppetmotionloopframe(3, 0, -1);
		puppetmotionplay(3, 0x11000009);
		puppetrgbatrans(3, 1, 1, 1, 0, 0);
		puppetshow(3);
		puppetrgbatrans(3, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(4, 0x3000006, 1, 4);
		}
		puppethide(4);
		puppetsetpos(4, 94.3689575, -0.0505010001, 76.9027481);
		puppetdir(4, -0.357850999);
		puppetfetchambient(4, 96.9500046, -0.0517649986, 76.5931015);
		puppetmotionloop(4, 1);
		puppetmotionstartframe(4, 0);
		puppetmotionloopframe(4, 0, -1);
		puppetmotionplay(4, 0x11000012);
		puppetrgbatrans(4, 1, 1, 1, 0, 0);
		puppetshow(4);
		puppetrgbatrans(4, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			local_var_38 = 1;
		}
		return;
	}


	function パペットＧ０３()
	{
		if (local_var_38 == 0)
		{
			puppetbind(0, 0x3000002, 1, 3);
		}
		puppethide(0);
		puppetsetpos(0, 144.070404, 0.00461499998, 148.033386);
		puppetdir(0, -1.16265798);
		puppetfetchambient(0, 142.54306, -0.121289, 147.839554);
		puppetmotionloop(0, 1);
		puppetmotionstartframe(0, 0);
		puppetmotionloopframe(0, 0, -1);
		puppetmotionplay(0, 0x11000006);
		puppetrgbatrans(0, 1, 1, 1, 0, 0);
		puppetshow(0);
		puppetrgbatrans(0, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(1, 0x3000008, 2, 2);
		}
		puppethide(1);
		puppetsetpos(1, 144.957474, -0.0758450031, 100.751228);
		puppetdir(1, -1.36910295);
		puppetfetchambient(1, 143.049988, -0.0758939981, 99.613678);
		puppetmotionloop(1, 1);
		puppetmotionstartframe(1, 0);
		puppetmotionloopframe(1, 0, -1);
		puppetmotionplay(1, 0x11000015);
		puppetrgbatrans(1, 1, 1, 1, 0, 0);
		puppetshow(1);
		puppetrgbatrans(1, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(2, 0x3000005, 3, 4);
		}
		puppethide(2);
		puppetsetpos(2, 144.704437, -0.0361940004, 91.319603);
		puppetdir(2, -1.61536598);
		puppetfetchambient(2, 142.630844, -0.101524003, 91.3437271);
		puppetmotionloop(2, 1);
		puppetmotionstartframe(2, 0);
		puppetmotionloopframe(2, 0, -1);
		puppetmotionplay(2, 0x11000019);
		puppetrgbatrans(2, 1, 1, 1, 0, 0);
		puppetshow(2);
		puppetrgbatrans(2, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(3, 0x3000003, 4, 2);
		}
		puppethide(3);
		puppetsetpos(3, 144.127029, 0.0187650006, 139.832703);
		puppetdir(3, 2.55253196);
		puppetfetchambient(3, 143.038971, -0.055918999, 139.144257);
		puppetmotionloop(3, 1);
		puppetmotionstartframe(3, 0);
		puppetmotionloopframe(3, 0, -1);
		puppetmotionplay(3, 0x1100000b);
		puppetrgbatrans(3, 1, 1, 1, 0, 0);
		puppetshow(3);
		puppetrgbatrans(3, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(4, 0x3000006, 1, 4);
		}
		puppethide(4);
		puppetsetpos(4, 136.516769, -0.0904610008, 74.8908234);
		puppetdir(4, 1.79995596);
		puppetfetchambient(4, 137.747131, -0.160262004, 75.2101822);
		puppetmotionloop(4, 1);
		puppetmotionstartframe(4, 0);
		puppetmotionloopframe(4, 0, -1);
		puppetmotionplay(4, 0x11000011);
		puppetrgbatrans(4, 1, 1, 1, 0, 0);
		puppetshow(4);
		puppetrgbatrans(4, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			local_var_38 = 1;
		}
		return;
	}


	function パペットＧ０４()
	{
		if (local_var_38 == 0)
		{
			puppetbind(0, 0x3000002, 1, 3);
		}
		puppethide(0);
		puppetsetpos(0, 195.429977, -0.0176669993, 96.2703857);
		puppetdir(0, 3.078866);
		puppetfetchambient(0, 195.630219, -0.117687002, 95.0500031);
		puppetmotionloop(0, 1);
		puppetmotionstartframe(0, 0);
		puppetmotionloopframe(0, 0, -1);
		puppetmotionplay(0, 0x11000006);
		puppetrgbatrans(0, 1, 1, 1, 0, 0);
		puppetshow(0);
		puppetrgbatrans(0, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(1, 0x3000008, 2, 2);
		}
		puppethide(1);
		puppetsetpos(1, 180.169434, 0.0135749998, 86.6040115);
		puppetdir(1, 0.168577999);
		puppetfetchambient(1, 97.3744965, -0.109385997, 148.454269);
		puppetmotionloop(1, 1);
		puppetmotionstartframe(1, 0);
		puppetmotionloopframe(1, 0, -1);
		puppetmotionplay(1, 0x11000016);
		puppetrgbatrans(1, 1, 1, 1, 0, 0);
		puppetshow(1);
		puppetrgbatrans(1, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(2, 0x3000005, 3, 4);
		}
		puppethide(2);
		puppetsetpos(2, 168.057571, -0.0256479997, 100.069366);
		puppetdir(2, 2.18748403);
		puppetfetchambient(2, 168.926376, -0.139265999, 100.335518);
		puppetmotionloop(2, 1);
		puppetmotionstartframe(2, 0);
		puppetmotionloopframe(2, 0, -1);
		puppetmotionplay(2, 0x1100001a);
		puppetrgbatrans(2, 1, 1, 1, 0, 0);
		puppetshow(2);
		puppetrgbatrans(2, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(3, 0x3000003, 4, 2);
		}
		puppethide(3);
		puppetsetpos(3, 159.804047, -0.0114249997, 67.5091324);
		puppetdir(3, 0.89813298);
		puppetfetchambient(3, 110.700005, -0.137463003, 92.2663498);
		puppetmotionloop(3, 1);
		puppetmotionstartframe(3, 0);
		puppetmotionloopframe(3, 0, -1);
		puppetmotionplay(3, 0x11000009);
		puppetrgbatrans(3, 1, 1, 1, 0, 0);
		puppetshow(3);
		puppetrgbatrans(3, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			puppetbind(4, 0x3000006, 1, 4);
		}
		puppethide(4);
		puppetsetpos(4, 158.687332, 0.674498022, 91.5108566);
		puppetdir(4, 0.329382986);
		puppetfetchambient(4, 161.437012, -0.156193003, 92.884407);
		puppetmotionloop(4, 1);
		puppetmotionstartframe(4, 0);
		puppetmotionloopframe(4, 0, -1);
		puppetmotionplay(4, 0x11000014);
		puppetrgbatrans(4, 1, 1, 1, 0, 0);
		puppetshow(4);
		puppetrgbatrans(4, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (local_var_38 == 0)
		{
			local_var_38 = 1;
		}
		return;
	}
}


script イベント特殊効果画面(0)
{

	function init()
	{
		return;
	}


	function だいじなものゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x10000b2, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x10000b2, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_MJFADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout_d0(2, 15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆メッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000b2, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆ゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_2, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_GET表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000b2, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_3, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_SYSMES表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000b2, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_3);
		ames_1fe(0, 0x10000b2, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}
}


script Map_Director(0)
{

	function init()
	{
		setnavimapindex(0);
		hidemapmodel(30);
		setmapidwall(3, 5, 1);
		setmapidwall(3, 6, 1);
		setmapjumpgroupflag(0x156);
		if ((シナリオフラグ >= 180 && シナリオフラグ < 0x155))
		{
			steppoint(0);
			setmapjumpgroupflag(0x150);
		}
		else if (シナリオフラグ >= 0x155)
		{
			steppoint(1);
			releasemapjumpgroupflag(0x156);
		}
		else
		{
			steppoint(2);
			releasemapjumpgroupflag(0x150);
		}
		resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
		return;
	}


	function main(1)
	{
		return;
	}
}


script 制御レクト(1)
{

	function init()
	{
		return;
	}


	function セット()
	{
		setrect_25(92.0003586, -0.263594002, 53.9092979, 1, 1, 0);
		setwh(2.0999999, 2.5, 1.5, 2);
		reqenable(2);
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		fieldsignicon(3);
		fieldsignmes(0x1000032);
		showfieldsign();
		settouchuconly(1);
		talkang(1.57079637);
		if ((nowjumpindex() == 10 || nowjumpindex() == 9))
		{
			rectdisable();
		}
		else
		{
			rectenable();
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_39;            // pos: 0x0;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setmeswincaptionid(0, 3);
		amese(0, 0x1000033);
		messync(0, 1);
		if ((!((ミュートと集積場フラグ[0] & 4)) && シナリオフラグ < 180))
		{
			sysReq(2, ミュート::集積所オープン位置00);
			wait(20);
			fadeout(15);
			fadesync();
			g_com_navi_footcalc[0] = getx_12b(-1);
			g_com_navi_footcalc[1] = gety_12c(-1);
			g_com_navi_footcalc[2] = getz_12d(-1);
			setnavimapfootmarkstatus(0);
			sethpmenufast(0);
			settrapshowstatus(0);
			setcharseplayall(0);
			if (!(istownmap()))
			{
				setstatuserrordispdenystatus(1);
			}
			unkCall_5ac(1, 2.35800004, 100);
			sysReq(3, ミュート::集積所オープン位置);
			sysReq(1, 常駐カメラ::in_to_syuseki_cam01);
			partyusemapid(1);
			setposparty(92.6315155, -0.275323004, 54.9561882, 3.08947206);
			sysReqwait(ミュート::集積所オープン位置);
			wait(5);
			sethpmenufast(0);
			settrapshowstatus(0);
			setcharseplayall(1);
			fadein(15);
			ヴァン.capturepc(-1);
			ヴァン.setaturnlookatlockstatus(1);
			ヴァン.sysAturna(ミュート);
			ヴァン.sysLookata(ミュート);
			setmeswincaptionid(0, 1);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			askpos(0, 0, 127);
			local_var_39 = aaske(0, 0x1000034);
			mesclose(0);
			messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			switch (local_var_39)
			{
				case 0:
					ミュート.setkutipakustatus(1);
					ミュート.setunazukistatus(1);
					ミュート.amese(0, 0x1000035);
					ミュート.messync(0, 1);
					ミュート.setkutipakustatus(0);
					ミュート.setunazukistatus(0);
					sysReq(1, ミュート::集積所オープン移動);
					wait(8);
					sysReq(1, 常駐カメラ::in_to_syuseki_cam02);
					wait(15);
					fadeout(15);
					fadesync();
					sysReq(2, ミュート::集積所オープン位置);
					sysReq(1, 常駐カメラ::in_to_syuseki_cam01);
					wait(15);
					sebsoundplay(0, 30);
					wait(30);
					fadein(15);
					ミュート.setkutipakustatus(1);
					ミュート.setunazukistatus(1);
					ミュート.amese(0, 0x1000036);
					ミュート.messync(0, 1);
					ミュート.setkutipakustatus(0);
					ミュート.setunazukistatus(0);
					wait(5);
					sysReq(1, ミュート::集積所オープン元の位置に戻る);
					rectdisable();
					sysReq(1, map集積所扉南側::フィールドサインＯＮ);
					ミュートと集積場フラグ[0] = (ミュートと集積場フラグ[0] | 4);
					break;
				default:
					ミュート.setkutipakustatus(1);
					ミュート.setunazukistatus(1);
					ミュート.amese(0, 0x1000037);
					ミュート.messync(0, 1);
					ミュート.setkutipakustatus(0);
					ミュート.setunazukistatus(0);
					sysReq(1, ミュート::集積所オープン元の位置に戻る);
					break;
			}
			fadeout(15);
			fadesync();
			setcharseplayall(0);
			sethpmenufast(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			setstatuserrordispdenystatus(0);
			unkCall_5ac(0, 0, 0);
			ヴァン.lookatoff();
			ヴァン.releasepc();
			resetbehindcamera(2.96182299, 0.600000024);
			cameraclear();
			wait(5);
			showparty();
			if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
			{
				clearnavimapfootmark();
			}
			setnavimapfootmarkstatus(1);
			setcharseplayall(1);
			sethpmenufast(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			wait(1);
			fadein(15);
		}
		else
		{
			sysReq(2, ミュート::集積所オープン位置00_02);
			wait(15);
			setmeswincaptionid(0, 1);
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			askpos(0, 0, 127);
			local_var_39 = aaske(0, 0x1000034);
			mesclose(0);
			messync(0, 1);
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
			switch (local_var_39)
			{
				case 0:
					sysReqwait(ミュート::集積所オープン位置00_02);
					sysReq(1, ミュート::集積所オープン移動);
					wait(15);
					fadeout(5);
					fadesync();
					sysReq(2, ミュート::集積所オープン位置);
					wait(5);
					sebsoundplay(0, 30);
					wait(10);
					fadein(5);
					if (!((シナリオフラグ >= 180 && (ミュートと集積場フラグ[0] & 2))))
					{
						ミュート.setkutipakustatus(1);
						ミュート.setunazukistatus(1);
						ミュート.amese(0, 0x1000038);
						ミュート.messync(0, 1);
						ミュート.setkutipakustatus(0);
						ミュート.setunazukistatus(0);
					}
					wait(5);
					sysReq(1, ミュート::集積所オープン元の位置に戻る);
					rectdisable();
					sysReq(1, map集積所扉南側::フィールドサインＯＮ);
					break;
				default:
					ミュート.setkutipakustatus(1);
					ミュート.setunazukistatus(1);
					ミュート.amese(0, 0x1000037);
					ミュート.messync(0, 1);
					ミュート.setkutipakustatus(0);
					ミュート.setunazukistatus(0);
					sysReqwait(ミュート::集積所オープン位置00_02);
					sysReq(1, ミュート::集積所オープン元の位置に戻る);
					break;
			}
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function fieldsign1(8)
	{
		return;
	}


	function fieldsign2(13)
	{
		return;
	}


	function rect_on()
	{
		rectenable();
		return;
	}
}


script 制御レクト０２(1)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_3a;            // pos: 0x0;

	function touch(3)
	{
		rectdisable();
		ucoff();
		if (!(istownmap()))
		{
			suspendbattle();
		}
		rectdisable();
		switch (local_var_3a)
		{
			case 6:
				sethpmenu(0);
				settrapshowstatus(0);
				break;
		}
		wait(5);
		fadeout(15);
		fadesync();
		g_com_navi_footcalc[0] = getx_12b(-1);
		g_com_navi_footcalc[1] = gety_12c(-1);
		g_com_navi_footcalc[2] = getz_12d(-1);
		setnavimapfootmarkstatus(0);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(0);
		if (!(istownmap()))
		{
			setstatuserrordispdenystatus(1);
		}
		unkCall_5ac(1, 2.35800004, 100);
		wait(5);
		partyusemapid(1);
		setposparty(91.8773575, -0.271838009, 53.2539825, 3.02951598);
		hideparty();
		sysReq(1, ヴァン::ミュート扉開け配置);
		sysReqwait(ヴァン::ミュート扉開け配置);
		camerastart_7e(0, 0x2000004);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(1);
		fadein(15);
		ヴァン.sysLookata(ミュート);
		ミュート.stdmotionplay_2c2(0x1000016, 5);
		ミュート.setkutipakustatus(1);
		ミュート.setunazukistatus(1);
		ミュート.amese(0, 0x1000039);
		ミュート.messync(0, 1);
		ミュート.setkutipakustatus(0);
		ミュート.setunazukistatus(0);
		ヴァン.stdmotionplay_2c2(0x1000012, 5);
		ミュート.stdmotionplay_2c2(0x1000010, 5);
		ミュート.setkutipakustatus(1);
		ミュート.setunazukistatus(1);
		ミュート.amese(0, 0x100003a);
		ミュート.messync(0, 1);
		ミュート.setkutipakustatus(0);
		ミュート.setunazukistatus(0);
		ヴァン.stdmotionplay_2c2(0x1000010, 5);
		sysReq(1, ミュート::水路オープン位置移動３);
		wait(30);
		fadelayer(5);
		fadeout(15);
		fadesync();
		setcharseplayall(0);
		wait(10);
		sebsoundplay(0, 31);
		ミュート.setkutipakustatus(1);
		ミュート.setunazukistatus(1);
		ミュート.amese(0, 0x100003b);
		ミュート.messync(0, 1);
		ミュート.setkutipakustatus(0);
		ミュート.setunazukistatus(0);
		wait(15);
		sebsoundplay(0, 31);
		ミュート.setkutipakustatus(1);
		ミュート.setunazukistatus(1);
		ミュート.amese(0, 0x100003c);
		ミュート.messync(0, 1);
		ミュート.setkutipakustatus(0);
		ミュート.setunazukistatus(0);
		wait(20);
		sebsoundplay(0, 30);
		camerastart_7e(0, 0x2000005);
		wait(20);
		sysReqwait(ミュート::水路オープン位置移動３);
		sysReq(1, ミュート::水路オープン位置移動４);
		wait(2);
		setcharseplayall(1);
		fadein(15);
		sysReqwait(ミュート::水路オープン位置移動４);
		ミュート.stdmotionplay(0x1000010);
		ミュート.setkutipakustatus(1);
		ミュート.setunazukistatus(1);
		ミュート.amese(0, 0x100003d);
		ミュート.messync(0, 1);
		ミュート.setkutipakustatus(0);
		ミュート.setunazukistatus(0);
		ミュート.ramove_4cf(91.688736, -0.27018401, 52.2347527, ミュート.getdefaultwalkspeed());
		ヴァン.stdmotionplay_2c2(0x1000014, 5);
		sebsoundplay(0, 39);
		additemmes(0x9054, 1);
		wait(10);
		fadeout(15);
		fadesync();
		setcharseplayall(0);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		setstatuserrordispdenystatus(0);
		unkCall_5ac(0, 0, 0);
		sysReq(2, ミュート::初期位置戻り);
		sysReq(1, ヴァン::バインドオフ);
		ミュートと集積場フラグ[0] = (ミュートと集積場フラグ[0] | 2);
		resetbehindcamera(-1.179667, 0.600000024);
		cameraclear();
		showparty();
		wait(5);
		showparty();
		if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
		{
			clearnavimapfootmark();
		}
		setnavimapfootmarkstatus(1);
		setcharseplayall(1);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		wait(1);
		fadein(15);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function touchon(4)
	{
		return;
	}


	function touchoff(5)
	{
		return;
	}


	function into_grm_set()
	{
		setrect_25(92.1300201, -0.272742987, 51.5170326, 3, 2, 0);
		local_var_3a = 2;
		if ((local_var_3a & 0x10000))
		{
			recttocircle();
		}
		if ((local_var_3a & 0x20000))
		{
			enable_by_chocobo(0);
		}
		else
		{
			enable_by_chocobo(1);
		}
		if ((local_var_3a & 0x40000))
		{
			enable_by_summon(0);
		}
		else
		{
			enable_by_summon(1);
		}
		if ((local_var_3a & 0x80000))
		{
			seteventwakerect(0);
		}
		else
		{
			seteventwakerect(1);
		}
		if ((local_var_3a & 0x100000))
		{
			settouchuconly(1);
		}
		else
		{
			settouchuconly(0);
		}
		local_var_3a = (local_var_3a & 0xffff);
		rectenable();
		return;
	}


	function レクトオフ()
	{
		rectdisable();
		return 0;
		return;
	}
}


script map集積所扉南側(1)
{

	function init()
	{
		if (シナリオフラグ < 0x155)
		{
			fieldsign(9);
			reqenable(2);
			fieldsignicon(2);
		}
		else
		{
			reqdisable(8);
			reqdisable(13);
			reqdisable(2);
			bganimeplay_9e(15, 14, 14);
			setmapidwall(4, 5, 1);
			setmapidwall(4, 6, 1);
		}
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		if (getz_12d(-1) < 53.7999992)
		{
			mp_map_set_angle = 0;
		}
		else
		{
			mp_map_set_angle = 3.14159274;
		}
		sysReqew(1, map_ダミーＰＣ::ターン);
		sysReqew(1, map_ダミーＰＣ::扉オープン);
		mapsoundplay(7);
		bganimeplay_9e(15, 0, 16);
		bganimesync(15);
		setmapidfloor(4, 0, 1);
		setmapidfloor(4, 7, 1);
		setmapidfloor(4, 1, 1);
		setmapidfloor(4, 2, 1);
		setmapidfloor(4, 3, 1);
		setmapidfloor(4, 4, 1);
		setmapidfloor(4, 9, 1);
		setmapidwall(4, 5, 1);
		setmapidwall(4, 6, 1);
		sysReqew(1, map集積所扉北側::フィールドサインＯＦＦ);
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		sysReq(1, 常駐監督::集積場扉開いたら呼んでねタグ);
		sysReqwait(常駐監督::集積場扉開いたら呼んでねタグ);
		setnoupdatebehindcamera(0);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqdisable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}
}


script map集積所扉北側(1)
{

	function init()
	{
		if (シナリオフラグ < 0x155)
		{
			fieldsign(3);
			reqenable(2);
			fieldsignicon(2);
		}
		else
		{
			reqdisable(8);
			reqdisable(13);
			reqdisable(2);
		}
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		if (getz_12d(-1) < 53.7999992)
		{
			mp_map_set_angle = 0;
		}
		else
		{
			mp_map_set_angle = 3.14159274;
		}
		sysReqew(1, map_ダミーＰＣ::ターン);
		sysReqew(1, map_ダミーＰＣ::扉オープン);
		mapsoundplay(7);
		bganimeplay_9e(15, 0, 16);
		wait(6);
		fadelayer(6);
		fadeprior(255);
		fadeout_d0(2, 30);
		wait(30);
		wait(60);
		bganimeplay_9e(15, 0, 0);
		sysReqew(1, map_ダミーＰＣ::map_syuuseki_door);
		sysReq(1, 制御レクト::rect_on);
		fadein_d2(2, 30);
		wait(30);
		setnoupdatebehindcamera(0);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqdisable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}
}


script sml_door_01(1)
{

	function init()
	{
		fieldsign(0);
		reqenable(2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 3.14159274;
		pausesestop();
		sysReqew(1, map_ダミーＰＣ::扉オープン１);
		mp_map_flg = 0;
		mp_map_flg = sebsoundplay(1, 3);
		animeplay(4);
		wait(15);
		regI3 = sysucoff();
		if (regI3)
		{
			clearmapjumpstatus();
			sysucon();
		}
		else
		{
			mp_last_weather = getweatherslot();
			steppoint(3);
			mp_4map = 30;
			fadelayer(6);
			fadeprior(255);
			fadeout_d0(2, mp_4map);
			steppoint(4);
			wait(15);
			steppoint(3);
			spotsoundtrans(15, 0);
			steppoint(2);
			wait(17);
			stopspotsound();
			pausesestop();
			hideparty();
			eventsoundplaysync(mp_map_flg);
			steppoint(1);
			setbattlethinkstatus_freetarget_group(-1, 0);
			voicestopall();
			mapjump(0x121, 3, 1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_3b;            // pos: 0x2;

	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_3b = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_3b = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}


script sml_door_03(1)
{

	function init()
	{
		fieldsign(2);
		reqenable(2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 3.14159274;
		pausesestop();
		sysReqew(1, map_ダミーＰＣ::扉オープン１);
		mp_map_flg = 0;
		mp_map_flg = sebsoundplay(1, 3);
		animeplay(6);
		wait(15);
		regI3 = sysucoff();
		if (regI3)
		{
			clearmapjumpstatus();
			sysucon();
		}
		else
		{
			mp_last_weather = getweatherslot();
			steppoint(3);
			mp_4map = 30;
			fadelayer(6);
			fadeprior(255);
			fadeout_d0(2, mp_4map);
			steppoint(4);
			wait(15);
			steppoint(3);
			spotsoundtrans(15, 0);
			steppoint(2);
			wait(17);
			stopspotsound();
			pausesestop();
			hideparty();
			eventsoundplaysync(mp_map_flg);
			steppoint(1);
			setbattlethinkstatus_freetarget_group(-1, 0);
			voicestopall();
			mapjump(0x123, 3, 1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_3c;            // pos: 0x2;

	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_3c = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_3c = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}


script map閉じこめ民家(1)
{

	function init()
	{
		fieldsign(5);
		reqenable(2);
		fieldsignicon(2);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_3d;            // pos: 0x0;
	char    local_var_3e;            // pos: 0x2;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 1.57079637;
		if (local_var_3e)
		{
			sysReqew(1, map_ダミーＰＣ::ターン);
			setmeswincaptionid(1, 3);
			amese(1, 0x10000b3);
			messync(1, 1);
			setnoupdatebehindcamera(0);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		else
		{
			local_var_3d = 1;
			if (local_var_3d == 1)
			{
				pausesestop();
				sysReqew(1, map_ダミーＰＣ::扉オープン１);
				mp_map_flg = 0;
				startscene(6);
				mp_map_flg = mapsoundplay(6);
				animeplay(3);
				wait(15);
				regI3 = sysucoff();
				if (regI3)
				{
					clearmapjumpstatus();
					sysucon();
				}
				else
				{
					mp_last_weather = getweatherslot();
					steppoint(3);
					mp_4map = 30;
					fadelayer(6);
					fadeprior(255);
					fadeout_d0(2, mp_4map);
					steppoint(4);
					wait(15);
					steppoint(3);
					spotsoundtrans(15, 0);
					steppoint(2);
					wait(17);
					stopspotsound();
					pausesestop();
					hideparty();
					eventsoundplaysync(mp_map_flg);
					steppoint(1);
					setbattlethinkstatus_freetarget_group(-1, 0);
					voicestopall();
					mapjump(0x2c0, 0, 0);
				}
			}
			else
			{
				setnoupdatebehindcamera(0);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
			}
		}
		return;
	}


	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_3e = 0;
		setmapjumpgroupflag(0x14f);
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_3e = 1;
		releasemapjumpgroupflag(0x14f);
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}


script mapガラムサイズ水路扉（最初）(1)
{

	function init()
	{
		fieldsign(7);
		reqenable(2);
		if ((シナリオフラグ >= 180 && シナリオフラグ < 0x155))
		{
			fieldsignicon(2);
		}
		else
		{
			fieldsignicon(3);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_3f;            // pos: 0x2;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 4.71238899;
		if (local_var_3f)
		{
			sysReqew(1, map_ダミーＰＣ::ターン);
			setmeswincaptionid(1, 3);
			amese(1, 0x10000b4);
			messync(1, 1);
			setnoupdatebehindcamera(0);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		else if ((シナリオフラグ >= 180 && シナリオフラグ < 0x155))
		{
			pausesestop();
			sysReqew(1, map_ダミーＰＣ::扉オープン１);
			mp_map_flg = 0;
			mp_map_flg = sebsoundplay(1, 3);
			animeplay(8);
			wait(15);
			regI3 = sysucoff();
			if (regI3)
			{
				clearmapjumpstatus();
				sysucon();
			}
			else
			{
				mp_last_weather = getweatherslot();
				steppoint(3);
				mp_4map = 30;
				fadelayer(6);
				fadeprior(255);
				fadeout_d0(2, mp_4map);
				steppoint(4);
				wait(15);
				steppoint(3);
				spotsoundtrans(15, 0);
				steppoint(2);
				wait(17);
				stopspotsound();
				pausesestop();
				hideparty();
				eventsoundplaysync(mp_map_flg);
				steppoint(1);
				setbattlethinkstatus_freetarget_group(-1, 0);
				voicestopall();
				mapjump(0x137, 2, 1);
			}
		}
		else
		{
			sysReqew(1, map_ダミーＰＣ::ターン);
			setmeswincaptionid(1, 3);
			amese(1, 0x10000b4);
			messync(1, 1);
			setnoupdatebehindcamera(0);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}


	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_3f = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_3f = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}


script sml_grm02(1)
{

	function init()
	{
		fieldsign(8);
		reqenable(2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 1.57079637;
		pausesestop();
		sysReqew(1, map_ダミーＰＣ::扉オープン１);
		mp_map_flg = 0;
		mp_map_flg = sebsoundplay(1, 3);
		animeplay(7);
		wait(15);
		regI3 = sysucoff();
		if (regI3)
		{
			clearmapjumpstatus();
			sysucon();
		}
		else
		{
			mp_last_weather = getweatherslot();
			steppoint(3);
			mp_4map = 30;
			fadelayer(6);
			fadeprior(255);
			fadeout_d0(2, mp_4map);
			steppoint(4);
			wait(15);
			steppoint(3);
			spotsoundtrans(15, 0);
			steppoint(2);
			wait(17);
			stopspotsound();
			pausesestop();
			hideparty();
			eventsoundplaysync(mp_map_flg);
			steppoint(1);
			setbattlethinkstatus_freetarget_group(-1, 0);
			voicestopall();
			mapjump(0x479, 1, 1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_40;            // pos: 0x2;

	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_40 = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_40 = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}


script PC00(5) : 0x80
{

	function init()
	{
		setupbattle(0);
		return;
	}
}


script PC01(5) : 0x81
{

	function init()
	{
		setupbattle(1);
		return;
	}
}


script PC02(5) : 0x82
{

	function init()
	{
		setupbattle(2);
		return;
	}
}


script PC03(5) : 0x83
{

	function init()
	{
		setupbattle(3);
		return;
	}
}


script treasure_00(6)
{

	function init()
	{
		setuptreasure(0x3000000);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000000);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000000);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000000);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_01(6)
{

	function init()
	{
		setuptreasure(0x3000018);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000018);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000018);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000018);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_02(6)
{

	function init()
	{
		setuptreasure(0x3000030);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000030);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000030);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000030);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_03(6)
{

	function init()
	{
		setuptreasure(0x3000048);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000048);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000048);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000048);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_04(6)
{

	function init()
	{
		setuptreasure(0x3000060);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000060);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000060);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000060);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script treasure_05(6)
{

	function init()
	{
		setuptreasure(0x3000078);
		npc_to_gimmick();
		btlAtelTreasureInitWithRange(0.5, 5);
		setnpcname(0x1b2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		btlAtelOpenTreasureStart();
		talktreasure(0x3000078);
		sethpmenu_436(0, 5);
		btlAtelOpenTreasureMessage(0x3000078);
		sethpmenu_436(1, 5);
		talktreasureafter(0x3000078);
		btlAtelOpenTreasureFinish();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}

symlink NPC02(NPC01) : 0x1;

symlink NPC03(NPC01) : 0x2;

symlink NPC04(NPC01) : 0x3;

symlink NPC05(NPC01) : 0x4;

symlink NPC06(NPC01) : 0x5;

symlink NPC07(NPC01) : 0x6;

symlink NPC08(NPC01) : 0x7;

symlink NPC09(NPC01) : 0x8;

symlink NPC10(NPC01) : 0x9;

symlink NPC11(NPC01) : 0xa;

symlink NPC12(NPC01) : 0xb;

symlink NPC13(NPC01) : 0xc;

symlink NPC14(NPC01) : 0xd;

symlink NPC15(NPC01) : 0xe;

symlink NPC16(NPC01) : 0xf;

symlink NPC17(NPC01) : 0x10;

symlink NPC18(NPC01) : 0x11;

symlink NPC19(NPC01) : 0x12;

symlink NPC20(NPC01) : 0x13;

symlink NPC21(NPC01) : 0x14;

symlink LITTSSET02(LITTSSET01) : 0x1;


script NPC01(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_41;            // pos: 0x0;

	function init()
	{
		local_var_41 = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_57;            // pos: 0xc4;

	function distance_hidecomp_npc_tag()
	{
		if (local_var_57 != 1)
		{
			return;
		}
		hidecomplete();
		motionratio(0);
		setwalkspeed(0);
		file_var_24[local_var_41] = 1;
		while (true)
		{
			wait(10);
		}
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_48;            // pos: 0x6c;

	function distance_clearhidecomp_npc_tag()
	{
		if (local_var_57 != 1)
		{
			return;
		}
		if (((local_var_41 == 16 && file_var_1e[local_var_41] == 1) && file_var_23 == 1))
		{
			file_var_24[local_var_41] = 1;
			hidecomplete();
		}
		else
		{
			rgbatrans(1, 1, 1, 0, 0);
			istouchucsync();
			clearhidecomplete();
			rgbatrans(1, 1, 1, 1, 10);
			motionratio(1);
			setwalkspeed(local_var_48);
			file_var_24[local_var_41] = 2;
		}
		return 1;
	}


	function talk(2)
	{
		return;
	}


	function talkhold(16)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_44;            // pos: 0x17;

	function talkterm(17)
	{
		if (local_var_44 != 1)
		{
			if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
			{
				setaturnlookatlockstatus(1);
				aturn_261(getdestrotangzx_227(0));
			}
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_43;            // pos: 0x15;
	float   local_var_45[15];        // pos: 0x18;
	float   local_var_46[5];         // pos: 0x54;
	float   local_var_49[4];         // pos: 0x70;
	float   local_var_4c;            // pos: 0x88;
	int     local_var_4f;            // pos: 0xa4;
	u_char  local_var_51;            // pos: 0xac;
	u_char  local_var_53;            // pos: 0xae;
	u_char  local_var_54;            // pos: 0xaf;
	u_char  local_var_55;            // pos: 0xb0;
	u_char  local_var_58;            // pos: 0xc5;
	float   local_var_59[3];         // pos: 0xc8;

	function NPC配置01()
	{
		local_var_43 = 0;
		local_var_54 = 0;
		local_var_55 = 0;
		local_var_51 = 0;
		local_var_53 = 0;
		local_var_4f = 0;
		local_var_4c = 0;
		local_var_45[0] = 0;
		local_var_45[1] = 0;
		local_var_45[2] = 0;
		local_var_45[3] = 0;
		local_var_45[4] = 0;
		local_var_45[5] = 0;
		local_var_45[6] = 0;
		local_var_45[7] = 0;
		local_var_45[8] = 0;
		local_var_45[9] = 0;
		local_var_45[10] = 0;
		local_var_45[11] = 0;
		local_var_46[0] = 0;
		local_var_46[1] = 0;
		local_var_46[2] = 0;
		local_var_46[3] = 0;
		local_var_49[0] = 0;
		local_var_49[1] = 0;
		local_var_49[2] = 0;
		local_var_49[3] = 0;
		local_var_58 = 0;
		local_var_59[0] = 0;
		local_var_59[1] = 0;
		local_var_59[2] = 0;
		local_var_44 = 0;
		incentryid();
		reqdisable(2);
		switch (local_var_41)
		{
			case 0:
				local_var_43 = 3;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.39999998;
				local_var_45[0] = 46.5264854;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 59.8507347;
				local_var_46[0] = -1.241624;
				local_var_49[0] = 46.5264854;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 59.8507347;
				local_var_49[3] = -1.241624;
				setnpcname(0x248);
				reqenable(2);
				break;
			case 1:
				local_var_43 = 1;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 51.9915237;
				local_var_45[1] = -0.131075993;
				local_var_45[2] = 63.2873917;
				local_var_46[0] = 2.10969901;
				local_var_49[0] = 51.9915237;
				local_var_49[1] = -0.131075993;
				local_var_49[2] = 63.2873917;
				local_var_49[3] = 2.10969901;
				break;
			case 2:
				if (file_var_27 == 0)
				{
					local_var_43 = 2;
					local_var_54 = 3;
					local_var_55 = 0;
					local_var_51 = 0;
					local_var_53 = 1;
					local_var_4f = 0;
					local_var_4c = 1.10000002;
					local_var_45[0] = 76.4179688;
					local_var_45[1] = -0.275570989;
					local_var_45[2] = 61.190136;
					local_var_46[0] = -0.165601;
					local_var_49[0] = 76.4179688;
					local_var_49[1] = -0.275570989;
					local_var_49[2] = 61.190136;
					local_var_49[3] = -0.165601;
				}
				else
				{
					local_var_43 = 2;
					local_var_54 = 3;
					local_var_55 = 0;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 1.10000002;
					local_var_45[0] = 90.1967621;
					local_var_45[1] = -0.240398005;
					local_var_45[2] = 56.8203316;
					local_var_46[0] = 2.65524793;
					local_var_49[0] = 90.1967621;
					local_var_49[1] = -0.240398005;
					local_var_49[2] = 56.8203316;
					local_var_49[3] = 2.65524793;
				}
				setnpcname(0x24b);
				reqenable(2);
				break;
			case 3:
				if (file_var_27 == 0)
				{
					local_var_43 = 4;
					local_var_54 = 3;
					local_var_55 = 0;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 80.8837204;
					local_var_45[1] = -0.135110006;
					local_var_45[2] = 56.6339874;
					local_var_46[0] = -0.353821993;
					local_var_49[0] = 80.8837204;
					local_var_49[1] = -0.135110006;
					local_var_49[2] = 56.6339874;
					local_var_49[3] = -0.353821993;
				}
				else
				{
					local_var_43 = 4;
					local_var_54 = 3;
					local_var_55 = 0;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 92.1456757;
					local_var_45[1] = 0.105769999;
					local_var_45[2] = 44.7902031;
					local_var_46[0] = 2.54174089;
					local_var_49[0] = 92.1456757;
					local_var_49[1] = 0.105769999;
					local_var_49[2] = 44.7902031;
					local_var_49[3] = 2.54174089;
				}
				break;
			case 4:
				if (file_var_27 == 0)
				{
					local_var_43 = 8;
					local_var_54 = 2;
					local_var_55 = 1;
					local_var_51 = 0;
					local_var_53 = 1;
					local_var_4f = 0;
					local_var_4c = 1.29999995;
					local_var_45[0] = 98.9784241;
					local_var_45[1] = -0.275168002;
					local_var_45[2] = 58.997982;
					local_var_46[0] = 2.21587801;
					local_var_49[0] = 98.9784241;
					local_var_49[1] = -0.275168002;
					local_var_49[2] = 58.997982;
					local_var_49[3] = 2.21587801;
				}
				else
				{
					local_var_43 = 8;
					local_var_54 = 2;
					local_var_55 = 1;
					local_var_51 = 0;
					local_var_53 = 1;
					local_var_4f = 0;
					local_var_4c = 1.29999995;
					local_var_45[0] = 94.723999;
					local_var_45[1] = -0.275570989;
					local_var_45[2] = 50.2381248;
					local_var_46[0] = -2.9627831;
					local_var_49[0] = 94.723999;
					local_var_49[1] = -0.275570989;
					local_var_49[2] = 50.2381248;
					local_var_49[3] = -2.9627831;
				}
				break;
			case 5:
				if (file_var_27 == 0)
				{
					local_var_44 = 1;
					local_var_43 = 1;
					local_var_54 = 4;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 94.5581284;
					local_var_45[1] = -1.21459401;
					local_var_45[2] = 62.659874;
					local_var_46[0] = -2.55771804;
					local_var_49[0] = 94.5581284;
					local_var_49[1] = -1.21459401;
					local_var_49[2] = 62.659874;
					local_var_49[3] = -2.55771804;
				}
				else
				{
					local_var_43 = 1;
					local_var_54 = 4;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 95.4209595;
					local_var_45[1] = -0.184525996;
					local_var_45[2] = 45.3790283;
					local_var_46[0] = -2.86196804;
					local_var_49[0] = 95.4209595;
					local_var_49[1] = -0.184525996;
					local_var_49[2] = 45.3790283;
					local_var_49[3] = -2.86196804;
				}
				setnpcname(0x250);
				reqenable(2);
				break;
			case 6:
				local_var_43 = 3;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 75.1515503;
				local_var_45[1] = -0.272199005;
				local_var_45[2] = 66.572525;
				local_var_45[3] = 73.7871933;
				local_var_45[4] = -0.139710993;
				local_var_45[5] = 81.059021;
				local_var_46[0] = 0.0571939982;
				local_var_46[1] = -0.343201011;
				local_var_49[0] = 75.1515503;
				local_var_49[1] = -0.272199005;
				local_var_49[2] = 66.572525;
				local_var_49[3] = 0.0571939982;
				break;
			case 7:
				local_var_44 = 1;
				local_var_43 = 8;
				local_var_54 = 4;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 78.8040619;
				local_var_45[1] = -0.162343994;
				local_var_45[2] = 78.2505493;
				local_var_45[3] = 0;
				local_var_45[4] = 0;
				local_var_45[5] = 0;
				local_var_45[6] = 0;
				local_var_45[7] = 0;
				local_var_45[8] = 0;
				local_var_45[9] = 0;
				local_var_45[10] = 0;
				local_var_45[11] = 0;
				local_var_46[0] = -2.14346099;
				local_var_46[1] = 0;
				local_var_46[2] = 0;
				local_var_46[3] = 0;
				local_var_49[0] = 78.8040619;
				local_var_49[1] = -0.162343994;
				local_var_49[2] = 78.2505493;
				local_var_49[3] = -2.14346099;
				setnpcname(0x253);
				reqenable(2);
				break;
			case 8:
				local_var_44 = 1;
				if (((isquestorder(53) && !(isquestclear(53))) && (0x350000 | getquestscenarioflag(53)) >= 0x350032))
				{
					local_var_43 = 1;
					local_var_54 = 3;
					local_var_55 = 1;
					local_var_51 = 1;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 502.633301;
					local_var_45[1] = -0.143848002;
					local_var_45[2] = 77.9164505;
					local_var_45[3] = 0;
					local_var_45[4] = 0;
					local_var_45[5] = 0;
					local_var_45[6] = 0;
					local_var_45[7] = 0;
					local_var_45[8] = 0;
					local_var_45[9] = 0;
					local_var_45[10] = 0;
					local_var_45[11] = 0;
					local_var_46[0] = -0.692746997;
					local_var_46[1] = 0;
					local_var_46[2] = 0;
					local_var_46[3] = 0;
					local_var_49[0] = 502.633301;
					local_var_49[1] = -0.143848002;
					local_var_49[2] = 77.9164505;
					local_var_49[3] = -0.692746997;
					usemapid(0);
					reqdisable(2);
				}
				else
				{
					local_var_43 = 1;
					local_var_54 = 3;
					local_var_55 = 1;
					local_var_51 = 1;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 102.949081;
					local_var_45[1] = -0.143848002;
					local_var_45[2] = 78.1175613;
					local_var_45[3] = 0;
					local_var_45[4] = 0;
					local_var_45[5] = 0;
					local_var_45[6] = 0;
					local_var_45[7] = 0;
					local_var_45[8] = 0;
					local_var_45[9] = 0;
					local_var_45[10] = 0;
					local_var_45[11] = 0;
					local_var_46[0] = -0.692746997;
					local_var_46[1] = 0;
					local_var_46[2] = 0;
					local_var_46[3] = 0;
					local_var_49[0] = 102.949081;
					local_var_49[1] = -0.143848002;
					local_var_49[2] = 78.1175613;
					local_var_49[3] = -0.692746997;
					setnpcname(0x254);
					reqenable(2);
				}
				break;
			case 9:
				local_var_43 = 4;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 33;
				local_var_4c = 0;
				local_var_45[0] = 99.1930389;
				local_var_45[1] = -0.266317993;
				local_var_45[2] = 89.6306839;
				local_var_45[3] = 98.4678955;
				local_var_45[4] = -0.242717996;
				local_var_45[5] = 77.373024;
				local_var_46[0] = -0.985240996;
				local_var_46[1] = -2.53085399;
				local_var_49[0] = 99.1930389;
				local_var_49[1] = -0.266317993;
				local_var_49[2] = 89.6306839;
				local_var_49[3] = -0.985240996;
				break;
			case 10:
				local_var_43 = 2;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 29;
				local_var_4c = 0;
				local_var_45[0] = 165.832962;
				local_var_45[1] = -0.213653997;
				local_var_45[2] = 77.4612656;
				local_var_45[3] = 165.884537;
				local_var_45[4] = -0.210878998;
				local_var_45[5] = 74.4196014;
				local_var_46[0] = 2.3166101;
				local_var_46[1] = 0.868341029;
				local_var_49[0] = 165.832962;
				local_var_49[1] = -0.213653997;
				local_var_49[2] = 77.4612656;
				local_var_49[3] = 2.3166101;
				setnpcname(0x257);
				reqenable(2);
				break;
			case 11:
				local_var_43 = 8;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 164.19574;
				local_var_45[1] = -0.274378985;
				local_var_45[2] = 68.3704071;
				local_var_46[0] = 0.118725002;
				local_var_49[0] = 164.19574;
				local_var_49[1] = -0.274378985;
				local_var_49[2] = 68.3704071;
				local_var_49[3] = 0.118725002;
				break;
			case 12:
				local_var_43 = 5;
				local_var_54 = 4;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 52.9496994;
				local_var_45[1] = -0.118157998;
				local_var_45[2] = 63.2837105;
				local_var_46[0] = -2.09123707;
				local_var_49[0] = 52.9496994;
				local_var_49[1] = -0.118157998;
				local_var_49[2] = 63.2837105;
				local_var_49[3] = -2.09123707;
				break;
			case 13:
				local_var_43 = 8;
				local_var_54 = 1;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 69.860733;
				local_var_45[1] = -0.226605996;
				local_var_45[2] = 61.4772453;
				local_var_46[0] = 0.770467997;
				local_var_49[0] = 69.860733;
				local_var_49[1] = -0.226605996;
				local_var_49[2] = 61.4772453;
				local_var_49[3] = 0.770467997;
				break;
			case 14:
				if (file_var_27 == 0)
				{
					local_var_43 = 7;
					local_var_54 = 4;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 93.0538483;
					local_var_45[1] = -0.143893003;
					local_var_45[2] = 62.630249;
					local_var_46[0] = 2.60937309;
					local_var_49[0] = 93.0538483;
					local_var_49[1] = -0.143893003;
					local_var_49[2] = 62.630249;
					local_var_49[3] = 2.60937309;
				}
				else
				{
					local_var_43 = 7;
					local_var_54 = 4;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 93.0484085;
					local_var_45[1] = -0.275570989;
					local_var_45[2] = 56.6256943;
					local_var_46[0] = -2.93832707;
					local_var_49[0] = 93.0484085;
					local_var_49[1] = -0.275570989;
					local_var_49[2] = 56.6256943;
					local_var_49[3] = -2.93832707;
				}
				break;
			case 15:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 79.1315536;
				local_var_45[1] = -0.120627999;
				local_var_45[2] = 76.5032959;
				local_var_46[0] = -0.444600999;
				local_var_49[0] = 79.1315536;
				local_var_49[1] = -0.120627999;
				local_var_49[2] = 76.5032959;
				local_var_49[3] = -0.444600999;
				break;
			case 16:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 2;
				local_var_45[0] = 100.976494;
				local_var_45[1] = -0.275366992;
				local_var_45[2] = 68.5214233;
				local_var_46[0] = -2.62610412;
				local_var_49[0] = 100.976494;
				local_var_49[1] = -0.275366992;
				local_var_49[2] = 68.5214233;
				local_var_49[3] = -2.62610412;
				break;
			case 17:
				local_var_43 = 4;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 162.155853;
				local_var_45[1] = -0.415383011;
				local_var_45[2] = 84.0873871;
				local_var_46[0] = -1.081797;
				local_var_49[0] = 162.155853;
				local_var_49[1] = -0.415383011;
				local_var_49[2] = 84.0873871;
				local_var_49[3] = -1.081797;
				break;
			case 18:
				if (((シナリオフラグ >= 0x1004 && isquestclear(58)) && isquestclear(131)))
				{
					local_var_43 = 3;
					local_var_54 = 2;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 589.834778;
					local_var_45[1] = 0.401246995;
					local_var_45[2] = 56.1463356;
					local_var_46[0] = 0.0156110004;
					local_var_49[0] = 589.834778;
					local_var_49[1] = 0.401246995;
					local_var_49[2] = 56.1463356;
					local_var_49[3] = 0.0156110004;
					reqdisable(2);
					usemapid(0);
				}
				else if (file_var_27 == 0)
				{
					local_var_43 = 3;
					local_var_54 = 2;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 89.8347855;
					local_var_45[1] = 0.401246995;
					local_var_45[2] = 56.1463356;
					local_var_46[0] = 0.0156110004;
					local_var_49[0] = 89.8347855;
					local_var_49[1] = 0.401246995;
					local_var_49[2] = 56.1463356;
					local_var_49[3] = 0.0156110004;
				}
				else
				{
					local_var_43 = 3;
					local_var_54 = 2;
					local_var_55 = 2;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 92.3777695;
					local_var_45[1] = 0.105769999;
					local_var_45[2] = 42.001606;
					local_var_46[0] = 2.06441593;
					local_var_49[0] = 92.3777695;
					local_var_49[1] = 0.105769999;
					local_var_49[2] = 42.001606;
					local_var_49[3] = 2.06441593;
				}
				break;
			case 19:
				if (((シナリオフラグ >= 0x1004 && isquestclear(58)) && isquestclear(131)))
				{
					local_var_43 = 4;
					local_var_54 = 3;
					local_var_55 = 1;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 594.352966;
					local_var_45[1] = 1.46561396;
					local_var_45[2] = 55.7362289;
					local_var_46[0] = -0.170139998;
					local_var_49[0] = 594.352966;
					local_var_49[1] = 1.46561396;
					local_var_49[2] = 55.7362289;
					local_var_49[3] = -0.170139998;
					reqdisable(2);
					usemapid(0);
				}
				else if (file_var_27 == 0)
				{
					local_var_43 = 4;
					local_var_54 = 3;
					local_var_55 = 1;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 94.3529892;
					local_var_45[1] = 1.46561396;
					local_var_45[2] = 55.7362289;
					local_var_46[0] = -0.170139998;
					local_var_49[0] = 94.3529892;
					local_var_49[1] = 1.46561396;
					local_var_49[2] = 55.7362289;
					local_var_49[3] = -0.170139998;
					local_var_58 = 1;
					local_var_59[0] = 95.4217682;
					local_var_59[1] = -0.117578998;
					local_var_59[2] = 56.7909241;
				}
				else
				{
					local_var_43 = 4;
					local_var_54 = 3;
					local_var_55 = 1;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 97.4971695;
					local_var_45[1] = 0.715614974;
					local_var_45[2] = 41.6212044;
					local_var_46[0] = -2.24406099;
					local_var_49[0] = 97.4971695;
					local_var_49[1] = 0.715614974;
					local_var_49[2] = 41.6212044;
					local_var_49[3] = -2.24406099;
					local_var_58 = 1;
					local_var_59[0] = 96.1599808;
					local_var_59[1] = 0.105769999;
					local_var_59[2] = 41.8833961;
				}
				break;
			case 20:
				if (((シナリオフラグ >= 0x1004 && isquestclear(58)) && isquestclear(131)))
				{
					local_var_43 = 3;
					local_var_54 = 1;
					local_var_55 = 0;
					local_var_51 = 0;
					local_var_53 = 0;
					local_var_4f = 0;
					local_var_4c = 0;
					local_var_45[0] = 594.454956;
					local_var_45[1] = -0.171851993;
					local_var_45[2] = 56.9025269;
					local_var_46[0] = 3.01530099;
					local_var_49[0] = 594.454956;
					local_var_49[1] = -0.171851993;
					local_var_49[2] = 56.9025269;
					local_var_49[3] = 3.01530099;
					reqdisable(2);
					usemapid(0);
				}
				else
				{
					if (file_var_27 == 0)
					{
						local_var_43 = 3;
						local_var_54 = 1;
						local_var_55 = 0;
						local_var_51 = 0;
						local_var_53 = 0;
						local_var_4f = 0;
						local_var_4c = 0;
						local_var_45[0] = 95.4731674;
						local_var_45[1] = -0.146852002;
						local_var_45[2] = 56.518692;
						local_var_46[0] = -2.40885401;
						local_var_49[0] = 95.4731674;
						local_var_49[1] = -0.146852002;
						local_var_49[2] = 56.518692;
						local_var_49[3] = -2.40885401;
					}
					else
					{
						local_var_43 = 3;
						local_var_54 = 1;
						local_var_55 = 0;
						local_var_51 = 0;
						local_var_53 = 0;
						local_var_4f = 0;
						local_var_4c = 0;
						local_var_45[0] = 96.046608;
						local_var_45[1] = 0.105769999;
						local_var_45[2] = 38.3662338;
						local_var_46[0] = -0.180170998;
						local_var_49[0] = 96.046608;
						local_var_49[1] = 0.105769999;
						local_var_49[2] = 38.3662338;
						local_var_49[3] = -0.180170998;
					}
					setnpcname(0x275);
					reqenable(2);
				}
				break;
		}
		file_var_1e[local_var_41] = 1;
		goto NPC配置05::localjmp_6;
	}


	function NPC配置02()
	{
		local_var_43 = 0;
		local_var_54 = 0;
		local_var_55 = 0;
		local_var_51 = 0;
		local_var_53 = 0;
		local_var_4f = 0;
		local_var_4c = 0;
		local_var_45[0] = 0;
		local_var_45[1] = 0;
		local_var_45[2] = 0;
		local_var_45[3] = 0;
		local_var_45[4] = 0;
		local_var_45[5] = 0;
		local_var_45[6] = 0;
		local_var_45[7] = 0;
		local_var_45[8] = 0;
		local_var_45[9] = 0;
		local_var_45[10] = 0;
		local_var_45[11] = 0;
		local_var_46[0] = 0;
		local_var_46[1] = 0;
		local_var_46[2] = 0;
		local_var_46[3] = 0;
		local_var_49[0] = 0;
		local_var_49[1] = 0;
		local_var_49[2] = 0;
		local_var_49[3] = 0;
		local_var_58 = 0;
		local_var_59[0] = 0;
		local_var_59[1] = 0;
		local_var_59[2] = 0;
		local_var_44 = 0;
		incentryid();
		reqdisable(2);
		switch (local_var_41)
		{
			case 0:
				local_var_43 = 3;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 139.94989;
				local_var_45[1] = -0.269445986;
				local_var_45[2] = 74.6728363;
				local_var_46[0] = 1.36358798;
				local_var_49[0] = 139.94989;
				local_var_49[1] = -0.269445986;
				local_var_49[2] = 74.6728363;
				local_var_49[3] = 1.36358798;
				break;
			case 1:
				local_var_43 = 1;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 122.069992;
				local_var_45[1] = -0.182221994;
				local_var_45[2] = 70.1665268;
				local_var_45[3] = 125.742447;
				local_var_45[4] = -0.142388999;
				local_var_45[5] = 70.2810059;
				local_var_46[0] = 2.96096206;
				local_var_46[1] = -2.66177392;
				local_var_49[0] = 122.069992;
				local_var_49[1] = -0.182221994;
				local_var_49[2] = 70.1665268;
				local_var_49[3] = 2.96096206;
				setnpcname(0x24a);
				reqenable(2);
				break;
			case 2:
				local_var_43 = 2;
				local_var_54 = 3;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.5;
				local_var_45[0] = 140.064133;
				local_var_45[1] = -0.272197992;
				local_var_45[2] = 56.6699829;
				local_var_46[0] = 3.10874391;
				local_var_49[0] = 140.064133;
				local_var_49[1] = -0.272197992;
				local_var_49[2] = 56.6699829;
				local_var_49[3] = 3.10874391;
				setnpcname(0x24c);
				reqenable(2);
				break;
			case 3:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 146.963837;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 59.1551361;
				local_var_46[0] = 2.1422019;
				local_var_49[0] = 146.963837;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 59.1551361;
				local_var_49[3] = 2.1422019;
				break;
			case 4:
				local_var_43 = 8;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 1.29999995;
				local_var_45[0] = 162.470352;
				local_var_45[1] = -0.262652993;
				local_var_45[2] = 49.4011078;
				local_var_45[3] = 162.378555;
				local_var_45[4] = -0.266458005;
				local_var_45[5] = 30.4801884;
				local_var_46[0] = -0.881908;
				local_var_46[1] = -2.52250504;
				local_var_49[0] = 162.470352;
				local_var_49[1] = -0.262652993;
				local_var_49[2] = 49.4011078;
				local_var_49[3] = -0.881908;
				break;
			case 5:
				local_var_44 = 1;
				local_var_43 = 1;
				local_var_54 = 4;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 167.249451;
				local_var_45[1] = -1.24736297;
				local_var_45[2] = 47.5613594;
				local_var_46[0] = -1.26783204;
				local_var_49[0] = 167.249451;
				local_var_49[1] = -1.24736297;
				local_var_49[2] = 47.5613594;
				local_var_49[3] = -1.26783204;
				setnpcname(0x251);
				reqenable(2);
				break;
			case 6:
				local_var_43 = 3;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 162.283539;
				local_var_45[1] = -0.244601995;
				local_var_45[2] = 91.4859543;
				local_var_45[3] = 163.606583;
				local_var_45[4] = -0.151532993;
				local_var_45[5] = 94.5268707;
				local_var_46[0] = -2.05085111;
				local_var_46[1] = 0.101709999;
				local_var_49[0] = 162.283539;
				local_var_49[1] = -0.244601995;
				local_var_49[2] = 91.4859543;
				local_var_49[3] = -2.05085111;
				break;
			case 7:
				local_var_43 = 8;
				local_var_54 = 4;
				local_var_55 = 0;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 170.227951;
				local_var_45[1] = -0.244065002;
				local_var_45[2] = 96.4511032;
				local_var_45[3] = 170.2211;
				local_var_45[4] = -0.243523002;
				local_var_45[5] = 103.117653;
				local_var_46[0] = -0.589583993;
				local_var_46[1] = -2.21398401;
				local_var_49[0] = 170.227951;
				local_var_49[1] = -0.244065002;
				local_var_49[2] = 96.4511032;
				local_var_49[3] = -0.589583993;
				break;
			case 8:
				local_var_43 = 1;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 4;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 44.9553909;
				local_var_45[1] = -0.275570005;
				local_var_45[2] = 92.2243805;
				local_var_45[3] = 58.7828102;
				local_var_45[4] = -0.275570989;
				local_var_45[5] = 92.9598236;
				local_var_45[6] = 58.6151543;
				local_var_45[7] = -0.274154991;
				local_var_45[8] = 105.989418;
				local_var_45[9] = 59.0643463;
				local_var_45[10] = -0.275570989;
				local_var_45[11] = 93.6374893;
				local_var_46[0] = -0.858404994;
				local_var_46[1] = -2.94322491;
				local_var_46[2] = -1.10363805;
				local_var_46[3] = -0.739531994;
				local_var_49[0] = 44.9553909;
				local_var_49[1] = -0.275570005;
				local_var_49[2] = 92.2243805;
				local_var_49[3] = -0.858404994;
				break;
			case 9:
				local_var_43 = 4;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 33;
				local_var_4c = 1.29999995;
				local_var_45[0] = 52.2941856;
				local_var_45[1] = -0.139144003;
				local_var_45[2] = 112.343124;
				local_var_46[0] = 0.645361006;
				local_var_49[0] = 52.2941856;
				local_var_49[1] = -0.139144003;
				local_var_49[2] = 112.343124;
				local_var_49[3] = 0.645361006;
				break;
			case 10:
				local_var_43 = 2;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.5;
				local_var_45[0] = 66.9266205;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 100.619888;
				local_var_46[0] = -0.931382;
				local_var_49[0] = 66.9266205;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 100.619888;
				local_var_49[3] = -0.931382;
				break;
			case 11:
				local_var_43 = 8;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 1.29999995;
				local_var_45[0] = 69.3345947;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 114.733658;
				local_var_45[3] = 67.2976227;
				local_var_45[4] = -0.236650005;
				local_var_45[5] = 117.679291;
				local_var_46[0] = 1.92907095;
				local_var_46[1] = 0.201787993;
				local_var_49[0] = 69.3345947;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 114.733658;
				local_var_49[3] = 1.92907095;
				setnpcname(0x258);
				reqenable(2);
				break;
			case 12:
				local_var_43 = 5;
				local_var_54 = 4;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 143.16452;
				local_var_45[1] = -0.136226997;
				local_var_45[2] = 68.1657715;
				local_var_46[0] = -1.52782905;
				local_var_49[0] = 143.16452;
				local_var_49[1] = -0.136226997;
				local_var_49[2] = 68.1657715;
				local_var_49[3] = -1.52782905;
				local_var_44 = 1;
				setnpcname(0x27a);
				reqenable(2);
				break;
			case 13:
				local_var_43 = 8;
				local_var_54 = 1;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 134.727005;
				local_var_45[1] = 0.650756001;
				local_var_45[2] = 51.4469147;
				local_var_46[0] = 0.561716974;
				local_var_49[0] = 134.727005;
				local_var_49[1] = 0.650756001;
				local_var_49[2] = 51.4469147;
				local_var_49[3] = 0.561716974;
				break;
			case 14:
				local_var_43 = 7;
				local_var_54 = 4;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 166.712982;
				local_var_45[1] = -0.115726002;
				local_var_45[2] = 38.9677048;
				local_var_46[0] = -2.60268593;
				local_var_49[0] = 166.712982;
				local_var_49[1] = -0.115726002;
				local_var_49[2] = 38.9677048;
				local_var_49[3] = -2.60268593;
				break;
			case 15:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 164.073242;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 108.346008;
				local_var_45[3] = 166.790955;
				local_var_45[4] = -0.275570989;
				local_var_45[5] = 108.298874;
				local_var_46[0] = -0.0144440001;
				local_var_46[1] = 0.171589002;
				local_var_49[0] = 164.073242;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 108.346008;
				local_var_49[3] = -0.0144440001;
				break;
			case 16:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 63.5743217;
				local_var_45[1] = -0.152353004;
				local_var_45[2] = 96.6769867;
				local_var_46[0] = -0.284682006;
				local_var_49[0] = 63.5743217;
				local_var_49[1] = -0.152353004;
				local_var_49[2] = 96.6769867;
				local_var_49[3] = -0.284682006;
				break;
			case 17:
				local_var_43 = 4;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 65.3781967;
				local_var_45[1] = 0.460741997;
				local_var_45[2] = 104.732285;
				local_var_46[0] = 1.30667806;
				local_var_49[0] = 65.3781967;
				local_var_49[1] = 0.460741997;
				local_var_49[2] = 104.732285;
				local_var_49[3] = 1.30667806;
				break;
			case 18:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 172.535187;
				local_var_45[1] = 0.32840699;
				local_var_45[2] = 61.3945847;
				local_var_46[0] = -1.22522104;
				local_var_49[0] = 172.535187;
				local_var_49[1] = 0.32840699;
				local_var_49[2] = 61.3945847;
				local_var_49[3] = -1.22522104;
				break;
			case 19:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 176.524597;
				local_var_45[1] = 1.56704104;
				local_var_45[2] = 62.2865334;
				local_var_46[0] = -1.19647002;
				local_var_49[0] = 176.524597;
				local_var_49[1] = 1.56704104;
				local_var_49[2] = 62.2865334;
				local_var_49[3] = -1.19647002;
				break;
			case 20:
				local_var_43 = 3;
				local_var_54 = 1;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 173.546021;
				local_var_45[1] = 1.61486602;
				local_var_45[2] = 62.5530548;
				local_var_46[0] = 0.579108;
				local_var_49[0] = 173.546021;
				local_var_49[1] = 1.61486602;
				local_var_49[2] = 62.5530548;
				local_var_49[3] = 0.579108;
				break;
		}
		file_var_1e[local_var_41] = 2;
		goto NPC配置05::localjmp_6;
	}


	function NPC配置03()
	{
		local_var_43 = 0;
		local_var_54 = 0;
		local_var_55 = 0;
		local_var_51 = 0;
		local_var_53 = 0;
		local_var_4f = 0;
		local_var_4c = 0;
		local_var_45[0] = 0;
		local_var_45[1] = 0;
		local_var_45[2] = 0;
		local_var_45[3] = 0;
		local_var_45[4] = 0;
		local_var_45[5] = 0;
		local_var_45[6] = 0;
		local_var_45[7] = 0;
		local_var_45[8] = 0;
		local_var_45[9] = 0;
		local_var_45[10] = 0;
		local_var_45[11] = 0;
		local_var_46[0] = 0;
		local_var_46[1] = 0;
		local_var_46[2] = 0;
		local_var_46[3] = 0;
		local_var_49[0] = 0;
		local_var_49[1] = 0;
		local_var_49[2] = 0;
		local_var_49[3] = 0;
		local_var_58 = 0;
		local_var_59[0] = 0;
		local_var_59[1] = 0;
		local_var_59[2] = 0;
		local_var_44 = 0;
		incentryid();
		reqdisable(2);
		switch (local_var_41)
		{
			case 0:
				local_var_43 = 3;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 1.29999995;
				local_var_45[0] = 195.625885;
				local_var_45[1] = -0.275570005;
				local_var_45[2] = 92.6480713;
				local_var_45[3] = 183.297424;
				local_var_45[4] = -0.275570005;
				local_var_45[5] = 92.6568527;
				local_var_46[0] = 2.26845598;
				local_var_46[1] = -2.42212105;
				local_var_49[0] = 195.625885;
				local_var_49[1] = -0.275570005;
				local_var_49[2] = 92.6480713;
				local_var_49[3] = 2.26845598;
				setnpcname(0x249);
				reqenable(2);
				break;
			case 1:
				local_var_43 = 1;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 30;
				local_var_4c = 1.29999995;
				local_var_45[0] = 179.484131;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 100.15094;
				local_var_46[0] = 0.101304002;
				local_var_49[0] = 179.484131;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 100.15094;
				local_var_49[3] = 0.101304002;
				break;
			case 2:
				local_var_43 = 2;
				local_var_54 = 3;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.20000005;
				local_var_45[0] = 60.1431961;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 114.739014;
				local_var_46[0] = -0.876694024;
				local_var_49[0] = 60.1431961;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 114.739014;
				local_var_49[3] = -0.876694024;
				break;
			case 3:
				local_var_44 = 1;
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 63.4007225;
				local_var_45[1] = -0.174292997;
				local_var_45[2] = 103.746506;
				local_var_46[0] = -1.895172;
				local_var_49[0] = 63.4007225;
				local_var_49[1] = -0.174292997;
				local_var_49[2] = 103.746506;
				local_var_49[3] = -1.895172;
				setnpcname(0x24e);
				reqenable(2);
				break;
			case 4:
				local_var_43 = 8;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 30;
				local_var_4c = 1.29999995;
				local_var_45[0] = 124.710831;
				local_var_45[1] = -0.139651999;
				local_var_45[2] = 110.296173;
				local_var_46[0] = 1.53278399;
				local_var_49[0] = 124.710831;
				local_var_49[1] = -0.139651999;
				local_var_49[2] = 110.296173;
				local_var_49[3] = 1.53278399;
				break;
			case 5:
				local_var_43 = 1;
				local_var_54 = 4;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 138.954681;
				local_var_45[1] = -0.274046987;
				local_var_45[2] = 93.3271484;
				local_var_46[0] = -2.05506992;
				local_var_49[0] = 138.954681;
				local_var_49[1] = -0.274046987;
				local_var_49[2] = 93.3271484;
				local_var_49[3] = -2.05506992;
				break;
			case 6:
				local_var_44 = 1;
				local_var_43 = 3;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 113.822556;
				local_var_45[1] = -0.659931004;
				local_var_45[2] = 117.241219;
				local_var_46[0] = 0.0494880006;
				local_var_49[0] = 113.822556;
				local_var_49[1] = -0.659931004;
				local_var_49[2] = 117.241219;
				local_var_49[3] = 0.0494880006;
				setnpcname(0x252);
				reqenable(2);
				break;
			case 7:
				local_var_43 = 8;
				local_var_54 = 4;
				local_var_55 = 0;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 97.9847031;
				local_var_45[1] = -0.215261996;
				local_var_45[2] = 110.817657;
				local_var_45[3] = 98.5311584;
				local_var_45[4] = -0.263464004;
				local_var_45[5] = 127.61129;
				local_var_46[0] = -1.97476804;
				local_var_46[1] = 0.0813980028;
				local_var_49[0] = 97.9847031;
				local_var_49[1] = -0.215261996;
				local_var_49[2] = 110.817657;
				local_var_49[3] = -1.97476804;
				break;
			case 8:
				local_var_43 = 1;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 156.266678;
				local_var_45[1] = -0.229139;
				local_var_45[2] = 125.796646;
				local_var_46[0] = 2.43544292;
				local_var_49[0] = 156.266678;
				local_var_49[1] = -0.229139;
				local_var_49[2] = 125.796646;
				local_var_49[3] = 2.43544292;
				setnpcname(0x255);
				reqenable(2);
				break;
			case 9:
				local_var_43 = 4;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 33;
				local_var_4c = 1.29999995;
				local_var_45[0] = 156.89827;
				local_var_45[1] = -0.252746999;
				local_var_45[2] = 125.081413;
				local_var_46[0] = -1.03954303;
				local_var_49[0] = 156.89827;
				local_var_49[1] = -0.252746999;
				local_var_49[2] = 125.081413;
				local_var_49[3] = -1.03954303;
				break;
			case 10:
				local_var_43 = 2;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.20000005;
				local_var_45[0] = 102.480782;
				local_var_45[1] = -0.241200998;
				local_var_45[2] = 91.6527863;
				local_var_46[0] = -1.33824396;
				local_var_49[0] = 102.480782;
				local_var_49[1] = -0.241200998;
				local_var_49[2] = 91.6527863;
				local_var_49[3] = -1.33824396;
				break;
			case 11:
				local_var_43 = 8;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 4;
				local_var_4f = 30;
				local_var_4c = 1.29999995;
				local_var_45[0] = 104.736938;
				local_var_45[1] = -0.225183994;
				local_var_45[2] = 107.12249;
				local_var_45[3] = 102.255051;
				local_var_45[4] = -0.250743002;
				local_var_45[5] = 107.053337;
				local_var_45[6] = 101.84716;
				local_var_45[7] = -0.236569002;
				local_var_45[8] = 103.695076;
				local_var_45[9] = 102.299118;
				local_var_45[10] = -0.221901998;
				local_var_45[11] = 106.031731;
				local_var_46[0] = 0.370317012;
				local_var_46[1] = -1.138834;
				local_var_46[2] = -2.40606809;
				local_var_46[3] = -1.06211698;
				local_var_49[0] = 104.736938;
				local_var_49[1] = -0.225183994;
				local_var_49[2] = 107.12249;
				local_var_49[3] = 0.370317012;
				setnpcname(0x259);
				reqenable(2);
				break;
			case 12:
				local_var_43 = 5;
				local_var_54 = 4;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 178.386322;
				local_var_45[1] = -0.252225995;
				local_var_45[2] = 105.618851;
				local_var_45[3] = 177.737656;
				local_var_45[4] = -0.255171001;
				local_var_45[5] = 109.576271;
				local_var_46[0] = -1.39972901;
				local_var_46[1] = -1.47657704;
				local_var_49[0] = 178.386322;
				local_var_49[1] = -0.252225995;
				local_var_49[2] = 105.618851;
				local_var_49[3] = -1.39972901;
				break;
			case 13:
				local_var_43 = 8;
				local_var_54 = 1;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 48.8267975;
				local_var_45[1] = -0.177430004;
				local_var_45[2] = 118.581787;
				local_var_46[0] = 2.20282602;
				local_var_49[0] = 48.8267975;
				local_var_49[1] = -0.177430004;
				local_var_49[2] = 118.581787;
				local_var_49[3] = 2.20282602;
				break;
			case 14:
				local_var_43 = 7;
				local_var_54 = 4;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 137.237381;
				local_var_45[1] = -0.201924995;
				local_var_45[2] = 89.9335403;
				local_var_46[0] = -2.36799908;
				local_var_49[0] = 137.237381;
				local_var_49[1] = -0.201924995;
				local_var_49[2] = 89.9335403;
				local_var_49[3] = -2.36799908;
				setnpcname(0x25a);
				reqenable(2);
				break;
			case 15:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 109.832336;
				local_var_45[1] = 0.287405998;
				local_var_45[2] = 135.191742;
				local_var_46[0] = -0.0257719997;
				local_var_49[0] = 109.832336;
				local_var_49[1] = 0.287405998;
				local_var_49[2] = 135.191742;
				local_var_49[3] = -0.0257719997;
				break;
			case 16:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 159.305161;
				local_var_45[1] = 0.484414995;
				local_var_45[2] = 116.079353;
				local_var_46[0] = -1.65584505;
				local_var_49[0] = 159.305161;
				local_var_49[1] = 0.484414995;
				local_var_49[2] = 116.079353;
				local_var_49[3] = -1.65584505;
				break;
			case 17:
				local_var_43 = 4;
				local_var_54 = 1;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 102.12793;
				local_var_45[1] = 0.476924986;
				local_var_45[2] = 99.5107193;
				local_var_46[0] = -2.21589398;
				local_var_49[0] = 102.12793;
				local_var_49[1] = 0.476924986;
				local_var_49[2] = 99.5107193;
				local_var_49[3] = -2.21589398;
				break;
			case 18:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 137.956375;
				local_var_45[1] = -0.158162996;
				local_var_45[2] = 100.444588;
				local_var_46[0] = -1.70319903;
				local_var_49[0] = 137.956375;
				local_var_49[1] = -0.158162996;
				local_var_49[2] = 100.444588;
				local_var_49[3] = -1.70319903;
				break;
			case 19:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 127.671005;
				local_var_45[1] = -0.139203995;
				local_var_45[2] = 107.154472;
				local_var_46[0] = -0.939015985;
				local_var_49[0] = 127.671005;
				local_var_49[1] = -0.139203995;
				local_var_49[2] = 107.154472;
				local_var_49[3] = -0.939015985;
				break;
			case 20:
				local_var_43 = 3;
				local_var_54 = 1;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 133.68924;
				local_var_45[1] = -0.161978006;
				local_var_45[2] = 106.035698;
				local_var_46[0] = -0.916936994;
				local_var_49[0] = 133.68924;
				local_var_49[1] = -0.161978006;
				local_var_49[2] = 106.035698;
				local_var_49[3] = -0.916936994;
				break;
		}
		file_var_1e[local_var_41] = 3;
		goto NPC配置05::localjmp_6;
	}


	function NPC配置04()
	{
		local_var_43 = 0;
		local_var_54 = 0;
		local_var_55 = 0;
		local_var_51 = 0;
		local_var_53 = 0;
		local_var_4f = 0;
		local_var_4c = 0;
		local_var_45[0] = 0;
		local_var_45[1] = 0;
		local_var_45[2] = 0;
		local_var_45[3] = 0;
		local_var_45[4] = 0;
		local_var_45[5] = 0;
		local_var_45[6] = 0;
		local_var_45[7] = 0;
		local_var_45[8] = 0;
		local_var_45[9] = 0;
		local_var_45[10] = 0;
		local_var_45[11] = 0;
		local_var_46[0] = 0;
		local_var_46[1] = 0;
		local_var_46[2] = 0;
		local_var_46[3] = 0;
		local_var_49[0] = 0;
		local_var_49[1] = 0;
		local_var_49[2] = 0;
		local_var_49[3] = 0;
		local_var_58 = 0;
		local_var_59[0] = 0;
		local_var_59[1] = 0;
		local_var_59[2] = 0;
		local_var_44 = 0;
		incentryid();
		reqdisable(2);
		switch (local_var_41)
		{
			case 0:
				local_var_43 = 3;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 95.3038025;
				local_var_45[1] = -0.243517995;
				local_var_45[2] = 125.675522;
				local_var_45[3] = 83.3452682;
				local_var_45[4] = -0.268133998;
				local_var_45[5] = 125.443901;
				local_var_46[0] = 1.70210195;
				local_var_46[1] = 0.228916004;
				local_var_49[0] = 95.3038025;
				local_var_49[1] = -0.243517995;
				local_var_49[2] = 125.675522;
				local_var_49[3] = 1.70210195;
				break;
			case 1:
				local_var_43 = 1;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 83.5595245;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 115.970917;
				local_var_46[0] = 3.03246593;
				local_var_49[0] = 83.5595245;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 115.970917;
				local_var_49[3] = 3.03246593;
				break;
			case 2:
				local_var_43 = 2;
				local_var_54 = 3;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 139.377258;
				local_var_45[1] = -0.27522099;
				local_var_45[2] = 110.092972;
				local_var_46[0] = -0.639423013;
				local_var_49[0] = 139.377258;
				local_var_49[1] = -0.27522099;
				local_var_49[2] = 110.092972;
				local_var_49[3] = -0.639423013;
				setnpcname(0x24d);
				reqenable(2);
				break;
			case 3:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 143.174088;
				local_var_45[1] = -0.135508999;
				local_var_45[2] = 135.93251;
				local_var_46[0] = -1.56654596;
				local_var_49[0] = 143.174088;
				local_var_49[1] = -0.135508999;
				local_var_49[2] = 135.93251;
				local_var_49[3] = -1.56654596;
				break;
			case 4:
				local_var_43 = 8;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 172.099258;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 112.517113;
				local_var_46[0] = 1.88668096;
				local_var_49[0] = 172.099258;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 112.517113;
				local_var_49[3] = 1.88668096;
				setnpcname(0x24f);
				reqenable(2);
				break;
			case 5:
				local_var_43 = 1;
				local_var_54 = 4;
				local_var_55 = 2;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 187.088104;
				local_var_45[1] = -0.264661998;
				local_var_45[2] = 114.532722;
				local_var_45[3] = 195.43367;
				local_var_45[4] = -0.112392001;
				local_var_45[5] = 118.335266;
				local_var_46[0] = -3.0501101;
				local_var_46[1] = -0.0227419995;
				local_var_49[0] = 187.088104;
				local_var_49[1] = -0.264661998;
				local_var_49[2] = 114.532722;
				local_var_49[3] = -3.0501101;
				break;
			case 8:
				local_var_43 = 1;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 1;
				local_var_4f = 30;
				local_var_4c = 1.5;
				local_var_45[0] = 101.483131;
				local_var_45[1] = -0.272179991;
				local_var_45[2] = 132.559891;
				local_var_46[0] = -1.42448604;
				local_var_49[0] = 101.483131;
				local_var_49[1] = -0.272179991;
				local_var_49[2] = 132.559891;
				local_var_49[3] = -1.42448604;
				break;
			case 9:
				local_var_43 = 4;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 97.8916245;
				local_var_45[1] = -0.185127005;
				local_var_45[2] = 139.985504;
				local_var_45[3] = 98.3992538;
				local_var_45[4] = -0.246639997;
				local_var_45[5] = 134.705124;
				local_var_46[0] = -1.65556204;
				local_var_46[1] = -2.14052105;
				local_var_49[0] = 97.8916245;
				local_var_49[1] = -0.185127005;
				local_var_49[2] = 139.985504;
				local_var_49[3] = -1.65556204;
				setnpcname(0x256);
				reqenable(2);
				break;
			case 12:
				local_var_43 = 5;
				local_var_54 = 4;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 81.0173416;
				local_var_45[1] = -0.150755003;
				local_var_45[2] = 124.378548;
				local_var_46[0] = 1.92461896;
				local_var_49[0] = 81.0173416;
				local_var_49[1] = -0.150755003;
				local_var_49[2] = 124.378548;
				local_var_49[3] = 1.92461896;
				local_var_44 = 1;
				setnpcname(0x27a);
				reqenable(2);
				break;
			case 13:
				local_var_43 = 8;
				local_var_54 = 1;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 134.168274;
				local_var_45[1] = 0.93522501;
				local_var_45[2] = 135.599274;
				local_var_46[0] = 2.68463397;
				local_var_49[0] = 134.168274;
				local_var_49[1] = 0.93522501;
				local_var_49[2] = 135.599274;
				local_var_49[3] = 2.68463397;
				break;
			case 14:
				local_var_43 = 7;
				local_var_54 = 4;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 188.52655;
				local_var_45[1] = -0.104728997;
				local_var_45[2] = 112.85463;
				local_var_46[0] = -2.82776308;
				local_var_49[0] = 188.52655;
				local_var_49[1] = -0.104728997;
				local_var_49[2] = 112.85463;
				local_var_49[3] = -2.82776308;
				break;
			case 16:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 110.632576;
				local_var_45[1] = 0.328823;
				local_var_45[2] = 135.6371;
				local_var_46[0] = -1.62320805;
				local_var_49[0] = 110.632576;
				local_var_49[1] = 0.328823;
				local_var_49[2] = 135.6371;
				local_var_49[3] = -1.62320805;
				break;
			case 18:
				local_var_43 = 3;
				local_var_54 = 2;
				local_var_55 = 2;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 185.973297;
				local_var_45[1] = -0.195438996;
				local_var_45[2] = 118.332344;
				local_var_46[0] = 0.772720993;
				local_var_49[0] = 185.973297;
				local_var_49[1] = -0.195438996;
				local_var_49[2] = 118.332344;
				local_var_49[3] = 0.772720993;
				break;
			case 19:
				local_var_43 = 4;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 187.183929;
				local_var_45[1] = -0.136366993;
				local_var_45[2] = 118.203827;
				local_var_46[0] = -0.656507015;
				local_var_49[0] = 187.183929;
				local_var_49[1] = -0.136366993;
				local_var_49[2] = 118.203827;
				local_var_49[3] = -0.656507015;
				break;
			case 20:
				local_var_43 = 3;
				local_var_54 = 1;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 196.602371;
				local_var_45[1] = -0.0334109999;
				local_var_45[2] = 119.491142;
				local_var_46[0] = -0.342817992;
				local_var_49[0] = 196.602371;
				local_var_49[1] = -0.0334109999;
				local_var_49[2] = 119.491142;
				local_var_49[3] = -0.342817992;
				break;
		}
		file_var_1e[local_var_41] = 4;
		goto NPC配置05::localjmp_6;
	}


	function NPC配置05()
	{
		local_var_43 = 0;
		local_var_54 = 0;
		local_var_55 = 0;
		local_var_51 = 0;
		local_var_53 = 0;
		local_var_4f = 0;
		local_var_4c = 0;
		local_var_45[0] = 0;
		local_var_45[1] = 0;
		local_var_45[2] = 0;
		local_var_45[3] = 0;
		local_var_45[4] = 0;
		local_var_45[5] = 0;
		local_var_45[6] = 0;
		local_var_45[7] = 0;
		local_var_45[8] = 0;
		local_var_45[9] = 0;
		local_var_45[10] = 0;
		local_var_45[11] = 0;
		local_var_46[0] = 0;
		local_var_46[1] = 0;
		local_var_46[2] = 0;
		local_var_46[3] = 0;
		local_var_49[0] = 0;
		local_var_49[1] = 0;
		local_var_49[2] = 0;
		local_var_49[3] = 0;
		local_var_58 = 0;
		local_var_59[0] = 0;
		local_var_59[1] = 0;
		local_var_59[2] = 0;
		local_var_44 = 0;
		incentryid();
		reqdisable(2);
		switch (local_var_41)
		{
			case 0:
				local_var_43 = 3;
				local_var_54 = 3;
				local_var_55 = 1;
				local_var_51 = 1;
				local_var_53 = 2;
				local_var_4f = 30;
				local_var_4c = 0;
				local_var_45[0] = 95.3038025;
				local_var_45[1] = -0.243517995;
				local_var_45[2] = 125.675522;
				local_var_45[3] = 83.3452682;
				local_var_45[4] = -0.268133998;
				local_var_45[5] = 125.443901;
				local_var_46[0] = 1.70210195;
				local_var_46[1] = 0.228916004;
				local_var_49[0] = 95.3038025;
				local_var_49[1] = -0.243517995;
				local_var_49[2] = 125.675522;
				local_var_49[3] = 1.70210195;
				break;
			case 1:
				local_var_43 = 1;
				local_var_54 = 2;
				local_var_55 = 0;
				local_var_51 = 0;
				local_var_53 = 1;
				local_var_4f = 0;
				local_var_4c = 1.29999995;
				local_var_45[0] = 83.5595245;
				local_var_45[1] = -0.275570989;
				local_var_45[2] = 115.970917;
				local_var_46[0] = 3.03246593;
				local_var_49[0] = 83.5595245;
				local_var_49[1] = -0.275570989;
				local_var_49[2] = 115.970917;
				local_var_49[3] = 3.03246593;
				break;
			case 12:
				local_var_43 = 5;
				local_var_54 = 4;
				local_var_55 = 1;
				local_var_51 = 0;
				local_var_53 = 0;
				local_var_4f = 0;
				local_var_4c = 0;
				local_var_45[0] = 143.348312;
				local_var_45[1] = -0.0794129968;
				local_var_45[2] = 140.303696;
				local_var_46[0] = -2.03137898;
				local_var_49[0] = 143.348312;
				local_var_49[1] = -0.0794129968;
				local_var_49[2] = 140.303696;
				local_var_49[3] = -2.03137898;
				local_var_44 = 1;
				setnpcname(0x27a);
				reqenable(2);
				break;
		}
		file_var_1e[local_var_41] = 5;
	localjmp_6:
		switch (local_var_41)
		{
			case 2:
				sysReqchg(2, NPC03::talk_step01);
				break;
			case 5:
				sysReqchg(2, NPC06::talk_step01);
				break;
		}
		setautorelax(1);
		if (local_var_57 == 0)
		{
			hidecomplete();
			file_var_24[local_var_41] = 99;
		}
		else
		{
			hidecomplete();
			file_var_24[local_var_41] = 99;
		}
		usemapid(0);
		setpos(local_var_45[0], local_var_45[1], local_var_45[2]);
		dir(local_var_46[0]);
		switch (local_var_53)
		{
			case 0:
				usemapid(0);
				break;
			default:
				usemapid(1);
				break;
		}
		usecharhit(1);
		setweight(-1);
		if (local_var_57 == 0)
		{
			goto NPCバインド;
		}
		returnT 1;
	}


	function NPCバインド()
	{
		switch (local_var_43)
		{
			case 1:
				bindp2_d4(0x3000002, local_var_54, local_var_55);
				break;
			case 2:
				bindp2_d4(0x3000003, local_var_54, local_var_55);
				break;
			case 3:
				bindp2_d4(0x3000006, local_var_54, local_var_55);
				break;
			case 4:
				bindp2_d4(0x3000007, local_var_54, local_var_55);
				break;
			case 8:
				bindp2_d4(0x3000004, local_var_54, local_var_55);
				setradius_221((getdefaultradiusw() * 0.800000012), (getdefaultradiusd() * 0.800000012));
				break;
			case 5:
				bindp2_d4(0x3000008, local_var_54, local_var_55);
				break;
			case 7:
				bindp2_d4(0x3000005, local_var_54, local_var_55);
				setradius_221((getdefaultradiusw() * 0.800000012), (getdefaultradiusd() * 0.800000012));
				break;
		}
		set_ignore_hitgroup(1);
		setreachr(0.5);
		switch (local_var_41)
		{
			case 7:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						stdmotionread(18);
						stdmotionreadsync();
						stdmotionplay(0x1000000);
						break;
					case 2:
					case 3:
					case 4:
						stdmotionread(16);
						stdmotionreadsync();
						stdmotionplay(0x1000000);
						break;
				}
				break;
			default:
				stdmotionread(16);
				stdmotionreadsync();
				stdmotionplay(0x1000000);
				break;
		}
		local_var_48 = getdefaultwalkspeed();
		switch (local_var_53)
		{
			case 0:
				usemapid(0);
				break;
			default:
				usemapid(1);
				break;
		}
		usecharhit(1);
		setweight(-1);
		if (local_var_58 == 1)
		{
			fetchambient_4e1(local_var_59[0], local_var_59[1], local_var_59[2]);
			local_var_58 = 0;
			local_var_59[0] = 0;
			local_var_59[1] = 0;
			local_var_59[2] = 0;
		}
		switch (local_var_41)
		{
			case 4:
			case 5:
			case 1:
			case 6:
			case 9:
			case 11:
			case 12:
			case 16:
				break;
			default:
				local_var_57 = 1;
				break;
		}
		//Couldn't get labels for REQ because either script or function is not immediate
		sysReq(1, getmyid(), 13);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_47;            // pos: 0x68;
	float   local_var_4a;            // pos: 0x80;
	float   local_var_4b;            // pos: 0x84;
	float   local_var_4d;            // pos: 0x8c;
	int     local_var_4e;            // pos: 0xa0;
	int     local_var_50;            // pos: 0xa8;
	u_char  local_var_52;            // pos: 0xad;
	int     local_var_56[4];         // pos: 0xb4;

	function NPC挙動()
	{
		setkutipakustatus(0);
		setunazukistatus(0);
		lookatoff();
		stdmotionplay(0x1000000);
		switch (local_var_41)
		{
			case 0:
				break;
			case 1:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						sysRlookata(NPC13);
						setkutipakustatus(1);
						setunazukistatus(1);
						local_var_56[0] = 0x1000016;
						local_var_56[1] = 0x1000015;
						goto localjmp_278;
					case 2:
						local_var_56[0] = 0x1000015;
						local_var_56[1] = 0x1000014;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000014;
						goto localjmp_239;
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 12:
				setkutipakustatus(0);
				setunazukistatus(0);
				lookatoff();
				switch (file_var_1e[local_var_41])
				{
					case 1:
						local_var_57 = 1;
						sysRlookata(NPC02);
						setkutipakustatus(1);
						setunazukistatus(1);
						local_var_56[0] = 0x1000010;
						local_var_56[1] = 0x1000014;
						goto localjmp_278;
					case 2:
					case 4:
					case 5:
						local_var_57 = 1;
						setkutipakustatus(1);
						setunazukistatus(1);
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000015, 0);
						while (true)
						{
							wait(10);
						}
					case 3:
						local_var_56[0] = 0x1000014;
						local_var_56[1] = 0x1000010;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000014;
						goto localjmp_265;
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 2:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						if (file_var_27 != 0)
						{
							while (true)
							{
								stdmotionplay(0x1000011);
								wait(1);
								motionsync_282(1);
								stdmotionplay(0x1000015);
								wait(3);
								wait(1);
								motionsync_282(1);
							}
						}
						break;
					case 2:
						break;
					case 3:
						break;
					case 4:
						break;
				}
				break;
			case 3:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						if (file_var_27 == 0)
						{
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x1100000f, 0);
							lookat_24f(78.8456879, 1.939888, 56.5347786);
						}
						else
						{
							setautorelax(0);
							motionstartframe(56);
							motionloopframe(56, 116);
							motionloop(1);
							motionplay_bb(0x1100000d, 0);
							sysLookata(空き巣シーク);
						}
						while (true)
						{
							wait(100);
						}
					case 2:
						stdmotionread(16);
						stdmotionreadsync();
						stdmotionvariation(0);
						stdmotionplay(0x1000000);
						break;
					case 3:
						stdmotionread(18);
						stdmotionreadsync();
						stdmotionplay_2c2(0x1000000, 0);
						while (true)
						{
							wait(100);
						}
					case 4:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000010, 0);
						while (true)
						{
							wait(100);
						}
				}
				break;
			case 13:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						setradius_221(getdefaultradiusw(), 1.5);
						setposoffset((sin(getrotangzx()) * 0.800000012), 0, (cos(getrotangzx()) * 0.800000012), 3);
						while (true)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							lookatt(normalang((getrotangzx() + 0.785398185)), 45);
							stdmotionplay(0x1000013);
							wait(1);
							motionsync_282(1);
							wait((60 + rand_29(15)));
							setkutipakustatus(0);
							setunazukistatus(0);
							lookatt(normalang((getrotangzx() - 0.785398185)), 45);
							stdmotionplay(0x1000010);
							wait(1);
							motionsync_282(1);
							wait(30);
							setkutipakustatus(1);
							setunazukistatus(1);
							wait((60 + rand_29(15)));
							setkutipakustatus(0);
							setunazukistatus(0);
						}
					case 2:
						setradius_221(getdefaultradiusw(), getdefaultradiusw());
						setposoffset(0, 0, 0, 3);
						rlookatt(normalang((getrotangzx() + 0.785398185)), 45);
						setkutipakustatus(1);
						setunazukistatus(1);
						if (file_var_23 != 0)
						{
							local_var_56[0] = 0x1000012;
							local_var_56[1] = 0x1000012;
						}
						else
						{
							local_var_56[0] = 0x1000013;
							local_var_56[1] = 0x1000010;
						}
						goto localjmp_278;
					case 3:
						setradius_221(getdefaultradiusw(), 1);
						setposoffset(0, 0, 0.200000003, 3);
						setautorelax(0);
						motionstartframe(120);
						motionloopframe(120, 160);
						motionloop(1);
						motionplay_bb(0x11000000, 0);
						while (true)
						{
							wait(100);
						}
					case 4:
						setradius_221(getdefaultradiusw(), getdefaultradiusw());
						setposoffset(0, 0, 0, 3);
						setautorelax(0);
						motionstartframe(120);
						motionloopframe(120, 160);
						motionloop(1);
						motionplay_bb(0x11000000, 0);
						while (true)
						{
							wait(100);
						}
				}
				break;
			case 4:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						if (file_var_27 != 0)
						{
							while (true)
							{
								stdmotionplay(0x1000012);
								wait(1);
								motionsync_282(1);
								stdmotionplay(0x1000015);
								wait(3);
								wait(1);
								motionsync_282(1);
							}
						}
						local_var_57 = 1;
						break;
					case 2:
						local_var_52 = 0;
						local_var_4e = 0;
						local_var_50 = 0;
						local_var_4a = 0.00600000005;
						local_var_4b = 0.00300000003;
						stdmotionplaywithoutsame_2c3(0x1000000, 5);
						setpos(local_var_45[((local_var_53 - 1) * 3)], local_var_45[(((local_var_53 - 1) * 3) + 1)], local_var_45[(((local_var_53 - 1) * 3) + 2)]);
						local_var_57 = 1;
						while (true)
						{
							setreachr(0.100000001);
							if (local_var_48 <= 0)
							{
								wait(1);
							}
							else
							{
								setwalkspeed(local_var_48);
								stdmotionplay_2c2(0x1000000, 20);
								move(local_var_45[local_var_52], local_var_45[(local_var_52 + 1)], local_var_45[(local_var_52 + 2)]);
								wait(5);
								if (local_var_51 != 0)
								{
									wait(3);
									setaturnlookatlockstatus(1);
									aturn_261(local_var_46[(local_var_52 / 3)]);
									switch ((local_var_52 / 3))
									{
										case 0:
											motionplay_bb(0x11000001, 20);
											break;
										case 1:
											motionplay_bb(0x11000001, 20);
											break;
										case 2:
											motionplay_bb(0x11000001, 20);
											break;
										case 3:
											motionplay_bb(0x11000001, 20);
											break;
									}
									wait(1);
									motionsync_282(1);
									wait(5);
								}
								local_var_52 = (local_var_52 + 3);
								if (local_var_52 >= (local_var_53 * 3))
								{
									local_var_52 = 0;
								}
								wait(1);
							}
						}
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 5:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						local_var_57 = 1;
						if (file_var_27 == 0)
						{
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x11000008, 0);
							while (true)
							{
								lookat_24f(93.6891861, 2.13540196, 61.9751778);
								setkutipakustatus(1);
								setunazukistatus(1);
								wait(90);
								lookatoff();
								setkutipakustatus(0);
								setunazukistatus(0);
								wait(60);
							}
						}
						while (true)
						{
							stdmotionplay(0x1000011);
							wait(1);
							motionsync_282(1);
							stdmotionplay(0x1000015);
							wait(3);
							wait(1);
							motionsync_282(1);
						}
						break;
					case 2:
						local_var_57 = 1;
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000007, 0);
						while (true)
						{
							wait(60);
						}
					case 3:
						local_var_57 = 1;
						break;
					case 4:
						local_var_56[0] = 0x1000015;
						local_var_56[1] = 0x1000016;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000014;
						goto localjmp_239;
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 14:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						if (file_var_27 == 0)
						{
							sysRlookata(NPC06);
							setkutipakustatus(1);
							setunazukistatus(1);
							setradius_221(getdefaultradiusw(), getdefaultradiusw());
							local_var_56[0] = 0x1000011;
							local_var_56[1] = 0x1000013;
							goto localjmp_278;
						}
						while (true)
						{
							stdmotionplay(0x1000012);
							wait(3);
							wait(1);
							motionsync_282(1);
						}
						break;
					case 2:
						setradius_221(getdefaultradiusw(), getdefaultradiusw());
						setautorelax(0);
						motionstartframe(118);
						motionloopframe(118, 171);
						motionloop(1);
						motionplay_bb(0x11000018, 0);
						sysRlookata(-1);
						while (true)
						{
							wait(60);
						}
					case 3:
						setradius_221(getdefaultradiusw(), 0.899999976);
						setposoffset((sin(getrotangzx()) * 0.200000003), 0, (cos(getrotangzx()) * 0.200000003), 3);
						while (true)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							lookatt(normalang((getrotangzx() + 0.785398185)), 45);
							stdmotionplay(0x1000011);
							wait(1);
							motionsync_282(1);
							wait((60 + rand_29(15)));
							setkutipakustatus(0);
							setunazukistatus(0);
							lookatt(normalang((getrotangzx() - 0.785398185)), 45);
							stdmotionplay(0x1000015);
							wait(1);
							motionsync_282(1);
							wait(30);
							setkutipakustatus(1);
							setunazukistatus(1);
							wait((60 + rand_29(15)));
							setkutipakustatus(0);
							setunazukistatus(0);
						}
					case 4:
						setkutipakustatus(1);
						setunazukistatus(1);
						setradius_221(getdefaultradiusw(), getdefaultradiusw());
						local_var_56[0] = 0x1000011;
						local_var_56[1] = 0x1000012;
						goto localjmp_278;
				}
				break;
			case 18:
				usecharhit(1);
				switch (file_var_1e[local_var_41])
				{
					case 1:
						if (file_var_27 == 0)
						{
							usecharhit(0);
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x11000011, 0);
							sysLookata(-1);
							while (true)
							{
								wait(100);
							}
						}
						while (true)
						{
							stdmotionplay(0x1000011);
							wait(3);
							wait(1);
							motionsync_282(1);
							stdmotionplay(0x1000016);
							wait(1);
							motionsync_282(1);
						}
						break;
					case 2:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000011, 0);
						sysLookata(-1);
						usecharhit(0);
						while (true)
						{
							wait(100);
						}
					case 3:
						local_var_56[0] = 0x1000016;
						local_var_56[1] = 0x1000012;
						goto localjmp_278;
					case 4:
						local_var_56[0] = 0x1000011;
						local_var_56[1] = 0x1000016;
						goto localjmp_278;
				}
				break;
			case 19:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000e, 0);
						if (file_var_27 == 0)
						{
							sysLookata(NPC21);
						}
						else
						{
							sysLookata(空き巣シーク);
						}
						while (true)
						{
							wait(100);
						}
					case 2:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000e, 0);
						while (true)
						{
							wait(100);
						}
					case 3:
						setkutipakustatus(1);
						setunazukistatus(1);
						rlookat_257(129.351227, 1.13789701, 107.206558);
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000f, 0);
						while (true)
						{
							wait(100);
						}
					case 4:
						setkutipakustatus(1);
						setunazukistatus(1);
						local_var_56[0] = 0x1000010;
						local_var_56[1] = 0x1000016;
						goto localjmp_278;
				}
				break;
			case 20:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						if (file_var_27 == 0)
						{
							if (シナリオフラグ <= 0x1004)
							{
								sysRlookata(NPC20);
								local_var_56[0] = 0x1000013;
								local_var_56[1] = 0x1000010;
							}
							else
							{
								local_var_56[0] = 0x1000015;
								local_var_56[1] = 0x1000012;
							}
						}
						else
						{
							sysRlookata(空き巣シーク);
							local_var_56[0] = 0x1000015;
							local_var_56[1] = 0x1000011;
						}
						goto localjmp_278;
					case 2:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000e, 0);
						sysLookata(NPC20);
						setkutipakustatus(1);
						setunazukistatus(1);
						while (true)
						{
							wait(100);
						}
					case 3:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000014, 0);
						while (true)
						{
							wait(100);
						}
					case 4:
						setkutipakustatus(1);
						setunazukistatus(1);
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000013, 0);
						while (true)
						{
							wait(43);
						}
				}
				break;
			case 6:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						local_var_56[0] = 0x1000015;
						local_var_56[1] = 0x1000014;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000014;
						goto localjmp_239;
					case 2:
						local_var_56[0] = 0x1000010;
						local_var_56[1] = 0x1000013;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000014;
						goto localjmp_239;
					case 3:
						local_var_57 = 1;
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000011, 0);
						while (true)
						{
							wait(100);
						}
					case 4:
						local_var_57 = 1;
						break;
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 7:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						stdmotionread(18);
						stdmotionreadsync();
						stdmotionplay_2c2(0x1000000, 0);
						while (true)
						{
							sysLookata(NPC16);
							setkutipakustatus(1);
							setunazukistatus(1);
							wait(100);
						}
					case 2:
					case 3:
					case 4:
						stdmotionread(16);
						stdmotionreadsync();
						stdmotionvariation(0);
						stdmotionplay(0x1000000);
						break;
				}
				break;
			case 15:
				usecharhit(1);
				switch (file_var_1e[local_var_41])
				{
					case 1:
						sysRlookata(NPC08);
						setkutipakustatus(1);
						setunazukistatus(1);
						local_var_56[0] = 0x1000016;
						local_var_56[1] = 0x1000010;
						goto localjmp_278;
					case 2:
						rlookat_257(165.344193, -0.275570989, 110.739998);
						break;
					case 3:
						usecharhit(0);
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000e, 0);
						setkutipakustatus(1);
						setunazukistatus(1);
						sysRlookata(NPC17);
						while (true)
						{
							wait(100);
						}
				}
				break;
			case 8:
				stdmotionplay_2c2(0x1000000, 0);
				switch (file_var_1e[local_var_41])
				{
					case 1:
						stdmotionread(18);
						stdmotionreadsync();
						stdmotionvariation(1);
						stdmotionplay_2c2(0x1000000, 0);
						while (true)
						{
							rlookat_257(102.730217, 1.70615101, 79.5020447);
							setkutipakustatus(1);
							setunazukistatus(1);
							wait(90);
							lookatoff();
							setkutipakustatus(0);
							setunazukistatus(0);
							wait(60);
						}
					case 2:
						lookatoff();
						setkutipakustatus(0);
						setunazukistatus(0);
						stdmotionread(16);
						stdmotionreadsync();
						stdmotionvariation(0);
						stdmotionplay(0x1000000);
						local_var_56[0] = 0x1000015;
						local_var_56[1] = 0x1000000;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000000;
						goto localjmp_239;
					case 3:
						stdmotionread(16);
						stdmotionreadsync();
						stdmotionvariation(0);
						stdmotionplay(0x1000000);
						while (true)
						{
							lookatoff();
							setkutipakustatus(1);
							setunazukistatus(1);
							switch ((rand_29(100) % 2))
							{
								case 0:
									stdmotionplay(0x1000011);
									wait(1);
									motionsync_282(1);
									break;
								default:
									stdmotionplay(0x1000015);
									wait(1);
									motionsync_282(1);
									break;
							}
							wait(1);
						}
					case 4:
						stdmotionread(16);
						stdmotionreadsync();
						stdmotionvariation(0);
						stdmotionplay(0x1000000);
						break;
				}
				break;
			case 9:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						local_var_57 = 1;
						break;
					case 2:
						local_var_57 = 1;
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000f, 0);
						while (true)
						{
							wait(100);
						}
					case 3:
						local_var_57 = 1;
						local_var_56[0] = 0x1000012;
						local_var_56[1] = 0x1000015;
						goto localjmp_278;
					case 4:
						local_var_56[0] = 0x1000010;
						local_var_56[1] = 0x1000013;
						local_var_56[2] = 0x1000014;
						local_var_56[3] = 0x1000014;
						goto localjmp_239;
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 16:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						local_var_57 = 1;
						if (file_var_23 == 1)
						{
							hidecomplete();
						}
						else
						{
							clearhidecomplete();
						}
						break;
					case 2:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000013, 0);
						local_var_57 = 1;
						while (true)
						{
							wait(90);
						}
					case 3:
						sysRlookata(-1);
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000011, 0);
						local_var_57 = 1;
						while (true)
						{
							wait(100);
						}
					case 4:
						setkutipakustatus(1);
						setunazukistatus(1);
						sysRlookata(NPC16);
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x11000011, 0);
						local_var_57 = 1;
						while (true)
						{
							wait(100);
						}
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 10:
				break;
			case 11:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						local_var_57 = 1;
						break;
					case 2:
						local_var_56[0] = 0x1000013;
						local_var_56[1] = 0x1000013;
						local_var_56[2] = 0x1000013;
						local_var_56[3] = 0x1000013;
						goto localjmp_239;
					case 3:
						local_var_56[0] = 0x1000000;
						local_var_56[1] = 0x1000000;
						local_var_56[2] = 0x1000013;
						local_var_56[3] = 0x1000000;
						goto localjmp_252;
					default:
						local_var_57 = 1;
						break;
				}
				break;
			case 17:
				switch (file_var_1e[local_var_41])
				{
					case 1:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000e, 0);
						while (true)
						{
							wait(100);
						}
					case 2:
					case 3:
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x1100000e, 0);
						sysRlookata(-1);
						while (true)
						{
							wait(100);
						}
				}
				break;
			localjmp_239:
				local_var_52 = 0;
				local_var_4e = 0;
				local_var_50 = 0;
				local_var_4a = 0.00600000005;
				local_var_4b = 0.00300000003;
				stdmotionplaywithoutsame_2c3(0x1000000, 5);
				setpos(local_var_45[((local_var_53 - 1) * 3)], local_var_45[(((local_var_53 - 1) * 3) + 1)], local_var_45[(((local_var_53 - 1) * 3) + 2)]);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				local_var_57 = 1;
			localjmp_240:
				setreachr(0.100000001);
				if (local_var_48 <= 0)
				{
					wait(1);
				}
				else
				{
					setwalkspeed(local_var_48);
					stdmotionplay_2c2(0x1000000, 20);
					move(local_var_45[local_var_52], local_var_45[(local_var_52 + 1)], local_var_45[(local_var_52 + 2)]);
					wait(5);
					if (local_var_51 != 0)
					{
						wait(3);
						setaturnlookatlockstatus(1);
						aturn_261(local_var_46[(local_var_52 / 3)]);
						switch ((local_var_52 / 3))
						{
							case 0:
								stdmotionplay_2c2(local_var_56[0], 20);
								break;
							case 1:
								stdmotionplay_2c2(local_var_56[1], 20);
								break;
							case 2:
								stdmotionplay_2c2(local_var_56[2], 20);
								break;
							case 3:
								stdmotionplay_2c2(local_var_56[3], 20);
								break;
						}
						wait(1);
						motionsync_282(1);
						wait(5);
					}
					local_var_52 = (local_var_52 + 3);
					if (local_var_52 >= (local_var_53 * 3))
					{
						local_var_52 = 0;
					}
					wait(1);
				}
				goto localjmp_240;
			localjmp_252:
				local_var_52 = 0;
				local_var_4e = 0;
				local_var_50 = 0;
				local_var_4a = 0.00600000005;
				local_var_4b = 0.00300000003;
				stdmotionplay(0x1000000);
				setpos(local_var_45[((local_var_53 - 1) * 3)], local_var_45[(((local_var_53 - 1) * 3) + 1)], local_var_45[(((local_var_53 - 1) * 3) + 2)]);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				local_var_57 = 1;
			localjmp_253:
				setreachr(0.100000001);
				if (local_var_48 <= 0)
				{
					wait(1);
				}
				else
				{
					setwalkspeed(local_var_48);
					stdmotionplay_2c2(0x1000000, 20);
					move(local_var_45[local_var_52], local_var_45[(local_var_52 + 1)], local_var_45[(local_var_52 + 2)]);
					wait(5);
					if (local_var_51 != 0)
					{
						wait(3);
						switch ((local_var_52 / 3))
						{
							case 0:
								stdmotionplay_2c2(local_var_56[0], 20);
								break;
							case 1:
								stdmotionplay_2c2(local_var_56[1], 20);
								break;
							case 2:
								stdmotionplay_2c2(local_var_56[2], 20);
								break;
							case 3:
								stdmotionplay_2c2(local_var_56[3], 20);
								break;
						}
						wait(1);
						motionsync_282(1);
						wait(5);
					}
					local_var_52 = (local_var_52 + 3);
					if (local_var_52 >= (local_var_53 * 3))
					{
						local_var_52 = 0;
					}
					wait(1);
				}
				goto localjmp_253;
			localjmp_265:
				local_var_52 = 0;
				local_var_4e = 0;
				local_var_50 = 0;
				local_var_4a = 0.00600000005;
				local_var_4b = 0.00300000003;
				stdmotionplay(0x1000000);
				setpos(local_var_45[((local_var_53 - 1) * 3)], local_var_45[(((local_var_53 - 1) * 3) + 1)], local_var_45[(((local_var_53 - 1) * 3) + 2)]);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				local_var_57 = 1;
			localjmp_266:
				setreachr(0.100000001);
				if (local_var_48 <= 0)
				{
					wait(1);
				}
				else
				{
					setwalkspeed(local_var_48);
					stdmotionplay_2c2(0x1000000, 20);
					move(local_var_45[local_var_52], local_var_45[(local_var_52 + 1)], local_var_45[(local_var_52 + 2)]);
					wait(5);
					if (local_var_51 != 0)
					{
						wait(3);
						setaturnlookatlockstatus(1);
						aturn_261(local_var_46[(local_var_52 / 3)]);
						switch ((local_var_52 / 3))
						{
							case 0:
								stdmotionplay_2c2(local_var_56[0], 20);
								break;
							case 1:
								stdmotionplay_2c2(local_var_56[1], 20);
								break;
							case 2:
								stdmotionplay_2c2(local_var_56[2], 20);
								break;
							case 3:
								stdmotionplay_2c2(local_var_56[3], 20);
								break;
						}
						wait(1);
						motionsync_282(1);
						wait(5);
					}
					local_var_52 = (local_var_52 + 3);
					if (local_var_52 >= (local_var_53 * 3))
					{
						local_var_52 = 0;
					}
					wait(1);
				}
				goto localjmp_266;
				while (true)
				{
				localjmp_278:
					stdmotionplay(local_var_56[0]);
					wait(1);
					motionsync_282(1);
					wait((15 + rand_29(15)));
					stdmotionplay(local_var_56[1]);
					wait(1);
					motionsync_282(1);
					wait((15 + rand_29(15)));
				}
		}
		local_var_52 = 0;
		local_var_4e = 0;
		local_var_50 = 0;
		local_var_4a = 0.00600000005;
		local_var_4b = 0.00300000003;
		switch (local_var_53)
		{
			case 0:
				usemapid(0);
				setweight(-1);
				stdmotionplay(0x1000000);
				while (true)
				{
					wait(0x12c);
				}
			case 1:
				local_var_52 = 0;
				local_var_4e = 0;
				local_var_50 = 0;
				local_var_4a = 0.00600000005;
				local_var_4b = 0.00300000003;
				motioncancel();
				stdmotionplay(0x1000000);
				while (true)
				{
					setreachr(0.100000001);
					local_var_4d = ((((local_var_4c - 1) * 0.800000012) + (((local_var_4c - 1) * (rand_29(100) % 20)) / 100)) + 1);
					local_var_46[1] = rand_29(100);
					local_var_46[1] = (local_var_46[1] / 100);
					local_var_46[2] = local_var_46[0];
					switch ((rand_29(100) % 2))
					{
						case 0:
							local_var_46[0] = normalang(((local_var_46[0] + 1.57079637) + (1.57079637 * local_var_46[1])));
							break;
						default:
							local_var_46[0] = normalang(((local_var_46[0] + -1.57079637) - (1.57079637 * local_var_46[1])));
							break;
					}
					if (local_var_51 != 0)
					{
						wait(local_var_4f);
						local_var_46[2] = normalang(((local_var_46[2] + 6.28318548) - (local_var_46[0] + 6.28318548)));
						if (!((local_var_46[2] >= -0.52359879 && local_var_46[2] <= 0.52359879)))
						{
							setaturnlookatlockstatus(1);
							aturn_261(local_var_46[0]);
						}
						wait(15);
					}
					else
					{
						wait((rand_29(30) + 40));
					}
					local_var_45[0] = (local_var_49[0] + (sin(local_var_46[0]) * local_var_4d));
					local_var_45[1] = local_var_49[1];
					local_var_45[2] = (local_var_49[2] + (cos(local_var_46[0]) * local_var_4d));
					local_var_47 = ((getlength3(local_var_45[0], local_var_45[1], local_var_45[2]) / local_var_48) + 1);
					setwalkspeed(local_var_48);
					stdmotionplay_2c2(0x1000000, 20);
					rmove(local_var_45[0], local_var_45[1], local_var_45[2]);
					local_var_4e = 0;
					while (!(local_var_4e >= local_var_47))
					{
						local_var_4e = (local_var_4e + 1);
						wait(1);
					}
					wait(1);
				}
			default:
				local_var_52 = 0;
				local_var_4e = 0;
				local_var_50 = 0;
				local_var_4a = 0.00600000005;
				local_var_4b = 0.00300000003;
				stdmotionplay(0x1000000);
				setpos(local_var_45[((local_var_53 - 1) * 3)], local_var_45[(((local_var_53 - 1) * 3) + 1)], local_var_45[(((local_var_53 - 1) * 3) + 2)]);
				while (true)
				{
					setreachr(0.100000001);
					if (local_var_48 <= 0)
					{
						wait(1);
					}
					else
					{
						setaturnlookatlockstatus(1);
						aturny(local_var_45[local_var_52], local_var_45[(local_var_52 + 2)]);
						setwalkspeed(local_var_48);
						stdmotionplay_2c2(0x1000000, 20);
						move(local_var_45[local_var_52], local_var_45[(local_var_52 + 1)], local_var_45[(local_var_52 + 2)]);
						if (local_var_51 != 0)
						{
							wait(local_var_4f);
						}
						local_var_52 = (local_var_52 + 3);
						if (local_var_52 >= (local_var_53 * 3))
						{
							local_var_52 = 0;
						}
						wait(1);
					}
				}
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_42[6];         // pos: 0x6;

	function talk_step01()
	{
		if (local_var_44 != 1)
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			setaturnlookatlockstatus(0);
			sysRaturna(-1);
			regI2 = 0;
			if (isturn())
			{
				turnsync();
				stdmotionplay_2c2(0x1000002, 20);
			}
			else
			{
				regI2 = 1;
			}
		}
		else
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			sysLookata(-1);
		}
		file_var_21 = ((local_var_41 << 16) | file_var_1e[local_var_41]);
		file_var_22 = local_var_42[file_var_1e[local_var_41]];
		sysDVAR(1, file_var_21);
		sysReq(1, 常駐助監督::NPC台詞代理);
		sysReqwait(常駐助監督::NPC台詞代理);
		local_var_42[file_var_1e[local_var_41]] = file_var_22;
		if (local_var_44 != 1)
		{
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			stdmotionplay_2c2(0x1000000, 20);
		}
		else
		{
			lookatoff();
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}


	function レイス後セット()
	{
		usemapid(1);
		stdmotionplay_2c2(0x1000000, 0);
		setpos(142.518051, 0, 52.6037064);
		dir_4e(149.682693, -0.225610003, 51.6340218);
		setcharclipone(0);
		return 0;
	}


	function ceek_papa_cut03()
	{
		sysLookata(シーク子);
		setwalkspeed(getdefaultrunspeed());
		move(145.64212, -0.0492429994, 52.0353966);
		return;
	}


	function ceek_papa_end()
	{
		usemapid(0);
		setpos(134.727005, 0.650756001, 51.4469147);
		dir(0.561716974);
		setcharclipone(1);
		return;
	}


	function NPCリクエスト停止()
	{
		return 0;
		return;
	}
}


script shop_npc_gd01(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function shopnpc_bindon()
	{
		setpos(78.5642548, -0.134308994, 56.2538986);
		dir(0.011399);
		bindp2_d4(0x3000009, 2, 0);
		set_ignore_hitgroup(1);
		setweight(-1);
		usecharhit(0);
		return;
	}
}


script shop_npc01(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x1000000);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		openfullscreenmenu_48d(2, 0);
		if (g_com_counter_for_horidasi[0] >= 255)
		{
			g_com_counter_for_horidasi[0] = 255;
		}
		else
		{
			g_com_counter_for_horidasi[0] = (g_com_counter_for_horidasi[0] + 1);
		}
		if ((g_com_counter_for_horidasi[0] >= 100 && !((g_com_check_for_horidasi[0] & 1))))
		{
			sethoridashi(0xd066);
			g_com_check_for_horidasi[0] = (g_com_check_for_horidasi[0] | 1);
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function shopnpc_bindon()
	{
		motionread(5);
		setpos(78.5642548, -0.134308994, 56.2538986);
		dir(0.011399);
		bindp2(0x3000002);
		set_ignore_hitgroup(1);
		setweight(-1);
		motionreadsync(5);
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(0, -1);
		motionloop(1);
		motionplay_bb(0x15000000, 0);
		sysReqi(1, shop_npc_gd01::shopnpc_bindon);
		sysReqwait(shop_npc_gd01::shopnpc_bindon);
		setshopname(0);
		setoverheadicontype(1);
		settalkradiusoffset(0, 0, 0.800000012);
		talkradius(0.150000006);
		setposbynaviicon(6, 0);
		setpos(78.5642548, -0.134308994, 56.2538986);
		dir(0.011399);
		return;
	}
}


script レイス依頼人(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_5a;            // pos: 0x0;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		if (!(isquestclear(131)))
		{
			switch (local_var_5a)
			{
				case lte(0):
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000001);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_5a = (local_var_5a + 1);
					break;
				default:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000001);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
			}
		}
		else
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x1000002);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
		}
		sysLookata(NPC17);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_5b;            // pos: 0x1;

	function talk_offerable()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		if (isquestorder(131))
		{
			switch (g_btl_nm_レイス)
			{
				case 0:
					setkutipakustatus(1);
					setunazukistatus(1);
					askpos(0, 0, 127);
					local_var_5b = aaske(0, 0x1000003);
					mesclose(0);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					switch (local_var_5b)
					{
						case 0:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000004);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							questeffectread(195);
							effectreadsync();
							effectplay(0);
							effectsync();
							g_btl_nm_レイス = 1;
							setquestscenarioflag(131, 30);
							g_btl_ネズミ = 2;
							sysReq(1, 常駐助監督::レイス判定);
							sysReqwait(常駐助監督::レイス判定);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000006);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
					break;
				case 1:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000007);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 2:
					fadeout(15);
					fadesync();
					g_com_navi_footcalc[0] = getx_12b(-1);
					g_com_navi_footcalc[1] = gety_12c(-1);
					g_com_navi_footcalc[2] = getz_12d(-1);
					setnavimapfootmarkstatus(0);
					sethpmenufast(0);
					settrapshowstatus(0);
					setcharseplayall(0);
					if (!(istownmap()))
					{
						setstatuserrordispdenystatus(1);
					}
					unkCall_5ac(1, 2.35800004, 100);
					sysReq(1, シーク子::レイス後セット);
					sysReq(3, NPC14::レイス後セット);
					partyusemapid(1);
					setposparty(147.628815, -0.270886987, 50.9111214, 0.806671023);
					setpos(148.11087, -0.270455003, 51.9912224);
					dir(-2.623878);
					stdmotionread(21);
					stdmotionreadsync();
					stdmotionplay(0x1000000);
					ヴァン.capturepc(-1);
					sysReqwait(シーク子::レイス後セット);
					sysReqall(1, reqArr0);
					wait(5);
					unkCall_5cf(1);
					sethpmenufast(0);
					settrapshowstatus(0);
					setcharseplayall(1);
					fadein(15);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000008);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					stdmotionplay(0x1000010);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000009);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					sysReqall(1, reqArr1);
					ヴァン.sysLookata(シーク子);
					sysLookata(シーク子);
					wait(45);
					setaturnlookatlockstatus(1);
					sysAturna(シーク子);
					stdmotionplay_2c2(0x1000016, 20);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100000a);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					NPC14.setkutipakustatus(1);
					NPC14.setunazukistatus(1);
					NPC14.amese(0, 0x100000b);
					NPC14.messync(0, 1);
					NPC14.setkutipakustatus(0);
					NPC14.setunazukistatus(0);
					setaturnlookatlockstatus(1);
					aturn_261(-3.05846095);
					sysLookata(NPC14);
					ヴァン.sysLookata(NPC14);
					sysReq(1, NPC14::ceek_papa_cut03);
					sysReqall(1, reqArr2);
					シーク子.setkutipakustatus(1);
					シーク子.setunazukistatus(1);
					シーク子.amese(0, 0x100000c);
					シーク子.messync(0, 1);
					シーク子.setkutipakustatus(0);
					シーク子.setunazukistatus(0);
					sysReqwait(NPC14::ceek_papa_cut03);
					NPC14.setkutipakustatus(1);
					NPC14.setunazukistatus(1);
					NPC14.amese(0, 0x100000d);
					NPC14.messync(0, 1);
					NPC14.setkutipakustatus(0);
					NPC14.setunazukistatus(0);
					ヴァン.sysLookata(レイス依頼人);
					NPC14.sysLookata(ヴァン);
					NPC14.stdmotionplay(0x1000013);
					sysLookata(ヴァン);
					stdmotionplay_2c2(0x1000010, 20);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100000e);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					sysReq(1, 常駐監督::レイス撃破);
					sysReqwait(常駐監督::レイス撃破);
					setcharseplayall(0);
					sethpmenufast(1);
					clear_force_char_nearfade();
					setmaphighmodeldepth(-1);
					setmapmodelstatus(1);
					setstatuserrordispdenystatus(0);
					settrapshowstatus(1);
					setstatuserrordispdenystatus(0);
					setpos(102.949165, -0.0842159986, 72.0658264);
					dir(-2.08562207);
					stdmotionread(18);
					stdmotionreadsync();
					stdmotionvariation(1);
					stdmotionplay_2c2(0x1000000, 0);
					setweight(-1);
					sysLookata(NPC17);
					setnpcname(0x26d);
					reqenable(2);
					ヴァン.lookatoff();
					ヴァン.releasepc();
					partyusemapid(1);
					setposparty(147.628815, -0.270886987, 50.9111214, 0.806671023);
					sysReq(1, シーク子::バインドオフ);
					sysReqwait(シーク子::バインドオフ);
					sysReq(1, NPC14::ceek_papa_end);
					sysReqwait(NPC14::ceek_papa_end);
					file_var_23 = 0;
					sysReq(1, NPC14::NPC挙動);
					setff12screenmode(-1);
					cameraclear();
					sysReq(1, 常駐助監督::レイス判定);
					sysReqwait(常駐助監督::レイス判定);
					sysReq(1, map閉じこめ民家::フィールドサインＯＫ);
					wait(5);
					ucon();
					sethpmenu(1);
					clear_force_char_nearfade();
					setmaphighmodeldepth(-1);
					setmapmodelstatus(1);
					setstatuserrordispdenystatus(0);
					settrapshowstatus(1);
					showparty();
					if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
					{
						clearnavimapfootmark();
					}
					setnavimapfootmarkstatus(1);
					setcharseplayall(1);
					sethpmenufast(1);
					clear_force_char_nearfade();
					setmaphighmodeldepth(-1);
					setmapmodelstatus(1);
					setstatuserrordispdenystatus(0);
					settrapshowstatus(1);
					wait(1);
					fadein(15);
					sysReqchg(2, レイス依頼人::talk);
					unkCall_5cf(0);
					break;
			}
		}
		else
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x100000f);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function レイス依頼人通常時セット()
	{
		hidecomplete();
		usemapid(0);
		setpos(103.167503, -0.125594005, 71.7191391);
		dir(-2.33105206);
		bindp2_d4(0x3000003, 3, 2);
		set_ignore_hitgroup(1);
		fetchambient();
		stdmotionread(18);
		stdmotionreadsync();
		stdmotionvariation(1);
		stdmotionplay_2c2(0x1000000, 0);
		setweight(-1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		sysLookata(NPC17);
		setnpcname(0x26d);
		reqenable(2);
		sysReqchg(2, レイス依頼人::talk);
		return;
	}


	function レイス依頼人依頼時セット()
	{
		hidecomplete();
		usemapid(0);
		setpos(151.208389, -0.113527998, 52.5859299);
		dir(2.18669605);
		bindp2_d4(0x3000003, 3, 2);
		fetchambient();
		set_ignore_hitgroup(1);
		stdmotionread(18);
		stdmotionreadsync();
		stdmotionvariation(1);
		stdmotionplay_2c2(0x1000000, 0);
		setweight(-1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		setnpcname(0x26d);
		reqenable(2);
		sysReqchg(2, レイス依頼人::talk_offerable);
		return;
	}
}


script シーク子(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function レイス後セット()
	{
		setpos(150.490906, -0.144670993, 51.677433);
		dir(-1.51940894);
		bindp2_d6(0x3000004, 1.29999995);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setscale(0.600000024);
		return;
	}


	function reis_cut01()
	{
		return;
	}


	function reis_cut0102()
	{
		setwalkspeed((getdefaultwalkspeed() * 0.600000024));
		setreachr(0);
		move(149.682693, -0.225610003, 51.6340218);
		return;
	}


	function バインドオフ()
	{
		bindoff();
		return 0;
	}


	function サマル配置()
	{
		setpos(92.1282196, -0.271631002, 52.6047096);
		dir(3.08191395);
		bindp2_d4(0x3000002, 3, 1);
		stdmotionread(21);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		return;
	}


	function サマル配置２()
	{
		setwalkspeed(getdefaultwalkspeed());
		setpos(93.4346848, -0.285573006, 47.5126114);
		dir(2.86102009);
		setautorelax(1);
		motionstartframe(80);
		motionloopframe(80, -1);
		motionloop(0);
		motionplay(0x14000004);
		wait(1);
		motionsync_282(1);
		stdmotionplay(0x1000016);
		return;
	}
}


script ヴァン(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function オルトロス最後配置()
	{
		usemapid(0);
		setpos(93.9195938, 0.105769999, 42.1235619);
		dir(2.34257698);
		bindp2(0x300000a);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(60, 120);
		motionloop(1);
		motionplay(0x14000002);
		sysLookata(空き巣シーク);
		setautorelax(1);
		return;
	}


	function オルトロス最後()
	{
		setpos(93.687149, -0.231049001, 46.1820831);
		dir(-0.230535999);
		motionplay_bb(0x14000001, 0);
		sysLookata(シーク子);
		wait(1);
		motionsync_282(1);
		return;
	}


	function オルトロスもらう()
	{
		motionplay_bb(0x14000000, 5);
		return;
	}


	function バインドオフ()
	{
		bindoff();
		usemapid(1);
		return 0;
	}


	function ミュート扉開け配置()
	{
		usemapid(0);
		setpos(91.8773575, -0.271838009, 53.2539825);
		dir(3.02951598);
		bindp2(0x300000a);
		usecharhit(0);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setautorelax(1);
		return;
	}


	function アジトはいるとき配置()
	{
		usemapid(0);
		setpos(48.4809456, -0.223937005, 116.843475);
		dir(-1.20690799);
		bindp2(0x300000a);
		usecharhit(0);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setautorelax(1);
		return;
	}
}


script ミュート(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		if (((シナリオフラグ >= 180 && シナリオフラグ <= 0x122) && (ミュートと集積場フラグ[0] & 2)))
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x1000010);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
		}
		else
		{
			ミュート.setkutipakustatus(1);
			ミュート.setunazukistatus(1);
			switch (シナリオフラグ)
			{
				case lte(0x157):
					ミュート.amese(0, 0x1000011);
					messync(0, 1);
					break;
				case lte(0x5f0):
					ミュート.amese(0, 0x1000012);
					messync(0, 1);
					break;
				case lte(0x1004):
					ミュート.amese(0, 0x1000013);
					messync(0, 1);
					break;
				default:
					ミュート.amese(0, 0x1000013);
					messync(0, 1);
					break;
			}
			ミュート.setkutipakustatus(0);
			ミュート.setunazukistatus(0);
		}
		stdmotionplay(0x1000000);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		aturn_261(getdestrotangzx_227(0));
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_5c;            // pos: 0x0;
	u_char  local_var_5d;            // pos: 0x1;

	function talk_medal2()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		switch (メダルかけら２進行フラグ)
		{
			case lte(2):
				if (!((メダルかけら２進行フラグ２ & 2)))
				{
					stdmotionplay_2c2(0x1000010, 20);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000014);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					メダルかけら２進行フラグ２ = (メダルかけら２進行フラグ２ | 2);
					メダルかけら２進行フラグ = 2;
				}
				else
				{
					stdmotionplay(0x1000016);
					setkutipakustatus(1);
					setunazukistatus(1);
					askpos(0, 0, 127);
					local_var_5d = aaske(0, 0x1000015);
					mesclose(0);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					switch (local_var_5d)
					{
						case 0:
							stdmotionplay(0x1000010);
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000016);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 1:
							stdmotionplay(0x1000016);
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000017);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						default:
							break;
					}
				}
				break;
			case lte(6):
				if (local_var_5c == 0)
				{
					stdmotionplay(0x1000010);
					setkutipakustatus(1);
					setunazukistatus(1);
					askpos(0, 0, -1);
					local_var_5d = aaske(0, 0x1000018);
					mesclose(0);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					switch (local_var_5d)
					{
						case 0:
							stdmotionplay(0x1000010);
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000019);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						case 1:
							stdmotionplay(0x1000012);
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100001a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
						default:
							break;
					}
					メダルかけら２進行フラグ２ = (メダルかけら２進行フラグ２ | 4);
					local_var_5c = (local_var_5c + 1);
				}
				else
				{
					stdmotionplay(0x1000016);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100001b);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				if (メダルかけら２進行フラグ >= 5)
				{
					lookatoff();
					wait(15);
					sysAturna(-1);
					stdmotionplay(0x1000010);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100001c);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					stdmotionplay(0x1000000);
				}
				break;
			default:
				break;
		}
		stdmotionplay(0x1000000);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talk_after_medal()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x100001d);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		return;
	}


	function talkhold_medal2()
	{
		return;
	}


	function talkterm_medal2()
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_5e;            // pos: 0x9;
	float   local_var_5f[15];        // pos: 0xc;
	float   local_var_60[5];         // pos: 0x48;
	float   local_var_62;            // pos: 0x60;
	float   local_var_63[4];         // pos: 0x64;
	float   local_var_66;            // pos: 0x7c;
	int     local_var_69;            // pos: 0x98;
	u_char  local_var_6b;            // pos: 0xa0;
	u_char  local_var_6d;            // pos: 0xa2;
	u_char  local_var_6e;            // pos: 0xa3;
	u_char  local_var_6f;            // pos: 0xa4;

	function 終盤位置()
	{
		hidecomplete();
		local_var_5e = 0;
		local_var_6e = 1;
		local_var_6f = 0;
		local_var_6b = 0;
		local_var_6d = 1;
		local_var_69 = 0;
		local_var_66 = 2;
		local_var_5f[0] = 85.9494858;
		local_var_5f[1] = -0.275570989;
		local_var_5f[2] = 59.6175919;
		local_var_60[0] = -1.553352;
		local_var_63[0] = 85.9494858;
		local_var_63[1] = -0.275570989;
		local_var_63[2] = 59.6175919;
		local_var_63[3] = -1.553352;
		setpos(local_var_5f[0], local_var_5f[1], local_var_5f[2]);
		dir(local_var_60[0]);
		bindp2(0x300000b);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(47);
		local_var_62 = getdefaultwalkspeed();
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_61;            // pos: 0x5c;
	float   local_var_64;            // pos: 0x74;
	float   local_var_65;            // pos: 0x78;
	float   local_var_67;            // pos: 0x80;
	int     local_var_68;            // pos: 0x94;
	int     local_var_6a;            // pos: 0x9c;
	u_char  local_var_6c;            // pos: 0xa1;

	function 終盤挙動()
	{
		local_var_6c = 0;
		local_var_68 = 0;
		local_var_6a = 0;
		local_var_64 = 0.00600000005;
		local_var_65 = 0.00300000003;
		motioncancel();
		stdmotionplay(0x1000000);
		while (true)
		{
			setreachr(0.100000001);
			local_var_67 = ((((local_var_66 - 1) * 0.800000012) + (((local_var_66 - 1) * (rand_29(100) % 20)) / 100)) + 1);
			local_var_60[1] = rand_29(100);
			local_var_60[1] = (local_var_60[1] / 100);
			local_var_60[2] = local_var_60[0];
			switch ((rand_29(100) % 2))
			{
				case 0:
					local_var_60[0] = normalang(((local_var_60[0] + 1.57079637) + (1.57079637 * local_var_60[1])));
					break;
				default:
					local_var_60[0] = normalang(((local_var_60[0] + -1.57079637) - (1.57079637 * local_var_60[1])));
					break;
			}
			if (local_var_6b != 0)
			{
				wait(local_var_69);
				local_var_60[2] = normalang(((local_var_60[2] + 6.28318548) - (local_var_60[0] + 6.28318548)));
				if (!((local_var_60[2] >= -0.52359879 && local_var_60[2] <= 0.52359879)))
				{
					setaturnlookatlockstatus(1);
					aturn_261(local_var_60[0]);
				}
				wait(15);
			}
			else
			{
				wait((rand_29(30) + 40));
			}
			local_var_5f[0] = (local_var_63[0] + (sin(local_var_60[0]) * local_var_67));
			local_var_5f[1] = local_var_63[1];
			local_var_5f[2] = (local_var_63[2] + (cos(local_var_60[0]) * local_var_67));
			local_var_61 = ((getlength3(local_var_5f[0], local_var_5f[1], local_var_5f[2]) / local_var_62) + 1);
			setwalkspeed(local_var_62);
			stdmotionplay_2c2(0x1000000, 20);
			rmove(local_var_5f[0], local_var_5f[1], local_var_5f[2]);
			local_var_68 = 0;
			while (!(local_var_68 >= local_var_61))
			{
				local_var_68 = (local_var_68 + 1);
				wait(1);
			}
			wait(1);
		}
	}


	function 解錠配置()
	{
		hidecomplete();
		dir(2.26501894);
		setpos(89.4015121, -0.130230993, 56.8483505);
		bindp2(0x300000b);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(47);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function 立ち去る()
	{
		setwalkspeed(getdefaultrunspeed());
		move(87.9774551, -0.275570989, 59.447094);
		move(78.1080093, -0.275570989, 59.8111725);
		move(78.765831, -0.126662999, 66.2366486);
		return;
	}


	function 消す()
	{
		bindoff();
		return 0;
	}


	function 集積所オープン位置00()
	{
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);
		aturn(91.4692459, -0.124884002, 56.4972343);
		move(91.4692459, -0.124884002, 56.4972343);
		return;
	}


	function 集積所オープン位置00_02()
	{
		movecancel();
		turncancel();
		setwalkspeed(getdefaultrunspeed());
		stdmotionplay(0x1000000);
		setaturnlookatlockstatus(1);
		aturn(91.4692459, -0.124884002, 56.4972343);
		move(91.4692459, -0.124884002, 56.4972343);
		sysAturna(-1);
		return 0;
	}


	function 集積所オープン位置()
	{
		setpos(91.4692459, -0.124884002, 56.4972343);
		dir(2.26501894);
		stdmotionplay(0x1000016);
		lookat_24f(92.6315155, 1.27532303, 54.9561882);
		return 0;
	}


	function 集積所オープン移動()
	{
		lookatoff();
		stdmotionplay_2c2(0x1000000, 5);
		setwalkspeed(getdefaultwalkspeed());
		usecharhit(0);
		rmove(91.4876175, -0.124884002, 54.4460526);
		return;
	}


	function 集積所オープン元の位置に戻る()
	{
		reqdisable(17);
		reqdisable(16);
		movecancel();
		usecharhit(1);
		setwalkspeed(getdefaultwalkspeed());
		aturn(89.5174255, -0.124884002, 57.7139854);
		move(89.5174255, -0.124884002, 57.7139854);
		move(89.4015121, -0.130230993, 56.8483505);
		setaturnlookatlockstatus(1);
		aturn_261(2.26501894);
		reqenable(17);
		reqenable(16);
		return;
	}


	function 水路オープン位置()
	{
		setpos(91.3155136, -0.275047004, 56.9818268);
		dir(2.97677994);
		stdmotionplay_2c2(0x1000000, 0);
		return 0;
	}


	function 水路オープン位置移動()
	{
		usecharhit(0);
		stdmotionplay_2c2(0x1000000, 2);
		setwalkspeed(getdefaultwalkspeed());
		amove(91.8066177, -0.272188008, 53.8123627);
		sysLookata(ヴァン);
		return;
	}


	function 水路オープン位置移動２()
	{
		setwalkspeed(getdefaultwalkspeed());
		move(91.7288895, -0.26956901, 51.6336479);
		setaturnlookatlockstatus(1);
		sysAturna(-1);
		return;
	}


	function 水路オープン位置移動３()
	{
		stdmotionplay_2c2(0x1000000, 2);
		amove(89.0448151, -0.133811995, 51.9664116);
		return;
	}


	function 水路オープン位置移動４()
	{
		setpos(88.962944, -0.133811995, 51.7170067);
		dir(1.55324697);
		setwalkspeed(getdefaultwalkspeed());
		move(91.3237152, -0.133811995, 51.7784538);
		sysAturna(-1);
		return;
	}


	function 初期位置戻り()
	{
		usecharhit(1);
		stdmotionplay_2c2(0x1000000, 0);
		dir(2.26501894);
		setpos(89.4015121, -0.130230993, 56.8483505);
		reqenable(2);
		return 0;
	}


	function 左の扉配置()
	{
		setpos(91.5191116, -0.268945992, 51.5252953);
		dir(0.336661994);
		bindp2(0x300000b);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(47);
		reqdisable(2);
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_25[2];          // pos: 0x4f;


script LITTSSET01(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_7c;            // pos: 0xa8;

	function init()
	{
		local_var_7c = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_70;            // pos: 0x0;
	u_char  local_var_80;            // pos: 0x216;

	function talk(2)
	{
		if (scratch2_var_2e == 0)
		{
			sysSysreq(7, リッツ::talk);
		}
		else
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			local_var_80 = 0;
			switch (local_var_7c)
			{
				case 0:
					if (file_var_25[0])
					{
						stdmotionplay(0x1000000);
						local_var_80 = 1;
					}
					break;
				case 1:
					if (file_var_25[1])
					{
						stdmotionplay(0x1000000);
						local_var_80 = 1;
					}
					break;
			}
			sysLookata(-1);
			reqdisable(17);
			reqdisable(16);
			switch (local_var_7c)
			{
				case 0:
					switch (local_var_70)
					{
						case lte(0):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100001e);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							local_var_70 = (local_var_70 + 1);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100001e);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
					if (local_var_80 != 0)
					{
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x12000000, 5);
					}
					break;
				case 1:
					switch (local_var_70)
					{
						case lte(0):
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100001f);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							local_var_70 = (local_var_70 + 1);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100001f);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
					if (local_var_80 != 0)
					{
						setautorelax(0);
						motionstartframe(0);
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x12000003, 5);
					}
					break;
			}
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}


	function talk_beforeajito()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		switch (local_var_70)
		{
			case lte(0):
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000020);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				local_var_70 = (local_var_70 + 1);
				break;
			default:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000020);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		aturn_261(getdestrotangzx_227(0));
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_74;            // pos: 0x60;
	float   local_var_7d[30][3];     // pos: 0xac;
	u_char  local_var_7e;            // pos: 0x214;

	function リッツ軍ダラン爺前配置()
	{
		hidecomplete();
		sysDVAR(2, scratch2_var_2d[0]);
		sysDVAR(3, scratch2_var_2e);
		scratch2_var_2f[0] = 28;
		scratch2_var_2f[1] = 14;
		switch (local_var_7c)
		{
			case 0:
				local_var_7d[0][0] = 164.13913;
				local_var_7d[0][1] = -0.275570989;
				local_var_7d[0][2] = 47.4619751;
				local_var_7d[1][0] = 160.972412;
				local_var_7d[1][1] = -0.275570989;
				local_var_7d[1][2] = 61.1836815;
				local_var_7d[2][0] = 152.003265;
				local_var_7d[2][1] = -0.275570989;
				local_var_7d[2][2] = 60.0644569;
				local_var_7d[3][0] = 142.190369;
				local_var_7d[3][1] = -0.196744993;
				local_var_7d[3][2] = 62.2241325;
				local_var_7d[4][0] = 136.661957;
				local_var_7d[4][1] = -0.214541003;
				local_var_7d[4][2] = 67.7713928;
				local_var_7d[5][0] = 128.901382;
				local_var_7d[5][1] = -0.199693993;
				local_var_7d[5][2] = 68.8246307;
				local_var_7d[6][0] = 116.258217;
				local_var_7d[6][1] = -0.251892;
				local_var_7d[6][2] = 66.5057449;
				local_var_7d[7][0] = 107.146904;
				local_var_7d[7][1] = 0.0596170016;
				local_var_7d[7][2] = 69.1481781;
				local_var_7d[8][0] = 119.554695;
				local_var_7d[8][1] = -0.273187011;
				local_var_7d[8][2] = 66.6857681;
				local_var_7d[9][0] = 131.271805;
				local_var_7d[9][1] = 0.0596170016;
				local_var_7d[9][2] = 68.872345;
				local_var_7d[10][0] = 136.959015;
				local_var_7d[10][1] = -0.223771006;
				local_var_7d[10][2] = 67.0375977;
				local_var_7d[11][0] = 141.709274;
				local_var_7d[11][1] = -0.253383994;
				local_var_7d[11][2] = 72.4564972;
				local_var_7d[12][0] = 141.979904;
				local_var_7d[12][1] = -0.210346997;
				local_var_7d[12][2] = 79.4188461;
				local_var_7d[13][0] = 137.655853;
				local_var_7d[13][1] = -0.241749004;
				local_var_7d[13][2] = 92.52948;
				local_var_7d[14][0] = 139.403992;
				local_var_7d[14][1] = -0.272702992;
				local_var_7d[14][2] = 102.921783;
				local_var_7d[15][0] = 135.01236;
				local_var_7d[15][1] = -0.177757993;
				local_var_7d[15][2] = 109.62162;
				local_var_7d[16][0] = 129.36116;
				local_var_7d[16][1] = -0.139651999;
				local_var_7d[16][2] = 109.232307;
				local_var_7d[17][0] = 125.146103;
				local_var_7d[17][1] = -0.134373993;
				local_var_7d[17][2] = 115.852684;
				local_var_7d[18][0] = 112.3517;
				local_var_7d[18][1] = -0.139651999;
				local_var_7d[18][2] = 109.916771;
				local_var_7d[19][0] = 101.871971;
				local_var_7d[19][1] = -0.220780998;
				local_var_7d[19][2] = 110.812256;
				local_var_7d[20][0] = 100.800339;
				local_var_7d[20][1] = -0.275570989;
				local_var_7d[20][2] = 121.46991;
				local_var_7d[21][0] = 92.8553085;
				local_var_7d[21][1] = -0.275570989;
				local_var_7d[21][2] = 124.072388;
				local_var_7d[22][0] = 86.4998322;
				local_var_7d[22][1] = -0.275570989;
				local_var_7d[22][2] = 122.918869;
				local_var_7d[23][0] = 84.1387863;
				local_var_7d[23][1] = -0.275570989;
				local_var_7d[23][2] = 120.648331;
				local_var_7d[24][0] = 85.8115845;
				local_var_7d[24][1] = -0.249687001;
				local_var_7d[24][2] = 122.398918;
				local_var_7d[25][0] = 97.8897781;
				local_var_7d[25][1] = -0.275570989;
				local_var_7d[25][2] = 122.687714;
				local_var_7d[26][0] = 98.6415176;
				local_var_7d[26][1] = -0.275570989;
				local_var_7d[26][2] = 131.591187;
				local_var_7d[27][0] = 101.25592;
				local_var_7d[27][1] = -0.275570989;
				local_var_7d[27][2] = 145.293564;
				local_var_7d[28][0] = 98.2619934;
				local_var_7d[28][1] = -0.227232993;
				local_var_7d[28][2] = 190.693039;
				local_var_7d[29][0] = 98.2619934;
				local_var_7d[29][1] = -0.227232993;
				local_var_7d[29][2] = 220.693039;
				local_var_7e = 28;
				setnpcname(0x27b);
				break;
			default:
				local_var_7d[0][0] = 165.529144;
				local_var_7d[0][1] = -0.254088998;
				local_var_7d[0][2] = 48.4301987;
				local_var_7d[1][0] = 165.496338;
				local_var_7d[1][1] = -0.275490999;
				local_var_7d[1][2] = 59.7094421;
				local_var_7d[2][0] = 161.079254;
				local_var_7d[2][1] = -0.0745730028;
				local_var_7d[2][2] = 66.5787201;
				local_var_7d[3][0] = 162.414612;
				local_var_7d[3][1] = -0.199823007;
				local_var_7d[3][2] = 80.899498;
				local_var_7d[4][0] = 166.564728;
				local_var_7d[4][1] = -0.196468994;
				local_var_7d[4][2] = 89.8718567;
				local_var_7d[5][0] = 173.310959;
				local_var_7d[5][1] = -0.275570989;
				local_var_7d[5][2] = 94.9910049;
				local_var_7d[6][0] = 174.082932;
				local_var_7d[6][1] = -0.211555004;
				local_var_7d[6][2] = 105.978783;
				local_var_7d[7][0] = 159.425415;
				local_var_7d[7][1] = -0.275570989;
				local_var_7d[7][2] = 106.88501;
				local_var_7d[8][0] = 156.656708;
				local_var_7d[8][1] = -0.275570989;
				local_var_7d[8][2] = 110.473511;
				local_var_7d[9][0] = 157.81192;
				local_var_7d[9][1] = -0.228266999;
				local_var_7d[9][2] = 116.198364;
				local_var_7d[10][0] = 155.560043;
				local_var_7d[10][1] = -0.275570989;
				local_var_7d[10][2] = 123.783005;
				local_var_7d[11][0] = 141.722702;
				local_var_7d[11][1] = -0.233779997;
				local_var_7d[11][2] = 125.956383;
				local_var_7d[12][0] = 137.725616;
				local_var_7d[12][1] = -0.176508993;
				local_var_7d[12][2] = 134.925095;
				local_var_7d[13][0] = 138.381653;
				local_var_7d[13][1] = -0.250984013;
				local_var_7d[13][2] = 145.717117;
				local_var_7d[14][0] = 138.381653;
				local_var_7d[14][1] = -0.250984013;
				local_var_7d[14][2] = 220.717117;
				local_var_7d[15][0] = 138.381653;
				local_var_7d[15][1] = -0.250984013;
				local_var_7d[15][2] = 230.717117;
				local_var_7e = 14;
				setnpcname(0x27c);
				break;
		}
		switch (scratch2_var_2e)
		{
			case 0:
				reqdisable(2);
				switch (local_var_7c)
				{
					case 0:
						setpos(166.02623, -0.197705001, 41.4640846);
						dir(1.271945);
						bindp2_d4(0x3000007, 1, 0);
						break;
					case 1:
						setpos(166.092926, -0.189742997, 42.6203804);
						dir(2.05867791);
						bindp2_d4(0x3000006, 1, 2);
						break;
				}
				set_ignore_hitgroup(2);
				break;
			default:
				if (scratch2_var_2d[local_var_7c] >= scratch2_var_2f[local_var_7c])
				{
					return;
				}
				setpos(local_var_7d[scratch2_var_2d[local_var_7c]][0], local_var_7d[scratch2_var_2d[local_var_7c]][1], local_var_7d[scratch2_var_2d[local_var_7c]][2]);
				dir_4e(local_var_7d[(scratch2_var_2d[local_var_7c] + 1)][0], local_var_7d[(scratch2_var_2d[local_var_7c] + 1)][1], local_var_7d[(scratch2_var_2d[local_var_7c] + 1)][2]);
				reqenable(2);
				switch (local_var_7c)
				{
					case 0:
						bindp2_d4(0x3000007, 1, 0);
						setnpcname(0x27b);
						sysReq(1, LITTSSET01::訓練);
						break;
					case 1:
						bindp2_d4(0x3000006, 1, 2);
						setnpcname(0x27c);
						sysReq(1, LITTSSET02::訓練);
						break;
				}
				set_ignore_hitgroup(2);
				break;
		}
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		local_var_74 = getdefaultwalkspeed();
		setweight(-1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function リッツ軍挙動()
	{
		switch (scratch2_var_2e)
		{
			case 0:
				break;
			default:
				break;
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_7f;            // pos: 0x215;

	function 訓練()
	{
		setpos(local_var_7d[scratch2_var_2d[local_var_7c]][0], local_var_7d[scratch2_var_2d[local_var_7c]][1], local_var_7d[scratch2_var_2d[local_var_7c]][2]);
		dir_4e(local_var_7d[(scratch2_var_2d[local_var_7c] + 1)][0], local_var_7d[(scratch2_var_2d[local_var_7c] + 1)][1], local_var_7d[(scratch2_var_2d[local_var_7c] + 1)][2]);
		switch (local_var_7c)
		{
			case 0:
				setnpcname(0x27b);
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x12000000, 0);
				break;
			case 1:
				setautorelax(0);
				motionstartframe(0);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x12000003, 0);
				break;
		}
		for (local_var_7f = (scratch2_var_2d[local_var_7c] + 1); local_var_7f <= local_var_7e; local_var_7f = (local_var_7f + 1))
		{
			switch (local_var_7c)
			{
				case 0:
					switch (local_var_7f)
					{
						case 8:
						case 24:
							sethomingangzx(1.57079637);
							sethomingrotangzx(1.57079637);
							break;
						case gte(26):
							usemapid(0);
						default:
							sethomingangzx(0.052359879);
							sethomingrotangzx(0.052359879);
							break;
					}
					setwalkspeed(0.108669996);
					rlookat_257(local_var_7d[local_var_7f][0], local_var_7d[local_var_7f][1], local_var_7d[local_var_7f][2]);
					break;
				case 1:
					switch (local_var_7f)
					{
						case gte(13):
							usemapid(0);
							break;
					}
					setwalkspeed(getdefaultwalkspeed());
					sethomingangzx(0.157079637);
					sethomingrotangzx(0.157079637);
					break;
			}
			scratch2_var_2d[local_var_7c] = local_var_7f;
			move(local_var_7d[local_var_7f][0], local_var_7d[local_var_7f][1], local_var_7d[local_var_7f][2]);
			switch (local_var_7c)
			{
				case 0:
					sysDVAR(11, local_var_7f);
					switch (local_var_7f)
					{
						case 7:
						case 23:
							setautorelax(1);
							stdmotionplay_2c2(0x1000012, 20);
							wait(1);
							motionsync_282(1);
							stdmotionplay(0x1000000);
							wait(15);
							sethomingangzx(3.14159274);
							sethomingrotangzx(3.14159274);
							aturn(local_var_7d[(local_var_7f + 1)][0], local_var_7d[(local_var_7f + 1)][1], local_var_7d[(local_var_7f + 1)][2]);
							sethomingangzx(0.052359879);
							sethomingrotangzx(0.052359879);
							lookatoff();
							stdmotionplay(0x1000000);
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x12000000, 5);
							break;
						case gte(26):
							usemapid(0);
							break;
					}
					break;
				case 1:
					sysDVAR(10, local_var_7f);
					switch (local_var_7f)
					{
						case 2:
						case 3:
							stdmotionplay_2c2(0x1000000, 8);
							wait(10);
							motionplay(0x12000002);
							wait(1);
							motionsync_282(1);
							wait(10);
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x12000003, 5);
							break;
						case 9:
							stdmotionplay_2c2(0x1000000, 8);
							setaturnlookatlockstatus(1);
							aturn_261(1.62714803);
							stdmotionplay(0x1000016);
							wait(1);
							motionsync_282(1);
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x12000003, 5);
							break;
						case 10:
							stdmotionplay_2c2(0x1000000, 8);
							setaturnlookatlockstatus(1);
							aturn_261(0.236059994);
							motionplay(0x12000002);
							wait(1);
							motionsync_282(1);
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x12000003, 5);
							break;
						case 12:
							stdmotionplay_2c2(0x1000000, 8);
							setaturnlookatlockstatus(1);
							aturn_261(-2.1452961);
							stdmotionplay(0x1000010);
							wait(1);
							motionsync_282(1);
							setautorelax(0);
							motionstartframe(0);
							motionloopframe(0, -1);
							motionloop(1);
							motionplay_bb(0x12000003, 5);
							break;
						case gte(13):
							usemapid(0);
							break;
					}
					break;
			}
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_71;            // pos: 0x9;
	float   local_var_72[15];        // pos: 0xc;
	float   local_var_73[5];         // pos: 0x48;
	float   local_var_75[4];         // pos: 0x64;
	float   local_var_76;            // pos: 0x7c;
	int     local_var_77;            // pos: 0x98;
	u_char  local_var_78;            // pos: 0xa0;
	u_char  local_var_79;            // pos: 0xa2;
	u_char  local_var_7a;            // pos: 0xa3;
	u_char  local_var_7b;            // pos: 0xa4;

	function リッツ軍アジト向かい時配置()
	{
		hidecomplete();
		switch (local_var_7c)
		{
			case 0:
				usemapid(0);
				local_var_71 = 4;
				local_var_7a = 1;
				local_var_7b = 0;
				local_var_78 = 0;
				local_var_79 = 0;
				local_var_77 = 0;
				local_var_76 = 0;
				local_var_72[0] = 95.3936844;
				local_var_72[1] = -0.137865007;
				local_var_72[2] = 120.998474;
				local_var_73[0] = -1.601192;
				local_var_75[0] = 95.3936844;
				local_var_75[1] = -0.137865007;
				local_var_75[2] = 120.998474;
				local_var_75[3] = -1.601192;
				setnpcname(0x27b);
				reqenable(2);
				break;
			case 1:
				usemapid(0);
				local_var_71 = 3;
				local_var_7a = 4;
				local_var_7b = 1;
				local_var_78 = 0;
				local_var_79 = 0;
				local_var_77 = 0;
				local_var_76 = 1.5;
				local_var_72[0] = 94.6260986;
				local_var_72[1] = -0.134403005;
				local_var_72[2] = 121.162437;
				local_var_73[0] = 1.66717994;
				local_var_75[0] = 94.6260986;
				local_var_75[1] = -0.134403005;
				local_var_75[2] = 121.162437;
				local_var_75[3] = 1.66717994;
				reqdisable(2);
				break;
		}
		setpos(local_var_72[0], local_var_72[1], local_var_72[2]);
		dir(local_var_73[0]);
		switch (local_var_71)
		{
			case 3:
				bindp2_d4(0x3000006, local_var_7a, local_var_7b);
				break;
			case 4:
				bindp2_d4(0x3000007, local_var_7a, local_var_7b);
				break;
		}
		set_ignore_hitgroup(2);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		local_var_74 = getdefaultwalkspeed();
		fetchambient();
		setweight(-1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function リッツ軍アジト向かい時挙動()
	{
		switch (local_var_7c)
		{
			case 0:
				setnpcname(0x27b);
				while (true)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					wait(60);
				}
			case 1:
				reqdisable(2);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					stdmotionplay(0x1000012);
					wait(1);
					motionsync_282(1);
					stdmotionplay(0x1000015);
					wait(1);
					motionsync_282(1);
				}
		}
		return;
	}
}


script リッツ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		if (scratch2_var_2e == 0)
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			if (!((g_iw_便利フラグ[0] & 8)))
			{
				if (getangle_1e9(-1) >= 0)
				{
					stdmotionplay(0x1000004);
				}
				else
				{
					stdmotionplay(0x1000003);
				}
				sysTurna(-1);
				turnsync();
				stdmotionplay(0x1000000);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000021);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				turn_62(-1.734653);
				turnsync();
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000022);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				g_iw_便利フラグ[0] = (g_iw_便利フラグ[0] | 8);
			}
			else
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000023);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
			}
			fadeout(15);
			fadesync();
			setcharseplayall(0);
			sethpmenufast(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			setstatuserrordispdenystatus(0);
			unkCall_5ac(0, 0, 0);
			sysReq(2, hitactor01_firo::clearHitObj);
			bindoff();
			partyusemapid(1);
			setposparty(165.279709, -0.275570989, 40.4224358, 0.0322300009);
			sysReq(1, LITTSSET01::訓練);
			sysReq(1, LITTSSET02::訓練);
			resetbehindcamera(0.0322300009, 0.600000024);
			wait(4);
			showparty();
			if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
			{
				clearnavimapfootmark();
			}
			setnavimapfootmarkstatus(1);
			setcharseplayall(1);
			sethpmenufast(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			wait(1);
			fadein(15);
			scratch2_var_2e = 1;
			sysReq(2, リッツ::移動監視);
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}


	function リッツ軍ダラン爺前配置()
	{
		hidecomplete();
		usemapid(0);
		switch (scratch2_var_2e)
		{
			case 0:
				setpos(167.24794, -0.0635590032, 41.8958511);
				dir(-1.734653);
				break;
			default:
				sysReq(1, リッツ::移動監視);
				return;
		}
		bindp2(0x300000c);
		set_ignore_hitgroup(2);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setnpcname(48);
		setweight(-1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		sysReq(1, リッツ::リッツ軍挙動);
		return;
	}


	function リッツ軍挙動()
	{
		switch (scratch2_var_2e)
		{
			case 0:
				LITTSSET01.reqenable(2);
				LITTSSET02.reqenable(2);
				while (true)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					stdmotionplay(0x1000011);
					wait(1);
					motionsync_282(1);
					wait(10);
					stdmotionplay(0x1000012);
					wait(1);
					motionsync_282(1);
					wait(10);
				}
			default:
				break;
		}
		return;
	}


	function 移動監視()
	{
		while (true)
		{
			file_var_25[0] = LITTSSET01.ismove();
			file_var_25[1] = LITTSSET02.ismove();
			wait(1);
		}
	}
}


script 空き巣シーク(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_81;            // pos: 0x0;
	u_char  local_var_82;            // pos: 0x1;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		sysReq(1, 常駐助監督::オルトロス判定);
		sysReqwait(常駐助監督::オルトロス判定);
		switch (file_var_26)
		{
			case 0:
				break;
			case 1:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000024);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
			case 2:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000025);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				lookatoff();
				motionratio(0);
				wait(10);
				fadecolor(64, 64, 64, 64);
				fadeout(30);
				fadesync();
				motionratio(1);
				sysLookata(-1);
				wait(15);
				fadein(5);
				setkutipakustatus(1);
				setunazukistatus(1);
				askpos(0, 0, 127);
				local_var_81 = aaske(0, 0x1000026);
				mesclose(0);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				switch (local_var_81)
				{
					case 0:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000027);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000028);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000029);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						questeffectread(226);
						effectreadsync();
						effectplay(0);
						effectsync();
						g_btl_nm_オルトロス = 1;
						setquestscenarioflag(162, 30);
						settalknpcname(0x25f);
						break;
					default:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100002a);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
				}
				break;
			case 3:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100002b);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
			case 4:
				fadeout(15);
				fadesync();
				g_com_navi_footcalc[0] = getx_12b(-1);
				g_com_navi_footcalc[1] = gety_12c(-1);
				g_com_navi_footcalc[2] = getz_12d(-1);
				setnavimapfootmarkstatus(0);
				sethpmenufast(0);
				settrapshowstatus(0);
				setcharseplayall(0);
				if (!(istownmap()))
				{
					setstatuserrordispdenystatus(1);
				}
				unkCall_5ac(1, 2.35800004, 100);
				motionread(4);
				motionreadsync(4);
				sysReq(1, シーク子::サマル配置);
				sysReq(1, ヴァン::オルトロス最後配置);
				sysReqwait(シーク子::サマル配置);
				sysReqwait(ヴァン::オルトロス最後配置);
				partyusemapid(1);
				setposparty(93.9195938, 0.105769999, 42.1235619, 2.34257698);
				hideparty();
				camerastart_7e(0, 0x2000000);
				wait(5);
				sethpmenufast(0);
				settrapshowstatus(0);
				setcharseplayall(1);
				fadein(15);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100002c);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				sysLookata(シーク子);
				ヴァン.sysLookata(シーク子);
				wait(15);
				camerastart_7e(0, 0x2000001);
				シーク子.stdmotionplay(0x1000016);
				シーク子.setkutipakustatus(1);
				シーク子.setunazukistatus(1);
				シーク子.amese(0, 0x100002d);
				シーク子.messync(0, 1);
				シーク子.setkutipakustatus(0);
				シーク子.setunazukistatus(0);
				wait(10);
				ヴァン.sysLookata(空き巣シーク);
				wait(5);
				camerastart_7e(0, 0x2000002);
				sysLookata(-1);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100002e);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				sysReq(1, 常駐監督::オルトロスクリア);
				sysReqwait(常駐監督::オルトロスクリア);
				sysReq(1, 常駐助監督::オルトロス判定);
				sysReqwait(常駐助監督::オルトロス判定);
				bindoff();
				wait(15);
				camerastart_7e(0, 0x2000003);
				sysReq(1, ヴァン::オルトロス最後);
				sysReq(1, シーク子::サマル配置２);
				partyusemapid(1);
				setposparty(94.2788696, -0.215418994, 44.671093, -0.230535999);
				wait(2);
				NPC06.hide();
				fadein(15);
				wait(25);
				シーク子.setkutipakustatus(1);
				シーク子.setunazukistatus(1);
				シーク子.amese(0, 0x100002f);
				シーク子.messync(0, 1);
				シーク子.setkutipakustatus(0);
				シーク子.setunazukistatus(0);
				sysReqwait(シーク子::サマル配置２);
				シーク子.stdmotionplay_2c2(0x1000016, 6);
				setmesmacro(0, 0, 1, 0x8090);
				シーク子.setkutipakustatus(1);
				シーク子.setunazukistatus(1);
				シーク子.amese(0, 0x1000030);
				シーク子.messync(0, 1);
				シーク子.setkutipakustatus(0);
				シーク子.setunazukistatus(0);
				シーク子.stdmotionplay_2c2(0x1000016, 6);
				シーク子.setkutipakustatus(1);
				シーク子.setunazukistatus(1);
				シーク子.amese(0, 0x1000031);
				シーク子.messync(0, 1);
				シーク子.setkutipakustatus(0);
				シーク子.setunazukistatus(0);
				sysReqwait(ヴァン::オルトロス最後);
				シーク子.motionplay_bb(0x14000003, 5);
				wait(30);
				sysReq(1, ヴァン::オルトロスもらう);
				wait(20);
				subitem(0x8092, 1);
				g_com_set_get_label = 0x8090;
				sysReqew(1, イベント特殊効果画面::だいじなものゲット_FADE付);
				file_var_27 = 0;
				file_var_20 = 1;
				for (local_var_82 = 0; local_var_82 <= 20; local_var_82 = (local_var_82 + 1))
				{
					file_var_1e[local_var_82] = 0;
				}
				NPC06.show();
				sysReq(3, 配置監視監督::NPC監視停止);
				sysReqwait(配置監視監督::NPC監視停止);
				sysReqall(5, reqArr3);
				sysReqwaitall(reqArr3);
				sysReqi(1, 配置監視監督::配置監視);
				sysReqwait(配置監視監督::配置監視);
				file_var_20 = 0;
				sysReqwaitall(reqArr4);
				sysReqwaitall(reqArr5);
				sysReqwaitall(reqArr6);
				sysReqwaitall(reqArr7);
				sysReqwaitall(reqArr8);
				sysReq(1, hitactor01::setHitObj);
				sysReqall(1, reqArr9);
				sysReq(2, シーク子::バインドオフ);
				sysReqwait(シーク子::バインドオフ);
				sysReq(2, ヴァン::バインドオフ);
				sysReq(1, 配置監視監督::配置監視);
				partyusemapid(1);
				setposparty(93.8114929, -0.270651013, 46.2049294, -0.230535999);
				motiondispose(4);
				cameraclear();
				showparty();
				showparty();
				if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
				{
					clearnavimapfootmark();
				}
				setnavimapfootmarkstatus(1);
				setcharseplayall(1);
				sethpmenufast(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				wait(1);
				fadein(15);
				break;
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function オルトロス配置()
	{
		hidecomplete();
		usemapid(0);
		setpos(94.8054047, 0.105769999, 41.0153618);
		dir(-2.47550988);
		bindp2_d4(0x3000004, 2, 0);
		set_ignore_hitgroup(1);
		setweight(-1);
		setnpcname(0x246);
		fetchambient();
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(0, -1);
		motionloop(1);
		motionplay_bb(0x13000000, 0);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}
}


script 解放軍(6)
{

	function init()
	{
		return;
	}


	function セット()
	{
		setpos(47.0193977, 0.174428999, 117.440002);
		dir(2.5276401);
		bindp2_d4(0x3000002, 4, 2);
		usemapid(0);
		setweight(-1);
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(0, -1);
		motionloop(1);
		motionplay_bb(0x11000005, 0);
		reqenable(2);
		setradius(0.300000012);
		setnpcname(0x247);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_84;            // pos: 0x1;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		switch (シナリオフラグ)
		{
			case lte(0x157):
				if (local_var_84 >= 10)
				{
					amese(0, 0x100003e);
					messync(0, 1);
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100003f);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					local_var_84 = (local_var_84 + 1);
				}
				break;
			case lte(0x5f0):
				switch (シナリオフラグ)
				{
					case lt(0x177):
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000040);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
					case lt(0x182):
						fadeout(15);
						fadesync();
						g_com_navi_footcalc[0] = getx_12b(-1);
						g_com_navi_footcalc[1] = gety_12c(-1);
						g_com_navi_footcalc[2] = getz_12d(-1);
						setnavimapfootmarkstatus(0);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(0);
						if (!(istownmap()))
						{
							setstatuserrordispdenystatus(1);
						}
						unkCall_5ac(1, 2.35800004, 100);
						hideparty();
						sysReq(1, ヴァン::アジトはいるとき配置);
						sysReqwait(ヴァン::アジトはいるとき配置);
						camerastart_7e(0, 0x2000006);
						sysLookata(ヴァン);
						NPC14.sysLookata(ヴァン);
						sethpmenufast(0);
						settrapshowstatus(0);
						setcharseplayall(1);
						fadein(15);
						ヴァン.stdmotionplay_2c2(0x1000016, 20);
						ヴァン.setkutipakustatus(1);
						ヴァン.setunazukistatus(1);
						ヴァン.amese(0, 0x1000041);
						ヴァン.messync(0, 1);
						ヴァン.setkutipakustatus(0);
						ヴァン.setunazukistatus(0);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000042);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						ヴァン.stdmotionplay_2c2(0x1000002, 20);
						ヴァン.setkutipakustatus(1);
						ヴァン.setunazukistatus(1);
						ヴァン.amese(0, 0x1000043);
						ヴァン.messync(0, 1);
						ヴァン.setkutipakustatus(0);
						ヴァン.setunazukistatus(0);
						ヴァン.motiontrigger();
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000044);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						unkCall_5ad(33);
						wait(15);
						fadeout(15);
						fadesync();
						setcharseplayall(0);
						sethpmenufast(1);
						clear_force_char_nearfade();
						setmaphighmodeldepth(-1);
						setmapmodelstatus(1);
						setstatuserrordispdenystatus(0);
						settrapshowstatus(1);
						setstatuserrordispdenystatus(0);
						unkCall_5ac(0, 0, 0);
						showparty();
						cameraclear();
						sysReq(1, ヴァン::バインドオフ);
						sysReqwait(ヴァン::バインドオフ);
						fadeout_d0(2, 0);
						fadesync_d3(2);
						fadein(0);
						wait(1);
						mapjump(0x358, 0, 1);
						return;
					case lt(0x1a4):
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000045);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
					default:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000046);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
				}
				break;
			default:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000047);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_83;            // pos: 0x0;

	function talk_wanted()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		switch (g_btl_nm_リングドラゴン)
		{
			case 0:
				if (isquestorder(134))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					askpos(0, 0, 127);
					local_var_83 = aaske(0, 0x1000048);
					mesclose(0);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					switch (local_var_83)
					{
						case 0:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000049);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							questeffectread(198);
							effectreadsync();
							effectplay(0);
							effectsync();
							g_btl_nm_リングドラゴン = 1;
							setquestscenarioflag(134, 30);
							break;
						default:
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x100004a);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
							break;
					}
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100004b);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				break;
			case 1:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100004c);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
			case 2:
				if (!(isquestclear(134)))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100004d);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					sysReq(1, 常駐監督::賞金首リングドラゴンクリア);
					sysReqwait(常駐監督::賞金首リングドラゴンクリア);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100004e);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					fadeout(30);
					fadesync();
					bindoff();
					wait(15);
					fadein(30);
					fadesync();
					ucon();
					sethpmenu(1);
					clear_force_char_nearfade();
					setmaphighmodeldepth(-1);
					setmapmodelstatus(1);
					setstatuserrordispdenystatus(0);
					settrapshowstatus(1);
					return;
				}
				break;
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script hitactor01(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(93.8143539, -0.0789349973, 63.2499962);
		dir(0.095325999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(93.8143539, -0.0789349973, 63.2499962);
		dir(0.095325999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor02(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(78.3752747, -0.163946003, 55.2242279);
		dir(0.095325999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(78.3752747, -0.163946003, 55.2242279);
		dir(0.095325999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 1.29999995);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor03(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(79.4619827, -0.0761319995, 77.6569138);
		dir(1.54836404);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(79.4619827, -0.0761319995, 77.6569138);
		dir(1.54836404);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.29999995, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor04(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(186.646423, -0.0687339976, 118.839638);
		dir(1.54836404);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(186.646423, -0.0687339976, 118.839638);
		dir(1.54836404);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor05(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(167.400192, -0.115726002, 38.121788);
		dir(1.70006096);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.89999998, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(167.400192, -0.115726002, 38.121788);
		dir(1.70006096);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.89999998, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor01_firo(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(166.760757, -0.141133994, 42.0306473);
		dir(1.70006096);
		bindhitobj();
		set_ignore_hitgroup(2);
		setradius_221(1, 1);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(166.760757, -0.141133994, 42.0306473);
		dir(1.70006096);
		bindhitobj();
		set_ignore_hitgroup(2);
		setradius_221(1, 1);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script map_ダミーＰＣ(6)
{

	function init()
	{
		return;
	}


	function 扉オープン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function アイテム１()
	{
		capturepc(-1);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(0);
		motionsync_282(1);
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック1()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ターン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ギミックアタックxyz()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１xyz()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２xyz()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタックxyz1()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタックxyz2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function アイテム１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ターンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function map_behind_return()
	{
		capturepc(-1);
		setnoupdatebehindcamera(0);
		releasepc();
		return;
	}


	function ＳＥＴＰＯＳxyz()
	{
		capturepc(-1);
		setpos(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		releasepc();
		return;
	}


	function ＳＥＴＴＵＲＮxyz()
	{
		capturepc(-1);
		turnt(mp_map_set_x, mp_map_set_y, mp_map_set_z, 0);
		releasepc();
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_85;            // pos: 0x4;
	float   local_var_86;            // pos: 0x8;
	float   local_var_87;            // pos: 0xc;
	float   local_var_88;            // pos: 0x10;

	function map_syuuseki_door()
	{
		capturepc(-1);
		getmapjumpposbyindex(11, &local_var_85, &local_var_86, &local_var_87, &local_var_88);
		setpos(local_var_85, local_var_86, local_var_87);
		dir(local_var_88);
		wait(1);
		resetbehindcamera(getmapjumpanglebyindex(11), 0);
		releasepc();
		return;
	}
}

//======================================================================
//                           Map exit arrays                            
//======================================================================

mapExitArray mapExitGroup0[17] = {

	exitStruct mapExit0 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit1 = {
		96.8762283, -0.275570631, 152, 1, 
		103.126831, -0.275570631, 152, 0x10f
	};

	exitStruct mapExit2 = {
		143.145569, -0.275570631, 152.367203, 1, 
		136.824829, -0.275570631, 152.332779, 0x20f
	};

	exitStruct mapExit3 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit4 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit5 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit6 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit7 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit8 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit9 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit10 = {
		161.5, 0, 16.3460083, 1, 
		166.5, 0, 16.3460083, 0x140f
	};

	exitStruct mapExit11 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit12 = {
		153.290268, -0.00845981389, 104.357422, 1, 
		158.290268, -0.00845981389, 104.357422, 0x160f
	};

	exitStruct mapExit13 = {
		151.655594, -0.13334015, 50.3785286, 1, 
		151.655594, -0.13334015, 52.3929214, 0x170f
	};

	exitStruct mapExit14 = {
		0, 0, 0, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit15 = {
		88.5705185, 0, 50.7581635, 1, 
		88.5705185, 0, 52.7818375, 0x190b
	};

	exitStruct mapExit16 = {
		103.426346, 0, 50.7767296, 1, 
		103.426346, 0, 52.7632675, 0x1a09
	};

};

mapExitArray mapExitGroup1[0] = {

};

mapExitArray mapExitGroup2[0] = {

};

mapExitArray mapExitGroup3[4] = {

	exitStruct mapExit0 = {
		156.020004, -0.129999995, 51.4000015, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit1 = {
		156.020004, -0.129999995, 51.4000015, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit2 = {
		78.5642548, -0.134308994, 56.2538986, 0, 
		0, 0, 0, 0x0
	};

	exitStruct mapExit3 = {
		78.5642548, -0.134308994, 56.2538986, 0, 
		0, 0, 0, 0x0
	};

};

mapExitArray mapExitGroup4[0] = {

};


//======================================================================
//                      Map jump position vectors                       
//======================================================================
mjPosArr1 mapJumpPositions1[12] = {

	mjPos mapJumpPos0 = {
		120, -0.139652252, 114, -3.1415906,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		163.785278, -0.275570601, 21.4423885, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		44.0015106, -0.0199999996, 51.8114395, -1.57079542,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		155.78685, -0.0175071433, 108, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos4 = {
		100, -0.275570542, 148, -3.14159131,
		0, 0, 0, 0
	};

	mjPos mapJumpPos5 = {
		140, 0, 148, -3.14159131,
		0, 0, 0, 0
	};

	mjPos mapJumpPos6 = {
		61.1210518, -0.275570542, 60, -3.14159131,
		0, 0, 0, 0
	};

	mjPos mapJumpPos7 = {
		147, -0.275570542, 51.3712082, -1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos8 = {
		164, -0.275570601, 76.6459808, 1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos9 = {
		91.9957809, -0.187683806, 51.76968, 1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos10 = {
		99.9940414, -0.202944681, 51.7353783, -1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos11 = {
		92.0044479, -0.263593704, 58.5538063, 0,
		0, 0, 0, 0
	};

};
mjPosArr2 mapJumpPositions2[14] = {

	mjPos mapJumpPos0 = {
		120, -0.139652252, 114, -3.1415906,
		0, 0, 0, 0
	};

	mjPos mapJumpPos1 = {
		163.785278, -0.275570601, 21.4423885, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos2 = {
		44.0015106, -0.0199999996, 51.8114395, -1.57079542,
		0, 0, 0, 0
	};

	mjPos mapJumpPos3 = {
		155.78685, -0.0175071433, 108, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos4 = {
		100, -0.275570542, 148, -3.14159131,
		0, 0, 0, 0
	};

	mjPos mapJumpPos5 = {
		140, 0, 148, -3.14159131,
		0, 0, 0, 0
	};

	mjPos mapJumpPos6 = {
		61.1210518, -0.275570542, 60, -3.14159131,
		0, 0, 0, 0
	};

	mjPos mapJumpPos7 = {
		147, -0.275570542, 51.3712082, -1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos8 = {
		164, -0.275570601, 76.6459808, 1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos9 = {
		91.9957809, -0.187683806, 51.76968, 1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos10 = {
		99.9940414, -0.202944681, 51.7353783, -1.57079566,
		0, 0, 0, 0
	};

	mjPos mapJumpPos11 = {
		100, -0.275570542, 249.033493, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos12 = {
		140, -0.275570542, 249.033493, 0,
		0, 0, 0, 0
	};

	mjPos mapJumpPos13 = {
		92.0044479, -0.263593704, 58.5538063, 0,
		0, 0, 0, 0
	};

};


//======================================================================
//                       Unknown position arrays                        
//======================================================================
unkPos1 unknownPosition0[7] = {163.789993, -0.100000001, 16.3460083, 2.5, 3.65999198, 0, 0};
unkPos1 unknownPosition1[7] = {163.789993, -0.100000001, 16.3508263, 0.698139668, 0.649999976, 0, 0};
unkPos1 unknownPosition2[7] = {47.6500015, -0.0183459371, 51.7900009, 2.64639902, 2.5, 0, 0};
unkPos1 unknownPosition3[7] = {47.6500015, -0.0199999996, 51.7900009, 0.649999976, 0.704755425, 0, 0};
unkPos1 unknownPosition4[7] = {155.790268, -0.0399999991, 104.357422, 2.5, 2.5, 0, 0};
unkPos1 unknownPosition5[7] = {155.790268, -0.039152883, 104.357422, 0.699999988, 0.649999976, 0, 0};
unkPos1 unknownPosition6[7] = {92.0003586, -0.263593704, 53.0399971, 2.03989315, 0.773638427, 0, 0};
unkPos1 unknownPosition7[7] = {92.0003586, -0.263593704, 53.4397202, 1.26313639, 0.372243047, 0, 0};
unkPos1 unknownPosition8[7] = {61.1324463, -0.129999995, 63.6547661, 4, 2.5, 0, 0};
unkPos1 unknownPosition9[7] = {61.1324348, -0.129999995, 63.6547661, 0.545654774, 0.649999976, 0, 0};
unkPos1 unknownPosition10[7] = {151.655594, -0.13334015, 51.3857269, 3.69000006, 4, 0, 0};
unkPos1 unknownPosition11[7] = {151.655594, -0.13334015, 51.3857269, 0.649999976, 0.620000005, 0, 0};
unkPos1 unknownPosition12[7] = {160.344086, -0.133340001, 76.6660995, 2.90990853, 4, 0, 0};
unkPos1 unknownPosition13[7] = {160.344086, -0.133340001, 76.6699982, 0.649999976, 0.620000005, 0, 0};
unkPos1 unknownPosition14[7] = {88.3499985, 0, 51.7700005, 2.23024106, 1.85673547, 0, 0};
unkPos1 unknownPosition15[7] = {88.5705185, 0, 51.7700005, 0.649999976, 0.699999988, 0, 0};
unkPos1 unknownPosition16[7] = {103.660004, 0, 51.5200005, 2.23000002, 2.39234328, 0, 0};
unkPos1 unknownPosition17[7] = {103.426346, 0, 51.7441139, 0.649999976, 0.699999988, 0, 0};
unkPos1 unknownPosition18[7] = {92.0044479, -0.263593704, 56.0045738, 2.04926205, 2.10662913, 0, 0};
unkPos1 unknownPosition19[7] = {92.0003586, 0.939999998, 54.2244492, 1.25846505, 0.330767304, 0, 0};

unkPos2 unknownPosition0[7] = {163.789993, 1.10000002, 16.3460083, 1, 1, 0, 0};
unkPos2 unknownPosition1[7] = {47.6500015, 1.17999995, 51.7900009, 1, 1, 0, 0};
unkPos2 unknownPosition2[7] = {155.790268, 1.15999997, 104.360001, 1, 1, 0, 0};
unkPos2 unknownPosition3[7] = {92.0003586, 0.939999998, 53.7619514, 1.25703251, 1.25703251, 0, 0};
unkPos2 unknownPosition4[7] = {61.1324463, 1.07000005, 63.6699982, 0.800000012, 0.800000012, 0, 0};
unkPos2 unknownPosition5[7] = {152.153763, 1.07000005, 51.3857269, 0.800000012, 0.800000012, 0, 0};
unkPos2 unknownPosition6[7] = {159.85611, 1.07000005, 76.6699982, 0.800000012, 0.800000012, 0, 0};
unkPos2 unknownPosition7[7] = {88.0845413, 1.20000005, 51.7700005, 1, 1, 0, 0};
unkPos2 unknownPosition8[7] = {103.906898, 1.20000005, 51.7617874, 1, 1, 0, 0};


//======================================================================
//                          Unknown u16 Arrays                          
//======================================================================
unk16Arr1 unknown16Arrays1[10] = {
	unk16ArrEntry unknown16Array0 = {0, 1, 0, 0, 1, 0, 0, 0};
	unk16ArrEntry unknown16Array1 = {2, 3, 1, 0, 1, 1, 0, 0};
	unk16ArrEntry unknown16Array2 = {4, 5, 2, 0, 1, 2, 0, 0};
	unk16ArrEntry unknown16Array3 = {6, 7, 3, 0, 0, 3, 0, 0};
	unk16ArrEntry unknown16Array4 = {8, 9, 4, 0, 1, 4, 0, 0};
	unk16ArrEntry unknown16Array5 = {10, 11, 5, 0, 0, 5, 0, 0};
	unk16ArrEntry unknown16Array6 = {12, 13, 6, 0, 0, 6, 0, 0};
	unk16ArrEntry unknown16Array7 = {14, 15, 7, 0, 0, 7, 0, 0};
	unk16ArrEntry unknown16Array8 = {16, 17, 8, 0, 1, 8, 0, 0};
	unk16ArrEntry unknown16Array9 = {18, 19, 3, 0, 0, 9, 0, 0};
};
unk16Arr2 unknown16Arrays2[27] = {
	unk16ArrEntry unknown16Array0 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array1 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array2 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array3 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array4 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array5 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array6 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array7 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array8 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array9 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array10 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array11 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array12 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array13 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array14 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array15 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array16 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array17 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array18 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array19 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array20 = {0, 1, 16486, 16486, 1, 65535, 0, 0};
	unk16ArrEntry unknown16Array21 = {0, 1, 16486, 16486, 1, 65535, 0, 0};
	unk16ArrEntry unknown16Array22 = {0, 1, 16486, 16486, 1, 65535, 0, 0};
	unk16ArrEntry unknown16Array23 = {0, 1, 335, 335, 1, 65535, 0, 0};
	unk16ArrEntry unknown16Array24 = {0, 1, 0, 0, 1, 65535, 0, 0};
	unk16ArrEntry unknown16Array25 = {342, 1, 336, 336, 1, 17, 30, 0};
	unk16ArrEntry unknown16Array26 = {0, 1, 0, 0, 1, 17, 31, 0};
};
unk16Arr3 unknown16Arrays3[8] = {
	unk16ArrEntry unknown16Array0 = {2, 768, 1, 1};
	unk16ArrEntry unknown16Array1 = {3, 768, 1, 1};
	unk16ArrEntry unknown16Array2 = {4, 768, 1, 1};
	unk16ArrEntry unknown16Array3 = {5, 768, 1, 1};
	unk16ArrEntry unknown16Array4 = {6, 768, 1, 1};
	unk16ArrEntry unknown16Array5 = {7, 768, 1, 1};
	unk16ArrEntry unknown16Array6 = {8, 768, 1, 1};
	unk16ArrEntry unknown16Array7 = {9, 768, 1, 0};
};

//======================================================================
//                       REQALL/REQWAITALL arrays                       
//======================================================================
reqa   reqArr0[] = {
	常駐カメラ::reis_cut01,
	シーク子::reis_cut01
};

reqa   reqArr1[] = {
	常駐カメラ::reis_cut0102,
	シーク子::reis_cut0102
};

reqa   reqArr2[] = {
	常駐カメラ::reis_cut03
};

reqa   reqArr3[] = {
	NPC02::NPCリクエスト停止,
	NPC03::NPCリクエスト停止,
	NPC04::NPCリクエスト停止,
	NPC05::NPCリクエスト停止,
	NPC06::NPCリクエスト停止,
	NPC07::NPCリクエスト停止,
	NPC08::NPCリクエスト停止,
	NPC09::NPCリクエスト停止,
	NPC10::NPCリクエスト停止,
	NPC11::NPCリクエスト停止,
	NPC12::NPCリクエスト停止,
	NPC13::NPCリクエスト停止,
	NPC14::NPCリクエスト停止,
	NPC15::NPCリクエスト停止,
	NPC16::NPCリクエスト停止,
	NPC17::NPCリクエスト停止,
	NPC18::NPCリクエスト停止,
	NPC19::NPCリクエスト停止,
	NPC20::NPCリクエスト停止,
	NPC21::NPCリクエスト停止,
	NPC01::NPCリクエスト停止
};

reqa   reqArr4[] = {
	NPC02::NPC配置01,
	NPC03::NPC配置01,
	NPC04::NPC配置01,
	NPC05::NPC配置01,
	NPC06::NPC配置01,
	NPC07::NPC配置01,
	NPC08::NPC配置01,
	NPC09::NPC配置01,
	NPC10::NPC配置01,
	NPC11::NPC配置01,
	NPC12::NPC配置01,
	NPC13::NPC配置01,
	NPC14::NPC配置01,
	NPC15::NPC配置01,
	NPC16::NPC配置01,
	NPC17::NPC配置01,
	NPC18::NPC配置01,
	NPC19::NPC配置01,
	NPC20::NPC配置01,
	NPC21::NPC配置01,
	NPC01::NPC配置01
};

reqa   reqArr5[] = {
	NPC02::NPC配置02,
	NPC03::NPC配置02,
	NPC04::NPC配置02,
	NPC05::NPC配置02,
	NPC06::NPC配置02,
	NPC07::NPC配置02,
	NPC08::NPC配置02,
	NPC09::NPC配置02,
	NPC10::NPC配置02,
	NPC11::NPC配置02,
	NPC12::NPC配置02,
	NPC13::NPC配置02,
	NPC14::NPC配置02,
	NPC15::NPC配置02,
	NPC16::NPC配置02,
	NPC17::NPC配置02,
	NPC18::NPC配置02,
	NPC19::NPC配置02,
	NPC20::NPC配置02,
	NPC21::NPC配置02,
	NPC01::NPC配置02
};

reqa   reqArr6[] = {
	NPC02::NPC配置03,
	NPC03::NPC配置03,
	NPC04::NPC配置03,
	NPC05::NPC配置03,
	NPC06::NPC配置03,
	NPC07::NPC配置03,
	NPC08::NPC配置03,
	NPC09::NPC配置03,
	NPC10::NPC配置03,
	NPC11::NPC配置03,
	NPC12::NPC配置03,
	NPC13::NPC配置03,
	NPC14::NPC配置03,
	NPC15::NPC配置03,
	NPC16::NPC配置03,
	NPC17::NPC配置03,
	NPC18::NPC配置03,
	NPC19::NPC配置03,
	NPC20::NPC配置03,
	NPC21::NPC配置03,
	NPC01::NPC配置03
};

reqa   reqArr7[] = {
	NPC02::NPC配置04,
	NPC03::NPC配置04,
	NPC04::NPC配置04,
	NPC05::NPC配置04,
	NPC06::NPC配置04,
	NPC07::NPC配置04,
	NPC08::NPC配置04,
	NPC09::NPC配置04,
	NPC10::NPC配置04,
	NPC11::NPC配置04,
	NPC12::NPC配置04,
	NPC13::NPC配置04,
	NPC14::NPC配置04,
	NPC15::NPC配置04,
	NPC16::NPC配置04,
	NPC17::NPC配置04,
	NPC18::NPC配置04,
	NPC19::NPC配置04,
	NPC20::NPC配置04,
	NPC21::NPC配置04,
	NPC01::NPC配置04
};

reqa   reqArr8[] = {
	NPC02::NPC配置05,
	NPC03::NPC配置05,
	NPC04::NPC配置05,
	NPC05::NPC配置05,
	NPC06::NPC配置05,
	NPC07::NPC配置05,
	NPC08::NPC配置05,
	NPC09::NPC配置05,
	NPC10::NPC配置05,
	NPC11::NPC配置05,
	NPC12::NPC配置05,
	NPC13::NPC配置05,
	NPC14::NPC配置05,
	NPC15::NPC配置05,
	NPC16::NPC配置05,
	NPC17::NPC配置05,
	NPC18::NPC配置05,
	NPC19::NPC配置05,
	NPC20::NPC配置05,
	NPC21::NPC配置05,
	NPC01::NPC配置05
};

reqa   reqArr9[] = {
	NPC02::NPC挙動,
	NPC03::NPC挙動,
	NPC04::NPC挙動,
	NPC05::NPC挙動,
	NPC06::NPC挙動,
	NPC07::NPC挙動,
	NPC08::NPC挙動,
	NPC09::NPC挙動,
	NPC10::NPC挙動,
	NPC11::NPC挙動,
	NPC12::NPC挙動,
	NPC13::NPC挙動,
	NPC14::NPC挙動,
	NPC15::NPC挙動,
	NPC16::NPC挙動,
	NPC17::NPC挙動,
	NPC18::NPC挙動,
	NPC19::NPC挙動,
	NPC20::NPC挙動,
	NPC21::NPC挙動,
	NPC01::NPC挙動
};

reqa  reqArr10[] = {
	LITTSSET02::リッツ軍ダラン爺前配置,
	LITTSSET01::リッツ軍ダラン爺前配置,
	リッツ::リッツ軍ダラン爺前配置
};

reqa  reqArr11[] = {
	LITTSSET02::リッツ軍アジト向かい時配置,
	LITTSSET01::リッツ軍アジト向かい時配置
};

reqa  reqArr12[] = {
	LITTSSET02::リッツ軍アジト向かい時挙動,
	LITTSSET01::リッツ軍アジト向かい時挙動
};


